<?php

namespace ContainerHH4xHQn;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getMailerServiceService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'App\Services\MailerService' shared autowired service.
     *
     * @return \App\Services\MailerService
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/src/Services/MailerService.php';

        $a = ($container->privates['mailer.mailer'] ?? $container->load('getMailer_MailerService'));

        if (isset($container->privates['App\\Services\\MailerService'])) {
            return $container->privates['App\\Services\\MailerService'];
        }

        return $container->privates['App\\Services\\MailerService'] = new \App\Services\MailerService($a);
    }
}
