<?php

namespace ContainerHH4xHQn;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getGroupControllerService extends App_KernelDevDebugContainer
{
    /**
     * Gets the public 'App\Controller\GroupController' shared autowired service.
     *
     * @return \App\Controller\GroupController
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/ApiController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/GroupController.php';

        $container->services['App\\Controller\\GroupController'] = $instance = new \App\Controller\GroupController(($container->services['doctrine.orm.default_entity_manager'] ?? $container->getDoctrine_Orm_DefaultEntityManagerService()), ($container->services['.container.private.security.token_storage'] ?? $container->get_Container_Private_Security_TokenStorageService()), ($container->services['lexik_jwt_authentication.jwt_manager'] ?? $container->load('getLexikJwtAuthentication_JwtManagerService')), ($container->services['.container.private.serializer'] ?? $container->load('get_Container_Private_SerializerService')), ($container->privates['App\\Repository\\GroupRepository'] ?? $container->load('getGroupRepositoryService')));

        $instance->setContainer(($container->privates['.service_locator.GNc8e5B'] ?? $container->load('get_ServiceLocator_GNc8e5BService'))->withContext('App\\Controller\\GroupController', $container));

        return $instance;
    }
}
