<?php

namespace ContainerHH4xHQn;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_1FjCNhzService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.1FjCNhz' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.1FjCNhz'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'UserProjectRepository' => ['privates', 'App\\Repository\\UserProjectRepository', 'getUserProjectRepositoryService', true],
            'UserRepository' => ['privates', 'App\\Repository\\UserRepository', 'getUserRepositoryService', true],
            'normalizer' => ['services', '.container.private.serializer', 'get_Container_Private_SerializerService', true],
        ], [
            'UserProjectRepository' => 'App\\Repository\\UserProjectRepository',
            'UserRepository' => 'App\\Repository\\UserRepository',
            'normalizer' => '?',
        ]);
    }
}
