<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerHH4xHQn\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerHH4xHQn/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerHH4xHQn.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerHH4xHQn\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerHH4xHQn\App_KernelDevDebugContainer([
    'container.build_hash' => 'HH4xHQn',
    'container.build_id' => 'c4b96c16',
    'container.build_time' => 1692623871,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerHH4xHQn');
