<?php

namespace Proxies\__CG__\App\Entity;


/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class TypeObservation extends \App\Entity\TypeObservation implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array<string, null> properties to be lazy loaded, indexed by property name
     */
    public static $lazyPropertiesNames = array (
);

    /**
     * @var array<string, mixed> default values of properties to be lazy loaded, with keys being the property names
     *
     * @see \Doctrine\Common\Proxy\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array (
);



    public function __construct(?\Closure $initializer = null, ?\Closure $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'id', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'name', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'type_question', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'image', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'level', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'question', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'observations', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'typeObservationProjects', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'colors', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'updatedAt'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'id', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'name', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'type_question', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'image', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'level', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'question', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'observations', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'typeObservationProjects', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'colors', '' . "\0" . 'App\\Entity\\TypeObservation' . "\0" . 'updatedAt'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (TypeObservation $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy::$lazyPropertiesDefaults as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load(): void
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized(): bool
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized): void
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null): void
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer(): ?\Closure
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null): void
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner(): ?\Closure
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @deprecated no longer in use - generated code now relies on internal components rather than generated public API
     * @static
     */
    public function __getLazyProperties(): array
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId(): ?int
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setId(int $id): \App\Entity\TypeObservation
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setId', [$id]);

        return parent::setId($id);
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getName', []);

        return parent::getName();
    }

    /**
     * {@inheritDoc}
     */
    public function setName(string $name): \App\Entity\TypeObservation
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setName', [$name]);

        return parent::setName($name);
    }

    /**
     * {@inheritDoc}
     */
    public function getTypeQuestion(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTypeQuestion', []);

        return parent::getTypeQuestion();
    }

    /**
     * {@inheritDoc}
     */
    public function setTypeQuestion(string $type_question): \App\Entity\TypeObservation
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTypeQuestion', [$type_question]);

        return parent::setTypeQuestion($type_question);
    }

    /**
     * {@inheritDoc}
     */
    public function getImage(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getImage', []);

        return parent::getImage();
    }

    /**
     * {@inheritDoc}
     */
    public function setImage(string $image): \App\Entity\TypeObservation
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setImage', [$image]);

        return parent::setImage($image);
    }

    /**
     * {@inheritDoc}
     */
    public function getLevel(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLevel', []);

        return parent::getLevel();
    }

    /**
     * {@inheritDoc}
     */
    public function setLevel(int $level): \App\Entity\TypeObservation
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLevel', [$level]);

        return parent::setLevel($level);
    }

    /**
     * {@inheritDoc}
     */
    public function getQuestion(): ?\App\Entity\Question
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getQuestion', []);

        return parent::getQuestion();
    }

    /**
     * {@inheritDoc}
     */
    public function setQuestion(?\App\Entity\Question $question): \App\Entity\TypeObservation
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setQuestion', [$question]);

        return parent::setQuestion($question);
    }

    /**
     * {@inheritDoc}
     */
    public function getObservations(): \Doctrine\Common\Collections\Collection
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getObservations', []);

        return parent::getObservations();
    }

    /**
     * {@inheritDoc}
     */
    public function addObservation(\App\Entity\Observation $observation): \App\Entity\TypeObservation
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addObservation', [$observation]);

        return parent::addObservation($observation);
    }

    /**
     * {@inheritDoc}
     */
    public function removeObservation(\App\Entity\Observation $observation): \App\Entity\TypeObservation
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeObservation', [$observation]);

        return parent::removeObservation($observation);
    }

    /**
     * {@inheritDoc}
     */
    public function getTypeObservationProjects(): \Doctrine\Common\Collections\Collection
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTypeObservationProjects', []);

        return parent::getTypeObservationProjects();
    }

    /**
     * {@inheritDoc}
     */
    public function addTypeObservationProject(\App\Entity\TypeObservationProject $typeObservationProject): \App\Entity\TypeObservation
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addTypeObservationProject', [$typeObservationProject]);

        return parent::addTypeObservationProject($typeObservationProject);
    }

    /**
     * {@inheritDoc}
     */
    public function removeTypeObservationProject(\App\Entity\TypeObservationProject $typeObservationProject): \App\Entity\TypeObservation
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeTypeObservationProject', [$typeObservationProject]);

        return parent::removeTypeObservationProject($typeObservationProject);
    }

    /**
     * {@inheritDoc}
     */
    public function getColors(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getColors', []);

        return parent::getColors();
    }

    /**
     * {@inheritDoc}
     */
    public function setColors(string $colors): \App\Entity\TypeObservation
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setColors', [$colors]);

        return parent::setColors($colors);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUpdatedAt', []);

        return parent::getUpdatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt(\DateTimeInterface $updatedAt): \App\Entity\TypeObservation
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUpdatedAt', [$updatedAt]);

        return parent::setUpdatedAt($updatedAt);
    }

}
