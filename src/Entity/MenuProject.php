<?php

namespace App\Entity;

use App\Repository\MenuProjectRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=MenuProjectRepository::class)
 */
class MenuProject
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Groups("menuProject:read")
     * 
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Menu::class, inversedBy="menuProjects")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("menuProject:read")
     */
    private $menu;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="menuProjects")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("menuProject:read")
     */
    private $project;

    public function getId(): ?int
    {
        return $this->id;
    }
    
     public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getMenu(): ?Menu
    {
        return $this->menu;
    }

    public function setMenu(?Menu $menu): self
    {
        $this->menu = $menu;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }
}
