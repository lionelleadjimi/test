<?php

namespace App\Entity;

use App\Repository\NotificationsProjectRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=NotificationsProjectRepository::class)
 */
class NotificationsProject
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Groups("notificationsProject:read")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="notificationsProjects")
     * @Groups("notificationsProject:read")
     */
    private $project;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("notificationsProject:read")
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("notificationsProject:read")
     */
    private $date;


    public function __construct() {
        $this->date = new \DateTime('now');
    }
    public function getId(): ?int
    {
        return $this->id;
    }
    
    
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
