<?php

namespace App\Entity;

use App\Repository\CountryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=CountryRepository::class)
 */
class Country
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Groups("country:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("country:read")
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Groups("country:read")
     */
    private $codeTel;


    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("country:read")
     */
    private $sigle;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="country", orphanRemoval=true)
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

   

    public function getId(): ?int
    {
        return $this->id;
    }
     public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCodeTel(): ?int
    {
        return $this->codeTel;
    }

    public function setCodeTel(int $codeTel): self
    {
        $this->codeTel = $codeTel;

        return $this;
    }


    public function getSigle(): ?string
    {
        return $this->sigle;
    }

    public function setSigle(string $sigle): self
    {
        $this->sigle = $sigle;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setCountry($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getCountry() === $this) {
                $user->setCountry(null);
            }
        }

        return $this;
    }

   
   

}
