<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Groups("user:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups("user:read")
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @Groups("user:read")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups("user:read")
     */
    private $password;
     /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="bigint")
     * @Groups("user:read")
     */
    private $phone;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date; 
    

    
    
     /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expirationCode;  
    
      /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expirationToken; 
    
    /**
     * @ORM\Column(type="boolean")
     * @Groups("user:read")
     */
    private $enabled = "0";


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $code; 
    
   

    /**
     * @ORM\OneToMany(targetEntity=Patrol::class, mappedBy="collector")
     */
    private $patrols;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="user")
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity=Compass::class, mappedBy="user")
     */
    private $compasses;

    /**
     * @ORM\OneToMany(targetEntity=Marketplace::class, mappedBy="user")
     */
    private $marketplaces;

    /**
     * @ORM\OneToMany(targetEntity=Project::class, mappedBy="user")
     */
    private $userProjects;

    /**
     * @ORM\OneToMany(targetEntity=Observation::class, mappedBy="user")
     */
    private $observations;
    
    /**
     * @ORM\ManyToOne(targetEntity=Fonction::class, inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fonction;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $country;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Project;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $organization;

    /**
     * @ORM\Column(type="integer")
     */
    private $codeTel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sigle;

    /**
     * @ORM\Column(type="boolean")
     */
    private $migrate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateDate;

    public function __construct()
    {
        $this->date = new \DateTime('now');
        $this->patrols = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->compasses = new ArrayCollection();
        $this->marketplaces = new ArrayCollection();
        $this->userProjects = new ArrayCollection();
        $this->observations = new ArrayCollection();
       
    }
    
    public function __toString(): string
    {
        return $this->email;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

     public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
    
    public function get1Role(): string
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER

        return $roles;
    }
    public function convertRole(string $roles){
         
          $this->roles = $roles;
          
        $roles = json_decode($this, true);
        
       return $roles;
    //   return array_unique($roles);
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
    
    
 
    
     public function getExpirationCode(): ?\DateTimeInterface
    {
        return $this->expirationCode;
    }

    public function setExpirationCode(?\DateTimeInterface $expirationCode): self
    {
        $this->expirationCode = $expirationCode;

        return $this;
    }
    
      public function getExpirationToken(): ?\DateTimeInterface
    {
        return $this->expirationToken;
    }

    public function setExpirationToken(?\DateTimeInterface $expirationToken): self
    {
        $this->expirationToken = $expirationToken;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

   
   
    
    

    public function getCode(): ?int
    {
        return $this->code;
    }

    public function setCode(int $code): self
    {
        $this->code = $code;

        return $this;
    }

  

    /**
     * @return Collection<int, Patrol>
     */
    public function getPatrols(): Collection
    {
        return $this->patrols;
    }

    public function addPatrol(Patrol $patrol): self
    {
        if (!$this->patrols->contains($patrol)) {
            $this->patrols[] = $patrol;
            $patrol->setCollector($this);
        }

        return $this;
    }

    public function removePatrol(Patrol $patrol): self
    {
        if ($this->patrols->removeElement($patrol)) {
            // set the owning side to null (unless already changed)
            if ($patrol->getCollector() === $this) {
                $patrol->setCollector(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setUser($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getUser() === $this) {
                $comment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Compass>
     */
    public function getCompasses(): Collection
    {
        return $this->compasses;
    }

    public function addCompass(Compass $compass): self
    {
        if (!$this->compasses->contains($compass)) {
            $this->compasses[] = $compass;
            $compass->setUser($this);
        }

        return $this;
    }

    public function removeCompass(Compass $compass): self
    {
        if ($this->compasses->removeElement($compass)) {
            // set the owning side to null (unless already changed)
            if ($compass->getUser() === $this) {
                $compass->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Marketplace>
     */
    public function getMarketplaces(): Collection
    {
        return $this->marketplaces;
    }

    public function addMarketplace(Marketplace $marketplace): self
    {
        if (!$this->marketplaces->contains($marketplace)) {
            $this->marketplaces[] = $marketplace;
            $marketplace->setUser($this);
        }

        return $this;
    }

    public function removeMarketplace(Marketplace $marketplace): self
    {
        if ($this->marketplaces->removeElement($marketplace)) {
            // set the owning side to null (unless already changed)
            if ($marketplace->getUser() === $this) {
                $marketplace->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Project>
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->setUser($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->removeElement($project)) {
            // set the owning side to null (unless already changed)
            if ($project->getUser() === $this) {
                $project->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Observation>
     */
    public function getObservations(): Collection
    {
        return $this->observations;
    }

    public function addObservation(Observation $observation): self
    {
        if (!$this->observations->contains($observation)) {
            $this->observations[] = $observation;
            $observation->setUser($this);
        }

        return $this;
    }

    public function removeObservation(Observation $observation): self
    {
        if ($this->observations->removeElement($observation)) {
            // set the owning side to null (unless already changed)
            if ($observation->getUser() === $this) {
                $observation->setUser(null);
            }
        }

        return $this;
    }

    public function getFonction(): ?Fonction
    {
        return $this->fonction;
    }

    public function setFonction(?Fonction $fonction): self
    {
        $this->fonction = $fonction;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->Project;
    }

    public function setProject(?Project $Project): self
    {
        $this->Project = $Project;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getOrganization(): ?string
    {
        return $this->organization;
    }

    public function setOrganization(string $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getCodeTel(): ?int
    {
        return $this->codeTel;
    }

    public function setCodeTel(int $codeTel): self
    {
        $this->codeTel = $codeTel;

        return $this;
    }

    public function getSigle(): ?string
    {
        return $this->sigle;
    }

    public function setSigle(?string $sigle): self
    {
        $this->sigle = $sigle;

        return $this;
    }

    public function isMigrate(): ?bool
    {
        return $this->migrate;
    }

    public function setMigrate(bool $migrate): self
    {
        $this->migrate = $migrate;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }
    

}
