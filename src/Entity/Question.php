<?php

namespace App\Entity;

use App\Repository\QuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass=QuestionRepository::class)
 */
class Question
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Groups("question:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("question:read")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("question:read")
     */
    private $TypeAnswer;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("question:read")
     */
    private $typeBegin;

    /**
     * @ORM\Column(type="integer")
     * @Groups("question:read")
     */
    private $intervale;

    /**
     * @ORM\Column(type="boolean")
     * @Groups("question:read")
     */
    private $required = false;

   /**
     * @ORM\OneToMany(targetEntity=Answer::class, mappedBy="question")
     */
    private $answers;

    

    /**
     * @ORM\Column(type="datetime")
     * @Groups("question:read")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean")
     * @Groups("question:read")
     */
    private $isPicture = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("question:read")
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="questions")
     */
    private $project;

    /**
     * @ORM\OneToMany(targetEntity=TypeObservation::class, mappedBy="question")
     */
    private $typeObservations;

    /**
     * @ORM\OneToMany(targetEntity=Result::class, mappedBy="question")
     */
    private $results;

    /**
     * @ORM\ManyToOne(targetEntity=Question::class, inversedBy="questions")
     * @ORM\JoinColumn(nullable=true)
     */
    private $nextQuestion;

    /**
     * @ORM\OneToMany(targetEntity=Question::class, mappedBy="nextQuestion", orphanRemoval=true)
     */
    private $questions;


    public function __construct()
    {
        $this->updatedAt = new \DateTime('now');
        $this->answers = new ArrayCollection();
        $this->typeObservations = new ArrayCollection();
        $this->results = new ArrayCollection();
        $this->questions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    
     public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTypeAnswer(): ?string
    {
        return $this->TypeAnswer;
    }

    public function setTypeAnswer(string $TypeAnswer): self
    {
        $this->TypeAnswer = $TypeAnswer;

        return $this;
    }

    public function getTypeBegin(): ?string
    {
        return $this->typeBegin;
    }

    public function setTypeBegin(string $typeBegin): self
    {
        $this->typeBegin = $typeBegin;

        return $this;
    }

    public function getIntervale(): ?int
    {
        return $this->intervale;
    }

    public function setIntervale(int $intervale): self
    {
        $this->intervale = $intervale;

        return $this;
    }

    public function getRequired(): ?bool
    {
        return $this->required;
    }

    public function setRequired(bool $required): self
    {
        $this->required = $required;

        return $this;
    }

   

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isIsPicture(): ?bool
    {
        return $this->isPicture;
    }

    public function setIsPicture(bool $isPicture): self
    {
        $this->isPicture = $isPicture;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection<int, Answer>
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    public function addAnswer(Answer $answer): self
    {
        if (!$this->answers->contains($answer)) {
            $this->answers[] = $answer;
            $answer->setQuestion($this);
        }

        return $this;
    }

    public function removeAnswer(Answer $answer): self
    {
        if ($this->answers->removeElement($answer)) {
            // set the owning side to null (unless already changed)
            if ($answer->getQuestion() === $this) {
                $answer->setQuestion(null);
            }
        }

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return Collection<int, TypeObservation>
     */
    public function getTypeObservations(): Collection
    {
        return $this->typeObservations;
    }

    public function addTypeObservation(TypeObservation $typeObservation): self
    {
        if (!$this->typeObservations->contains($typeObservation)) {
            $this->typeObservations[] = $typeObservation;
            $typeObservation->setQuestion($this);
        }

        return $this;
    }

    public function removeTypeObservation(TypeObservation $typeObservation): self
    {
        if ($this->typeObservations->removeElement($typeObservation)) {
            // set the owning side to null (unless already changed)
            if ($typeObservation->getQuestion() === $this) {
                $typeObservation->setQuestion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Result>
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function addResult(Result $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
            $result->setQuestion($this);
        }

        return $this;
    }

    public function removeResult(Result $result): self
    {
        if ($this->results->removeElement($result)) {
            // set the owning side to null (unless already changed)
            if ($result->getQuestion() === $this) {
                $result->setQuestion(null);
            }
        }

        return $this;
    }

    public function getNextQuestion(): ?self
    {
        return $this->nextQuestion;
    }

    public function setNextQuestion(?self $nextQuestion): self
    {
        $this->nextQuestion = $nextQuestion;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(self $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions[] = $question;
            $question->setNextQuestion($this);
        }

        return $this;
    }

    public function removeQuestion(self $question): self
    {
        if ($this->questions->removeElement($question)) {
            // set the owning side to null (unless already changed)
            if ($question->getNextQuestion() === $this) {
                $question->setNextQuestion(null);
            }
        }

        return $this;
    }


    
}
