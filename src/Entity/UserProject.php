<?php

namespace App\Entity;

use App\Repository\UserProjectRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=UserProjectRepository::class)
 */
class UserProject
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("userProject:read")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="userProjects")
     * @Groups("userProject:read")
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userProjects")
     * @Groups("userProject:read")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $defaul = 0;

    /**
     * @ORM\Column(type="text")
     */
    private $role = "Regular user";

    public function getId(): ?int
    {
        return $this->id;
    }
    
    /*public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }*/

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function isDefaul(): ?bool
    {
        return $this->defaul;
    }

    public function setDefaul(bool $defaul): self
    {
        $this->defaul = $defaul;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }
}
