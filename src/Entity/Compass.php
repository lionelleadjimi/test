<?php

namespace App\Entity;

use App\Repository\CompassRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=CompassRepository::class)
 */
class Compass
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Groups("compass:read")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("compass:read")
     */
    private $date;

    /**
     * @ORM\Column(type="float")
     * @Groups("compass:read")
     */
    private $coordX;

    /**
     * @ORM\Column(type="float")
     * @Groups("compass:read")
     */
    private $coordY;


    /**
     * @ORM\Column(type="string", length=1024)
     * @Groups("compass:read")
     */
    private $title;


    /**
     * @ORM\Column(type="integer")
     * @Groups("answer:read")
     */
    private $status = 1;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="compasses")
     * @ORM\JoinColumn(nullable=true)
     * @Groups("compass:read")
     */
    private $user;

    public function __construct() {
        $this->date = new \DateTime('now');
    }
    

    public function __toString() {
        return $this->title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCoordX(): ?float
    {
        return $this->coordX;
    }

    public function setCoordX(float $coordX): self
    {
        $this->coordX = $coordX;

        return $this;
    }

    public function getCoordY(): ?float
    {
        return $this->coordY;
    }

    public function setCoordY(float $coordY): self
    {
        $this->coordY = $coordY;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }


    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
