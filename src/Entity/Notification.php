<?php

namespace App\Entity;

use App\Repository\NotificationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass=NotificationRepository::class)
 * 
 */
class Notification
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Groups("notification:read")
     */
    private $id;

   /**
     * @ORM\Column(type="string", length=255)
     * @Groups("notification:read")
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("notification:read")
     */
    private $date;

    public function __construct() {
        $this->date = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }
    
    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
