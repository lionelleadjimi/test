<?php

namespace App\Entity;

use App\Repository\SpecieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=SpecieRepository::class)
 */
class Specie
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Groups("specie:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("specie:read")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("specie:read")
     */
    private $name_inaturalist;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("specie:read")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("specie:read")
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity=Subgroup::class, inversedBy="species")
     */
    private $subgroup;

     /**
     * @ORM\ManyToOne(targetEntity=Question::class, inversedBy="species")
     */
    private $question;


    /**
     * @ORM\Column(type="integer")
     * @Groups("specie:read")
     */
    private $defaut = 0;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("specie:read")
     */
    private $date;

    /**
     * @ORM\OneToMany(targetEntity=Observation::class, mappedBy="specie")
     */
    private $observations;



    public function __construct()
    {
        $this->date = new \DateTime('now');
        $this->observations = new ArrayCollection();
    }
   

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }
  
    
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNameInaturalist(): ?string
    {
        return $this->name_inaturalist;
    }

    public function setNameInaturalist(?string $name_inaturalist): self
    {
        $this->name_inaturalist = $name_inaturalist;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getSubgroup(): ?Subgroup
    {
        return $this->subgroup;
    }

    public function setSubgroup(?Subgroup $subgroup): self
    {
        $this->subgroup = $subgroup;

        return $this;
    }


    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    public function setQuestion(?Question $question): self
    {
        $this->question= $question;

        return $this;
    }



    public function getDefaut(): ?int
    {
        return $this->defaut;
    }

    public function setDefaut(int $defaut): self
    {
        $this->defaut = $defaut;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection<int, Observation>
     */
    public function getObservations(): Collection
    {
        return $this->observations;
    }

    public function addObservation(Observation $observation): self
    {
        if (!$this->observations->contains($observation)) {
            $this->observations[] = $observation;
            $observation->setSpecie($this);
        }

        return $this;
    }

    public function removeObservation(Observation $observation): self
    {
        if ($this->observations->removeElement($observation)) {
            // set the owning side to null (unless already changed)
            if ($observation->getSpecie() === $this) {
                $observation->setSpecie(null);
            }
        }

        return $this;
    }


   


    

}
