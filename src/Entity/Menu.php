<?php

namespace App\Entity;

use App\Repository\MenuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=MenuRepository::class)
 */
class Menu
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Groups("menu:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("menu:read")
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity=MenuProject::class, mappedBy="menu")
     */
    private $menuProjects;

    public function __construct()
    {
        $this->menuProjects = new ArrayCollection();
    }

  

    public function getId(): ?int
    {
        return $this->id;
    }
    
     public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection<int, MenuProject>
     */
    public function getMenuProjects(): Collection
    {
        return $this->menuProjects;
    }

    public function addMenuProject(MenuProject $menuProject): self
    {
        if (!$this->menuProjects->contains($menuProject)) {
            $this->menuProjects[] = $menuProject;
            $menuProject->setMenu($this);
        }

        return $this;
    }

    public function removeMenuProject(MenuProject $menuProject): self
    {
        if ($this->menuProjects->removeElement($menuProject)) {
            // set the owning side to null (unless already changed)
            if ($menuProject->getMenu() === $this) {
                $menuProject->setMenu(null);
            }
        }

        return $this;
    }

  
}
