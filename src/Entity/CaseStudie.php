<?php

namespace App\Entity;

use App\Repository\CaseStudieRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=CaseStudieRepository::class)
 */
class CaseStudie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("caseStudie:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("caseStudie:read")
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups("caseStudie:read")
     */
    private $shortDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("caseStudie:read")
     */
    private $fullDescription;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("caseStudie:read")
     */
    private $image;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("caseStudie:read")
     */
    private $dateCreate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    public function getFullDescription(): ?string
    {
        return $this->fullDescription;
    }

    public function setFullDescription(?string $fullDescription): self
    {
        $this->fullDescription = $fullDescription;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDateCreate(): ?\DateTimeInterface
    {
        return $this->dateCreate;
    }

    public function setDateCreate(\DateTimeInterface $dateCreate): self
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }
}
