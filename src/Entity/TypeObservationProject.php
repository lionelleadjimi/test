<?php

namespace App\Entity;

use App\Repository\TypeObservationProjectRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeObservationProjectRepository::class)
 */
class TypeObservationProject
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=TypeObservation::class, inversedBy="typeObservationProjects")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeObservation;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="typeObservationProjects")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    public function getId(): ?int
    {
        return $this->id;
    }
    
     public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTypeObservation(): ?TypeObservation
    {
        return $this->typeObservation;
    }

    public function setTypeObservation(?TypeObservation $typeObservation): self
    {
        $this->typeObservation = $typeObservation;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }
}
