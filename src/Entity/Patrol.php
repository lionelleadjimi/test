<?php

namespace App\Entity;

use App\Repository\PatrolRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PatrolRepository::class)
 */
class Patrol
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("patrol:read")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="patrols")
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="patrols")
     */
    private $collector;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("patrol:read")
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("patrol:read")
     */
    private $endDate;

    /**
     * @ORM\Column(type="float")
     * @Groups("patrol:read")
     */
    private $coordXStart;

    /**
     * @ORM\Column(type="float")
     * @Groups("patrol:read")
     */
    private $coordYStart;

    /**
     * @ORM\Column(type="float")
     * @Groups("patrol:read")
     */
    private $coordXEnd;

    /**
     * @ORM\Column(type="float")
     * @Groups("patrol:read")
     */
    private $coordYEnd;

    /**
     * @ORM\OneToMany(targetEntity=Observation::class, mappedBy="patrol")
     */
    private $observations;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEnreg;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("patrol:read")
     */
    private $note;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("patrol:read")
     */
    private $totalPatrolTime;

    /**
     * @ORM\Column(type="float")
     * @Groups("patrol:read")
     */
    private $totalDistance;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberObservations;

    public function __construct()
    {
        $this->observations = new ArrayCollection();
        $this->dateEnreg = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getCollector(): ?User
    {
        return $this->collector;
    }

    public function setCollector(?User $collector): self
    {
        $this->collector = $collector;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getCoordXStart(): ?float
    {
        return $this->coordXStart;
    }

    public function setCoordXStart(float $coordXStart): self
    {
        $this->coordXStart = $coordXStart;

        return $this;
    }

    public function getCoordYStart(): ?float
    {
        return $this->coordYStart;
    }

    public function setCoordYStart(float $coordYStart): self
    {
        $this->coordYStart = $coordYStart;

        return $this;
    }

    public function getCoordXEnd(): ?float
    {
        return $this->coordXEnd;
    }

    public function setCoordXEnd(float $coordXEnd): self
    {
        $this->coordXEnd = $coordXEnd;

        return $this;
    }

    public function getCoordYEnd(): ?float
    {
        return $this->coordYEnd;
    }

    public function setCoordYEnd(float $coordYEnd): self
    {
        $this->coordYEnd = $coordYEnd;

        return $this;
    }

    /**
     * @return Collection<int, Observation>
     */
    public function getObservations(): Collection
    {
        return $this->observations;
    }

    public function addObservation(Observation $observation): self
    {
        if (!$this->observations->contains($observation)) {
            $this->observations[] = $observation;
            $observation->setPatrol($this);
        }

        return $this;
    }

    public function removeObservation(Observation $observation): self
    {
        if ($this->observations->removeElement($observation)) {
            // set the owning side to null (unless already changed)
            if ($observation->getPatrol() === $this) {
                $observation->setPatrol(null);
            }
        }

        return $this;
    }

    public function getDateEnreg(): ?\DateTimeInterface
    {
        return $this->dateEnreg;
    }

    public function setDateEnreg(\DateTimeInterface $dateEnreg): self
    {
        $this->dateEnreg = $dateEnreg;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getTotalPatrolTime(): ?string
    {
        return $this->totalPatrolTime;
    }

    public function setTotalPatrolTime(string $totalPatrolTime): self
    {
        $this->totalPatrolTime = $totalPatrolTime;

        return $this;
    }

    public function getTotalDistance(): ?float
    {
        return $this->totalDistance;
    }

    public function setTotalDistance(float $totalDistance): self
    {
        $this->totalDistance = $totalDistance;

        return $this;
    }

    public function getNumberObservations(): ?int
    {
        return $this->numberObservations;
    }

    public function setNumberObservations(int $numberObservations): self
    {
        $this->numberObservations = $numberObservations;

        return $this;
    }
}
