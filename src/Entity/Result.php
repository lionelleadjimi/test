<?php

namespace App\Entity;

use App\Repository\ResultRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ResultRepository::class)
 */
class Result
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Groups("result:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("result:read")
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity=Question::class, inversedBy="results")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("result:read")
     */
    private $question;

    /**
     * @ORM\ManyToOne(targetEntity=Observation::class, inversedBy="results")
     * @ORM\JoinColumn(nullable=true)
     * @Groups("result:read")
     */
    private $observation;


    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    public function setQuestion(?Question $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getObservation(): ?Observation
    {
        return $this->observation;
    }

    public function setObservation(?Observation $observation): self
    {
        $this->observation = $observation;

        return $this;
    }

  
    

}
