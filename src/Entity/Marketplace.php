<?php

namespace App\Entity;

use App\Repository\MarketplaceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=MarketplaceRepository::class)
 */
class Marketplace
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("marketplace:read")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("marketplace:read")
     */
    private $date;

    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typeOfFish;

    /**
     * @ORM\Column(type="string", length=1024)
     * @Groups("marketplace:read")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=1024)
     * @Groups("marketplace:read")
     */
    private $unloader;

    /**
     * @ORM\Column(type="string", length=1024)
     * @Groups("marketplace:read")
     */
    private $fishSite;

    /**
     * @ORM\Column(type="integer")
     * @Groups("marketplace:read")
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer")
     * @Groups("marketplace:read")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=1024)
     * @Groups("marketplace:read")
     */
    private $phone;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("marketplace:read")
     */
    private $img1;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("marketplace:read")
     */
    private $img2;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("marketplace:read")
     */
    private $img3;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("marketplace:read")
     */
    private $img4;

  

    /**
     * @ORM\Column(type="integer")
     * @Groups("marketplace:read")
     */
    private $status = 1;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="marketplace", orphanRemoval=true)
     */
    private $comments;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="marketplaces")
     */
    private $user;




    public function __construct() {
        $this->date = new \DateTime('now');
        $this->comments = new ArrayCollection();
    }
   
    function getSlug() {
        $slugify = new Slugify();
        return $slugify->slugify(microtime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTypeOfFish(): ?string
    {
        return $this->typeOfFish;
    }

    public function setTypeOfFish(string $typeOfFish): self
    {
        $this->typeOfFish = $typeOfFish;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUnloader(): ?string
    {
        return $this->unloader;
    }

    public function setUnloader(string $unloader): self
    {
        $this->unloader = $unloader;

        return $this;
    }

    public function getFishSite(): ?string
    {
        return $this->fishSite;
    }

    public function setFishSite(string $fishSite): self
    {
        $this->fishSite = $fishSite;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getImg1(): ?string
    {
        return $this->img1;
    }

    public function setImg1(?string $img1): self
    {
        $this->img1 = $img1;

        return $this;
    }

    public function getImg2(): ?string
    {
        return $this->img2;
    }

    public function setImg2(?string $img2): self
    {
        $this->img2 = $img2;

        return $this;
    }

    public function getImg3(): ?string
    {
        return $this->img3;
    }

    public function setImg3(?string $img3): self
    {
        $this->img3 = $img3;

        return $this;
    }

    public function getImg4(): ?string
    {
        return $this->img4;
    }

    public function setImg4(?string $img4): self
    {
        $this->img4 = $img4;

        return $this;
    }

    

   

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setMarketplace($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getMarketplace() === $this) {
                $comment->setMarketplace(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    


}
