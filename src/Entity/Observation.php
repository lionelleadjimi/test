<?php

namespace App\Entity;

use App\Repository\ObservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ObservationRepository::class)
 */
class Observation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Groups("observation:read")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("observation:read")
     */
    private $date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups("observation:read")
     */
    private $id_inaturalist;

    /**
     * @ORM\Column(type="float")
     * @Groups("observation:read")
     */
    private $coordX;

    /**
     * @ORM\Column(type="float")
     * @Groups("observation:read")
     */
    private $coordY;


    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Groups("observation:read")
     */
    private $note;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Groups("observation:read")
     */
    private $alpha;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Groups("observation:read")
     */
    private $collector = "admin";

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("observation:read")
     */
    private $img1 = "observation1.png";

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("observation:read")
     */
    private $img2;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("observation:read")
     */
    private $img3;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("observation:read")
     */
    private $img4;


    /**
     * @ORM\Column(type="integer")
     * @Groups("observation:read")
     */
    private $dead = 0;

    /**
     * @ORM\Column(type="integer")
     * @Groups("observation:read")
     */
    private $status = 0;
   

    /**
     * @ORM\OneToMany(targetEntity=Result::class, mappedBy="observation")
     * @Groups("observation:read")
     */
    private $results;

    /**
     * @ORM\ManyToOne(targetEntity=TypeObservation::class, inversedBy="observations")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("observation:read")
     */
    private $typeObservation;

    /**
     * @ORM\ManyToOne(targetEntity=Specie::class, inversedBy="observations")
     * @Groups("observation:read")
     */
    private $specie;



    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="observations")
     * @ORM\JoinColumn(nullable=false)
    // * @Groups("observation:read")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="observations")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("observation:read")
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity=Segment::class, inversedBy="observations")
     * @ORM\JoinColumn(nullable=true)
     * @Groups("observation:read")
     */
    private $segment;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateDate;

    /**
     * @ORM\ManyToOne(targetEntity=Site::class, inversedBy="observations")
     * @ORM\JoinColumn(nullable=true)
     */
    private $site;

    /**
     * @ORM\ManyToOne(targetEntity=Patrol::class, inversedBy="observations")
     */
    private $patrol;

  


    public function __construct()
    {
        $this->results = new ArrayCollection();
       // $this->date = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    
     public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getIdInaturalist(): ?int
    {
        return $this->id_inaturalist;
    }

    public function setIdInaturalist(?int $id_inaturalist): self
    {
        $this->id_inaturalist = $id_inaturalist;

        return $this;
    }

    public function getCoordX(): ?float
    {
        return $this->coordX;
    }

    public function setCoordX(float $coordX): self
    {
        $this->coordX = $coordX;

        return $this;
    }

    public function getCoordY(): ?float
    {
        return $this->coordY;
    }

    public function setCoordY(float $coordY): self
    {
        $this->coordY = $coordY;

        return $this;
    }

    

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getAlpha(): ?string
    {
        return $this->alpha;
    }

    public function setAlpha(?string $alpha): self
    {
        $this->alpha = $alpha;

        return $this;
    }

    public function getCollector(): ?string
    {
        return $this->collector;
    }

    public function setCollector(?string $collector): self
    {
        $this->collector = $collector;

        return $this;
    }

    public function getImg1(): ?string
    {
        return $this->img1;
    }

    public function setImg1(?string $img1): self
    {
        $this->img1 = $img1;

        return $this;
    }

    public function getImg2(): ?string
    {
        return $this->img2;
    }

    public function setImg2(?string $img2): self
    {
        $this->img2 = $img2;

        return $this;
    }

    public function getImg3(): ?string
    {
        return $this->img3;
    }

    public function setImg3(?string $img3): self
    {
        $this->img3 = $img3;

        return $this;
    }

    public function getImg4(): ?string
    {
        return $this->img4;
    }

    public function setImg4(?string $img4): self
    {
        $this->img4 = $img4;

        return $this;
    }

  
   

    public function getDead(): ?int
    {
        return $this->dead;
    }

    public function setDead(int $dead): self
    {
        $this->dead = $dead;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, Result>
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function addResult(Result $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
            $result->setObservation($this);
        }

        return $this;
    }

    public function removeResult(Result $result): self
    {
        if ($this->results->removeElement($result)) {
            // set the owning side to null (unless already changed)
            if ($result->getObservation() === $this) {
                $result->setObservation(null);
            }
        }

        return $this;
    }

    public function getTypeObservation(): ?TypeObservation
    {
        return $this->typeObservation;
    }

    public function setTypeObservation(?TypeObservation $typeObservation): self
    {
        $this->typeObservation = $typeObservation;

        return $this;
    }

    public function getSpecie(): ?Specie
    {
        return $this->specie;
    }

    public function setSpecie(?Specie $specie): self
    {
        $this->specie = $specie;

        return $this;
    }

   

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getSegment(): ?Segment
    {
        return $this->segment;
    }

    public function setSegment(?Segment $segment): self
    {
        $this->segment = $segment;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getSite(): ?Site
    {
        return $this->site;
    }

    public function setSite(?Site $site): self
    {
        $this->site = $site;

        return $this;
    }

    public function getPatrol(): ?Patrol
    {
        return $this->patrol;
    }

    public function setPatrol(?Patrol $patrol): self
    {
        $this->patrol = $patrol;

        return $this;
    }


   

   

   
}
