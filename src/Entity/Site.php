<?php

namespace App\Entity;

use App\Repository\SiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=SiteRepository::class)
 */
class Site
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("site:read")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Groups("site:read")
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("site:read")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="sites")
     * @ORM\JoinColumn(nullable=true)
     */
    private $project;

    /**
     * @ORM\OneToMany(targetEntity=Segment::class, mappedBy="site")
     */
    private $segments;

    /**
     * @ORM\OneToMany(targetEntity=Observation::class, mappedBy="site")
     */
    private $observations;

   

    public function __construct() {
        $this->date = new \DateTime('now');
        $this->segments = new ArrayCollection();
        $this->observations = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    /* public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }*/
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return Collection<int, Segment>
     */
    public function getSegments(): Collection
    {
        return $this->segments;
    }

    public function addSegment(Segment $segment): self
    {
        if (!$this->segments->contains($segment)) {
            $this->segments[] = $segment;
            $segment->setSite($this);
        }

        return $this;
    }

    public function removeSegment(Segment $segment): self
    {
        if ($this->segments->removeElement($segment)) {
            // set the owning side to null (unless already changed)
            if ($segment->getSite() === $this) {
                $segment->setSite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Observation>
     */
    public function getObservations(): Collection
    {
        return $this->observations;
    }

    public function addObservation(Observation $observation): self
    {
        if (!$this->observations->contains($observation)) {
            $this->observations[] = $observation;
            $observation->setSite($this);
        }

        return $this;
    }

    public function removeObservation(Observation $observation): self
    {
        if ($this->observations->removeElement($observation)) {
            // set the owning side to null (unless already changed)
            if ($observation->getSite() === $this) {
                $observation->setSite(null);
            }
        }

        return $this;
    }

    

}
