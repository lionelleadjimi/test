<?php

namespace App\Entity;

use App\Repository\ProjectMenuRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProjectMenuRepository::class)
 */
class ProjectMenu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="projectMenus")
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity=Menu::class, inversedBy="projectMenus")
     */
    private $menu;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getMenu(): ?Menu
    {
        return $this->menu;
    }

    public function setMenu(?Menu $menu): self
    {
        $this->menu = $menu;

        return $this;
    }
}
