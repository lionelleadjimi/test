<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("patrol:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("project:read")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("project:read")
     */
    private $note;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("project:read")
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("project:read")
     */
    private $organization;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("project:read")
     */
    private $maplink;

    /**
     * @ORM\Column(type="integer")
     * @Groups("project:read")
     */
    private $public;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="projects")
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="projects")
     */
    private $admin;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("project:read")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("project:read")
     */
    private $color;

   /**
     * @ORM\Column(type="float")
     * @Groups("project:read")
     */
    private $coordX;

    /**
     * @ORM\Column(type="float")
     * @Groups("project:read")
     */
    private $coordY;

    /**
     * @ORM\Column(type="integer")
     * @Groups("project:read")
     */
    private $zoom;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("project:read")
     */
    private $legalNotices;

     /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("project:read")
     */
    private $websiteLink;

     /**
     * @ORM\Column(type="integer")
     * @Groups("project:read")
     */
    private $gpsPrecision;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("project:read")
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity=Patrol::class, mappedBy="project")
     */
    private $patrols;

    /**
     * @ORM\ManyToOne(targetEntity=Question::class, inversedBy="projects")
     */
    private $question;

     /**
     * @ORM\OneToMany(targetEntity=MenuProject::class, mappedBy="project")
     */
    private $menuProjects;

     /**
     * @ORM\OneToMany(targetEntity=UserProject::class, mappedBy="project")
     */
    private $userProjects; 
    
     /**
     * @ORM\OneToMany(targetEntity=NotificationsProject::class, mappedBy="project")
     */
    private $notificationsProjects;

      /**
     * @ORM\OneToMany(targetEntity=TypeObservationProject::class, mappedBy="project")
     */
    private $typeObservationProjects;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="Project", orphanRemoval=true)
     */
    private $users;
    
      /**
     * @ORM\OneToMany(targetEntity=Observation::class, mappedBy="project")
     */
    private $observations;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $timeZone;

   
    public function __construct() {
        $this->date = new \DateTime('now');
        $this->patrols = new ArrayCollection();
        $this->menuProjects = new ArrayCollection();
        $this->userProjects = new ArrayCollection();
        $this->notificationsProjects = new ArrayCollection();
        $this->typeObservationProjects = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->observations = new ArrayCollection();
    }

   
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }
    
   /*public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }*/
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getOrganization(): ?string
    {
        return $this->organization;
    }

    public function setOrganization(?string $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getMaplink(): ?string
    {
        return $this->maplink;
    }

    public function setMaplink(?string $maplink): self
    {
        $this->maplink = $maplink;

        return $this;
    }

    public function getPublic(): ?int
    {
        return $this->public;
    }

    public function setPublic(int $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getAdmin(): ?User
    {
        return $this->admin;
    }

    public function setAdmin(?User $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }
   public function getCoordX(): ?float
    {
        return $this->coordX;
    }

    public function setCoordX(float $coordX): self
    {
        $this->coordX = $coordX;

        return $this;
    }

    public function getCoordY(): ?float
    {
        return $this->coordY;
    }

    public function setCoordY(float $coordY): self
    {
        $this->coordY = $coordY;

        return $this;
    }

    public function getZoom(): ?int
    {
        return $this->zoom;
    }

    public function setZoom(int $zoom): self
    {
        $this->zoom = $zoom;

        return $this;
    }
    public function getLegalNotices(): ?string
    {
        return $this->legalNotices;
    }

    public function setLegalNotices(?string $legalNotices): self
    {
        $this->legalNotices = $legalNotices;

        return $this;
    }

    public function getWebsiteLink(): ?string
    {
        return $this->websiteLink;
    }

    public function setWebsiteLink(?string $websiteLink): self
    {
        $this->websiteLink = $websiteLink;

        return $this;
    }
    public function getGpsPrecision(): ?string
    {
        return $this->gpsPrecision;
    }

    public function setGpsPrecision(?string $gpsPrecision): self
    {
        $this->gpsPrecision = $gpsPrecision;

        return $this;
    }
    
    

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }


    /**
     * @return Collection<int, Patrol>
     */
    public function getPatrols(): Collection
    {
        return $this->patrols;
    }

    public function addPatrol(Patrol $patrol): self
    {
        if (!$this->patrols->contains($patrol)) {
            $this->patrols[] = $patrol;
            $patrol->setProject($this);
        }

        return $this;
    }

    public function removePatrol(Patrol $patrol): self
    {
        if ($this->patrols->removeElement($patrol)) {
            // set the owning side to null (unless already changed)
            if ($patrol->getProject() === $this) {
                $patrol->setProject(null);
            }
        }

        return $this;
    }

    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    public function setQuestion(?Question $question): self
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setProject($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getProject() === $this) {
                $user->setProject(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection<int, Observation>
     */
    public function getObservation(): Collection
    {
        return $this->observations;
    }

    public function addObservation(Observation $observations): self
    {
        if (!$this->observations->contains($observations)) {
            $this->observation[] = $observations;
            $observation->setProject($this);
        }

        return $this;
    }

    public function removeObservation(Observation $observation): self
    {
        if ($this->observations->removeElement($observations)) {
            // set the owning side to null (unless already changed)
            if ($observation->getProject() === $this) {
                $observation->setProject(null);
            }
        }

        return $this;
    }

    public function getTimeZone(): ?string
    {
        return $this->timeZone;
    }

    public function setTimeZone(?string $timeZone): self
    {
        $this->timeZone = $timeZone;

        return $this;
    }

   


}
