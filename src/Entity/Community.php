<?php

namespace App\Entity;

use App\Repository\CommunityRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=CommunityRepository::class)
 */
class Community
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("community:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("community:read")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("community:read")
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("community:read")
     */
    private $photo;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("community:read")
     */
    private $dateCreate;

    /**
     * @ORM\Column(type="integer")
     * @Groups("community:read")
     */
    private $level;

    public function __construct()
    {
        $this->dateCreate = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getDateCreate(): ?\DateTimeInterface
    {
        return $this->dateCreate;
    }

    public function setDateCreate(\DateTimeInterface $dateCreate): self
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }
}
