<?php

namespace App\Entity;

use App\Repository\TypeObservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=TypeObservationRepository::class)
 */
class TypeObservation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Groups("typeObservation:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("typeObservation:read")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("typeObservation:read")
     */
    private $type_question;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("typeObservation:read")
     */
    private $image;


    /**
     * @ORM\Column(type="integer")
     * @Groups("typeObservation:read")
     */
    private $level;

    /**
     * @ORM\ManyToOne(targetEntity=Question::class, inversedBy="typeObservations")
     */
    private $question;

    /**
     * @ORM\OneToMany(targetEntity=Observation::class, mappedBy="typeObservation")
     */
    private $observations;

    /**
     * @ORM\OneToMany(targetEntity=TypeObservationProject::class, mappedBy="typeObservation")
     */
    private $typeObservationProjects;

    /**
     * @ORM\Column(type="text")
     */
    private $colors = "NULL";


    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->observations = new ArrayCollection();
        $this->typeObservationProjects = new ArrayCollection();
          $this->updatedAt = new \DateTime('now');
    }

   

    public function getId(): ?int
    {
        return $this->id;
    }
    
      public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTypeQuestion(): ?string
    {
        return $this->type_question;
    }

    public function setTypeQuestion(string $type_question): self
    {
        $this->type_question = $type_question;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

   
    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    public function setQuestion(?Question $question): self
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @return Collection<int, Observation>
     */
    public function getObservations(): Collection
    {
        return $this->observations;
    }

    public function addObservation(Observation $observation): self
    {
        if (!$this->observations->contains($observation)) {
            $this->observations[] = $observation;
            $observation->setTypeObservation($this);
        }

        return $this;
    }

    public function removeObservation(Observation $observation): self
    {
        if ($this->observations->removeElement($observation)) {
            // set the owning side to null (unless already changed)
            if ($observation->getTypeObservation() === $this) {
                $observation->setTypeObservation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, TypeObservationProject>
     */
    public function getTypeObservationProjects(): Collection
    {
        return $this->typeObservationProjects;
    }

    public function addTypeObservationProject(TypeObservationProject $typeObservationProject): self
    {
        if (!$this->typeObservationProjects->contains($typeObservationProject)) {
            $this->typeObservationProjects[] = $typeObservationProject;
            $typeObservationProject->setTypeObservation($this);
        }

        return $this;
    }

    public function removeTypeObservationProject(TypeObservationProject $typeObservationProject): self
    {
        if ($this->typeObservationProjects->removeElement($typeObservationProject)) {
            // set the owning side to null (unless already changed)
            if ($typeObservationProject->getTypeObservation() === $this) {
                $typeObservationProject->setTypeObservation(null);
            }
        }

        return $this;
    }

    public function getColors(): ?string
    {
        return $this->colors;
    }

    public function setColors(string $colors): self
    {
        $this->colors = $colors;

        return $this;
    }

   

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

}
