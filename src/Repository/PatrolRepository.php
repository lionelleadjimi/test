<?php

namespace App\Repository;

use App\Entity\Patrol;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Patrol>
 *
 * @method Patrol|null find($id, $lockMode = null, $lockVersion = null)
 * @method Patrol|null findOneBy(array $criteria, array $orderBy = null)
 * @method Patrol[]    findAll()
 * @method Patrol[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatrolRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Patrol::class);
    }

    public function add(Patrol $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Patrol $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return Patrol[] Returns an array of Patrol objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Patrol
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

          public function getexport($project, $start, $end) {
       
       
      $qb = $this->createQueryBuilder('p');
       
        $qb->where('p.project = :project')
           ->setParameter('project', $project);
           
        if($start != null)
            $qb->andWhere($qb->expr()->between('p.dateEnreg', ':start', ':end'))
                ->setParameter('start', $start)
                ->setParameter('end', $end);  
       
    return $qb->getQuery()->getResult();
    }
}
