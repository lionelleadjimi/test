<?php

namespace App\Repository;

use App\Entity\Observation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Observation>
 *
 * @method Observation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Observation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Observation[]    findAll()
 * @method Observation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Observation::class);
    }

    public function add(Observation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Observation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return Observations[] Returns an array of Observations objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('o.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

  /**
    * @return Observations[] Returns an array of Observations objects
    */

   public function findOneBySomeField($value): array
    {
       return $this->createQueryBuilder('o')
            ->andWhere('o.project = :val')
           ->setParameter('val', $value)
           //->distinct()
           ->getQuery()
            ->getResult()
        ;
   }
   
   /**
    * @return Observations[] Returns an array of Observations objects
    */
   public function findByInterval($project, $status, $obsType, $start, $end, $begin, $limit): array
    {
        // $qb = $this->getEntityManager()->createQueryBuilder();
        $qb = $this->createQueryBuilder('o');
        
        // return $this->createQueryBuilder('o')
        // return $qb->where('o.project = :project')
        //         ->andWhere('o.status = :status')
        //         ->andWhere($qb->expr()->in('o.typeObservation', $obsType))
        //         ->andWhere($qb->expr()->between('o.date', ':start', ':end'))
        //         ->setParameter('start', $start)
        //         ->setParameter('end', $end)
        //         ->setParameter('project', $project)
        //         // ->setParameter('obsType', $obsType)
        //         ->setParameter('status', $status)
        //         ->orderBy('o.id', 'DESC')
        //         ->getQuery()
        //         ->getResult();
        
        $qb->where('o.project = :project')
           ->andWhere('o.status = :status')
           ->setParameter('project', $project)
           ->setParameter('status', $status);
                
        if($start > 0)
            $qb->andWhere($qb->expr()->between('o.date', ':start', ':end'))
                ->setParameter('start', $start)
                ->setParameter('end', $end);
                
        if(count($obsType) > 0)
            $qb->andWhere($qb->expr()->in('o.typeObservation', $obsType));
        
        if($limit > 0)
            $qb->setFirstResult($begin)->setMaxResults($limit);
                
        return $qb
                ->orderBy('o.id', 'DESC')
                ->getQuery()
                ->getResult();
        
    }
    
    
   /**
    * @return Observations[] Returns an array of Observations objects
    */
   public function findAllByfilter($status, $obsType, $start, $end, $begin, $limit): array
    {
        // $qb = $this->getEntityManager()->createQueryBuilder();
        $qb = $this->createQueryBuilder('o');
        
        
        $qb->where('o.status = :status')
           ->setParameter('status', $status);
                
        if($start > 0)
            $qb->andWhere($qb->expr()->between('o.date', ':start', ':end'))
                ->setParameter('start', $start)
                ->setParameter('end', $end);
                
        if(count($obsType) > 0)
            $qb->andWhere($qb->expr()->in('o.typeObservation', $obsType));
        
        if($limit > 0)
            $qb->setFirstResult($begin)->setMaxResults($limit);
                
        return $qb
                ->orderBy('o.id', 'DESC')
                ->getQuery()
                ->getResult();
        
    }
    
    public function getexport($project, $status, $start, $end) {
       
       
      $qb = $this->createQueryBuilder('o');
       
        $qb->where('o.project = :project')
           ->andWhere('o.status = :status')
           ->setParameter('project', $project)
           ->setParameter('status', $status);
           
        if($start != null)
            $qb->andWhere($qb->expr()->between('o.date', ':start', ':end'))
                ->setParameter('start', $start)
                ->setParameter('end', $end);  
       
      /*  $query = $this->createQueryBuilder("o"); 
        
    if ($debut != null) { 
        $query = $query->where("o.date >= :debut")
            ->setParameter("debut", $debut);
    }

    if ($fin != null) {
        if($debut != null){
            $query = $query->andWhere("o.date <= :fin");
        }
        else {
            $query = $query->where("o.date <= :fin");
        }
        $query = $query->setParameter("fin", $fin);
    }

    if($project != null){
        $query = $query->join("o.project", "p");
        if($debut == null && $fin == null)
            $query = $query->where("p.id = :projet")->setParameter("projet", $project);
        else
            $query = $query->andWhere("p.id = :projet")->setParameter("projet", $project);
    }*/
    return $qb->getQuery()->getResult();
    }
    
    public function getexportByPage($project, $status, $start, $end, $begin, $limit) {
       
       
      $qb = $this->createQueryBuilder('o');
       
        $qb->where('o.project = :project')
           ->andWhere('o.status = :status')
           ->setParameter('project', $project)
           ->setParameter('status', $status);
           
        if($start != null)
            $qb->andWhere($qb->expr()->between('o.date', ':start', ':end'))
                ->setParameter('start', $start)
                ->setParameter('end', $end);  
             $qb->setFirstResult($begin)->setMaxResults($limit);
    return $qb->getQuery()->getResult();
    }
}
