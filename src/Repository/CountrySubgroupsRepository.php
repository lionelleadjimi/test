<?php

namespace App\Repository;

use App\Entity\CountrySubgroups;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CountrySubgroups>
 *
 * @method CountrySubgroups|null find($id, $lockMode = null, $lockVersion = null)
 * @method CountrySubgroups|null findOneBy(array $criteria, array $orderBy = null)
 * @method CountrySubgroups[]    findAll()
 * @method CountrySubgroups[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CountrySubgroupsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CountrySubgroups::class);
    }

    public function add(CountrySubgroups $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(CountrySubgroups $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return CountrySubgroups[] Returns an array of CountrySubgroups objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CountrySubgroups
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
