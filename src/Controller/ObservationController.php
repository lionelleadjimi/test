<?php

namespace App\Controller;

use App\Entity\Observation;
use App\Repository\ObservationRepository;
use App\Repository\TypeObservationRepository;
use App\Repository\SpecieRepository;
use App\Repository\GroupRepository;
use App\Repository\SubgroupRepository;
use App\Repository\ProjectRepository;
use App\Repository\PatrolRepository;
use App\Repository\SegmentRepository;
use App\Repository\ResultRepository;
use App\Repository\UserRepository;
use App\Repository\SiteRepository;
use App\Repository\UserProjectRepository;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use App\ImageOptimizer;
use Symfony\Contracts\HttpClient\HttpClientInterface;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class ObservationController extends ApiController
{
    private $em;
    private $sirenUser;
    
    private $apiUrl = 'https://www.inaturalist.org';
    private $clientId = 'ceb09035af7c21cd73bf157e73ad8fb03df24a3e6bc766c097d6dcf19cf82b20';
    private $clientSecret = '64d649a40dd464d2f1a65afa29c73b6e24b7b43afa56349152f7f6f6bc8bb647';
    private $username = 'akamla@ammco.org';
    private $password = 'LcU.@a7T&985]daU';



    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                ObservationRepository $repository,
                                ImageOptimizer $imageOptimizer,
                                HttpClientInterface $sirenUser)
    {
        $this->sirenUser = $sirenUser;
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
          $this->imageOptimizer = $imageOptimizer;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }


      /**
     * Register.
     *
     * @Route("api/observation/create", name="observation_create", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="id_inaturalist",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="coordX",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="coordY",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="note",
     *     in="query", 
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="dead",
     *     in="query", 
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="alpha",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="img1",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="img2",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="specie_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="user_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="type_observation_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="segment_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="siteId",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="date",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="time",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="observation")
     */
    public function createAction(Request $request, 
                                 SpecieRepository $SpecieRepository,
                                 TypeObservationRepository $TypeObservationRepository,
                                 ProjectRepository $ProjectRepository,
                                 SegmentRepository $SegmentRepository,
                                 UserRepository $UserRepository,
                                 SiteRepository $SiteRepository): Response
    {
      
        $request = $this->transformJsonBody($request);
        
        $date = $request->get('date');
        $time = $request->get('time');
        $dateok = strtotime($date." ".$time);
        $datetime = date('Y-m-d H:i:s', $dateok);
        $id_inaturalist = $request->get('id_inaturalist');
        $coordX = $request->get('coordX');
        $coordY = $request->get('coordY');
        $alpha = $request->get('alpha');
        $note = $request->get('note');
        $dead = $request->get('dead');
        $img1 = $request->get('img1');
        $img2 = $request->get('img2');
        $img3 = $request->get('img3');
        $img4 = $request->get('img4');
        $specie_id = $request->get('specie_id'); 
        $type_observation_id = $request->get('type_observation_id');
        $segment_id = $request->get('segment_id');
         $siteId = $request->get('siteId');

       // $user_id = $request->get('user_id');
        $project_id = $request->get('project_id');
        $specie = $SpecieRepository->findOneBy(['id' => $specie_id]);
        $typeObservation = $TypeObservationRepository->findOneBy(['id' => $type_observation_id]);
        $segment = $SegmentRepository->findOneBy(['id' => $segment_id]);
         $site = $SiteRepository->findOneBy(['id' => $siteId]);
        //$user = $UserRepository->findOneBy(['id' => $user_id]);
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       $username = $decodedJwtToken['username'];
        $user = $UserRepository->findOneBy(['email' => $username]);
        
        if($user->getFirstname() != NULL && $user->getLastname() != NULL){
            $collector = $user->getFirstname()." ".$user->getLastname();
        }
        
        if($user->getFirstname() == NULL && $user->getLastname() == NULL){
            $collector = $user->getEmail();
        }
        
        //var_dump($collector); die();

       /* if (empty($coordX) || empty($coordY)) {
            return $this->respondValidationError("All fields are required");
        }*/
          $ob = $this->repository->findOneBy([], ['id' => 'DESC']);
          $id = $ob->getId() + 1;
          
        $observation = new Observation(); 
        $observation->setId($id);
        //$observation->setDate($dateok);
        $observation->setCoordX($coordX);
        $observation->setCoordY($coordY);
        $observation->setIdInaturalist($id_inaturalist);
        $observation->setNote($note);
        $observation->setDead($dead);
        $observation->setAlpha($alpha);
        $observation->setCollector($collector);
        $observation->setUser($user);
        if($specie == NULL){
            $spe = $SpecieRepository->findOneBy(['id' => 9]);
        }else{
               $observation->setSpecie($specie);
        }
     
        $observation->setTypeObservation($typeObservation);
        $observation->setSegment($segment);
        $observation->setSite($site);
        $observation->setProject($project);
        $observation->setDate(new \DateTime($datetime));
        
        if ($img1)
        {
           
                            $photo = $img1;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img1/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img1Miniature/';
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                    $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                    $observation->setImg1($photoUrl);
                                   // $observation->setImg1($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        if ($img2)
        {
           
                            $photo = $img2;
                           
                                

                             try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img2/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img2Miniature/';
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                    $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                   
                                    $observation->setImg2($photoUrl);
                                    //$observation->setImg2($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $observation->setImg2('observation2.png');
        }
        if ($img3)
        {
           
                            $photo = $img3;
                           
                                

                             try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img3/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img3Miniature/';
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                    $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                   $observation->setImg3($photoUrl);
                                  //  $observation->setImg3($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $observation->setImg3('observation3.png');
        }
        if ($img4)
        {
           
                            $photo = $img4;
                           
                                

                             try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img4/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img4Miniature/';
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                    $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                   
                                    $observation->setImg4($photoUrl);
                                    //$observation->setImg4($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $observation->setImg4('observation4.png');
        }
        
      //  var_dump($observation->getImg4()); die();
        $this->em->persist($observation);
        $this->em->flush();

        $query = "api/observation/create";
        $method = "POST";
        $param = [
                 'id_inaturalist' => $id_inaturalist,
                 'coordx' => $coordX,
                 'coordY' => $coordX,
                 'note' => $note,
                 'dead' => $dead,
                 'alpha' => $alpha,
                 'collector' => $collector,
                 'img1' => $img1,
                 'img2' => $img2,
                 'img3' => $img3,
                 'img4' => $img4,
                 'typeobservation_id' => $type_observation_id,
                 'specie_id' => $specie_id,
                 'project_id ' => $project_id,
                 'segment_id ' => $segment_id,
                  ];
        $data = ['id' => $observation->getId(),
                 'date' => $observation->getDate(),
                'name_inaturalist' => $observation->getIdInaturalist(),
                'coorx' => $observation->getCoordX(),
                'coory' => $observation->getCoordY(),
                'note' => $observation->getNote(),
                'dead' => $observation->getDead(),
                'alpha' => $observation->getalpha(),
                'collector' => $observation->getCollector(),
                'collector' => $observation->getCollector(),
                'img1' => $observation->getImg1(),
                'img2' => $observation->getImg2(),
                'img3' => $observation->getImg3(),
                'img4' => $observation->getImg4(),
                'typeObservation' => $observation->getTypeObservation(),
                'specie' => $observation->getSpecie(),
                'user' => $observation->getUser(),
                'project' => $observation->getProject(),
                'segment' => $observation->getSegment(),
                        
                  ];          
        return $this->respondWithSuccess(sprintf('The observation has been successfully created'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }
       /**
     * transfert.
     *
     * @Route("api/observation/transfert", name="observation_transfert", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="id_inaturalist",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="coordX",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="coordY",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="note",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="alpha",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="collector",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="img1",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="img2",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="im32",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="img4",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="specie_id",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="user_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="type_observation_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="segment_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="observation")
     */
    public function migrateAction(Request $request, 
                                 SpecieRepository $SpecieRepository,
                                 TypeObservationRepository $TypeObservationRepository,
                                 ProjectRepository $ProjectRepository,
                                 SegmentRepository $SegmentRepository,
                                 UserRepository $UserRepository): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $id = $request->get('id');
        $observation = $this->repository->findOneBy(['id' => $id]);
       /* if($observation){
                        return $this->respondValidationError("the observation already exists");
        }*/
        $date = $request->get('date');
        $id_inaturalist = $request->get('id_inaturalist');
        $coordX = $request->get('coordX');
        
      //  var_dump($coordX); die();
        $coordY = $request->get('coordY');
        $etat = $request->get('etat');
        $alpha = $request->get('alpha');
        $note = $request->get('note');
        $dead = $request->get('dead');
        $collector = $request->get('collector');
        $img1 = $request->get('img1');
        $img2 = $request->get('img2');
        $img3 = $request->get('img3');
        $img4 = $request->get('img4');
       /* echo "<br>";
        var_dump($img1); echo "<br>";  die();*/
        $specie_id = $request->get('specie_id');
        $specie = $SpecieRepository->findOneBy(['id' => $specie_id]);
        
        $user_id = $request->get('user_id');
         $user = $UserRepository->findOneBy(['id' => $user_id]);
         
        $type_observation_id = $request->get('typeObservation_id');
      /*  if($type_observation_id == 19 || $type_observation_id == 18){
            $type_observation_id = 1;
        }*/
        $typeObservation = $TypeObservationRepository->findOneBy(['id' => $type_observation_id]);
        
        $segment_id = $request->get('segment_id');
        $segment = $SegmentRepository->findOneBy(['id' => $segment_id]);
        
        $project_id = 62156;
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);
    
        

        $observation = new Observation();
        $observation->setId($id);
        $observation->setCoordX($coordX);
        $observation->setCoordY($coordY);
        $observation->setDead($dead);
        $observation->setStatus($etat); 
        $observation->setIdInaturalist($id_inaturalist);
        $observation->setNote($note);
        $observation->setAlpha($alpha);
        $observation->setCollector($collector);
        $observation->setUser($user);
        $observation->setSpecie($specie);
        $observation->setTypeObservation($typeObservation);
        $observation->setSegment($segment);
        $observation->setProject($project);
        $observation->setDate(new \DateTime($date));
        $observation->setImg1('observation1.png');
        $observation->setImg2('observation2.png');
        $observation->setImg3('observation3.png');
        $observation->setImg4('observation4.png');
       /* if ($img1)
        {
           
                            $photo = $img1;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img1/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img1Miniature/';
                                   // var_dump($webPath); die();
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                    //$thumbs = $this->imageOptimizer->resize($sourcename_thumb);
                                    $observation->setImg1($photoUrl);
                                     //$observation->setImageTumbnail($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        if ($img2)
        {
           
                            $photo = $img2;
                           
                                

                             try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img2/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img2Miniature/';
                                   // var_dump($webPath); die();
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 read
                                    //$thumbs = $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                   
                                    $observation->setImg2($photoUrl);
                                     //$observation->setImageTumbnail($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $observation->setImg2('observation2.png');
        }
        if ($img3)
        {
           
                            $photo = $img3;
                           
                                

                             try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img3/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img3Miniature/';
                                   // var_dump($webPath); die();
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                    $thumbs = $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                   
                                    $observation->setImg3($photoUrl);
                                     //$observation->setImageTumbnail($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $observation->setImg3('observation3.png');
        }
        if ($img4)
        {
           
                            $photo = $img4;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img4/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img4Miniature/';
                                   // var_dump($webPath); die();
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                    //$thumbs = $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                   
                                    $observation->setImg4($photoUrl);
                                     //$observation->setImageTumbnail($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $observation->setImg4('observation4.png');
        }*/


      

        $this->em->persist($observation);
        $this->em->flush();

        $query = "api/observation/create";
        $method = "POST";
        $param = [
                 'id_inaturalist' => $id_inaturalist,
                 'coordx' => $coordX,
                 'coordY' => $coordX,
                 'note' => $note,
                 'alpha' => $alpha,
                 'collector' => $collector,
                 'img1' => $img1,
                 'img2' => $img2,
                 'img3' => $img3,
                 'img4' => $img4,
                 'typeobservation_id' => $type_observation_id,
                 'specie_id' => $specie_id,
                 'user_id ' => $user_id,
                 'project_id ' => $project_id,
                 'segment_id ' => $segment_id,
                  ];
        $data = ['id' => $observation->getId(),
                 'date' => $observation->getDate(),
                'name_inaturalist' => $observation->getIdInaturalist(),
                'coorx' => $observation->getCoordX(),
                'coory' => $observation->getCoordY(),
                'note' => $observation->getNote(),
                'alpha' => $observation->getalpha(),
                'collector' => $observation->getCollector(),
                'collector' => $observation->getCollector(),
                'img1' => $observation->getImg1(),
                'img2' => $observation->getImg2(),
                'img3' => $observation->getImg3(),
                'typeObservation' => $observation->getTypeObservation(),
                'specie' => $observation->getSpecie(),
                'user' => $observation->getUser(),
                'project' => $observation->getProject(),
                'segment' => $observation->getSegment(),
                        
                  ];          
        return $this->respondWithSuccess(sprintf('The observation has been successfully created'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     
            /**
     * Read.
     *
     * @Route("/api/observation/readOne  ", name="observation_read", methods={"GET"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation's information",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="observation")
     */
    public function readAction(Request $request): Response 
    {

       
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $observation = $this->repository->findOneBy(['id' => $id]);
            if(!$observation){
                return $this->respondValidationError("This observation does not exist");
            }
            
            $site = "";
            $siteId = 0;
            $segment = "";
            $segmentId = 0;
            $img1 = "";
            $img2 = "";
            $img3 = "";
            $img4 = "";
            $images = array();
            
                           if($observation->getSegment()){
                            $segmentId= $observation->getSegment()->getId();
                            $segment = $observation->getSegment()->getName();
                           }else{
                               
                             $segment  =  "";  
                           }
                            if($observation->getSite()){
                                 $siteId = $observation->getSite()->getId();
                                $site = $observation->getSite()->getName();
                            
                           }else{
                            $site =  "";
                             }
                             
                            if($observation->getImg1() == "observation1.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$observation->getImg1();
                            }
                            if($observation->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$observation->getImg2();
                            }
                            if($observation->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$observation->getImg3();   
                            }
                            if($observation->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$observation->getImg4();
                            }

        $query = "api/observation/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $observation->getId(),
                'TypeObservation' => $observation->getTypeObservation()->getName(),
                'TypeObservationId' => $observation->getTypeObservation()->getId(),
                'alpha' => $observation->getAlpha(),
                'collector' => $observation->getCollector(),
                'coordX' => $observation->getCoordX(),
                'coordY' => $observation->getCoordY(),
                 'date' => $observation->getDate(),
                 'dead' => $observation->getDead(),
                'family' => $observation->getSpecie()->getSubgroup()->getName(),
                'familyId' => $observation->getSpecie()->getSubgroup()->getId(),
                'group' => $observation->getSpecie()->getSubgroup()->getGroupe()->getName(),
                'groupId' => $observation->getSpecie()->getSubgroup()->getGroupe()->getId(),
                'idInaturalist' => $observation->getIdInaturalist(),
                'note' => $observation->getNote(),
                'images' => $images,
                'specie' => $observation->getSpecie()->getName(),
                'specieId' => $observation->getSpecie()->getId(),
                'user' => $observation->getUser()->getEmail(),
                'projectName' => $observation->getProject()->getName(),
                'projectId' => $observation->getProject()->getId(),
                'site' => $site,
                'siteId' => $siteId,
                'segment' => $segment,
                'segmentId' => $segmentId,
                'status' => $observation->getStatus(),
                 'updatedate' =>  $observation->getUpdateDate()
                
              //  'segment' => $observation->getSegment(),
                        
                  ];  
        return $this->respondWithSuccess(sprintf('observation information '), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }
           
     /**
     * @Route("/api/observation/list  ", name="observation_list", methods={"GET"})
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/specie/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $observations= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $observationsNormalizer = $normalizer->normalize($observations, null, ['groups' => 'observation:read']);
       
           return $this->respondWithSuccess(sprintf('List of observations'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $observationsNormalizer);
        }

        
        $observations = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $observationsNormalizer = $normalizer->normalize($observations, null, ['groups' => 'observation:read']);
       
        return $this->respondWithSuccess(sprintf('List of observations'), 
       $query, 
       $method,
      $param,
      $observationsNormalizer);
    }

     
         /**
     * Delete .
     *
     * @Route("/api/observation/delete", name="observation_delete", methods={"DELETE"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="observation")
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $observation = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($observation);
            $this->em->flush();
            
            $query = "api/observation/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Observation successfully delete'),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }
        
        
             /**
     * Delete image .
     *
     * @Route("/api/observationImage/delete", name="observationImage_delete", methods={"POST"})
     * @OA\Response(
     *     response=200,
     *     description="Returns null",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * 
     * @OA\Parameter(
     *     name="lien",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="observation")
     */
    public function deleteImageAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
          $observation = $this->repository->findOneBy(['id' => $id]);
            $lien =$request->get('lien');

           $img = substr($lien, 55, 4);
           // var_dump($img);die();
          
          
            if($img == "img1"){
                
                $image = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img1/'.$observation->getImg1() ;
                if(file_exists($image)){
                    unlink($image);
            
                }
                $observation->setImg1('observation1.png');
            } 
            
          
              if($img == "img2"){
                   $image = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img1/'.$observation->getImg2() ;
                if(file_exists($image)){
                    unlink($image);
            
                }
                $observation->setImg2('observation2.png');
            }
              if($img == "img3"){
                   $image = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img3/'.$observation->getImg3() ;
                if(file_exists($image)){
                    unlink($image);
            
                }
                $observation->setImg3('observation3.png');
            }
              if($img == "img4"){
                  
                   $image = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img4/'.$observation->getImg4() ;
                if(file_exists($image)){
                    unlink($image);
            
                }
                $observation->setImg4('observation4.png');
            }
            $this->em->persist($observation);
            $this->em->flush();
            
            $query = "api/observationImage/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Image successfully delete'),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }
        

      /**
     * array observation delete.
     *
     * @Route("/api/ArrayObservation/delete", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * )
     *  @OA\Parameter(
     *     name="observations",
     *     in="query",
     *     @OA\Schema(type="array()")
     * )
     * 
     * @OA\Tag(name="list view")
     */
    public function deleteArrayAction(Request $request, 
                                      ProjectRepository $ProjectRepository): Response 
    {
        
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
     
            $request = $this->transformJsonBody($request);
           $observations = $request->get('observations');
         // $observations =[743,744];
          $i = 0;
          // var_dump($observations);dis();
            foreach($observations as $obs){
                //var_dump($obs); die();
                $observation = $this->repository->findOneBy(['id' => $obs]);
                  $this->em->remove($observation);
                     $this->em->flush();
                     $i++;
            }
            
          
            
            $query = "api/Arrayobservation/delete";
            $method = "DELETE";
            $param = ['ArrayObservation' => $observations]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Observations successfully delete'),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }
        
    /**
     * array observation validated.
     *
     * @Route("/api/ArrayObservation/validated", methods={"PUT"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * )
     *  @OA\Parameter(
     *     name="observations",
     *     in="query",
     *     @OA\Schema(type="array()")
     * )
     * 
     * @OA\Tag(name="list view")
     */
    public function UpdateArrayAction(Request $request, 
                                      ProjectRepository $ProjectRepository): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
     
            $request = $this->transformJsonBody($request);
         $observations =  $request->get('observations');
                   //$observations = [741,742,743,744,745];
          $i = 0;
           //var_dump($observations);die();
            foreach($observations as $obs){
               // var_dump($obs); die();
                $observation = $this->repository->findOneBy(['id' => $obs]);
                // var_dump($observation->getId()); die();
                $observation->setStatus(1);
                  $this->em->persist($observation);
                     $this->em->flush();
                     $i++;
            }
            
          
            
            $query = "api/Arrayobservation/validated";
            $method = "UPDATE";
            $param = ['ArrayObservation' => $observations]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Observations successfully update'),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }
        
             /**
     *  Validate observation.
     *
     * @Route("/api/OneObservation/validate", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation's infos",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * )
     *  @OA\Parameter(
     *     name="observation_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="list view")
     */
    public function UpdatestatusAction(Request $request, 
                                      ProjectRepository $ProjectRepository): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
     
            $request = $this->transformJsonBody($request);
               $id = $request->get('observation_id');
         
                $observation = $this->repository->findOneBy(['id' => $id]);
               // var_dump($observation); die();
                $observation->setStatus(1);
                  $this->em->persist($observation);
                     $this->em->flush();
                  
          
            
          
            
            $query = "api/Statusobservation/update";
            $method = "DELETE";
            $param = ['id' => $id]; 
                  $data = ['id' => $observation->getId(),
                'date' => $observation->getDate(),
               'name_inaturalist' => $observation->getIdInaturalist(),
               'coorx' => $observation->getCoordX(),
               'coory' => $observation->getCoordY(),
               'note' => $observation->getNote(),
               'status' => $observation->getStatus(),
               'alpha' => $observation->getalpha(),
               'collector' => $observation->getCollector(),
               'collector' => $observation->getCollector(),
               'img1' => $observation->getImg1(),
               'img2' => $observation->getImg2(),
               'img3' => $observation->getImg3(),
               'typeObservation' => $observation->getTypeObservation(),
               'specie' => $observation->getSpecie(),
               'user' => $observation->getUser(),
               'project' => $observation->getProject(),
               'segment' => $observation->getSegment(),
                       
                 ];         
            return $this->respondWithSuccess(sprintf('Observation successfully update'),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }
        
         /**
     * Validate observation.
     *
     * @Route("api/observation/validate", name="observation_validate", methods={"POSt"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation's information after validate",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"list view"}))
     *     )
     * )
     *  @OA\Parameter(
     *     name="status",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="list view")
     */ 
   /* public function ValidateAction(Request $request): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $username = $decodedJwtToken['username'];
        $user = $UserRepository->findOneBy(['email' => $username]);
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
        $statut = $request->get('status');
        
           $observation = $this->repository->findOneBy(['id' => $id]);
    
    
    
            if($statut)
            {
                $observation->setIdInaturaliste($statut);
                
            }
         

    
            $this->em->persist($observation);
            $this->em->flush();

            $query = "api/observation/update";
            $method = "PUT";
            $param = [
                'observation_id' => $observation_id,
                 ];
       $data = ['id' => $observation->getId(),
                'date' => $observation->getDate(),
               'name_inaturalist' => $observation->getIdInaturalist(),
               'coorx' => $observation->getCoordX(),
               'coory' => $observation->getCoordY(),
               'note' => $observation->getNote(),
               'alpha' => $observation->getalpha(),
               'collector' => $observation->getCollector(),
               'collector' => $observation->getCollector(),
               'img1' => $observation->getImg1(),
               'img2' => $observation->getImg2(),
               'img3' => $observation->getImg3(),
               'typeObservation' => $observation->getTypeObservation(),
               'specie' => $observation->getSpecie(),
               'user' => $observation->getUser(),
               'project' => $observation->getProject(),
               'segment' => $observation->getSegment(),
                       
                 ];         
        return $this->respondWithSuccess(sprintf('The observation has been successfully validate'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
*/
     
    
      /**
     * Update.
     *
     * @Route("api/observation/update", name="observation_update", methods={"POSt"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="id_inaturalist",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="coordX",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="coordY",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="note",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="dead",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="alpha",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="collector",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="img1",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="img2",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="specie_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="user_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="type_observation_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="segment_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="siteId",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     *  @OA\Parameter(
     *     name="status",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="date",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="time",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="observation")
     */ 
    public function UpdateAction(Request $request, GroupRepository $GroupRepository,
                                SubgroupRepository $SubgroupRepository,
                                SpecieRepository $SpecieRepository,
                                TypeObservationRepository $TypeObservationRepository,
                                SegmentRepository $SegmentRepository,
                                UserRepository $UserRepository,
                                 SiteRepository $SiteRepository): JsonResponse
    {
        

        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
              /*$observation = $this->repository->findOneBy(['id' => $id]);
        if($observation){
             return $this->respondValidationError("the observation already exists");
        }*/
        //  $date = $request->get('date');
       // $time = $request->get('time');
       // $datetime = $date." ".$time;
        $dateok = strtotime($request->get('date')." ".$request->get('time'));
        $datetime = date('Y-m-d H:i:s', $dateok);
            
         $id_inaturalist = $request->get('id_inaturalist');
        $coordX = $request->get('coordX');
        $coordY = $request->get('coordY');
        $alpha = $request->get('alpha');
        $note = $request->get('note');
        $dead = $request->get('dead');
        $statut = $request->get('status');
        
        $collector = $request->get('collector');
        $img1 = $request->get('img1');
       // echo $img1; echo "<br>"; 
       // die();
        $img2 = $request->get('img2');
        $img3 = $request->get('img3');
        $img4 = $request->get('img4');
        $specie_id = $request->get('specie_id');
        $segment_id = $request->get('segment_id');
       $siteId = $request->get('siteId');
      //  var_dump($siteId); die();
        $type_observation_id = $request->get('type_observation_id');
        $specie = $SpecieRepository->findOneBy(['id' => $specie_id]);
        $typeObservation = $TypeObservationRepository->findOneBy(['id' => $type_observation_id]);
        $segment = $SegmentRepository->findOneBy(['id' => $segment_id]);
        $site = $SiteRepository->findOneBy(['id' => $siteId]);
        $observation = $this->repository->findOneBy(['id' => $id]);
    
    
    
            if($id_inaturalist)
            {
                $observation->setIdInaturaliste($name);
                
            }
            
            if($datetime)
            {
                $observation->setDate(new \DateTime($datetime));
                
            }
              if($statut)
            {
                $observation->setStatus($statut);
                
            }

            if($coordX)
            {
                $observation->setCoordX($coordX);
                
            }
            if($coordY)
            {
                $observation->setCoordY($coordY);
                
            }
            if($note)
            {
                $observation->setNote($note);
                
            }
            
            if($dead)
            {
                $observation->setDead($dead);
                
            }
            if($alpha)
            {
                $observation->setAlpha($alpha);
                
            }
            if($collector)
            {
                $observation->setCollector($collector);
                
            }
            if($typeObservation)
            {
                $observation->setTypeObservation($typeObservation);
                
            }
            if($specie)
            {
                $observation->setSpecie($specie);
                
            }
           
            if($segment)
            {
                $observation->setSegment($segment);
                
            }
            
           if($site)
            {
                $observation->setSite($site);
                
            }
            if ($img1)
            {
               
                                $photo = $img1;
                               
                                    
      try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img1/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img1Miniature/';
                                   // var_dump($webPath); die();
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';
                                    /*echo $sourcename; echo "<br>"; 
                                    echo $sourcename_thumb; echo "<br><br>"; 
                                    die();*/

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                              
                                     $this->imageOptimizer->resize($sourcename_thumb);
                                     $observation->setImg1($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
            }else{
                $observation->setImg1("observation1.png");
                
            }
            if ($img2)
            {
               
                                $photo = $img2;
                               
                                    
    
                                 try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img2/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img2Miniature/';
                                   // var_dump($webPath); die();
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1]))); 
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                   $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                   
                                    $observation->setImg2($photoUrl);
                                     //$observation->setImageTumbnail($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
            }
            if ($img3)
            {
               
                                $photo = $img3;
                               
                                    
    
                              
                             try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img3/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img3Miniature/';
                                   // var_dump($webPath); die();
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                    $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                   
                                    $observation->setImg3($photoUrl);
                                     //$observation->setImageTumbnail($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
            }
            if ($img4)
            {
               
                                $photo = $img4;
                               
                                    
    
                             try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img4/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img4Miniature/';
                                   // var_dump($webPath); die();
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                    $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                   
                                    $observation->setImg4($photoUrl);
                                     //$observation->setImageTumbnail($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
            }

    
            $this->em->persist($observation);
            $this->em->flush();

            $query = "api/observation/update";
            $method = "PUT";
            $param = [
                'id_inaturalist' => $id_inaturalist,
                'coordx' => $coordX,
                'coordY' => $coordX,
                'note' => $note,
                'dead' => $dead,
                'alpha' => $alpha,
                'collector' => $collector,
                'img1' => $img1,
                'img2' => $img2,
                'img3' => $img3,
                'img4' => $img4,
                'typeobservation_id' => $type_observation_id,
                'specie_id' => $specie_id,
                'segment_id ' => $segment_id,
                 ];
       $data = ['id' => $observation->getId(),
                'date' => $observation->getDate(),
                 'updatedate' => $observation->getUpdateDate(),
               'name_inaturalist' => $observation->getIdInaturalist(),
               'coorx' => $observation->getCoordX(),
               'coory' => $observation->getCoordY(),
               'note' => $observation->getNote(),
               'dead' => $observation->getDead(),
               'alpha' => $observation->getalpha(),
               'collector' => $observation->getCollector(),
               'collector' => $observation->getCollector(),
               'img1' => $observation->getImg1(),
               'img2' => $observation->getImg2(),
               'img3' => $observation->getImg3(),
               'typeObservation' => $observation->getTypeObservation(),
               'specie' => $observation->getSpecie(),
               'user' => $observation->getUser(),
               'segment' => $observation->getSegment(),
                       
                 ];         
        return $this->respondWithSuccess(sprintf('The observation has been successfully update'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
       
          
           /**
     * Update1.
     *
     * @Route("api/observation/update1",  methods={"POSt"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="id_inaturalist",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="coordX",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="coordY",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="note",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="alpha",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="collector",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="img1",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="img2",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="specie_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="user_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="type_observation_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="segment_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="status",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="date",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="time",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="observation")
     */ 
    public function Update1Action(Request $request, 
                                UserRepository $UserRepository): Response
    {
        

        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
             $user_id = $request->get('user_id');
             
       
       /* $img1 = $request->get('img1');
        $img2 = $request->get('img2');
        $img3 = $request->get('img3');
       // var_dump($img3); die();
        $img4 = $request->get('img4');
        $results = $request->get('results');*/
        
           $observation = $this->repository->findOneBy(['id' => $id]);
         /* if($img1){
               $observation->setImg1($img1);
          }else{
               $observation->setImg1("observation1.png");
          }
          
           if($img2){
                 $observation->setImg2($img2);
          }else{
               $observation->setImg2("observation2.png");s
          }
          
           if($img3){
                 $observation->setImg3($img3);
          }else{
               $observation->setImg3("observation3.png");
          }
          
           if($img4){
               $observation->setImg4($img4);
          }else{
               $observation->setImg4("observation4.png"); 
          }*/
          
          $user = $UserRepository->findOneBy(['id' => $user_id]);
         if($user){
               $observation->setUser($user);
          }
            /*    $i = 0;
         foreach($results as  $res){ 
             $result = new Result();
             $question = $QuestionRepository->findOneBy(['id' => $res['questions_id']]);
             $result->setId($res['id']);
            $result->setContent($res['Contenu']);
             $result->setQuestion($question);
             $result->setObservation($observation);
             $i++;
             
             $this->em->persist($result);
         }*/
    
            $this->em->persist($observation);
            $this->em->flush();

            $query = "api/observation/update";
            $method = "PUT";
            
                         return new Response("Id  successfully update ");
        /*return $this->respondWithSuccess(sprintf('The observation has been successfully update'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);*/
       }
       
     
     
         /**
     * List observations on one project of a user by type observation.
     *
     * @Route("/api/ObservationByTypeObservation/list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the observation",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * ),
     *  @OA\Parameter(
     *     name="projectId",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="numPage",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="data",
     *     in="query",
     *     @OA\Schema(type="array()")
     * ),
     *  @OA\Parameter(
     *     name="itemsPerPage",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="list view")
     */
    public function ListeObserByTOAction(Request $request,NormalizerInterface $normalizer,
                                  ObservationRepository $ObservationRepository,
                                  ProjectRepository $ProjectRepository): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
     
            $request = $this->transformJsonBody($request);
       //  $observations =  $request->get('data');
                  
         
            
          
            
           
            $request = $this->transformJsonBody($request);
            $id = $request->get('projectId');
               $page = $request->get('numPage');
               $data = $request->get('data');
              
               $itemsPerPage = $request->get('itemsPerPage');
             $project = $ProjectRepository->findOneBy(['id' => $id]);
             
             
                  
                      $i = 0; $j = 0;
                      $obs = array();
                      $images = array();
                      
                      
                      
                      
                    
                      
              if(!$page)
        {
                       
            foreach($data as $da){
    $observations= $ObservationRepository->findBy(['project' => $project, 'typeObservation'=> $da, 'status' => 1],array('id' => 'DESC',),($itemsPerPage),($itemsPerPage-$itemsPerPage));
               
        
          $nbPage = count($observations)/$itemsPerPage;
         //  ceil($nbPage);
          
             $query = "api/user/project/observation/list";
            $method = "GET";
            $param = ['ID' => $id,
                    'nbPage'  => $nbPage,
                    'Page' =>  ceil($nbPage)]; 
    
       
                 foreach($observations as $ob){
                     
                             $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['projectId'] = $ob->getProject()->getId();
                            $obs[$i]['projectName'] = $ob->getProject()->getName();
                            $obs[$i]['idInaturalist'] = $ob->getIdInaturalist();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['collector'] = $ob->getCollector();
                           if($ob->getImg1() == "observation1.png" || $ob->getImg1() == "observation.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                            }
                           // $obs[$i]['image'] = $ob->getImg1();
                           $obs[$i]['images'] = $images ;
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservationId'] = $ob->getTypeObservation()->getId();
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "";
                            }
                            if($ob->getSpecie()){
                            $obs[$i]['specieId'] = $ob->getSpecie()->getId();
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                             if($ob->getSpecie()->getSubgroup() == NULL){
                                  $obs[$i]['family'] = "";
                           
                            }else{
                                $obs[$i]['familyId'] = $ob->getSpecie()->getSubgroup()->getId();
                                $obs[$i]['family'] = $ob->getSpecie()->getSubgroup()->getName();
                                
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                            $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            }
                            }else{
                                $obs[$i]['specie'] = "";
                            }
                            
                        
                            if($ob->getUser()){
                            $obs[$i]['user'] = $ob->getUser()->getEmail();
                            }else{
                                $obs[$i]['user'] = "";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                             $obs[$i]['updatedate'] = $ob->getUpdateDate();
                           if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                           
                             if($ob->getSite()){
                                 $obs[$i]['siteId'] = $ob->getSite()->getId();
                                 $obs[$i]['site'] = $ob->getSite()->getName();
                            
                           }else{
                             $obs[$i]['site'] =  "";
                             }
                           

                           $i++;
                            
                 }
                 $j++; 
            }   
     
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $obs);
            
        }
        
            foreach($data as $da){
                $observations= $ObservationRepository->findBy(['project' => $project, 'typeObservation'=> $da, 'status' => 1],array('id' => 'DESC',),($itemsPerPage),(($page *$itemsPerPage)-$itemsPerPage));
                
                
          $nbPage = count($observations)/$itemsPerPage;
          
             $query = "api/user/project/observation/list";
            $method = "GET";
            $param = ['ID' => $id,
                    'nbPage'  => $nbPage,
                    'Page' =>  ceil($nbPage)]; 
        
       

                 foreach($observations as $ob){
                     
                             $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['projectId'] = $ob->getProject()->getId();
                            $obs[$i]['projectName'] = $ob->getProject()->getName();
                            $obs[$i]['idInaturalist'] = $ob->getIdInaturalist();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['collector'] = $ob->getCollector();
                           // if($obs['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/'.$ob->getImg1();
                           //$images['img1'] =  'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                           if($ob->getImg1() == "observation1.png" || $ob->getImg1() == "observation.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                            }
                           // $obs[$i]['image'] = $ob->getImg1();
                           $obs[$i]['images'] = $images ;
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservationId'] = $ob->getTypeObservation()->getId();
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "";
                            }
                            if($ob->getSpecie()){
                            $obs[$i]['specieId'] = $ob->getSpecie()->getId();
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                             if($ob->getSpecie()->getSubgroup() == NULL){
                                  $obs[$i]['family'] = "";
                           
                            }else{
                                $obs[$i]['familyId'] = $ob->getSpecie()->getSubgroup()->getId();
                                $obs[$i]['family'] = $ob->getSpecie()->getSubgroup()->getName();
                                
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                            $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            }
                            }else{
                                $obs[$i]['specie'] = "";
                            }
                            
                        
                            if($ob->getUser()){
                            $obs[$i]['user'] = $ob->getUser()->getEmail();
                            }else{
                                $obs[$i]['user'] = "";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                             $obs[$i]['updatedate'] = $ob->getUpdateDate();
                           if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";  
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                            if($ob->getSite()){
                                 $obs[$i]['siteId'] = $ob->getSite()->getId();
                                 $obs[$i]['site'] = $ob->getSite()->getName();
                            
                           }else{
                             $obs[$i]['site'] =  "";
                             }
                        
                           $i++;  
                       }
                       $j++;
        
                 }
                  
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $obs);
     
           
        
    

        }
        
        
             /**
     * Export.
     *
     * @Route("/api/observation/export  ", methods={"GET"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the list of observation",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="start_date",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="end_date",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="include_image",
     *     in="query",
     *     @OA\Schema(type="boolean")
     * ),
     * @OA\Parameter(
     *     name="include_patrol",
     *     in="query",
     *     @OA\Schema(type="boolean")
     * )
     * 
     * @OA\Tag(name="observation")
     */
    public function exportAction(Request $request,NormalizerInterface $normalizer,
                                 ProjectRepository $ProjectRepository,
                                 PatrolRepository $PatrolRepository): Response 
    {

       
            $request = $this->transformJsonBody($request);
            $dateDebut = $request->get("start_date");
            $dateFin = $request->get("end_date");
            $heure = "23:59:59";
            $dateTime = $dateFin ." ". $heure; 
            $include_image = $request->get("include_image");
            $include_patrol = $request->get("include_patrol");
          // $end_date= date('Y-m-d h:i:s', $dateDebut);
           // var_dump($include_image); die();
              $project_id = $request->get("project_id");
            $project = $ProjectRepository->findOneBy(['id' => $project_id]);
            $observations = array();
            $patrols = array();
         /*   $dateDebut = ($dateDebut == "") ? NULL : $dateDebut;
            $dateFin = ($dateFin == "") ? NULL : $dateFin;

            if($dateDebut != NULL){
                $debut = $dateDebut;
                $dateDebut = new \DateTime();
                $dateDebut->setDate(substr($debut, strrpos($debut, "-")+1), substr($debut, strpos($debut, "-")+1, (strrpos($debut, "-")-strpos($debut, "-")-1)), substr($debut, 0, strpos($debut, "-")));
            }

            if($dateFin != NULL){
                $debut = $dateFin;
                $dateFin = new \DateTime();
                $dateFin->setDate(substr($debut, strrpos($debut, "-")+1), substr($debut, strpos($debut, "-")+1, (strrpos($debut, "-")-strpos($debut, "-")-1)), substr($debut, 0, strpos($debut, "-")));
            }
             
            if($project==NULL)
            {
                $projets = $ProjectRepository()->findAll();
                
                foreach ($projets as $prot) {
                    $obsver = $this->repository->getexport($prot->getId(), $dateDebut, $dateFin);
                    $observations = array_merge($observations, $obsver);
                }
                
            }
            else
            {
                
                $observations = $this->repository->getexport($project_id, $dateDebut, $dateFin);
                
            }
            
            if($dateDebut != null){
                
            }*/
            
            $observations = $this->repository->getexport($project, 1 , $dateDebut, $dateTime);
            $patrols = $PatrolRepository->getexport($project,$dateDebut, $dateTime);
            $pat = $normalizer->normalize($patrols, null, ['groups' => 'patrol:read']);
            
            
            $query = "api/user/project/observation/list";
            $method = "GET";
            $param = ['ID' => $project_id,
                      'nbObs' => count($observations),
                      'nbPat' => count($patrols)]; 
              
             
                  
                      $i = 0;
                      $j = 0;
                      $obs = array();
                      $images = array();
                      $tab = array();
                      $nbImage = 0;
           
                 foreach($observations as $ob){
                            
                             $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['projectId'] = $ob->getProject()->getId();
                            $obs[$i]['projectName'] = $ob->getProject()->getName();
                            $obs[$i]['idInaturalist'] = $ob->getIdInaturalist();
                           // $obs[$i]['email'] = $ob->getUser()->getEmail();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['collector'] = $ob->getCollector();
                           // if($obs['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/'.$ob->getImg1();
                           //$images['img1'] =  'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                           if($ob->getImg1() == "observation1.png" || $ob->getImg1() == "observation.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                               // $nbImage = $nbImage + 1;
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                               // $nbImage = $nbImage + 1;
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                              // $nbImage = $nbImage + 1;
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                                ///$nbImage = $nbImage + 1;
                            }
                           // $obs[$i]['image'] = $ob->getImg1();
                           if($include_image == "true"){
                               $obs[$i]['images'] = $images ;

                           }
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservationId'] = $ob->getTypeObservation()->getId();
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "";
                            }
                            if($ob->getSpecie()){
                            $obs[$i]['specieId'] = $ob->getSpecie()->getId();
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                            
                         $obs[$i]['familyId'] = $ob->getSpecie()->getSubgroup()->getId();
                                $obs[$i]['family'] = $ob->getSpecie()->getSubgroup()->getName();
                                
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                        $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            
                            }else{
                                $obs[$i]['specie'] = "";
                            }
                            
                        /*     if($ob->getSpecie()->getSubgroup()->getGroupe()){
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                            $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            }else{
                                $obs[$i]['group'] = "";
                            }*/
                            if($ob->getUser()){
                            $obs[$i]['user'] = $ob->getUser()->getEmail();
                            }else{
                                $obs[$i]['user'] = "";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                                                        $obs[$i]['updatedate'] = $ob->getUpdateDate();
                           if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                           
                            if($ob->getSite()){
                                 $obs[$i]['siteId'] = $ob->getSite()->getId();
                                 $obs[$i]['site'] = $ob->getSite()->getName();
                            
                           }else{
                             $obs[$i]['site'] =  "";
                             }
                           /*  if($ob->getPatrol()){
                                  if($include_patrol == "true"){
                               $obs[$i]['patrol'] =  $normalizer->normalize($ob->getPatrol(), null, ['groups' => 'patrol:read']);

                           }
                             }*/
                            
                           
                           $i++;
                 }
                 
                     foreach($patrols as $pat){   
            $tab[$j]['id'] = $pat->getId();
            $tab[$j]['projectName'] = $pat->getProject()->getName();
            $tab[$j]['observations'] = count($this->repository->findBy(['patrol' => $pat]));
            $tab[$j]['valObs'] = count($this->repository->findBy(['patrol' => $pat , 'status' => 1]));
            $tab[$j]['unValidObs'] = count($this->repository->findBy(['patrol' => $pat , 'status' => 0]));
            if($pat->getCollector()){
                $tab[$j]['author'] = $pat->getCollector()->getFirstName();
            }else{
                
            $tab[$j]['author'] = "";
            }
           $tab[$j]['date'] = $pat->getDateEnreg();
           $tab[$j]['startTime'] = $pat->getStartDate();
           $tab[$j]['endTime'] = $pat->getEndDate();
           $tab[$j]['gpsStartCoordX'] = $pat->getCoordXStart();
           $tab[$j]['gpsStartCoordY'] = $pat->getCoordYStart();
           $tab[$j]['gpsEndCoordX'] = $pat->getCoordXEnd();
           $tab[$j]['gpsEndCoordY'] = $pat->getCoordYEnd(); 
           $tab[$j]['totalPatrolTime'] = $pat->getTotalPatrolTime();
           $tab[$j]['totalDistance'] = $pat->getTotalDistance();
           $tab[$j]['note'] = $pat->getNote();
           
           $j++; 
        }
        
            $response['observations'] = $obs;
            $response['nbObs'] = count($obs);
            if($include_patrol == "true"){
    
                 $response['patrols'] = $tab;
                 
            }    
            
            return  new JsonResponse($response);
           
           /* return $this->respondWithSuccess(sprintf('observation information '), 
                                         $query, 
                                         $method,
                                        $param,
                                        $response);*/
    
      
    }
    
        /**
     * Export by page.
     *
     * @Route("/api/observation/exportByPage  ", methods={"GET"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the list of observation",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="start_date",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="end_date",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter( 
     *     name="include_image",
     *     in="query",
     *     @OA\Schema(type="boolean")
     * ),
     * @OA\Parameter(
     *     name="include_patrol",
     *     in="query",
     *     @OA\Schema(type="boolean")
     * ),
     * @OA\Parameter(
     *     name="page_num",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="per_page",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="observation")
     */
    public function exportByPageAction(Request $request,NormalizerInterface $normalizer,
                                 ProjectRepository $ProjectRepository,
                                 PatrolRepository $PatrolRepository): Response 
    {

       
            $request = $this->transformJsonBody($request);
            $dateDebut = $request->get("start_date");
            $dateFin = $request->get("end_date");
            $heure = "23:59:59";
            $dateTime = $dateFin ." ". $heure; 
            $include_image = $request->get("include_image");
            $include_patrol = $request->get("include_patrol");
            $page_num = $request->get("page_num");
            $per_page = $request->get("per_page");
          // $end_date= date('Y-m-d h:i:s', $dateDebut);
            //var_dump($page_num); die();
              $project_id = $request->get("project_id");
            $project = $ProjectRepository->findOneBy(['id' => $project_id]);
            $observations = array();
            $patrols = array();
         /*   $dateDebut = ($dateDebut == "") ? NULL : $dateDebut;
            $dateFin = ($dateFin == "") ? NULL : $dateFin;

            if($dateDebut != NULL){
                $debut = $dateDebut;
                $dateDebut = new \DateTime();
                $dateDebut->setDate(substr($debut, strrpos($debut, "-")+1), substr($debut, strpos($debut, "-")+1, (strrpos($debut, "-")-strpos($debut, "-")-1)), substr($debut, 0, strpos($debut, "-")));
            }

            if($dateFin != NULL){
                $debut = $dateFin;
                $dateFin = new \DateTime();
                $dateFin->setDate(substr($debut, strrpos($debut, "-")+1), substr($debut, strpos($debut, "-")+1, (strrpos($debut, "-")-strpos($debut, "-")-1)), substr($debut, 0, strpos($debut, "-")));
            }
             
            if($project==NULL)
            {
                $projets = $ProjectRepository()->findAll();
                
                foreach ($projets as $prot) {
                    $obsver = $this->repository->getexport($prot->getId(), $dateDebut, $dateFin);
                    $observations = array_merge($observations, $obsver);
                }
                
            }
            else
            {
                
                $observations = $this->repository->getexport($project_id, $dateDebut, $dateFin);
                
            }
            
            if($dateDebut != null){
                
            }*/
            
            $observations = $this->repository->getexportByPage($project, 1 , $dateDebut, $dateTime,($page_num*$per_page)-$per_page, $per_page);
            $patrols = $PatrolRepository->getexport($project,$dateDebut, $dateTime);
            $pat = $normalizer->normalize($patrols, null, ['groups' => 'patrol:read']);
            
            
            $query = "api/user/project/observation/list";
            $method = "GET";
            $param = ['ID' => $project_id,
                      'nbObs' => count($observations),
                      'nbPat' => count($patrols)]; 
              
             
                  
                      $i = 0;
                      $j = 0;
                      $obs = array();
                      $images = array();
                      $tab = array();
                      $nbImage = 0;
           
                 foreach($observations as $ob){
                            
                             $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['projectId'] = $ob->getProject()->getId();
                            $obs[$i]['projectName'] = $ob->getProject()->getName();
                            $obs[$i]['idInaturalist'] = $ob->getIdInaturalist();
                           // $obs[$i]['email'] = $ob->getUser()->getEmail();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['collector'] = $ob->getCollector();
                           // if($obs['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/'.$ob->getImg1();
                           //$images['img1'] =  'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                           if($ob->getImg1() == "observation1.png" || $ob->getImg1() == "observation.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                               // $nbImage = $nbImage + 1;
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                               // $nbImage = $nbImage + 1;
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                              // $nbImage = $nbImage + 1;
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                                ///$nbImage = $nbImage + 1;
                            }
                           // $obs[$i]['image'] = $ob->getImg1();
                           if($include_image == "true"){
                               $obs[$i]['images'] = $images ;

                           }
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservationId'] = $ob->getTypeObservation()->getId();
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "";
                            }
                            if($ob->getSpecie()){
                            $obs[$i]['specieId'] = $ob->getSpecie()->getId();
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                            
                         $obs[$i]['familyId'] = $ob->getSpecie()->getSubgroup()->getId();
                                $obs[$i]['family'] = $ob->getSpecie()->getSubgroup()->getName();
                                
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                        $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            
                            }else{
                                $obs[$i]['specie'] = "";
                            }
                            
                        /*     if($ob->getSpecie()->getSubgroup()->getGroupe()){
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                            $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            }else{
                                $obs[$i]['group'] = "";
                            }*/
                            if($ob->getUser()){
                            $obs[$i]['user'] = $ob->getUser()->getEmail();
                            }else{
                                $obs[$i]['user'] = "";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                                                        $obs[$i]['updatedate'] = $ob->getUpdateDate();
                           if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                           
                            if($ob->getSite()){
                                 $obs[$i]['siteId'] = $ob->getSite()->getId();
                                 $obs[$i]['site'] = $ob->getSite()->getName();
                            
                           }else{
                             $obs[$i]['site'] =  "";
                             }
                           /*  if($ob->getPatrol()){
                                  if($include_patrol == "true"){
                               $obs[$i]['patrol'] =  $normalizer->normalize($ob->getPatrol(), null, ['groups' => 'patrol:read']);

                           }
                             }*/
                            
                           
                           $i++;
                 }
                 
                     foreach($patrols as $pat){   
            $tab[$j]['id'] = $pat->getId();
            $tab[$j]['projectName'] = $pat->getProject()->getName();
            $tab[$j]['observations'] = count($this->repository->findBy(['patrol' => $pat]));
            $tab[$j]['valObs'] = count($this->repository->findBy(['patrol' => $pat , 'status' => 1]));
            $tab[$j]['unValidObs'] = count($this->repository->findBy(['patrol' => $pat , 'status' => 0]));
            if($pat->getCollector()){
                $tab[$j]['author'] = $pat->getCollector()->getFirstName();
            }else{
                
            $tab[$j]['author'] = "";
            }
           $tab[$j]['date'] = $pat->getDateEnreg();
           $tab[$j]['startTime'] = $pat->getStartDate();
           $tab[$j]['endTime'] = $pat->getEndDate();
           $tab[$j]['gpsStartCoordX'] = $pat->getCoordXStart();
           $tab[$j]['gpsStartCoordY'] = $pat->getCoordYStart();
           $tab[$j]['gpsEndCoordX'] = $pat->getCoordXEnd();
           $tab[$j]['gpsEndCoordY'] = $pat->getCoordYEnd(); 
           $tab[$j]['totalPatrolTime'] = $pat->getTotalPatrolTime();
           $tab[$j]['totalDistance'] = $pat->getTotalDistance();
           $tab[$j]['note'] = $pat->getNote();
           
           $j++; 
        }
        
            $response['observations'] = $obs;
            $response['nbObs'] = count($obs);
            if($include_patrol == "true"){
    
                 $response['patrols'] = $tab;
                 
            }    
            
            return  new JsonResponse($response);
           
           /* return $this->respondWithSuccess(sprintf('observation information '), 
                                         $query, 
                                         $method,
                                        $param,
                                        $response);*/
    
      
    }
    
    
            /**
     * Count data.
     *
     * @Route("/api/observation/countData  ", methods={"GET"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the list of observation",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="start_date",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="end_date",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="observation")
     */
    public function countDataAction(Request $request,NormalizerInterface $normalizer,
                                 ProjectRepository $ProjectRepository,
                                 PatrolRepository $PatrolRepository): Response 
    {

       
            $request = $this->transformJsonBody($request);
            $dateDebut = $request->get("start_date");
            $dateFin = $request->get("end_date");
            $heure = "23:59:59";
            $dateTime = $dateFin ." ". $heure; 
          // $end_date= date('Y-m-d h:i:s', $dateDebut);
           // var_dump($include_image); die();
              $project_id = $request->get("project_id");
            $project = $ProjectRepository->findOneBy(['id' => $project_id]);
            $observations = array();
            $patrols = array();
         
            
            $observations = $this->repository->getexport($project, 1 , $dateDebut, $dateTime);
            $patrols = $PatrolRepository->getexport($project,$dateDebut, $dateTime);
           // $pat = $normalizer->normalize($patrols, null, ['groups' => 'patrol:read']);
            
            
            $query = "api/user/project/observation/CounData";
            $method = "GET";
            $param = ['ID' => $project_id,
                      'nbObs' => count($observations),
                      'nbPat' => count($patrols)]; 
              
             
                  
                      $i = 0;
                      $j = 0;
                      $obs = array();
                      $images = array();
                      $nbImage = 0;
           
                 foreach($observations as $ob){
                            
                           if($ob->getImg1() == "observation1.png" || $ob->getImg1() == "observation.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                //$images['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                                $nbImage = $nbImage + 1;
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                               // $images['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                                $nbImage = $nbImage + 1;
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                              // $images['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                               $nbImage = $nbImage + 1;
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                //$images['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                                $nbImage = $nbImage + 1;
                            }
                            
                           
                           $i++;
                 }
         
        
            $response['totalObservations'] = count($observations);
            $response['totalPatrols'] = count($patrols);
            $response['totalImages'] = $nbImage;
    
                 
            
            
            return  new JsonResponse($response);
           
           /* return $this->respondWithSuccess(sprintf('observation information '), 
                                         $query, 
                                         $method,
                                        $param,
                                        $response);*/
    
      
    }
    
     public function geocode2($addressx,$addressy){
        $apiKey = "AIzaSyAynDbAhqx8Xj7aTyADvr6br02-R4Clkz0";

        try {
        $address = "None";
        $googleApiUrl = 'https://maps.googleapis.com/maps/api/geocode/json?';   
        $options = array("key"=>$apiKey,"latlng"=>$addressx.",".$addressy);
        $googleApiUrl .= http_build_query($options,'','&');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $googleApiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($ch);
        curl_close($ch);

        $obj = json_decode($json);
            if (count($obj->results) > 0) {
                $address = $obj->results[0]->formatted_address;
            }

        } catch (ErrorException $e) {
            $address = "error";
        }
        return  $address;
        
    }
    
    function build_data_files($boundary, $fields, $files){
    $data = '';
    $eol = "\r\n";

    $delimiter = '-------------' . $boundary;

    foreach ($fields as $name => $content) {
        $data .= "--" . $delimiter . $eol
            . 'Content-Disposition: form-data; name="' . $name . "\"".$eol.$eol
            . $content . $eol;
    }


    foreach ($files as $name => $content) {
        $data .= "--" . $delimiter . $eol
            . 'Content-Disposition: form-data; name="' . $name . '"; filename="' . $name . '"' . $eol
            //. 'Content-Type: image/png'.$eol
            . 'Content-Transfer-Encoding: binary'.$eol
            ;

        $data .= $eol;
        $data .= $content . $eol;
    }
    $data .= "--" . $delimiter . "--".$eol;


    return $data;
}
    
    
            /**
     * Upload iNaturalistId.
     *
     * @Route("/api/observation/iNaturalist  ", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the list of observation",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="observations",
     *     in="query",
     *     @OA\Schema(type="array()")
     * )
     * 
     * @OA\Tag(name="observation")
     */
    public function sendAction(Request $request,
                               ResultRepository $ResultRepository)
    {
         $request = $this->transformJsonBody($request);
         //$id = $request->get("id");
         $observations = $request->get("observations");
          
          $i = 0;
          $reponse = 0;
        //  foreach($observations as $ob){
              //echo $ob."  ";
           
          
         // die();
         
                $observation = $this->repository->findOneBy(['id'=> $observations ]);
                if ($observation->getIdInaturalist() == null) {
                    
                    
                
        
                $app_id = "ceb09035af7c21cd73bf157e73ad8fb03df24a3e6bc766c097d6dcf19cf82b20";
                $app_secret = "64d649a40dd464d2f1a65afa29c73b6e24b7b43afa56349152f7f6f6bc8bb647";
                $redirect_uri = 'http://siren.ammco.org/web/en/';
                $username = 'akamla@ammco.org';
                $password = 'LcU.@a7T&985]daU';
                
                $payload = [ 
                  "client_id" => $app_id,
                  "client_secret" => $app_secret,
                  "grant_type" => "password",
                  "username" => $username,
                  "password" => $password
                ];
                
                $url = "/oauth/token";
                
                $value = $this->CallAPI("POST", $url, $payload);
                $value = json_decode($value);
                $token = $value->access_token;
                
                // $token = $this->getToken();
                //var_dump($token); die();
                
                $em = $this->getDoctrine()->getManager();
                $repondres = $ResultRepository->findByObservation($observation);
                
                
                $description = "";
                if(!empty($observation->getNote()))
                {
                    $description .= "Description: ".$observation->getNote();
                }
                
               
                if(count($repondres) > 0)
                {
                    foreach ($repondres as $repondre) {
                        $rep=$repondre;
                        $description .= "~ ".$rep->getQuestion()->getTitle(). ": ".$rep->getContent();
                        
                    }
                }
                
               
                 // echo $description."<br> <br>";   die();
                 
                 $photo = "http://".$request->getHost().$this->container->get('router')->getContext()->getBaseUrl(). "/uploads/observations/img1/".$observation->getImg1();
                 
                //  var_dump($photo); die();

                            
                $payload = [
                    "observation[species_guess]" => $observation->getSpecie()->getNameInaturalist(),
                    "observation[observed_on_string]" => $observation->getDate()->format("Y/m/d"),
                    "observation[time_zone]" => $observation->getProject()->getTimeZone(),
                    "observation[description]" => "my description",
                    "observation[tag_list]" => $observation->getTypeObservation()->getName().",".$observation->getSpecie()->getSubGroup()->getName().",".$observation->getSpecie()->getSubGroup()->getGroupe()->getName(),
                    "observation[place_guess]" => $this->geocode2($observation->getCoordX(),$observation->getCoordY()),
                    "observation[latitude]"=> $observation->getCoordX(),
                    "observation[longitude]"=> $observation->getCoordY(),
                    "observation[map_scale]" => 16,
                    "observation[location_is_exact]" => true,
                    "observation[positional_accuracy]" => 10,
                    "observation[geoprivacy]" => "open",
                ];
                
                $url = "/observations.json";
                
                $obs = $this->CallAPIPOST("POST", $url, $token, $payload);
                
                
                $obs2 = json_decode($obs);
                // var_dump($obs2); die();
                $obsid = $obs2[0]->id;
                
                $observation->setIdInaturalist($obsid);
                
            $this->em->persist($observation); 
            $this->em->flush();
                
          //  for ($i = 0; $i < 2; $i++)
              if ($observation->getImg1() != "observation1.png")
                {
                  // $photo = $this->getParameter('kernel.project_dir').'/public/uploads/observations/img1/656c71f2c9a68.jpg';
                   //.$observation->getImg1();
                  $photo = "http://".$request->getHost().$this->container->get('router')->getContext()->getBaseUrl(). "/uploads/observations/img1/".$observation->getImg1();
                  // var_dump($photo); die();
                   
                   $fields = array("observation_photo[observation_id]"=> $obsid);
                
                 //var_dump($fields); die();
                    $files = array();
                    $files["file"] = file_get_contents($photo);
                    
                    
                    $boundary = uniqid();
                    $delimiter = '-------------' . $boundary;
                    
                    $post_data = $this->build_data_files($boundary, $fields, $files);
                    
            
                    
                    $url = "/observation_photos.json";
                    
                    $file = $this->CallAPIFILE("POST", $url, $token, $post_data, $delimiter);
                 
               }
                
            
            
                if ($observation->getImg2() != "observation2.png")
                {
                   $photo = "http://".$request->getHost().$this->container->get('router')->getContext()->getBaseUrl(). "/uploads/observations/img2/".$observation->getImg2();
                   
                   $fields = array("observation_photo[observation_id]"=> $obsid);
                
                
                    $files = array();
                    $files["file"] = file_get_contents($photo);
                    
                    
                    $boundary = uniqid();
                    $delimiter = '-------------' . $boundary;
                    
                    $post_data = $this->build_data_files($boundary, $fields, $files);
                    
            
                    
                    $url = "/observation_photos.json";
                    
                    $file = $this->CallAPIFILE("POST", $url, $token, $post_data, $delimiter);
                 
                }
                
                if ($observation->getImg3() != "observation3.png")
                {
                   $photo = "http://".$request->getHost().$this->container->get('router')->getContext()->getBaseUrl(). "/uploads/observations/img3/".$observation->getImg3();
                   
                   $fields = array("observation_photo[observation_id]"=> $obsid);
                
                
                    $files = array();
                    $files["file"] = file_get_contents($photo);
                    
                    
                    $boundary = uniqid();
                    $delimiter = '-------------' . $boundary;
                    
                    $post_data = $this->build_data_files($boundary, $fields, $files);
                    
            
                    
                    $url = "/observation_photos.json";
                    
                    $file = $this->CallAPIFILE("POST", $url, $token, $post_data, $delimiter);
                 
                }
                
               if ($observation->getImg4() != "observation4.png")
                {
                   $photo = "http://".$request->getHost().$this->container->get('router')->getContext()->getBaseUrl(). "/uploads/observations/img4/".$observation->getImg4();
                   
                   $fields = array("observation_photo[observation_id]"=> $obsid);
                
                
                    $files = array();
                    $files["file"] = file_get_contents($photo);
                    
                    
                    $boundary = uniqid();
                    $delimiter = '-------------' . $boundary;
                    
                    $post_data = $this->build_data_files($boundary, $fields, $files);
                    
            
                    
                    $url = "/observation_photos.json";
                    
                    $file = $this->CallAPIFILE("POST", $url, $token, $post_data, $delimiter);
                 
                }
                
                // $this->setInaturalistId($ob,$obsid);
                
                
            
           /* $return['idObs'] = $ob;
            $return['idInat'] = $obsid;

            return  new JsonResponse($return);*/
            
        $reponse = 1;
                
                }else{
                    $reponse = 0;
                }
          //i++;
         // }  
          
          
          if($reponse == 1){
              return    new JsonResponse("Ok");
          }else{
                return    new JsonResponse($reponse);
          }
                   
       // return $this->redirectToRoute('observations_show', array('id' => $observation->getId()));
        
    }
    
    public function setInaturalistId($obs,$inatId)
    {
        $observation = $this->repository->findOneBy(['id'=> $obs ]);
        $observation->setIdInaturalist($inatId);
        $this->em->persist($observation);
        $this->em->flush();
    }
    
    public function CallAPI($method, $url, $data = false)
    {
        $site = "https://www.inaturalist.org";
        
        $curl = curl_init();
    
        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
    
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $site.$url, http_build_query($data));
        }
    
        curl_setopt($curl, CURLOPT_URL, $site.$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
       // curl_setopt($curl, CURLOPT_TIMEOUT,1600000);
        
    
        $result = curl_exec($curl);
    
        curl_close($curl);
    
        return $result;
    }
    
    public function CallAPIPOST($method, $url, $token, $data = false)
    {
        $site = "https://www.inaturalist.org";
        
        $curl = curl_init();
    
        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
    
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $site.$url, http_build_query($data));
        }
        
    
        curl_setopt($curl, CURLOPT_URL, $site.$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($curl, CURLOPT_TIMEOUT,1600000);
        //Set your auth headers
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $token));
    
        $result = curl_exec($curl);
    
        curl_close($curl);
    
        return $result;
    }
    
    public function CallAPIFILE($method, $url, $token, $delimiter, $data = false)
    {
        $site = "https://www.inaturalist.org";
        
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => $site.$url,
          CURLOPT_RETURNTRANSFER => 1,
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 1600000,
          //CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => $method,
          CURLOPT_POST => 1,
          CURLOPT_POSTFIELDS => $data,
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $token",
            
            "Content-Type: multipart/form-data; boundary=" . $delimiter,
            "Content-Length: " . strlen($data)
            
        
          ),
        
        
        ));
        
        
        //
        $response = curl_exec($curl);

        return $response;
    }
    
   public function getToken()
    {
        $response = $this->sirenUser->request('POST', $this->apiUrl.'/oauth/token', [
            'body' => [
                "client_id" => $this->clientId,
                "client_secret" => $this->clientSecret,
                'grant_type' => 'client_credentials',
                // "grant_type" => "password",
                // "username" => $this->username,
                // "password" => $this->password
            ],
        ]);
        

        if ($response->getStatusCode() === 200) {
            $data = $response->toArray();
            if (isset($data['access_token'])) {
                return $data['access_token'];
            }
        }

        throw new \Exception('Unable to get token');
    }

    
    private function uploadPhoto(string $token, string $imagePath, int $observationId): array
    {
        // var_dump($this->getParameter('kernel.project_dir') . '/public/uploads/observations/img1/'.$imagePath);
        // die();
        
        $response = $this->sirenUser->request('POST', 'https://api.inaturalist.org/v1/observations', [
        // $response = $this->sirenUser->request('POST', $this->apiUrl.'/observations', [
            
            'headers' => [
                'Authorization' => "Bearer {$token}",
            ],
            
            'body' => [
                // 'local_photos[0]' => fopen($this->getParameter('kernel.project_dir') . '/public/uploads/observations/img1/'.$imagePath, 'r'),
                'observation_id' => $observationId,
            ],
        ]);

        return $response->toArray();
    }
    
       
        
}
