<?php

namespace App\Controller;

use App\Entity\Fonction;
use App\Repository\FonctionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;


class FonctionController extends ApiController
{

    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                FonctionRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }
    
     /**
     * @Route("api/fonction/create", name="fonction_create", methods={"POST"})
     */
    public function createAction(Request $request): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $title = $request->get('title');

        if (empty($title)) {
            return $this->respondValidationError("All fields are required");
        }

        $fonction = new Fonction();
        $fonction->setTitle($title);
        

        $this->em->persist($fonction);
        $this->em->flush();

        $query = "api/fonction/create";
        $method = "POST";
        $param = [
                 'title ' => $title,

                  ];
        $data = ['id' => $fonction->getId(),
                 'name ' => $fonction->getTitle(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The fonction named %s has been successfully created', 
                                         $fonction->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/fonction/read  ", name="fonction_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $fonction = $this->repository->findOneBy(['id' => $id]);

        $query = "api/fonction/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $fonction->getId(),
                'tiltle ' => $fonction->getTitle()
                        ];  
        return $this->respondWithSuccess(sprintf('infos of fonction %s', 
                                         $fonction->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }
    
     
    
    /**
     * Function list .
     *
     *
     * @Route("/api/noconnect/function/list  ", name="function_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns null",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Fonction::class, groups={"fonction"}))
     *     )
     * ),
     * @OA\Tag(name="function")
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
      

        $query = "api/function/list";
        $method = "GET";
        $param = ['NULL']; 

          $fonctions= $this->repository->findBy(array(),array('id' => 'DESC',));
           
            $fonctionsNormalizer = $normalizer->normalize($fonctions, null, ['groups' => 'fonction:read']);
       
           return $this->respondWithSuccess(sprintf('List of functions'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $fonctionsNormalizer);
      
       
       
    }
    
     /**
     * Function list .
     *
     *
     * @Route("/api/function1/list  ", name="function1_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns null",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Fonction::class, groups={"fonction"}))
     *     )
     * ),
     * @OA\Tag(name="function")
     */
    public function listFunctionAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
      

        $query = "api/country/list";
        $method = "GET";
        $param = ['NULL']; 

          $fonctions= $this->repository->findBy(array(),array('id' => 'DESC',));
           $foncs = array();
           $i = 0;
            $fonctionsNormalizer = $normalizer->normalize($fonctions, null, ['groups' => 'fonction:read']);
            foreach($fonctionsNormalizer as $fonc){
                $foncs[$i]['value'] = $fonc['id'];
                 $foncs[$i]['label'] = $fonc['title'];
                 $i++;
            } 
            
       
           return $this->respondWithSuccess(sprintf('List of functions'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $foncs);
      
       
       
    }


     /**
     * @Route("/api/fonction/listPage  ", name="fonction_listPage", methods={"GET"})
     */
    public function listPageAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/fonction/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $fonctions= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           

       return $this->json($fonctions, 200, ['groups' => 'fonction:read']);
      
       /*return $this->respondWithSuccess(sprintf('List of fonctions'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $fonctions);*/
        }

        
        $groups = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));


       return $this->json("list ofgroups", $groups, 201, [], ['groups' => 'group:read']);
      /* return $this->respondWithSuccess(sprintf('List of fonctions'), 
       $query, 
       $method,
      $param,
      $groups);*/
    }

/**
     * @Route("/api/fonction/delete", name="fonction_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $fonction = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($fonction);
            $this->em->flush();
            
            $query = "api/fonction/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Fonction %s successfully delete', 
                                                      $fonction->getTitle()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/fonction/update", name="fonction_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $title = $request->get('title');
    
           $fonction = $this->repository->findOneBy(['id' => $id]);
    
    
    
            if($title)
            {
                $fonction->setTitle($title);
                
            }
    
            $this->em->persist($fonction);
            $this->em->flush();

            $query = "api/fonction/update";
            $method = "PUT";
            $param = [
                 'title ' => $title,

                  ];
        $data = ['id' => $fonction->getId(),
                 'name ' => $fonction->getTitle()
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The fonction named %s has been successfully update', 
                                         $fonction->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
}
