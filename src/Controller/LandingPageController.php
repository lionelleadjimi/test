<?php

namespace App\Controller;

use App\Entity\Observation;
use App\Entity\Project;
use App\Repository\ObservationRepository;
use App\Repository\TypeObservationRepository;
use App\Repository\SpecieRepository;
use App\Repository\CountryRepository;
use App\Repository\GroupRepository;      
use App\Repository\SubgroupRepository;
use App\Repository\ProjectRepository;
use App\Repository\PatrolRepository;
use App\Repository\SegmentRepository;
use App\Repository\ResultRepository;
use App\Repository\UserRepository;
use App\Repository\SiteRepository;
use App\Repository\UserProjectRepository;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use App\ImageOptimizer;

class LandingPageController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                ObservationRepository $repository,
                                ImageOptimizer $imageOptimizer)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
          $this->imageOptimizer = $imageOptimizer;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }


            /**
     * Read.
     *
 * @Route("/api/landingPge", methods={"GET"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation's information",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"observation"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="observation")
     */
    public function readAction(Request $request,
                                SpecieRepository $SpecieRepository,
                                CountryRepository $CountryRepository,
                                ProjectRepository $ProjectRepository): Response 
    {
        
        $query = $this->em->createQuery('SELECT DISTINCT IDENTITY(p.country) FROM App\Entity\Project p');
        $country = $query->getResult();
       // dd($services);

       
            
               // $observations 
                 $response['observations'] = count($this->repository->findAll());
              //  $projects
                 $response['projects'] = count($ProjectRepository->findAll());
              //  $species
                 $response['species'] = count($SpecieRepository->findAll());
              //  $countries 
                 $response['countries'] = count($country);
               
                
                return new JsonResponse($response);
     
           
        
}
}