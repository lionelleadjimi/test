<?php

namespace App\Controller;

use App\Entity\Group;
use App\Repository\GroupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class GroupController extends ApiController
{


    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                GroupRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

     /**
     * @Route("api/group/create", name="create", methods={"POST"})
     */
    public function createGroupeAction(Request $request): JsonResponse
    {
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $name = $request->get('name');
        $description = $request->get('description');
        $image = $request->get('image');
       $id = $request->get('id');
        //var_dump($name,$description,$image); die();

        if (empty($name) || empty($description)) {
            return $this->respondValidationError("All fields are required");
        }

        $group = new Group();
         $group->setId($id);
        $group->setName($name);
        $group->setDescription($description);

      /*  if ($image)
        {
           
                            $photo = $image;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/groups/';
                                   // dd($webPath);
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                   
                                    $group->setImage($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $group->setImage('group.png');
        }*/

            $group->setImage($image);
        $this->em->persist($group);
        $this->em->flush();

        $query = "api/group/create";
        $method = "POST";
        $param = [
                 'name ' => $name,
                 'description ' => $description,
                 'image ' => $image,

                  ];
        $data = ['id' => $group->getId(),
                 'name ' => $group->getName(),
                 'description ' => $group->getDescription(),
                 'image ' => $group->getImage(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The group named %s has been successfully created', 
                                         $group->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/group/read  ", name="group_read", methods={"GET"})
     */
    public function readGroupAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $group = $this->repository->findOneBy(['id' => $id]);

        $query = "api/group/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $group->getId(),
                'name ' => $group->getName(),
                'description ' => $group->getDescription(),
                'image ' => $group->getImage()
                        ];  
        return $this->respondWithSuccess(sprintf('infos of Group %s', 
                                         $group->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }
    
    
     
    /**
     * List  .
     *
     * @Route("/api/group/list  ", name="group_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns list of groups ",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Group::class, groups={"group"}))
     *     )
     * )
     * 
     * @OA\Tag(name="group")
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       

        $query = "api/group/list";
        $method = "GET";
        $param = ['NULL']; 

            $groups= $this->repository->findAll();
           
           $groupsNormalizer = $normalizer->normalize($groups, null, ['groups' => 'group:read']);
       // return $this->json($groups, 201, ['groups' => 'group:read']);
      
        return $this->respondWithSuccess(sprintf('List of Groups'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $groupsNormalizer
                                    );
        
    }


     /**
     * @Route("/api/groupPage/list  ", name="groupPage_list", methods={"GET"})
     */
    public function listGroupPageAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/group/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $groups= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
           $groupsNormalizer = $normalizer->normalize($groups, null, ['groups' => 'group:read']);
       // return $this->json($groups, 201, ['groups' => 'group:read']);
      
        return $this->respondWithSuccess(sprintf('List of Groups'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $groupsNormalizer
                                    );
        }

        
        $groups = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));

        $groupsNormalizer = $normalizer->normalize($groups, null, ['groups' => 'group:read']);
       return $this->respondWithSuccess(sprintf('List of Groups'), 
       $query, 
       $method,
      $param,
      $groupsNormalizer);
    }

/**
     * @Route("/api/group/delete", name="group_delete", methods={"DELETE"})
     */
    public function groupdeleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $group = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($group);
            $this->em->flush();
            
            $query = "api/group/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Group %s successfully delete', 
                                                      $group->getName()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/group/update", name="group_update", methods={"PUT"})
     */
    public function groupUpdateAction(Request $request): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $name = $request->get('name');
            $description = $request->get('description');
            $image = $request->get('image');
    
           $group = $this->repository->findOneBy(['id' => $id]);
    
    
            if($name)
            {
                $group->setName($name);
                
            }
    
            if($description)
            {
                $group->setDescription($description);
                
            }
    
            if($image)
            {
                $group->setImage($image);
                
            }
    
            $this->em->persist($group);
            $this->em->flush();

            $query = "api/group/update";
            $method = "Put";
            $param = [
                 'name ' => $name,
                 'description ' => $description,
                 'image ' => $image,

                  ];
        $data = ['id' => $group->getId(),
                 'name ' => $group->getName(),
                 'description ' => $group->getDescription(),
                 'image ' => $group->getImage()
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The group named %s has been successfully update', 
                                         $group->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }

}
