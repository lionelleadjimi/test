<?php

namespace App\Controller;

use App\Repository\ProjectRepository;
use App\Repository\SpecieRepository;
use App\Repository\UserRepository;
use App\Repository\ObservationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends ApiController
{

     /**
     * @Route("home/", name="accueil", methods={"GET"})
     */
    public function createGroupeAction(Request $request): JsonResponse
    {
        /*$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $username = $decodedJwtToken['username'];
        $user = $this->repository->findOneBy(['email' => $username]);*/
        echo "hello";
    }
}