<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Result;
use App\Entity\Group;
use App\Entity\Patrol;
use App\Entity\Subgroup;
use App\Entity\Project;
use App\Entity\UserProject;
use App\Entity\Site;
use App\Entity\Specie;
use App\Entity\Observation;
use App\Entity\Tokens;   
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\UserRepository;
use App\Repository\SpecieRepository;
use App\Repository\PatrolRepository;
use App\Repository\SubgroupRepository;
use App\Repository\QuestionRepository;
use App\Repository\ResultRepository;
use App\Repository\SegmentRepository;
use App\Repository\CountryRepository;
use App\Repository\FonctionRepository; 
use App\Repository\ProjectRepository;
use App\Repository\GroupRepository;
use App\Repository\SiteRepository;
use App\Repository\UserProjectRepository;
use App\Repository\ObservationRepository;
use App\Repository\TypeObservationRepository;
use App\Services\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTEncodedEvent;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use App\ImageOptimizer;

class MobileController extends ApiController
{
    private $em;

    public function __construct(EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer ,
                                UserRepository $repository,
                                 ImageOptimizer $imageOptimizer)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
          $this->imageOptimizer = $imageOptimizer;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

/**
     * Register: create account 
     *
     * @Route("/api/userMobile/create", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the user's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"mobile"}))
     *     )
     * ) 
     * @OA\Parameter(
     *     name="email",
     *     in="query",
     *     @OA\Schema(type="string")
     * ), 
     * @OA\Parameter(
     *     name="password",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="phone",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="codeTel",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="sigle",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="mobile")
     */
    public function registerUserAction(Request $request, UserPasswordEncoderInterface $encoder,MailerService $MailerService,
                                  CountryRepository $CountryRepository,
                                  FonctionRepository $FonctionRepository,
                                  ProjectRepository $ProjectRepository,
                                  UserProjectRepository $UserProjectRepository,
                                  NormalizerInterface $normalizer): JsonResponse
    {
        $request = $this->transformJsonBody($request);
        $email = $request->get('email');
        $password = $request->get('password');
        $code = random_int(10000, 99999);
        $phone = $request->get('phone');
        $sigle = $request->get('sigle');
        $codeTel = $request->get('codeTel');
        //dd($code);
        $expirationCode = new \Datetime('+2 days'); 
        
        //dd($code);
        $user1 = $this->repository->findOneBy(['email' => $email]);

        if (empty($email) || empty($password)) {
            return $this->respondValidationError("All fields are required");
        }
        if($user1)
        {
           // return $this->respondValidationError("This email address already exists ");
            return  $this->respondWithSuccess(sprintf('User exist'),
                                         "null", 
                                         "null",
                                        [],
                                        array());
        }
 
        
        $us = $this->repository->findOneBy([], ['id' => 'DESC']);
        $id = $us->getId() + 1; 
        $user = new User();
        
        $user->setId($id);
        $user->setPassword($encoder->encodePassword($user, $password));
        $user->setEmail($email);
        $user->setFirstName("null");
        $user->setLastName("null");
        $user->setOrganization("null");
        $user->setCity("null");
        $user->setPhone($phone); 
       $user->setCodeTel($codeTel);
       $user->setCode($code);
       $user->setExpirationCode($expirationCode);
       $user->setSigle($sigle);
       // $user->setExpirationToken($expirationToken);
        $user->setCountry($CountryRepository->findOneBy(['id' => 218]));
        $user->setFonction($FonctionRepository->findOneBy(['id' => 6]));
        $user->setProject($ProjectRepository->findOneBy(['id' => 62184]));
        $user->setMigrate(1);
        
       $this->em->persist($user); 
       
        $userProject = new UserProject();

        $userProject->setProject($ProjectRepository->findOneBy(['id' => 62184]));
        $userProject->setUser($user);
        $userProject->setRole('Regular user');

        $this->em->persist($userProject);
       
        $this->em->flush();
        
       
       $MailerService->send("Siren Account Verification.",
                            'noreply@sirenammco.org',
                            $user->getEmail(),
                            "emails/singupMobile.html.twig",
                            [ 'code' => $code]);

        $functions = $FonctionRepository->findAll();
        $tab = array(); $i = 0;
        foreach($functions as $function)
        {
            $tab[$i]['id'] = $function->getId();
            $tab[$i]['name'] = $function->getTitle();
            $i++;
        }
        $query = "api/userMobile/create";
        $method = "POST";
        $param = [
                 'email ' => $email

                  ];
        $data = ['id' => $user->getId(),
                  'fonctions' => $tab
                 
                  ];          
        return $this->respondWithSuccess(sprintf('User %s successfully created', 
                                         $user->getUsername()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    }
    
    
     /**
     * Update the user's informations
     *
     * @Route("/api/userMobile/update", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the user's information after modification",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"mobile"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="firstname",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="lastname",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="organization",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="city",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="fonctionId",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="countryCode",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="userId",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="mobile")
     */
    public function updateUserAction(Request $request, UserPasswordEncoderInterface $encoder,
                                  CountryRepository $CountryRepository,
                                  FonctionRepository $FonctionRepository,
                                  ProjectRepository $ProjectRepository,
                                  NormalizerInterface $normalizer): JsonResponse
    {
        $request = $this->transformJsonBody($request);
        
        $firstname = $request->get('firstname');
        $lastname = $request->get('lastname');
        $organization = $request->get('organization');
        $city = $request->get('city');
        $fonctionId = $request->get('fonctionId');
        $userId = $request->get('userId');
        $fonction = $FonctionRepository->findOneBy(['id' => $fonctionId]);
       // $username = $request->get('username');
       // $email1 = $request->get('email');
       // $codeTel = $request->get('codeTel');
        /*$phone = $request->get('phone');
        $codeTel = $request->get('codeTel');
        $sigle = $request->get('sigle');*/
        $countryCode = $request->get('countryCode');
        $country = $CountryRepository->findOneBy(['codeTel' => $countryCode]);
    

        /*$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $email = $decodedJwtToken['username'];*/
        $user = $this->repository->findOneBy(['id' => $userId]); 


        if($firstname)
        {
            $user->setFirstName($firstname);
            
        }
        if($lastname)
        {
            $user->setLastName($lastname);
            
        }
       
       if($organization)
        {
           
            $user->setOrganization($organization);
            
        } 

        if($city)
        {
            $user->setCity($city);
          
        }

        
          if($fonctionId)
        {
            $user->setFonction($fonction);
          
        }

        if($countryCode)
        {
            $user->setCountry($country);
        }


        $this->em->persist($user);
        $this->em->flush();

        $query = "api/user/update";
        $method = "POST";
        
        $tab = [];
        $i = 0;
        $projects = $ProjectRepository->findBy(['public' => 1]);
        foreach($projects as $project){
                $tab[$i]['id'] = $project->getId();
                $tab[$i]['name'] = $project->getName();
                $tab[$i]['note'] = $project->getNote();
                $tab[$i]['location'] = $project->getLocation();
                $tab[$i]['organization'] = $project->getOrganization();
                $tab[$i]['maplink'] = $project->getMaplink();
                $tab[$i]['public'] = $project->getPublic();
                $tab[$i]['country'] = $project->getCountry()->getName();
                $tab[$i]['admin'] = $project->getAdmin()->getId();
                $tab[$i]['legalnotices'] = $project->getLegalNotices();
                $tab[$i]['websitelink'] = $project->getWebsiteLink();
                $tab[$i]['gpsprecision'] = $project->getGpsPrecision();
                $tab[$i]['timezone'] = $project->getTimeZone();
                $tab[$i]['note'] = $project->getNote();
                $i++;
        }
    
        return  $this->respondWithSuccess(sprintf('User successfully update'),
                                         "null", 
                                         "null",
                                        [],
                                        $tab);                                
    }
    
    /**
     * update code  
     *
     * @Route("/api/userMobile/updateCode", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the user's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"mobile"}))
     *     )
     * ) 
     * @OA\Parameter(
     *     name="userId",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ), 
     * @OA\Parameter(
     *     name="code",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="mobile")
     */
    public function updateCodeAction(Request $request, UserPasswordEncoderInterface $encoder,MailerService $MailerService,
                                  CountryRepository $CountryRepository,
                                  FonctionRepository $FonctionRepository,
                                  ProjectRepository $ProjectRepository,
                                  UserProjectRepository $UserProjectRepository,
                                  NormalizerInterface $normalizer): JsonResponse
    {
        $request = $this->transformJsonBody($request);
        $userId = $request->get('userId');
        $code = random_int(10000, 99999);
        //dd($code);
        $expirationCode = new \Datetime('+2 days'); 
        
        //dd($code);
      

         $user = $this->repository->findOneBy(['id' => $userId]);
           if($code)
        {
            $user->setCode($code);
             
        
       $this->em->persist($user); 
       
        $this->em->flush();
        
        
       
       $MailerService->send("Siren Account Verification.",
                            'noreply@sirenammco.org',
                            $user->getEmail(),
                            "emails/singupMobile.html.twig",
                            [ 'code' => $code]);
                            
                             return  $this->respondWithSuccess(sprintf('Code updated'),
                                         "null", 
                                         "null",
                                        [],
                                        [true]);
        }else{
           
                             return  $this->respondWithSuccess(sprintf('Code not updated'),
                                         "null", 
                                         "null",
                                        [],
                                        [false]);   
        }
                                
                            

    }
    

    
        /**
     * Acount validate
     *
     *
     * @Route("/api/userMobile/validate",  methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the rewards of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"mobile"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="code",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="mobile")
     */
     public function ValidatedAction(Request $request): Response 
    {
        $request = $this->transformJsonBody($request);
          $code = $request->get('code');
         // var_dump($code); die();
        
        $user = $this->repository->findOneBy(['code' => $code]);
    

        if($user){
            if($user->getExpirationCode() > new \DateTime("now")){

                $user->setEnabled(true);
               // $user->setCode(0);
                 $user->setCode(0);
                $this->em->persist($user);
                $this->em->flush();
                    
                    
                     return  $this->respondWithSuccess(sprintf('User activated'),
                                         "null", 
                                         "null",
                                        [],
                                        [true]);
               
        
                /*$query = "api/usermobile/validate";
               $method = "POST";
               $param = ['code' => $code];
               $data = ['id' => $user->getId(),
                         ];  
               return $this->respondWithSuccess(sprintf('The user\'s account has been successfully activated', 
                                                $user->getUsername()), 
                                                $query, 
                                                $method,
                                               $param,
                                               $data);*/
            }else{
                  return  $this->respondWithSuccess(sprintf('User activated'),
                                         "null", 
                                         "null",
                                        [],
                                        [false]);
            }                                   
        }else{
             return  $this->respondWithSuccess(sprintf('User activated'),
                                         "null", 
                                         "null",
                                        [],
                                        [false]);
        }
    }
    
        /**
     * assign project user 
     *
     *
     * @Route("/api/userMobile/AssignPojetct",  methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the rewards of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"mobile"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="userId",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="projects",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="array()")
     * )
     * 
     * @OA\Tag(name="mobile")
     */
     public function AssignPojetctAction(Request $request,
                                          ProjectRepository $ProjectRepository,
                                          UserProjectRepository $UserProjectRepository,
                                          UserRepository $UserRepository): Response 
    {
        $request = $this->transformJsonBody($request);
          $userId = $request->get('userId');
          $projects = $request->get('projects');
         // var_dump($code); die();
        
        $user = $this->repository->findOneBy(['id' => $userId]);
        $defaultProject = $ProjectRepository->findOneBy(['id' => $projects[0]]);
       // var_dump($defaultProject->getName()); die();
       
        $user->setProject($defaultProject); 
        $this->em->persist($user);
        $this->em->flush();
        $status = 0;
        $i = 0;
        foreach($projects as $pro){
            $project = $ProjectRepository->findOneBy(['id' => $pro]);
            if(!($UserProjectRepository->findOneBy(['project' => $project , 'user' => $user]))){
                
            
            $userProject = new UserProject();
            $userProject->setUser($user);
            $userProject->setProject($project);
            $userProject->setRole("Regular user");
                $this->em->persist($userProject);
                $i++;
                $status = 1;
            }else{
                $status = 0;
            }
             $this->em->flush();
        }
        
        
               
                
                     return  $this->respondWithSuccess(sprintf(' Successfully assigned projects '),
                                         "null", 
                                         "null",
                                        [],
                                        [true, $status]);
        
    }


    /**
     * Login .
     *
     * @Route("/api/mobile/login", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token and user info user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"mobile"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="email",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="password",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="mobile")
     */

    public function loginMobile(Request $request, 
                                UserPasswordEncoderInterface $encoder,
                                ProjectRepository $ProjectRepository): Response
    {
        $request = $this->transformJsonBody($request);
        $email = $request->get('email');
        $user = $this->repository->findOneBy(['email' => $email]);
        $password = $request->get('password');
        if(!$password || $email){
              if (empty($password) || empty($email)) {
            return $this->respondValidationError("This field cant be null");
        }
        }
        $test = $encoder->isPasswordValid($user,  $password);
         // var_dump($password,$test); die();

    
        $creationDate = new \DateTime('now');
        $expirationDate = $creationDate->add(new \DateInterval('P1M'));
       
      // var_dump($pass);die();
        if (empty($password) || empty($email)) {
            return $this->respondValidationError("All fields are required");
        }
        
        if ($test == false) {
            return $this->respondValidationError("Incorrect password");
        }
        
       $tokengenere = $this->jwtManager->create($user);
      // $user->setToken($tokengenere);
      // $user->setCreationDate(new \DateTime('now'));
       //$user->setExpirationDate($expirationDate);
        $this->em->persist($user);
       $this->em->flush();

     
     if($user->getFirstname() == NULL )
     {
         $firstname1 = "";
         
     }else{
         $firstname1 = $user->getFirstname();
     }
      if($user->getLastname() == NULL )
     {
         $lastname1 = "";
         
     }else{
         $lastname1 = $user->getLastname();
     }
     
       if(($user->getOrganization() == NULL ) ||($user->getOrganization() == "NULL" ))
     {
         $organization1 = "";
         
     }else{
         $organization1 = $user->getOrganization();
     }
     
     if($user->getCodeTel() == NULL )
     {
         $codeTel1 = "";
         
     }else{
         $codeTel1 = $user->getCodeTel();
     }
     
       if($user->getSigle() == NULL )
     {
         $sigle1 = "US";
         
     }else{
         $sigle1 = $user->getSigle();
     }
      
               $tab = [];
        $i = 0;
        $projects = $ProjectRepository->findBy(['public' => 1]);
        foreach($projects as $project){
                $tab[$i]['id'] = $project->getId();
                $tab[$i]['name'] = $project->getName();
                $tab[$i]['note'] = $project->getNote();
                $tab[$i]['location'] = $project->getLocation();
                $tab[$i]['organization'] = $project->getOrganization();
                $tab[$i]['maplink'] = $project->getMaplink();
                $tab[$i]['public'] = $project->getPublic();
                $tab[$i]['country'] = $project->getCountry()->getName();
                $tab[$i]['admin'] = $project->getAdmin()->getId();
                $tab[$i]['legalnotices'] = $project->getLegalNotices();
                $tab[$i]['websitelink'] = $project->getWebsiteLink();
                $tab[$i]['gpsprecision'] = $project->getGpsPrecision();
                $tab[$i]['timezone'] = $project->getTimeZone();
                $tab[$i]['note'] = $project->getNote();
                $i++;
        }
        $query = "api/mobile/login";
        $method = "POST";
        $param = [
                 'email ' => $email

                  ];
        $data = ['id' => $user->getId(),
                 'email' => $user->getEmail(),
                 'roles' => $user->getRoles(),
                 'city' => $user->getCity(),
                 'phone' => $user->getPhone(),
                 'codeTel' => $user->getCodeTel(),
                  'sigle' => $sigle1,
                  'firstname' => $firstname1,
                  'lastname' => $lastname1,
                 'organization' => $organization1,
                  'fonctionId' => $user->getFonction()->getId(),
                  'fonctionTitle' => $user->getFonction()->getTitle(),
                  'country' => $user->getCountry()->getName(),
                  'countryid' => $user->getCountry()->getId(),
                  'countryCodetel' => $user->getCountry()->getCodeTel(),
                  'organization' => $organization1,
                  'codeTel' => $codeTel1,
                  'projectId' => $user->getProject()->getId(),
                  'projectName' => $user->getProject()->getName(),
                  'projectsList' => $tab,
                  'token' => $tokengenere,
                  ];  
                  
         
        
        
        return $this->respondWithSuccess(sprintf('User %s successfully login', 
                                         $user->getUsername()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       
    }
    
    /**
     * Login facebook.
     *
     * @Route("/api/mobile/login/facebook", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token and user info user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"mobile"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="email",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="phone",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="mobile")
     */

    public function login1Mobile(Request $request, 
                                 UserPasswordEncoderInterface $encoder,
                                 UserRepository $UserRepository,
                                 CountryRepository $CountryRepository,
                                 FonctionRepository $FonctionRepository,
                                 ProjectRepository $ProjectRepository): Response
    {
        $request = $this->transformJsonBody($request);
        $email = $request->get('email');
        $phone = $request->get('phone');
        $user1 = $this->repository->findOneBy(['email' => $email]);
        $user2 = $this->repository->findOneBy(['phone' => $phone]);
        $user3 = $this->repository->findOneBy(['email' => $email , 'phone' => $phone]);
        
         $exist = 0;
         
          /*if($user3){
            $exist = 1;
            return new jsonResponse($exist);
        }
        if($email && $phone && !$user3){
            
        $us = $UserRepository->findOneBy([], ['id' => 'DESC']);
        $id = $us->getId() + 1; 
        $user = new User();
        
        $user->setId($id);
        $user->setPassword("null");
        $user->setEmail($email);
        $user->setFirstName("null");
        $user->setLastName("null");
        $user->setOrganization("null");
        $user->setCity("null");
        $user->setPhone($phone); 
       $user->setCodeTel(0);
       $user->setSigle("null");
       // $user->setExpirationToken($expirationToken);
        $user->setCountry($CountryRepository->findOneBy(['id' => 36]));
        $user->setFonction($FonctionRepository->findOneBy(['id' => 9]));
        $user->setProject($ProjectRepository->findOneBy(['id' => 62184]));
        $user->setMigrate(1);
        
       $this->em->persist($user); 
       
        $userProject = new UserProject();

        $userProject->setProject($ProjectRepository->findOneBy(['id' => 62184]));
        $userProject->setUser($user);
        $userProject->setRole('Regular user');

        $this->em->persist($userProject);
       
        $this->em->flush();
        }
        */
         
        if($user1){
            $exist = 1;
            return new jsonResponse($exist);
        }
        if($email && !$user1){
            
        $us = $UserRepository->findOneBy([], ['id' => 'DESC']);
        $id = $us->getId() + 1; 
        $user = new User();
        
        $user->setId($id);
        $user->setPassword("null");
        $user->setEmail($email);
        $user->setFirstName("null");
        $user->setLastName("null");
        $user->setOrganization("null");
        $user->setCity("null");
        $user->setPhone("null"); 
       $user->setCodeTel(0);
       $user->setSigle("null");
       // $user->setExpirationToken($expirationToken);
        $user->setCountry($CountryRepository->findOneBy(['id' => 36]));
        $user->setFonction($FonctionRepository->findOneBy(['id' => 9]));
        $user->setProject($ProjectRepository->findOneBy(['id' => 62184]));
        $user->setMigrate(1);
        
       $this->em->persist($user); 
       
        $userProject = new UserProject();

        $userProject->setProject($ProjectRepository->findOneBy(['id' => 62184]));
        $userProject->setUser($user);
        $userProject->setRole('Regular user');

        $this->em->persist($userProject);
       
        $this->em->flush();
        }
        
        if($user2){
            $exist = 1;
            return new jsonResponse($exist);
        }
        if($phone && !$user2){
            
        $us = $UserRepository->findOneBy([], ['id' => 'DESC']);
        $id = $us->getId() + 1; 
        $user = new User();
        
        $user->setId($id);
        $user->setPassword("null");
        $user->setEmail("null");
        $user->setFirstName("null");
        $user->setLastName("null");
        $user->setOrganization("null");
        $user->setCity("null");
        $user->setPhone($phone); 
       $user->setCodeTel(0);
       $user->setSigle("null");
       // $user->setExpirationToken($expirationToken);
        $user->setCountry($CountryRepository->findOneBy(['id' => 36]));
        $user->setFonction($FonctionRepository->findOneBy(['id' => 9]));
        $user->setProject($ProjectRepository->findOneBy(['id' => 62184]));
        $user->setMigrate(1);
        
       $this->em->persist($user); 
       
        $userProject = new UserProject();

        $userProject->setProject($ProjectRepository->findOneBy(['id' => 62184]));
        $userProject->setUser($user);
        $userProject->setRole('Regular user');

        $this->em->persist($userProject);
       
        $this->em->flush();
        }
        
        
        

      
        $query = "api/user/login";
        $method = "POST";
        $param = [
                 'email ' => $email,
                  'phone ' => $phone,
                 

                  ];
        $data = ['exist' => $exist,
                  'email' =>  $user->getEmail(),
                  'phone' =>  $user->getPhone()
                  ];  
        return $this->respondWithSuccess(sprintf('User %s successfully login', 
                                         $user->getUsername()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       
    }
    
    
    
     /**
     * List  .
     *
     * @Route("/api/mobile/site  ", name="siteprojectId_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns list of sites ",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Site::class, groups={"mobile"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="mobile")
     */
    public function listsiteProjectAction(Request $request, NormalizerInterface $normalizer,
                                            ProjectRepository $ProjectRepository,
                                            SiteRepository $SiteRepository): Response 
    {

               $request = $this->transformJsonBody($request);
 
        $project_id = $request->get('project_id');
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);
        $query = "api/site/projectId/list";
        $method = "GET";
        $param = ['NULL']; 

            $sites= $SiteRepository->findBy(['project' => $project]);
           
            $sitesNormalizer = $normalizer->normalize($sites, null, ['groups' => 'site:read']);
       
           return $this->respondWithSuccess(sprintf('List of sites'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $sitesNormalizer);

    }
    
    /**
     * List of observations of one project of a user  .
     *
     * @Route("/api/mobile/observations", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"mobile"}))
     *     )
     * ),
     * @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="mobile")
     */
       public function ListObservationByProjectAction(Request $request,NormalizerInterface $normalizer,
                                  ObservationRepository $ObservationRepository,
                                  ProjectRepository $ProjectRepository): JsonResponse
    {
         
        
      
         
       
            $request = $this->transformJsonBody($request);
            $id = $request->get('project_id');
            //$page = $request->get('NumPage');
             $project = $ProjectRepository->findOneBy(['id' => $id]);
             
            $query = "api/user/project/observation/list";
            $method = "GET";
            $param = ['ID' => $id]; 
              
             
                  
                      $i = 0;
                      $obs = array();
                      $images = array();
                      
           
                 //$observations = $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC',),(20),(20-20));
                 $observations = $ObservationRepository->findBy(['project' => $project]);
                 foreach($observations as $ob){
                     
                           // $obs[$i]['email'] = $ob->getUser()->getEmail();
                             $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['projectId'] = $ob->getProject()->getId();
                            $obs[$i]['projectName'] = $ob->getProject()->getName();
                            $obs[$i]['idInaturalist'] = $ob->getIdInaturalist();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['collector'] = $ob->getCollector();
                           // if($obs['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/'.$ob->getImg1();
                           //$images['img1'] =  'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                           if($ob->getImg1() == "observation1.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1Miniature/'.$ob->getImg1();
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img2Miniature/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img3Miniature/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img4Miniature/'.$ob->getImg4();
                            }
                           // $obs[$i]['image'] = $ob->getImg1();
                           $obs[$i]['images'] = $images ;
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservationId'] = $ob->getTypeObservation()->getId();
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "";
                            }
                            if($ob->getSpecie()){
                            $obs[$i]['specieId'] = $ob->getSpecie()->getId();
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                            }else{
                                $obs[$i]['specie'] = "";
                            }
                             if($ob->getSpecie()->getSubgroup() == NULL){
                                  $obs[$i]['family'] = "";
                           
                            }else{
                                $obs[$i]['familyId'] = $ob->getSpecie()->getSubgroup()->getID();
                                $obs[$i]['family'] = $ob->getSpecie()->getSubgroup()->getName();
                            }
                             if($ob->getSpecie()->getSubgroup()->getGroupe()){
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                            $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            }else{
                                $obs[$i]['group'] = "";
                            }
                            if($ob->getUser()){
                            $obs[$i]['user'] = $ob->getUser()->getEmail();
                            }else{
                                $obs[$i]['user'] = "";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                           if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                            $obs[$i]['siteId'] = $ob->getSegment()->getSite()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";            
                              $obs[$i]['site'] =  "";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                            /* if($ob->getSegment()->getSite() ==  'Null'){
                                  $obs[$i]['site'] =  "NULL";
                            
                           }else{
                               $obs[$i]['siteId'] = $ob->getSegment()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getName();
                             }*/
                           
                          /* if($ob->getSegment()->getSite()){
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['site'] =  "NULL";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }*/
                           
                           $i++;
                 }
                  
                   $observationsNormalizer = $normalizer->normalize($observations, null, ['groups' => 'observation:read']);
      
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $obs);
     
           
        
    }
    
      /**
     * Create.
     *
     * @Route("api/mobile/observation/create",  methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation's information after creation",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"mobile"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="id_inaturalist",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="coordX",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="coordY",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="note",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="alpha",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="img1",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="img2",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="specie_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="user_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="type_observation_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="segment_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="date",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="mobile")
     */
    public function createAction(Request $request, 
                                 SpecieRepository $SpecieRepository,
                                 TypeObservationRepository $TypeObservationRepository,
                                 ProjectRepository $ProjectRepository,
                                 SegmentRepository $SegmentRepository,
                                 UserRepository $UserRepository,
                                 ObservationRepository $ObservationRepository,
                                 SiteRepository $SiteRepository): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
       
        $date = $request->get('date');
        $datetime = date('Y-m-d H:i:s', $date);
        
        //echo $datetime; die();
        $id_inaturalist = $request->get('id_inaturalist');
        $coordX = $request->get('coordX');
        $coordY = $request->get('coordY');
        $alpha = $request->get('alpha');
        $note = $request->get('note');
        $img1 = $request->get('img1');
        $img2 = $request->get('img2');
        $img3 = $request->get('img3');
        $img4 = $request->get('img4');
        $specie_id = $request->get('specie_id');
        $type_observation_id = $request->get('type_observation_id');
       // $segment_id = $request->get('segment_id');
       $site_id = $request->get('segment_id');
        $project_id = $request->get('project_id');
        $specie = $SpecieRepository->findOneBy(['id' => $specie_id]);
        $typeObservation = $TypeObservationRepository->findOneBy(['id' => $type_observation_id]);
       // $segment = $SegmentRepository->findOneBy(['id' => $segment_id]);
        $site = $SiteRepository->findOneBy(['id' => $site_id]);
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);
        // var_dump($segment_id); die();
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       $username = $decodedJwtToken['username'];
       
        // ancien ecrit par lionel
        // $user = $UserRepository->findOneBy(['email' => $username]);
        // ---
        //nouveau ecrit par nirou
        $observer_id = $request->get('user_id');
        if( $observer_id == NULL)
            $user = $UserRepository->findOneBy(['email' => $username]);
        else
            $user = $UserRepository->findOneBy(['id' => $observer_id]);
        
         if($user->getFirstname() != NULL && $user->getLastname() != NULL){
            $collector = $user->getFirstname()." ".$user->getLastname();
        }
        
        if($user->getFirstname() == NULL && $user->getLastname() == NULL){
            $collector = $user->getEmail();
        }
        
      //  var_dump($collector); die();

        if (empty($coordX) || empty($coordY)) {
            return $this->respondValidationError("All fields are required");
        }
          $ob = $ObservationRepository->findOneBy([], ['id' => 'DESC']);
          $id = $ob->getId() + 1;
          
        $observation = new Observation(); 
        $observation->setId($id);
        $observation->setCoordX($coordX);
        $observation->setCoordY($coordY);
        $observation->setIdInaturalist($id_inaturalist);
        $observation->setNote($note);
        $observation->setAlpha($alpha);
        $observation->setCollector($collector);
        $observation->setUser($user);
        $observation->setTypeObservation($typeObservation);
        if($type_observation_id != 6  AND $specie_id != 0){
            $observation->setSpecie($specie);
        }
        
        $observation->setSite($site);
        $observation->setProject($project);
        $observation->setDate(new \DateTime($datetime));
        
        if ($img1)
        {
           
                            $photo = $img1;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img1/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img1Miniature/';
                                 
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';
  // var_dump($sourcename); die();
                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                    $this->imageOptimizer->resize($sourcename_thumb);
                                  // var_dump($thumbs);die();
                                   
                                    $observation->setImg1($photoUrl);
                                    // $observation->setImageTumbnail($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        if ($img2)
        {
           
                            $photo = $img2;
                           
                                

                             try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img2/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img2Miniature/';
                                   // var_dump($webPath); die();
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                   
                                    $observation->setImg2($photoUrl);
                                     //$observation->setImageTumbnail($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $observation->setImg2('observation2.png');
        }
        if ($img3)
        {
           
                            $photo = $img3;
                           
                                

                             try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img3/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img3Miniature/';
                                   // var_dump($webPath); die();
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                   
                                    $observation->setImg3($photoUrl);
                                     //$observation->setImageTumbnail($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $observation->setImg3('observation3.png');
        }
        if ($img4)
        {
           
                            $photo = $img4;
                           
                                

                             try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img4/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img4Miniature/';
                                   // var_dump($webPath); die();
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                    $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                   
                                    $observation->setImg4($photoUrl);
                                     //$observation->setImageTumbnail($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $observation->setImg4('observation4.png');
        }
        

        $this->em->persist($observation);
        $this->em->flush();

        $query = "api/observation/create";
        $method = "POST";
        $param = [
                 'id_inaturalist' => $id_inaturalist,
                 'coordx' => $coordX,
                 'coordY' => $coordX,
                 'note' => $note,
                 'alpha' => $alpha,
                 'collector' => $collector,
                 'img1' => $img1,
                 'img2' => $img2,
                 'img3' => $img3,
                 'img4' => $img4,
                 'typeobservation_id' => $type_observation_id,
                 'specie_id' => $specie_id,
                 'project_id ' => $project_id,
                 'site_id ' => $site_id,
                  ];
                  $data = ['id' =>  $observation->getId()];

                return $this->respondWithSuccess(sprintf("observation successfully created "), 
                $query, 
                $method,
               $param,
               $data);
    }
    
    
    
      /**
     * register.
     *
     * @Route("api/mobile/observation/register",  methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation's information after creation",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"mobile"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="id_inaturalist",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="coordX",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="coordY",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="note",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="alpha",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="collector",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="img1",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="img2",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="specie_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="user_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="type_observation_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="segment_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="date",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="mobile")
     */
    public function registerAction(Request $request, 
                                 SpecieRepository $SpecieRepository,
                                 TypeObservationRepository $TypeObservationRepository,
                                 ProjectRepository $ProjectRepository,
                                 SegmentRepository $SegmentRepository,
                                 UserRepository $UserRepository,
                                 ObservationRepository $ObservationRepository): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);

        $date = $request->get('date');
        $datetime = date('Y-m-d H:i:s', $date);
        
        //echo $datetime; die();
        $id_inaturalist = $request->get('id_inaturalist');
        $coordX = $request->get('coordX');
        $coordY = $request->get('coordY');
        $alpha = $request->get('alpha');
        $note = $request->get('note');
        $collector = $request->get('collector');
        $img1 = $request->get('img1');
        $img2 = $request->get('img2');
        $img3 = $request->get('img3');
        $img4 = $request->get('img4');
        $specie_id = $request->get('specie_id');
        $type_observation_id = $request->get('type_observation_id');
        $segment_id = $request->get('segment_id');
        $project_id = $request->get('project_id');
        $specie = $SpecieRepository->findOneBy(['id' => $specie_id]);
        $typeObservation = $TypeObservationRepository->findOneBy(['id' => $type_observation_id]);
        $segment = $SegmentRepository->findOneBy(['id' => $segment_id]);
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       $username = $decodedJwtToken['username'];
        $user = $UserRepository->findOneBy(['email' => $username]);
        
   /* var_dump($img1);
     foreach($img1 as $images){
        echo  $images['dat_aurl'];
     }*/
    //  die();
        //dd($site);

        if (empty($coordX) || empty($coordY)) {
            return $this->respondValidationError("All fields are required");
        }
          $ob = $ObservationRepository->findOneBy([], ['id' => 'DESC']);
          $id = $ob->getId() + 1;
          
        $observation = new Observation(); 
        $observation->setId($id);
        $observation->setCoordX($coordX);
        $observation->setCoordY($coordY);
        $observation->setIdInaturalist($id_inaturalist);
        $observation->setNote($note);
        $observation->setAlpha($alpha);
        $observation->setCollector($collector);
        $observation->setUser($user);  
        if($specie == NULL){
            $spe = $SpecieRepository->findOneBy(['id' => 9]);
            $observation->setSpecie($spe);
        }else{
               $observation->setSpecie($specie);
        }
        $observation->setTypeObservation($typeObservation);
        $observation->setSegment($segment);
        $observation->setProject($project);
        $observation->setDate(new \DateTime($datetime));
        
        if ($img1)
        {
           
                            $photo = $img1;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img1/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img1Miniature/';
                                 
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';
  // var_dump($sourcename); die();
                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                    $this->imageOptimizer->resize($sourcename_thumb);
                                  // var_dump($thumbs);die();
                                   
                                    $observation->setImg1($photoUrl);
                                    // $observation->setImageTumbnail($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        if ($img2)
        {
           
                            $photo = $img2;
                           
                                

                             try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img2/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img2Miniature/';
                                   // var_dump($webPath); die();
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                    $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                   
                                    $observation->setImg2($photoUrl);
                                     //$observation->setImageTumbnail($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $observation->setImg2('observation2.png');
        }
        if ($img3)
        {
           
                            $photo = $img3;
                           
                                

                             try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img3/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img3Miniature/';
                                   // var_dump($webPath); die();
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                    $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                   
                                    $observation->setImg3($photoUrl);
                                     //$observation->setImageTumbnail($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $observation->setImg3('observation3.png');
        }
        if ($img4)
        {
           
                            $photo = $img4;
                           
                                

                             try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img4/';
                                    $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img4Miniature/';
                                   // var_dump($webPath); die();
                                    $sourcename = $webPath . uniqid() . '.jpg';
                                    $sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);
                                    
                                     $file1 = fopen($sourcename_thumb, 'w+');
                                    fwrite($file1, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file1);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                     $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
                                    $this->imageOptimizer->resize($sourcename_thumb);
                                   
                                   
                                    $observation->setImg4($photoUrl);
                                     //$observation->setImageTumbnail($photoUrlThumb);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $observation->setImg4('observation4.png');
        }
        

        $this->em->persist($observation);
        $this->em->flush();

        $query = "api/observation/create";
        $method = "POST";
   

            $param = ['NULL']; 
            
                 
                  $taille = strlen($observation->getId());
                 // var_dump($id1); die();
                  $obs = ['id' => $observation->getId(), 'taille' => $taille];
                  
      return $this->respondWithSuccess(sprintf("Infos observations"), 
                $query, 
                $method,
               $param,
               $obs);
    }
    
   
     
       /**
     * Create.
     *
     * @Route("api/mobile/result/create", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the result's information after creation",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Result::class, groups={"mobile"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="content",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="question_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="observation_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="mobile")
     */
    public function createResultAction(Request $request,
                                 QuestionRepository $QuestionRepository,
                                 ObservationRepository $ObservationRepository,
                                 ResultRepository $ResultRepository): Response
    {


        $request = $this->transformJsonBody($request);
        $id = $request->get('id');
        $content = $request->get('content');
         $observation = $request->get('content');
        $question_id = $request->get('question_id');
       $observation_id = $request->get('observation_id');
        $question = $QuestionRepository->findOneBy(['id' => $question_id]);
       $observation = $ObservationRepository->findOneBy(['id' => $observation_id]);

        if (empty($content)) {
            return $this->respondValidationError("All fields are required");
        }

         $rsl = $ResultRepository->findOneBy([], ['id' => 'DESC']);
          $id = $rsl->getId() + 1;
        $result = new Result();
        $result->setId($id);
        $result->setContent($content);
        $result->setQuestion($question);
       $result->setObservation($observation);
        

        $this->em->persist($result);
        $this->em->flush();

        $query = "api/result/create";
        $method = "POST";
        $param = [
                 'content' => $content,
                 'question_id ' => $question_id,
                 'observation_id ' => $observation_id,
                  ];
        $data = ['id' => $result->getId(),
                'content ' => $result->getContent(),
                'question ' => $result->getQuestion(),
                'observation ' => $result->getObservation(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The result has been successfully created'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }
    
    
     /**
     * List of observations of one project of a user  .
     *
     * @Route("/api/mobile/observationSync", methods={"POST"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"mobile"}))
     *     )
     * ),
     * @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="data",
     *     in="query",
     *     @OA\Schema(type="array()")
     * )
     * 
     * @OA\Tag(name="mobile")
     */
       public function ObservationSyncAction(Request $request,NormalizerInterface $normalizer,
                                  ObservationRepository $ObservationRepository,
                                  ProjectRepository $ProjectRepository,
                                  ResultRepository $ResultRepository,
                                  UserProjectRepository $UserProjectRepository,
                                  PatrolRepository $patrolRepository,
                                  UserRepository $UserRepository): JsonResponse
                                  
    {
         
        
      
          $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
          $username = $decodedJwtToken['username'];
          $user = $UserRepository->findOneBy(['email' => $username]);
       
            $request = $this->transformJsonBody($request);
            $data = $request->get('data');
            $project_id = $request->get('project_id');
            //$page = $request->get('NumPage');
            $project = $ProjectRepository->findOneBy(['id' => $project_id]);
            $userProject = $UserProjectRepository->findOneBy(['user' => $user, 'project' => $project]);
                
            //return new JsonResponse($user->getId());
             
            $query = "api/user/project/observation/list";
            $method = "GET";
            $param = ['ID' => $project_id]; 
            
            //nirou add
            $patrolIdList = [];
            $obsIdList = [];
            
            if(count($data)>2){
                $patrolLen = $data[count($data)-2];
                $obsLen = $data[count($data)-1];  
                if($patrolLen > 0)
                    $patrolIdList = array_slice($data, 0, $patrolLen);
                if($obsLen > 0)
                    $obsIdList = array_slice($data, $patrolLen, $obsLen);
            }
           
             
                  
                      $i = 0; $j = 0; $k = 0; $l=0; $m=0;
                      $obs = array();
                      $images = array();
                      $resultats = array();
                      $patrolTab = array();
                      $patrolIdTab = array(); 
                      
                  //$data = [748, 754, 755, 760];
                 /* if($userProject->getRole() == Admin){
                         $obsTotal = $ObservationRepository->findBy(['project' => $project, 'status' => 1],['id' => 'ASC']);
                  }else{
                      $obsTotal = $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'user' => $user],['id' => 'ASC']);
                  }*/
                 if($userProject->getRole() == "Admin"){
                      
                $patrolTotal = $patrolRepository->findBy(['project' => $project],['id' => 'ASC']);
                
                for($o=0; $o<count($patrolTotal);$o++){
                    if(!in_array($patrolTotal[$o]->getId(),$patrolIdList)){
                        $patrolTab[$l]['id'] = $patrolTotal[$o]->getId();
                        $patrolTab[$l]['projectId'] = $patrolTotal[$o]->getProject()->getId();
                        $patrolTab[$l]['obsNbr'] = count($ObservationRepository->findBy(['patrol' => $patrolTotal[$o]]));
                        
                        if($patrolTotal[$o]->getCollector()){
                            $patrolTab[$l]['author'] = $patrolTotal[$o]->getCollector()->getFirstName();
                            $patrolTab[$l]['userId'] = $patrolTotal[$o]->getCollector()->getId();
                        }else{
                            
                            $patrolTab[$l]['author'] = "";
                            $patrolTab[$l]['userId'] = -1;
                        }
                      $patrolTab[$l]['startTime'] = $patrolTotal[$o]->getStartDate()->format('Y-m-d').'T'.$patrolTotal[$o]->getStartDate()->format('H:m:s');
                      $patrolTab[$l]['endTime'] = $patrolTotal[$o]->getEndDate()->format('Y-m-d').'T'.$patrolTotal[$o]->getEndDate()->format('H:m:s');
                      $patrolTab[$l]['gpsStartCoordX'] = $patrolTotal[$o]->getCoordXStart();
                      $patrolTab[$l]['gpsStartCoordY'] = $patrolTotal[$o]->getCoordYStart();
                      $patrolTab[$l]['gpsEndCoordX'] = $patrolTotal[$o]->getCoordXEnd();
                      $patrolTab[$l]['gpsEndCoordY'] = $patrolTotal[$o]->getCoordYEnd();
                      $patrolTab[$l]['totalPatrolTime'] = $patrolTotal[$o]->getTotalPatrolTime();
                      $patrolTab[$l]['totalDistance'] = $patrolTotal[$o]->getTotalDistance();
                        
                      $l = $l + 1;
                    }
                }
                
                // foreach($patrolTotal as $patr){ 
                //     $id = $patr->getId();
                //     if(in_array($id,$patrolIdList)){
                //         $patrolTab[$i]['id'] = $patr;
                //     }else
                //         $patrolTab[$i]['id'] = $id;
                        
                // }
                
                // foreach($patrolTotal as $patr){   
                //     $id = $patr->getId();
                //     if(in_array($id,$patrolIdList) == false){
                //         $pat = $patrolRepository->findOneBy(['id' => $id]);
                        
                //          $patrolTab[$i]['id'] = $id;
                         
                //     //     //$patrolTab[$l]['projectName'] = $pat->getProject()->getName();
                        
                    //     $patrolTab[$l]['projectId'] = $pat->getProject()->getId();
                    //     $patrolTab[$l]['obsNbr'] = count($ObservationRepository->findBy(['patrol' => $pat]));
                    //     //$patrolTab[$l]['valObsNbr'] = count($ObservationRepository->findBy(['patrol' => $pat , 'status' => 1]));
                    //     //$patrolTab[$l]['unValidObsNbr'] = count($ObservationRepository->findBy(['patrol' => $pat , 'status' => 0]));
                        
                    //     if($pat->getCollector()){
                    //         $patrolTab[$i]['author'] = $pat->getCollector()->getFirstName();
                    //         $patrolTab[$i]['userId'] = $pat->getCollector()->getId();
                    //     }else{
                            
                    //         $patrolTab[$l]['author'] = "";
                    //     }
                    //   //$patrolTab[$l]['date'] = $pat->getDateEnreg();
                    //   //$patrolTab[$l]['date'] = $pat->getDateEnreg()->format('Y-m-d').'T'.$pat->getDateEnreg()->format('H:m:s');
                    //   //$patrolTab[$l]['startTime'] = $pat->getStartDate();
                    //   $patrolTab[$l]['startTime'] = $pat->getStartDate()->format('Y-m-d').'T'.$pat->getStartDate()->format('H:m:s');
                    //   //$patrolTab[$l]['endTime'] = $pat->getEndDate();
                    //   $patrolTab[$l]['endTime'] = $pat->getEndDate()->format('Y-m-d').'T'.$pat->getEndDate()->format('H:m:s');
                    //   $patrolTab[$l]['gpsStartCoordX'] = $pat->getCoordXStart();
                    //   $patrolTab[$l]['gpsStartCoordY'] = $pat->getCoordYStart();
                    //   $patrolTab[$l]['gpsEndCoordX'] = $pat->getCoordXEnd();
                    //   $patrolTab[$l]['gpsEndCoordY'] = $pat->getCoordYEnd();
                    //   $patrolTab[$l]['totalPatrolTime'] = $pat->getTotalPatrolTime();
                    //   $patrolTab[$l]['totalDistance'] = $pat->getTotalDistance();
                      //$patrolTab[$l]['note'] = $pat->getNote();
                       
                      
                    
                //         $l++;
                //     }
                   
                    
                // }
        
               $obsTotal = $ObservationRepository->findBy(['project' => $project, 'status' => 1],['id' => 'ASC']);
                  
                  $observations = array(); 
                        foreach($obsTotal as $Ot){
                            $observations[$k] = $Ot->getId();
                            $k++;
                        }
                        
                       // print_r($observations); die();
                        
                             foreach($observations as $observation){
                                 
                                 //if(in_array($observation, $data) == false && $observation == 171549)
                                 if(in_array($observation, $data) == false){
                                   $ob = $ObservationRepository->findOneBy(['id' => $observation]);
                             $obs[$i]['id'] = $observation;
                            $obs[$i]['coord_x'] = $ob->getCoordX();
                            $obs[$i]['coord_y'] = $ob->getCoordY();
                            $obs[$i]['date'] = $ob->getDate()->format('Y-m-d').'T'.$ob->getDate()->format('H:m:s');
                            $obs[$i]['etat'] = $ob->getStatus(); 
                            $obs[$i]['type_observations'] =$normalizer->normalize($ob->getTypeObservation(), null, ['groups' => 'typeObservation:read']);
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            if(!empty($ob->getPatrol()))
                                $obs[$i]['patrol_id'] = $ob->getPatrol()->getId();
                            else
                                $obs[$i]['patrol_id'] = -1;
                            if($ob->getCollector() == NULL){
                                if($ob->getUser()->getFirstname() == NULL ){
                                     $obs[$i]['collecteur'] = $ob->getUser()->getEmail();
                                }else{
                                    $obs[$i]['collecteur'] = $ob->getUser()->getFirstname();
                                }
                            }else{
                               $obs[$i]['collecteur'] = $ob->getCollector(); 
                            }
                            
                            if($ob->getImg1() == "observation1.png"){
                                $obs[$i]['img1']  = "NULL";  
                            }else{
                                $obs[$i]['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $obs[$i]['img2']  = "NULL"; 
                            }else{
                                 $obs[$i]['img2']  = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                  $obs[$i]['img3']  = "NULL"; 
                            }else{
                                $obs[$i]['img3']  = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                              $obs[$i]['img4'] = "NULL"; 
                            }else{
                                 $obs[$i]['img4']  = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                            }
                             /*if($ob->getSpecie() == NULL || $ob->getSpecie() == -1 )
                                      $obs[$i]['especes'] =  $normalizer->normalize($ob->getSpecie(), null, ['groups' => 'specie:read']);
                                      if($ob->getSpecie()->getSubgroup() == NULL){
                                      $obs[$i]['sous_groupes'] = "";
                                        $obs[$i]['groupes'] = "";
                                            }else{
                                          $obs[$i]['sous_groupes'] = $normalizer->normalize($ob->getSpecie()->getSubgroup(), null, ['groups' => 'subgroup:read']); 
                                          $obs[$i]['groupes'] = $normalizer->normalize($ob->getSpecie()->getSubgroup()->getGroupe(), null, ['groups' => 'group:read']); 
                                     }*/ 
                             $subgroup = new Subgroup();
                             $group = new Group();   
                             $subgroup->setId(-1);
                             $group->setId(-1);
                                 
                          
                            if($ob->getSpecie() == NULL)
                            {
                                $species =  new Specie() ;
                                $species->setId(-1);
                                $obs[$i]['especes'] = $normalizer->normalize($species);
                                
                                
                                  $obs[$i]['sous_groupes'] = $normalizer->normalize($subgroup);
                                  $obs[$i]['groupes'] = $normalizer->normalize($group);
                            }else{
                                 $obs[$i]['especes'] =  $normalizer->normalize($ob->getSpecie(), null, ['groups' => 'specie:read']);
                                      if($ob->getSpecie()->getSubgroup() == NULL){
                                             $obs[$i]['sous_groupes'] = $normalizer->normalize($subgroup);
                                             $obs[$i]['groupes'] = $normalizer->normalize($group);
                                      }else{
                                          $obs[$i]['sous_groupes'] = $normalizer->normalize($ob->getSpecie()->getSubgroup(), null, ['groups' => 'subgroup:read']); 
                                          $obs[$i]['groupes'] = $normalizer->normalize($ob->getSpecie()->getSubgroup()->getGroupe(), null, ['groups' => 'group:read']); 
                                           }
                            }                   
                             if($ob->getUser()){
                            $obs[$i]['utilisateurs'] = $normalizer->normalize($ob->getUser(), null, ['groups' => 'user:read']);  
                            }else{
                                $obs[$i]['utilisateurs'] = "";
                            }
                             $obs[$i]['projet'] = $normalizer->normalize($ob->getProject(), null, ['groups' => 'project:read']); 
                          // $resultsnormalizer  = $normalizer->normalize($ob->getResults(), null, ['groups' => 'result:read']); 
                           
                             foreach($ob->getResults() as $res){
                                 
                               $resultats[$j]['id'] = $res->getId();
                                 $resultats[$j]['content'] = $res->getcontent();
                                  $resultats[$j]['questions'] = $normalizer->normalize($res->getQuestion(), null, ['groups' => 'question:read']);
                                 $j++;
                             }
                              $obs[$i]['resultats'] = $resultats; 
                            // $obs[$i]['question'] = $normalizer->normalize($ob->getResults()['question'], null, ['groups' => 'question:read']);                             
                            /* foreach($ResultRepository->findBy(['observation' => $ob]) as $re){
                                  $resultats[$j]['content']  = $re->getcontent(); $j++;
                             }*/
                             
                            //$obs[$i]['dead'] = $ob->getDead();
                        
                         /*  if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                            $obs[$i]['siteId'] = $ob->getSegment()->getSite()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";            
                              $obs[$i]['site'] =  "";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                             if($ob->getSegment()->getSite() ==  'Null'){
                                  $obs[$i]['site'] =  "NULL";
                            
                           }else{
                               $obs[$i]['siteId'] = $ob->getSegment()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getName();
                             }*/
                           
                          /* if($ob->getSegment()->getSite()){
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['site'] =  "NULL";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }*/
                           
                           $i++;
                 }
                             }
                  
                  
                  
                  /* $observationsNormalizer = $normalizer->normalize($observations, null, ['groups' => 'observation:read']);
      
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $obs);*/
               
               $responses['patrols'] = $patrolTab;
               $responses['observations'] = $obs;
               
              return  new JsonResponse($responses);
              //return  new JsonResponse($obs);
              //  return  new JsonResponse(count($obs));
            
             
    }else{
        
                $patrolTotal = $patrolRepository->findBy(['project' => $project, 'user' => $user],['id' => 'ASC']);
                
                
                foreach($patrolTotal as $pat){   
                    if(in_array($pat->getId(),$patrolIdList) == false){
                        $patrolTab[$l]['id'] = $pat->getId();
                        $patrolTab[$l]['projectName'] = $pat->getProject()->getName();
                        $patrolTab[$l]['observations'] = count($ObservationRepository->findBy(['patrol' => $pat]));
                        $patrolTab[$l]['valObs'] = count($ObservationRepository->findBy(['patrol' => $pat , 'status' => 1]));
                        $patrolTab[$l]['unValidObs'] = count($ObservationRepository->findBy(['patrol' => $pat , 'status' => 0]));
                        if($pat->getCollector()){
                            $tab[$i]['author'] = $pat->getCollector()->getFirstName();
                        }else{
                            
                        $patrolTab[$l]['author'] = "";
                        }
                       $patrolTab[$l]['date'] = $pat->getDateEnreg();
                       $patrolTab[$l]['startTime'] = $pat->getStartDate();
                       $patrolTab[$l]['endTime'] = $pat->getEndDate();
                       $patrolTab[$l]['gpsStartCoordX'] = $pat->getCoordXStart();
                       $patrolTab[$l]['gpsStartCoordY'] = $pat->getCoordYStart();
                       $patrolTab[$l]['gpsEndCoordX'] = $pat->getCoordXEnd();
                       $patrolTab[$l]['gpsEndCoordY'] = $pat->getCoordYEnd();
                       $patrolTab[$l]['totalPatrolTime'] = $pat->getTotalPatrolTime();
                       $patrolTab[$l]['totalDistance'] = $pat->getTotalDistance();
                       $patrolTab[$l]['note'] = $pat->getNote();
                       
                       $l++;
                    }
                   
                    
                }
                
                $obsTotal = $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'user' => $user],['id' => 'ASC']);
                  
                  $observations = array(); 
                        foreach($obsTotal as $Ot){
                            $observations[$k] = $Ot->getId();
                            $k++;
                        }
                        
                       // print_r($observations); die();
                        
                             foreach($observations as $observation){
                                 
                                 if(in_array($observation, $data) == false) {
                                   $ob = $ObservationRepository->findOneBy(['id' => $observation]);
                             $obs[$i]['id'] = $observation;
                            $obs[$i]['coord_x'] = $ob->getCoordX();
                            $obs[$i]['coord_y'] = $ob->getCoordY();
                            $obs[$i]['date'] = $ob->getDate()->format('Y-m-d').'T'.$ob->getDate()->format('H:m:s');
                            $obs[$i]['etat'] = $ob->getStatus(); 
                            $obs[$i]['type_observations'] =$normalizer->normalize($ob->getTypeObservation(), null, ['groups' => 'typeObservation:read']);
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['patrol_id'] = $ob->getPatrol().getId();
                             if($ob->getCollector() == NULL){
                                if($ob->getUser()->getFirstname() == NULL ){
                                     $obs[$i]['collecteur'] = $ob->getUser()->getEmail();
                                }else{
                                    $obs[$i]['collecteur'] = $ob->getUser()->getFirstname();
                                }
                            }else{
                               $obs[$i]['collecteur'] = $ob->getCollector(); 
                            }
                            if($ob->getImg1() == "observation1.png"){
                                $obs[$i]['img1']  = "NULL";  
                            }else{
                                $obs[$i]['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $obs[$i]['img2']  = "NULL"; 
                            }else{
                                 $obs[$i]['img2']  = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                  $obs[$i]['img3']  = "NULL"; 
                            }else{
                                $obs[$i]['img3']  = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                              $obs[$i]['img4'] = "NULL"; 
                            }else{
                                 $obs[$i]['img4']  = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                            }
                            
                            $subgroup = new Subgroup();
                                $group = new Group();
                                $subgroup->setId(-1);
                                 $group->setId(-1);
                                      if($ob->getSpecie() == NULL)
                            {
                                $species =  new Specie() ;
                                $species->setId(-1);
                                $obs[$i]['especes'] = $normalizer->normalize($species);
                                 
                                
                                 
                                  $obs[$i]['sous_groupes'] = $normalizer->normalize($subgroup);
                                  $obs[$i]['groupes'] = $normalizer->normalize($group);
                                    
                            }else{
                                 $obs[$i]['especes'] =  $normalizer->normalize($ob->getSpecie(), null, ['groups' => 'specie:read']);
                                      if($ob->getSpecie()->getSubgroup() == NULL){
                                               $obs[$i]['sous_groupes'] = $normalizer->normalize($subgroup);
                                               $obs[$i]['groupes'] = $normalizer->normalize($group);
                                      }else{
                                          $obs[$i]['sous_groupes'] = $normalizer->normalize($ob->getSpecie()->getSubgroup(), null, ['groups' => 'subgroup:read']); 
                                          $obs[$i]['groupes'] = $normalizer->normalize($ob->getSpecie()->getSubgroup()->getGroupe(), null, ['groups' => 'group:read']); 
                                           }
                            }                   
                                        
                          
                            
                             if($ob->getUser()){
                            $obs[$i]['utilisateurs'] = $normalizer->normalize($ob->getUser(), null, ['groups' => 'user:read']);  
                            }else{
                                $obs[$i]['utilisateurs'] = "";
                            }
                             $obs[$i]['projet'] = $normalizer->normalize($ob->getProject(), null, ['groups' => 'project:read']); 
                          // $resultsnormalizer  = $normalizer->normalize($ob->getResults(), null, ['groups' => 'result:read']); 
                           
                             foreach($ob->getResults() as $res){
                                 
                               $resultats[$j]['id'] = $res->getId();
                                 $resultats[$j]['content'] = $res->getcontent();
                                  $resultats[$j]['questions'] = $normalizer->normalize($res->getQuestion(), null, ['groups' => 'question:read']);
                                 $j++;
                             }
                              $obs[$i]['resultats'] = $resultats; 
                            // $obs[$i]['question'] = $normalizer->normalize($ob->getResults()['question'], null, ['groups' => 'question:read']);                             
                            /* foreach($ResultRepository->findBy(['observation' => $ob]) as $re){
                                  $resultats[$j]['content']  = $re->getcontent(); $j++;
                             }*/
                             
                            //$obs[$i]['dead'] = $ob->getDead();
                        
                         /*  if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                            $obs[$i]['siteId'] = $ob->getSegment()->getSite()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";            
                              $obs[$i]['site'] =  "";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                             if($ob->getSegment()->getSite() ==  'Null'){
                                  $obs[$i]['site'] =  "NULL";
                            
                           }else{
                               $obs[$i]['siteId'] = $ob->getSegment()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getName();
                             }*/
                           
                          /* if($ob->getSegment()->getSite()){
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['site'] =  "NULL";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }*/
                           
                         $i++;
               }
                            }
                            
                            
               $species =  new Specie() ;
                          $species->setId(-1);
                          $this->em->persist($species);
            
               $responses['patrols'] = $patrolTab;
               $responses['observations'] = $obs;
               
               return  new JsonResponse($responses);
               //return  new JsonResponse($obs);
               //  return  new JsonResponse(count($obs));
    }
           
        
    }
    
        
     /**
     * List of observations of one project of a user  .
     *
     * @Route("/api/mobile/observationSyncVerif", methods={"POST"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"mobile"}))
     *     )
     * ),
     * @OA\Parameter(
     *     name="data",
     *     in="query",
     *     @OA\Schema(type="array()")
     * )
     * 
     * @OA\Tag(name="mobile")
     */
       public function ObservationSyncVerifAction(Request $request,NormalizerInterface $normalizer,
                                  ObservationRepository $ObservationRepository,
                                  ProjectRepository $ProjectRepository): JsonResponse
    {
         
        
      
         
       
            $request = $this->transformJsonBody($request);
             $data = $request->get('data');
             
            $query = "api/user/project/observation/list";
            $method = "GET";
            $param = []; 
              
             
                  
                      $i = 0; $j = 0; $k = 0; 
                      $obs = array();
                      $images = array();
                      
                  //$data = [748, 754, 755, 760, 761];
                  $obsTotal = $ObservationRepository->findBy(['status' => 1],['id' => 'ASC']);
                  $observations = array(); 
                        foreach($obsTotal as $Ot){
                            $observations[$k] = $Ot->getId();
                            $k++;
                        }
                        
                       // print_r($observations); die();
                        
                             foreach($observations as $observation){
                                 
                                 if(in_array($observation, $data) == true){
                                   $ob = $ObservationRepository->findOneBy(['id' => $observation]);
                           // $obs[$i]['email'] = $ob->getUser()->getEmail();
                             $obs[$i] = $observation;

                           

                           
                           $i++;
                 }
                             }
                  
                   $observationsNormalizer = $normalizer->normalize($observations, null, ['groups' => 'observation:read']);
      
               
               return  new JsonResponse($obs);
            
             
     
           
        
    }
    
    
    
     /**
     * Create.
     *
     * @Route("api/mobile/observationQa/create",  methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation's information after creation",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Observation::class, groups={"mobile"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="id_inaturalist",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="coordX",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="coordY",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="note",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="alpha",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="img1",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="img2",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="specie_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="user_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="type_observation_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="segment_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="date",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="qa",
     *     in="query",
     *     @OA\Schema(type="array()")  
     * ),
     *  @OA\Parameter(
     *     name="patrol_id",
     *     in="query",
     *     @OA\Schema(type="integer")  
     * )
     * 
     * @OA\Tag(name="mobile")
     */
    public function createObsQaAction(Request $request, 
                                 SpecieRepository $SpecieRepository,
                                 TypeObservationRepository $TypeObservationRepository,
                                 ProjectRepository $ProjectRepository,
                                 SegmentRepository $SegmentRepository,
                                 UserRepository $UserRepository,
                                 ObservationRepository $ObservationRepository,
                                 SiteRepository $SiteRepository,
                                 QuestionRepository $QuestionRepository,
                                 ResultRepository $ResultRepository,
                                 PatrolRepository $PatrolRepository): Response
    {
       
       
       $original = $request;
        $request = $this->transformJsonBody($request);
       
        $date = $request->get('date');
            $data = $request->get('qa');
        $datetime = date('Y-m-d H:i:s', $date);
        
        
        
        //echo $datetime; die();
        $id_inaturalist = $request->get('id_inaturalist');
        $coordX = $request->get('coordX');
        $coordY = $request->get('coordY');
        $alpha = $request->get('alpha');
        $note = $request->get('note');
        $img1 = $request->files->get('img1');
        $img2 = $request->files->get('img2');
        $img3 = $request->files->get('img3');
        $img4 = $request->files->get('img4');
        $zip1 = $request->files->get('zip1');
        $zip2 = $request->files->get('zip2');
        $zip3 = $request->files->get('zip3');
        $zip4 = $request->files->get('zip4');
        $specie_id = $request->get('specie_id');
        $type_observation_id = $request->get('type_observation_id');
       // $segment_id = $request->get('segment_id');
       $site_id = $request->get('segment_id');
        $project_id = $request->get('project_id');
        $specie = $SpecieRepository->findOneBy(['id' => $specie_id]);
        $patrol_id = $request->get('patrol_id');
        $patrol = $PatrolRepository->findOneBy(['id' => $patrol_id]);
        
       
      //  $typeObservation = $TypeObservationRepository->findOneBy(['id' => $type_observation_id]);
       // $segment = $SegmentRepository->findOneBy(['id' => $segment_id]);
        $site = $SiteRepository->findOneBy(['id' => $site_id]);
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);
        // var_dump($segment_id); die();
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       $username = $decodedJwtToken['username'];
       
        // ancien ecrit par lionel
        // $user = $UserRepository->findOneBy(['email' => $username]);
        // ---
        //nouveau ecrit par nirou
        $observer_id = $request->get('user_id');
        if( $observer_id == NULL)
            $user = $UserRepository->findOneBy(['email' => $username]);
        else
            $user = $UserRepository->findOneBy(['id' => $observer_id]);
        
         if($user->getFirstname() != NULL && $user->getLastname() != NULL){
            $collector = $user->getFirstname()." ".$user->getLastname();
        }
        
        if($user->getFirstname() == NULL && $user->getLastname() == NULL){
            $collector = $user->getEmail();
        }
        
      //  var_dump($collector); die();

        if (empty($coordX) || empty($coordY)) {
            return $this->respondValidationError("All cordonate fields are required");
        }
          $ob = $ObservationRepository->findOneBy([], ['id' => 'DESC']);
          $id = $ob->getId() + 1;
          
        $observation = new Observation(); 
        $observation->setId($id);
        $observation->setCoordX($coordX);
        $observation->setCoordY($coordY);
        $observation->setIdInaturalist($id_inaturalist);
        $observation->setNote($note);
        $observation->setAlpha($alpha);
        $observation->setCollector($collector);
        $observation->setUser($user);
         if($type_observation_id == 16){
            $type_observation_id = 3;
             $typeObservation = $TypeObservationRepository->findOneBy(['id' => $type_observation_id]);
             $observation->setTypeObservation($typeObservation);
        }
         if($type_observation_id == 7){
            $type_observation_id = 6;
             $typeObservation = $TypeObservationRepository->findOneBy(['id' => $type_observation_id]);
             $observation->setTypeObservation($typeObservation);
        }
       if($type_observation_id == 17){
            $type_observation_id = 2;
             $typeObservation = $TypeObservationRepository->findOneBy(['id' => $type_observation_id]);
            $observation->setTypeObservation($typeObservation);
        }
        
         if($type_observation_id == 18 || $type_observation_id == 19){
            $type_observation_id = 1;
             $typeObservation = $TypeObservationRepository->findOneBy(['id' => $type_observation_id]);
            $observation->setTypeObservation($typeObservation);
        }
         $typeObservation = $TypeObservationRepository->findOneBy(['id' => $type_observation_id]);
            $observation->setTypeObservation($typeObservation);
        if($type_observation_id != 6 AND $specie_id != 0){
            $observation->setSpecie($specie);
        }
        
        $observation->setSite($site);
        $observation->setProject($project);
        $observation->setDate(new \DateTime($datetime));
        
        /*if($zip1)
        {
               $uploadedZipFile = $zip1;
               $destination = $this->getParameter('kernel.project_dir').'/public/uploads/observations/img1/';
               $sourcename_zip = $destination . uniqid() . '.zip';
               $photoZipUrl = str_replace($destination, '', $sourcename_zip);
               $uploadedZipFile->move($destination,$photoZipUrl);
            
        }*/
        
        if ($img1)
        {
               $uploadedFile = $img1;
               $filename ='fim'.$uploadedFile->getClientOriginalName();
               
               $destination = $this->getParameter('kernel.project_dir').'/public/uploads/observations/img1/';
              // $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img1Miniature/';
               
               $sourcename = $destination . uniqid() . '.jpg';
               //$sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';
               
               $photoUrl = str_replace($destination, '', $sourcename);
              // $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
               //$this->imageOptimizer->resize($sourcename_thumb);
               $uploadedFile->move($destination,$photoUrl );
           
                $observation->setImg1($photoUrl);
        }
        if ($img2)
        {
           $uploadedFile = $img2;
               $filename ='fim'.$uploadedFile->getClientOriginalName();
               
               $destination = $this->getParameter('kernel.project_dir').'/public/uploads/observations/img2/';
              // $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img2Miniature/';
               
               $sourcename = $destination . uniqid() . '.jpg';
               //$sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';
               
               $photoUrl = str_replace($destination, '', $sourcename);
              // $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
               //$this->imageOptimizer->resize($sourcename_thumb);
               $uploadedFile->move($destination,$photoUrl );
               $observation->setImg2($photoUrl);
                             
        }
        else
        {
            $observation->setImg2('observation2.png');
        }
        if ($img3)
        {
           $uploadedFile = $img3;
               $filename ='fim'.$uploadedFile->getClientOriginalName();
               
               $destination = $this->getParameter('kernel.project_dir').'/public/uploads/observations/img3/';
              // $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img3Miniature/';
               
               $sourcename = $destination . uniqid() . '.jpg';
               //$sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';
               
               $photoUrl = str_replace($destination, '', $sourcename);
              // $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
               //$this->imageOptimizer->resize($sourcename_thumb);
               $uploadedFile->move($destination,$photoUrl );
               $observation->setImg3($photoUrl);
                             
        }
        else
        {
            $observation->setImg3('observation3.png');
        }
        if ($img4)
        {
           $uploadedFile = $img4;
               $filename ='fim'.$uploadedFile->getClientOriginalName();
               
               $destination = $this->getParameter('kernel.project_dir').'/public/uploads/observations/img4/';
              // $webPath_thumb = $this->getParameter('kernel.project_dir') . '/public/uploads/observations/img4Miniature/';
               
               $sourcename = $destination . uniqid() . '.jpg';
               //$sourcename_thumb = $webPath_thumb . uniqid() . '.jpg';
               
               $photoUrl = str_replace($destination, '', $sourcename);
              // $photoUrlThumb = str_replace($webPath_thumb, '', $sourcename_thumb);
                                 
               //$this->imageOptimizer->resize($sourcename_thumb);
               $uploadedFile->move($destination,$photoUrl );      
                                    $observation->setImg4($photoUrl);
                             
        }
        else
        {
            $observation->setImg4('observation4.png');
        }
        if($patrol_id){
            
        $observation->setPatrol($patrol);
        }

        $this->em->persist($observation);
        
       
          $k = 0;
          if($data){
         
          foreach($data as $da){
           $rsl = $ResultRepository->findOneBy([], ['id' => 'DESC']);
          $id = $rsl->getId() + 1;
        $result = new Result();
        $result->setId($id);
        $result->setContent($da["content"]);
        $result->setQuestion($QuestionRepository->findOneBy(['id' => $da["question_id"]]));
       $result->setObservation($observation);
        
        $this->em->persist($result);
      $this->em->flush();
          $k++;
        
          }
          
          }
        $this->em->flush();
        
        if($patrol){
            $nb_obs = count($ObservationRepository->findBy(['patrol' => $patrol]));
            $obsRestant = $patrol->getNumberObservations() - $nb_obs;
        }else{
            $nb_obs = 0;
       $obsRestant = 0;
        }
        

        $query = "api/observation/create";
        $method = "POST";
        $param = [
                 'id_inaturalist' => $id_inaturalist,
                 'coordx' => $coordX,
                 'coordY' => $coordX,
                 'note' => $note,
                 'alpha' => $alpha,
                 'collector' => $collector,
                 'img1' => $img1,
                 'img2' => $img2,
                 'img3' => $img3,
                 'img4' => $img4,
                 'typeobservation_id' => $type_observation_id,
                 'specie_id' => $specie_id,
                 'project_id ' => $project_id,
                 'site_id ' => $site_id,
                 'ori ' => $original,
                 'modif ' => $request,
                  ];
        $data = ['id' =>  $observation->getId(),
                'nb_obs' => $nb_obs,
                'obsRestant' => $obsRestant
    ];

                return $this->respondWithSuccess(sprintf("observation successfully created "), 
                $query, 
                $method,
               $param,
               $data);
    }
    
    
    /**
     * Register.
     *
     * @Route("api/mobile/patrol/create", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Patrol::class, groups={"patrol"}))  
     *     )
     * )
     * @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="user_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="start_timestamp",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="end_timestamp",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="start_latitude",
     *     in="query", 
     *     @OA\Schema(type="float")
     * ),
     * @OA\Parameter(
     *     name="start_longitude",
     *     in="query",
     *     @OA\Schema(type="float")
     * ),
     * @OA\Parameter(
     *     name="end_latitude",
     *     in="query",
     *     @OA\Schema(type="float")
     * ),
     *  @OA\Parameter(
     *     name="end_longitude",
     *     in="query",
     *     @OA\Schema(type="float")
     * ),
     *  @OA\Parameter(
     *     name="timestamp",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="distance",
     *     in="query",
     *     @OA\Schema(type="float")
     * ),
     *  @OA\Parameter(
     *     name="note",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="obs_nbr",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="mobile")
     */
    public function Patrol(Request $request, ProjectRepository $ProjectRepository,
                                  UserRepository $UserRepository): Response
    {

        $request = $this->transformJsonBody($request);
       
        /*$start_date1 = $request->get('start_date');
        $start_date2 = $start_date1->format('y-m-y H:i:s');
        $start_date = \DateTime::createFromFormat('Y-m-d H:i:s', $start_date2); */
        
        
        $date1 = $request->get('start_timestamp')/1000;
        $date2 = $request->get('end_timestamp')/1000;
        $obs_nbr = $request->get('obs_nbr');
         $startDate = date('Y-m-d H:i:s', $date1);
        $endDate = date('Y-m-d H:i:s', $date2);
      //  var_dump($startDate,$endDate); die();
        $gpsStartCoordX = $request->get('start_latitude');
        $gpsStartCoordY = $request->get('start_longitude');
        $gpsEndCoordX = $request->get('end_latitude');
        $gpsEndCoordY = $request->get('end_longitude');
        $totalPatrolTime = $request->get('timestamp');
        $totalDistance = $request->get('distance');
        //$note = $request->get('note');
        $collectorId = $request->get('user_id');
        $projectId = $request->get('project_id');
        $collector = $UserRepository->findOneBy(['id' => $collectorId]);
        $project = $ProjectRepository->findOneBy(['id' => $projectId]);
        //dd($site);

       /* if (empty($start_date) || empty($end_date) || empty($coord_xstart) || empty($coord_ystart) || empty($coord_xend) || empty($coord_yend) || empty($collectorId) || empty($projectId)) {
            return $this->respondValidationError("All fields are required");
        }*/
        
         $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       $username = $decodedJwtToken['username'];
        $user = $UserRepository->findOneBy(['email' => $username]);

        $patrol = new Patrol();
        $patrol->setStartDate(new \DateTimeImmutable($startDate));
        $patrol->setEndDate(new \DateTimeImmutable($endDate));
        $patrol->setCoordXStart($gpsStartCoordX);
        $patrol->setCoordYStart($gpsStartCoordY);
        $patrol->setCoordXEnd($gpsEndCoordX);
        $patrol->setCoordYEnd($gpsEndCoordY);
        $patrol->setTotalPatrolTime($totalPatrolTime);
        $patrol->setTotalDistance($totalDistance);
        $patrol->setNote("Null");
        $patrol->setProject($project);
        $patrol->setNumberObservations($obs_nbr);
        $patrol->setCollector($collector);
         
        
        $this->em->persist($patrol);
        
        $this->em->flush();

        $query = "api/patrol/create";
        $method = "POST";
        /*$param = [
                 'start_date' => $startDate,
                 'end_date' => $endDate,
                 'coord_xstart' => $gpsStartCoordX,
                 'coord_ystart' => $gpsStartCoordY,
                 'coord_xend' => $gpsEndCoordX,
                 'coord_yend' => $gpsEndCoordY,
                 'project_id ' => $projectId,
                 'collector ' => $username,
                  ];
        $data = ['id' => $patrol->getId(),
                'start_date' => $patrol->getStartDate(),
                'end_date' => $patrol->getendDate(),
                'coord_xstart' => $patrol->getCoordXStart(),
                'coord_ystart' => $patrol->getCoordYStart(),
                'coord_xend' => $patrol->getCoordXEnd(),
                'coord_yend' => $patrol->getCoordYEnd(),
                'project ' => $patrol->getProject(),
                'collector ' => $patrol->getCollector(),
                 
                  ];     */     
                  
                  $param = [
                 'start_date' => $startDate,
                 'end_date' => $endDate,
                 'coord_xstart' => $gpsStartCoordX,
                 'coord_ystart' => $gpsStartCoordY,
                 'coord_xend' => $gpsEndCoordX,
                 'coord_yend' => $gpsEndCoordY,
                 'project_id ' => $projectId,
                 'collector ' => $username,
                 'totalDistance'  => $totalDistance,
                  ];
        $data = ['id' => $patrol->getId(),
                 
                  ];   
        return $this->respondWithSuccess(sprintf('The patrol has been successfully created'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       
    }

    
}




