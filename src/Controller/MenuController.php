<?php

namespace App\Controller;

use App\Entity\Menu;
use App\Repository\MenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class MenuController extends ApiController 
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                MenuRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }


     
       /**
     * Register.
     *
     * @Route("/api/menu/create", name="menu_create", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the menu's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Menu::class, groups={"menu"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="title",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="menu")
     */
    public function createAction(Request $request): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $title = $request->get('title');
        $id = $request->get('id');
        if (empty($title)){
        return $this->respondValidationError("All fields are required");;
        }

        $menu = new Menu();
        $menu->setTitle($title);
        $menu->setId($id);

        $this->em->persist($menu);
        $this->em->flush();

        $query = "api/menu/create";
        $method = "POST";
        $param = [
                 'title ' => $title,

                  ];
        $data = ['id' => $menu->getId(),
                'title ' => $menu->getTitle(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The menu named %s has been successfully created', 
                                         $menu->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/menu/read  ", name="menu_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $menu = $this->repository->findOneBy(['id' => $id]);

        $query = "api/menu/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $menu->getId(),
                'title ' => $menu->getTitle(),
                        ];  
        return $this->respondWithSuccess(sprintf('infos of menu %s', 
                                         $menu->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }


     /**
     * @Route("/api/menu/list  ", name="menu_list", methods={"GET"})
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/menu/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $menu= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $menuNormalizer = $normalizer->normalize($menu, null, ['groups' => 'menu:read']);
       
           return $this->respondWithSuccess(sprintf('List of menu'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $menuNormalizer);
        }

        
        $menu = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $menuNormalizer = $normalizer->normalize($menu, null, ['groups' => 'menu:read']);
       
        return $this->respondWithSuccess(sprintf('List of menu'), 
       $query, 
       $method,
      $param,
      $menuNormalizer);
    }

/**
     * @Route("/api/menu/delete", name="menu_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $menu = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($menu);
            $this->em->flush();
            
            $query = "api/menu/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Menu %s successfully delete', 
                                                      $menu->getTitle()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/menu/update", name="menu_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $title = $request->get('title');
    
           $menu = $this->repository->findOneBy(['id' => $id]);
    
    

            if($title)
            {
                $menu->setTitle($title);
                
            }

    
            $this->em->persist($menu);
            $this->em->flush();

            $query = "api/menu/update";
            $method = "PUT";
            $param = [
                'title ' => $title,

                 ];
            $data = ['id' => $menu->getId(),
                    'title ' => $menu->getTitle(),
                        
                        ];           
        return $this->respondWithSuccess(sprintf('The menu white message %s has been successfully update', 
                                         $menu->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
}
