<?php

namespace App\Controller;

use App\Entity\Subgroup;
use App\Repository\SubgroupRepository;
use App\Repository\GroupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;


class SubgroupController extends ApiController
{

    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                SubgroupRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }


     
       /**
     * Register.
     *
     * @Route("/api/subgroup/create", name="subgroup_create", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the subgroup's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Subgroup::class, groups={"subgroup"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="name",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="description",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="image",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="group_id",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="subgroup")
     */
    public function createAction(Request $request, GroupRepository $GroupRepository): JsonResponse
    {
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
          $id = $request->get('id');
        $name = $request->get('name');
        $description = $request->get('description');
        $image = $request->get('image');
         $date_create = $request->get('date_create');
       //  $date = strtotime($date_create);
                 
        $group_id = $request->get('group_id');
        $group = $GroupRepository->findOneBy(['id' => $group_id]);

        if (empty($name) || empty($description) || empty($image) || empty($group)) {
            return $this->respondValidationError("All fields are required");
        }

        $subgroup = new Subgroup();
        $subgroup->setId($id);
        $subgroup->setName($name);
        $subgroup->setDateCreate(new \DateTime($date_create));
        $subgroup->setGroupe($group);
        $subgroup->setDescription($description);
         $subgroup->setImage($image);

        /*if ($image)
        {
           
                            $photo = $image;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/subgroups/';
                                   // dd($webPath);
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                   
                                    $subgroup->setImage($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $subgroup->setImage('subgroup.png');
        }*/


        $this->em->persist($subgroup);
        $this->em->flush();

        $query = "api/subgroup/create";
        $method = "POST";
        $param = [
                 'name ' => $name,
                 'description ' => $description,
                 'image ' => $image,

                  ];
        $data = ['id' => $subgroup->getId(),
                 'name ' => $subgroup->getName(),
                 'description ' => $subgroup->getDescription(),
                 'image ' => $subgroup->getImage(),

                 
                  ];          
        return $this->respondWithSuccess(sprintf('The subgroup named %s has been successfully created', 
                                         $subgroup->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/subgroup/read  ", name="subgroup_read", methods={"GET"})
     */
    public function readAction(Request $request): JsonResponse 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $subgroup = $this->repository->findOneBy(['id' => $id]);

        $query = "api/subgroup/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $subgroup->getId(),
                'name ' => $subgroup->getName(),
                'description ' => $subgroup->getDescription(),
                'image ' => $subgroup->getImage()
                        ];  
        return $this->respondWithSuccess(sprintf('infos of subgroup %s', 
                                         $subgroup->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }


       
    /**
     * List  .
     *
     * @Route("/api/subgroup/list  ", name="subgroup_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns list of subgroups ",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Subgroup::class, groups={"subgroup"}))
     *     )
     * )
     * 
     * @OA\Tag(name="subgroup")
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): JsonResponse 
    {

     

        $query = "api/subgroup/list";
        $method = "GET";
        $param = ['NULL']; 

            $subgroups= $this->repository->findAll();
           

              $groupsNormalizer = $normalizer->normalize($subgroups, null, ['groups' => 'subgroup:read']);
      
        return $this->respondWithSuccess(sprintf('List of Groups'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $groupsNormalizer);
        }

        /**
     * List  .
     *
     * @Route("/api/subgroup/group/list  ", name="subgroup_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns list of subgroups ",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Subgroup::class, groups={"subgroup"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="group_id",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="subgroup")
     */
    public function listSubgroupAction(Request $request, NormalizerInterface $normalizer,
                                GroupRepository $GroupRepository): JsonResponse 
    {

     
        $request = $this->transformJsonBody($request);
        $id = $request->get('group_id');
            $group = $GroupRepository->findOneBy(['id' => $id]);
        $query = "api/subgroup/group/list";
        $method = "GET";
        $param = ['NULL']; 

            $subgroups= $this->repository->findBy(['groupe' => $group]);
           

              $groupsNormalizer = $normalizer->normalize($subgroups, null, ['groups' => 'subgroup:read']);
      
        return $this->respondWithSuccess(sprintf('List of Subgroups'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $groupsNormalizer);
        }  

     /**
     * @Route("/api/subgroupPage/list  ", name="subgroupPage_list", methods={"GET"})
     */
    public function listPageAction(Request $request): JsonResponse 
    {

      
    
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/subgroup/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $subgroups= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           

        return $this->json($subgroups, 201, ['groups' => 'group:read']);
      
       /* return $this->respondWithSuccess(sprintf('List of Groups'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $groups,
                                        ['groups' => 'group:read']);*/
        }

        
        $subgroups = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));


       // return $this->json("list ofgroups", $groups, 201, [], ['groups' => 'group:read']);
       return $this->respondWithSuccess(sprintf('List of Subgroups'), 
       $query, 
       $method,
      $param,
      $groups);
    }

/**
     * @Route("/api/subgroup/delete", name="subgroup_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $subgroup = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($subgroup);
            $this->em->flush();
            
            $query = "api/subgroup/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Subgroup %s successfully delete', 
                                                      $subgroup->getName()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/subgroup/update", name="subgroup_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $name = $request->get('name');
            $description = $request->get('description');
            $image = $request->get('image');
    
           $subgroup = $this->repository->findOneBy(['id' => $id]);
    
    
            if($name)
            {
                $subgroup->setName($name);
                
            }
    
            if($description)
            {
                $subgroup->setDescription($description);
                
            }
    
            if($image)
            {
                $subgroup->setImage($image);
                
            }
    
            $this->em->persist($subgroup);
            $this->em->flush();

            $query = "api/subgroup/update";
            $method = "Put";
            $param = [
                 'name ' => $name,
                 'description ' => $description,
                 'image ' => $image,

                  ];
        $data = ['id' => $subgroup->getId(),
                 'name ' => $subgroup->getName(),
                 'description ' => $subgroup->getDescription(),
                 'image ' => $subgroup->getImage()
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The Subgroup named %s has been successfully update', 
                                         $subgroup->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }

}
