<?php

namespace App\Controller;

use App\Entity\Result;
use App\Repository\ResultRepository;
use App\Repository\QuestionRepository;
use App\Repository\ObservationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class ResultController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                ResultRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

     /**
     * @Route("api/result/create", name="result_create", methods={"POST"})
     */
    public function createAction(Request $request,
                                 QuestionRepository $QuestionRepository,
                                 ObservationRepository $ObservationRepository): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        //$id = $request->get('id');
        $content = $request->get('content');
         $observation = $request->get('content');
        $question_id = $request->get('question_id');
       $observation_id = $request->get('observation_id');
        $question = $QuestionRepository->findOneBy(['id' => $question_id]);
        $observation = $ObservationRepository->findOneBy(['id' => $observation_id]);

        if (empty($content)) {
            return $this->respondValidationError("All fields are required");
        }
        
        $rel = $this->repository->findOneBy([], ['id' => 'DESC']);
        $id = $rel->getId() + 1; 

        $result = new Result();
        $result->setId($id);
        $result->setContent($content);
        $result->setQuestion($question);
        if($observation){
             $result->setObservation($observation);
        }
      

        $this->em->persist($result);
        $this->em->flush();

        $query = "api/result/create";
        $method = "POST";
        $param = [
                 'content' => $content,
                 'question_id ' => $question_id,
                 'observation_id ' => $observation_id,
                  ];
        $data = ['id' => $result->getId(),
                'content ' => $result->getContent(),
                'question ' => $result->getQuestion(),
                'observation ' => $result->getObservation(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The result has been successfully created'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }
    
     /**
     * @Route("api/result/migrate", name="migreresult_create", methods={"POST"})
     */
    public function migrationAction(Request $request,
                                 QuestionRepository $QuestionRepository,
                                 ObservationRepository $ObservationRepository): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $id = $request->get('id');
        $content = $request->get('content');
         $observation = $request->get('content');
        $question_id = $request->get('question_id');
       $observation_id = $request->get('observation_id');
        $question = $QuestionRepository->findOneBy(['id' => $question_id]);
        $observation = $ObservationRepository->findOneBy(['id' => $observation_id]);

        if (empty($content)) {
            return $this->respondValidationError("All fields are required");
        }

        $result = new Result();
        $result->setId($id);
        $result->setContent($content);
        $result->setQuestion($question);
        if($observation){
             $result->setObservation($observation);
        }
      

        $this->em->persist($result);
        $this->em->flush();

        $query = "api/result/create";
        $method = "POST";
        $param = [
                 'content' => $content,
                 'question_id ' => $question_id,
                 'observation_id ' => $observation_id,
                  ];
        $data = ['id' => $result->getId(),
                'content ' => $result->getContent(),
                'question ' => $result->getQuestion(),
                'observation ' => $result->getObservation(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The result has been successfully created'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/result/read  ", name="result_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $result = $this->repository->findOneBy(['id' => $id]);

        $query = "api/result/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $result->getId(),
                'content ' => $result->getContent(),
                'question ' => $result->getQuestion(),
                'observation ' => $result->getObservation(),
                
          ];  
        return $this->respondWithSuccess(sprintf('infos of result'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }
           
     /**
     * @Route("/api/result/list  ", name="result_list", methods={"GET"})
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/result/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $results= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $resultsNormalizer = $normalizer->normalize($results, null, ['groups' => 'result:read']);
       
           return $this->respondWithSuccess(sprintf('List of results'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $resultsNormalizer);
        }

        
        $results = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $resultsNormalizer = $normalizer->normalize($results, null, ['groups' => 'result:read']);
       
        return $this->respondWithSuccess(sprintf('List of results'), 
       $query, 
       $method,
      $param,
      $resultsNormalizer);
    }

/**
     * @Route("/api/result/delete", name="result_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $result = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($result);
            $this->em->flush();
            
            $query = "api/result/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Result successfully delete'),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/result/update", name="result_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request, QuestionRepository $QuestionRepository,
                                 ObservationRepository $ObservationRepository): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $content = $request->get('content');
            $question_id = $request->get('question_id');
            $observation_id = $request->get('observation_id');
            $question = $QuestionRepository->findOneBy(['id' => $question_id]);
            $observation = $ObservationRepository->findOneBy(['id' => $observation_id]);
        
            $result = $this->repository->findOneBy(['id' => $id]);
                
                     
            if($content)
            {
                $result->setContent($content);
                
            }

            if($question)
            {
                $result->setQuestion($question);
                
            }
            if($observation)
            {
                $result->setObservation($observation);
                
            }
  
    
            $this->em->persist($result);
            $this->em->flush();

            $query = "api/site/update";
            $method = "PUT";
            $param = [
                'content' => $content,
                'question_id ' => $question_id,
                'observation_id ' => $observation_id,
                 ];
       $data = ['id' => $result->getId(),
               'content ' => $result->getContent(),
               'question ' => $result->getQuestion(),
               'observation ' => $result->getObservation(),
                
                 ];         
        return $this->respondWithSuccess(sprintf('The result has been successfully update'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
}
