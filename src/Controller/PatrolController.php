<?php

namespace App\Controller;

use App\Entity\Patrol;
use App\Repository\PatrolRepository;
use App\Repository\ObservationRepository;
use App\Repository\UserRepository;
use App\Repository\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class PatrolController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                PatrolRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

       /**
     * Register.
     *
     * @Route("api/patrol/create", name="patrol_create", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Patrol::class, groups={"patrol"}))  
     *     )
     * )
     * @OA\Parameter(
     *     name="projectId",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="startDate",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="endDate",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="gpsStartCoordX",
     *     in="query", 
     *     @OA\Schema(type="float")
     * ),
     * @OA\Parameter(
     *     name="gpsStartCoordY",
     *     in="query",
     *     @OA\Schema(type="float")
     * ),
     * @OA\Parameter(
     *     name="gpsEndCoordX",
     *     in="query",
     *     @OA\Schema(type="float")
     * ),
     *  @OA\Parameter(
     *     name="gpsEndCoordY",
     *     in="query",
     *     @OA\Schema(type="float")
     * ),
     *  @OA\Parameter(
     *     name="totalPatrolTime",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="totalDistance",
     *     in="query",
     *     @OA\Schema(type="float")
     * ),
     *  @OA\Parameter(
     *     name="note",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="mobile")
     */
    public function createAction(Request $request, ProjectRepository $ProjectRepository,
                                  UserRepository $UserRepository): Response
    {

        $request = $this->transformJsonBody($request);
       
        /*$start_date1 = $request->get('start_date');
        $start_date2 = $start_date1->format('y-m-y H:i:s');
        $start_date = \DateTime::createFromFormat('Y-m-d H:i:s', $start_date2); */
        
         $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');
        $gpsStartCoordX = $request->get('gpsStartCoordX');
        $gpsStartCoordY = $request->get('gpsStartCoordY');
        $gpsEndCoordX = $request->get('gpsEndCoordX');
        $gpsEndCoordY = $request->get('gpsEndCoordY');
        $totalPatrolTime = $request->get('totalPatrolTime');
        $totalDistance = $request->get('totalDistance');
        $note = $request->get('note');
       // $collectorId = $request->get('collectorId');
        $projectId = $request->get('projectId');
       // $collector = $UserRepository->findOneBy(['id' => $collectorId]);
        $project = $ProjectRepository->findOneBy(['id' => $projectId]);
        //dd($site);

       /* if (empty($start_date) || empty($end_date) || empty($coord_xstart) || empty($coord_ystart) || empty($coord_xend) || empty($coord_yend) || empty($collectorId) || empty($projectId)) {
            return $this->respondValidationError("All fields are required");
        }*/
        
         $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       $username = $decodedJwtToken['username'];
        $user = $UserRepository->findOneBy(['email' => $username]);

        $patrol = new Patrol();
        $patrol->setStartDate(new \DateTimeImmutable($startDate));
        $patrol->setEndDate(new \DateTimeImmutable($endDate));
        $patrol->setCoordXStart($gpsStartCoordX);
        $patrol->setCoordYStart($gpsStartCoordY);
        $patrol->setCoordXEnd($gpsEndCoordX);
        $patrol->setCoordYEnd($gpsEndCoordY);
        $patrol->setTotalPatrolTime(new \DateTimeImmutable($totalPatrolTime));
        $patrol->setTotalDistance($totalDistance);
        $patrol->setNote($note);
        $patrol->setProject($project);
        $patrol->setCollector($user);
         

        $this->em->persist($patrol);
        $this->em->flush();

        $query = "api/patrol/create";
        $method = "POST";
        $param = [
                 'start_date' => $startDate,
                 'end_date' => $endDate,
                 'coord_xstart' => $gpsStartCoordX,
                 'coord_ystart' => $gpsStartCoordY,
                 'coord_xend' => $gpsEndCoordX,
                 'coord_yend' => $gpsEndCoordY,
                 'project_id ' => $projectId,
                 'collector ' => $username,
                  ];
        $data = ['id' => $patrol->getId(),
                'start_date' => $patrol->getStartDate(),
                'end_date' => $patrol->getendDate(),
                'coord_xstart' => $patrol->getCoordXStart(),
                'coord_ystart' => $patrol->getCoordYStart(),
                'coord_xend' => $patrol->getCoordXEnd(),
                'coord_yend' => $patrol->getCoordYEnd(),
                'project ' => $patrol->getProject(),
                'collector ' => $patrol->getCollector(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The patrol has been successfully created'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       
    }

     
       /**
     * Read  .
     *
     * @Route("/api/patrol/read  ",  methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Return info to a patrol",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Patrol::class, groups={"patrol"}))
     *     )
     * ),
     * @OA\Parameter(
     *     name="patrolId",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="patrol")
     */
    public function readAction(Request $request): Response 
    {

       
            $request = $this->transformJsonBody($request);
            $patrolId = $request->get('patrolId');
            $patrol = $this->repository->findOneBy(['id' => $patrolId]);
              
             if($patrol){
                  
            
                $query = "api/patrol/read";
                $method = "GET";
                $param = ['id' => $patrolId];
                $data = ['id' => $patrol->getId(),
                        'start_date' => $patrol->getStartDate(),
                        'end_date' => $patrol->getendDate(),
                        'coord_xstart' => $patrol->getCoordXStart(),
                        'coord_ystart' => $patrol->getCoordYStart(),
                        'coord_xend' => $patrol->getCoordXEnd(),
                        'coord_yend' => $patrol->getCoordYEnd(),
                        'projectId ' => $patrol->getProject()->getId(),
                        'project ' => $patrol->getProject()->getName(),
                        'collector ' => $patrol->getCollector()->getFirstName(),
                         
                          ];   
                return $this->respondWithSuccess(sprintf('infos of patrol '), 
                                                 $query, 
                                                 $method,
                                                $param,
                                                $data);
             }else{
                return $this->respondValidationError("This id does not exist"); 
             }
              
    }
           
    
     /**
      * List.
     *
     * @Route("/api/patrol/list  ", methods={"GET"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the observation's information",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Patrol::class, groups={"patrol"}))
     *     )
     * )
     *  @OA\Parameter(
     *     name="projectId",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="patrol")
     */
    public function listAction(Request $request, NormalizerInterface $normalizer,
                                ObservationRepository $ObservationRepository,
                                ProjectRepository $ProjectRepository): Response 
    {

       $request = $this->transformJsonBody($request);
        $projectId = $request->get('projectId');
        $project = $ProjectRepository->findOneBy(['id' => $projectId ] );

        $query = "api/patrol/list";
        $method = "GET";
        $param = ['NULL']; 

       /* if(!$page)
        {
            $patrols= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $patrolsNormalizer = $normalizer->normalize($patrols, null, ['groups' => 'patrol:read']);
       
           return $this->respondWithSuccess(sprintf('List of patrols'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $patrolsNormalizer);
        }

        
        $patrols = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $patrolsNormalizer = $normalizer->normalize($patrols, null, ['groups' => 'patrol:read']);*/
        
        $tab = array();
        $i = 0;
        
        $patrols = $this->repository->findBy(['project' => $project]);    
        foreach($patrols as $pat){   
            $tab[$i]['id'] = $pat->getId();
            $tab[$i]['projectName'] = $pat->getProject()->getName();
            $tab[$i]['observations'] = count($ObservationRepository->findBy(['patrol' => $pat]));
            $tab[$i]['valObs'] = count($ObservationRepository->findBy(['patrol' => $pat , 'status' => 1]));
            $tab[$i]['unValidObs'] = count($ObservationRepository->findBy(['patrol' => $pat , 'status' => 0]));
            if($pat->getCollector()){
                $tab[$i]['author'] = $pat->getCollector()->getFirstName();
            }else{
                
            $tab[$i]['author'] = "";
            }
           $tab[$i]['date'] = $pat->getDateEnreg();
           $tab[$i]['startTime'] = $pat->getStartDate();
           $tab[$i]['endTime'] = $pat->getEndDate();
           $tab[$i]['gpsStartCoordX'] = $pat->getCoordXStart();
           $tab[$i]['gpsStartCoordY'] = $pat->getCoordYStart();
           $tab[$i]['gpsEndCoordX'] = $pat->getCoordXEnd();
           $tab[$i]['gpsEndCoordY'] = $pat->getCoordYEnd();
           $tab[$i]['totalPatrolTime'] = $pat->getTotalPatrolTime();
           $tab[$i]['totalDistance'] = $pat->getTotalDistance();
           $tab[$i]['note'] = $pat->getNote();
           
           $i++; 
        }
        return $this->respondWithSuccess(sprintf('List of patrols'), 
       $query, 
       $method,
      $param,
      $tab);
    }

/**
     * @Route("/api/patrol/delete", name="patrol_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $patrol = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($patrol);
            $this->em->flush();
            
            $query = "api/patrol/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Patrol successfully delete'),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/patrol/update", name="patrol_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request, ProjectRepository $ProjectRepository,
                                UserRepository $UserRepository): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $start_date = $request->get('start_date');
            $end_date = $request->get('end_date');
            $coord_xstart = $request->get('coord_xstart');
            $coord_ystart = $request->get('coord_ystart');
            $coord_xend = $request->get('coord_xend');
            $coord_yend = $request->get('coord_yend');
            $collector_id = $request->get('collector_id');
            $project_id = $request->get('project_id');
            $collector = $UserRepository->findOneBy(['id' => $collector_id]);
            $project = $ProjectRepository->findOneBy(['id' => $project_id]);
        
           $patrol = $this->repository->findOneBy(['id' => $id]);
    
    
    
            if($start_date)
            {
                $patrol->setStartDate(new \DateTimeImmutable($start_date));
                
            }

            if($end_date)
            {
                $patrol->setStartDate(new \DateTimeImmutable($end_date));
                
            }
            if($coord_xstart)
            {
                $patrol->setCoordXStart($coord_xstart);
                
            }
            if($coord_ystart)
            {
                $patrol->setCoordYStart($coord_ystart);
                
            }
            if($coord_xend)
            {
                $patrol->setCoordXEnd($coord_xend);
                
            }
            if($coord_yend)
            {
                $patrol->setCoordYEnd($coord_yend);
                
            }
            if($collector)
            {
                $patrol->setCollector($collector);
                
            }
            if($project)
            {
                $patrol->setProject($project);
                
            }





    
            $this->em->persist($patrol);
            $this->em->flush();

            $query = "api/patrol/update";
            $method = "PUT";
            $param = [
                'start_date' => $start_date,
                'end_date' => $end_date,
                'coord_xstart' => $coord_xstart,
                'coord_ystart' => $coord_ystart,
                'coord_xend' => $coord_xend,
                'coord_yend' => $coord_yend,
                'project_id ' => $project_id,
                'collector_id ' => $collector_id,
                 ];
       $data = ['id' => $patrol->getId(),
               'start_date' => $patrol->getStartDate(),
               'end_date' => $patrol->getendDate(),
               'coord_xstart' => $patrol->getCoordXStart(),
               'coord_ystart' => $patrol->getCoordYStart(),
               'coord_xend' => $patrol->getCoordXEnd(),
               'coord_yend' => $patrol->getCoordYEnd(),
               'project ' => $patrol->getProject(),
               'collector ' => $patrol->getCollector(),
                
                 ];          
        return $this->respondWithSuccess(sprintf('The patrol has been successfully update'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
       
       
       /**
     * List of observations valid of one patrol of a user  .
     *
     * @Route("/api/Patrol/observationValid", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Patrol::class, groups={"patrol"}))
     *     )
     * )
     *  @OA\Parameter(
     *     name="projectId",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="patrolId",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="patrol")
     */
       public function ListObservationValidByPatrolAction(Request $request,NormalizerInterface $normalizer,
                                  ObservationRepository $ObservationRepository,
                                  ProjectRepository $ProjectRepository): JsonResponse
    {
         
        
      
         
       
            $request = $this->transformJsonBody($request);
            $patrolId = $request->get('patrolId');
            $projectId = $request->get('projectId');
            //$page = $request->get('NumPage');
             $patrol = $this->repository->findOneBy(['id' => $patrolId]);
             $project = $ProjectRepository->findOneBy(['id' => $projectId]);
             
                if($patrol){
            $query = "api/patrol/observationValid/list";
            $method = "GET";
            $param = ['ID' => $patrolId]; 
              
             
                  
                      $i = 0;
                      $obs = array();
                      $images = array();
                      
           
                 //$observations = $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC',),(20),(20-20));
                 $observations = $ObservationRepository->findBy(['project' => $project, 'patrol' => $patrol, 'status' => 1], array('id' => 'DESC'));
                 foreach($observations as $ob){
                     
                           // $obs[$i]['email'] = $ob->getUser()->getEmail();
                               
                              
                                    $obs[$i]['patrolId'] = $ob->getPatrol()->getId(); 
                             $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['projectId'] = $ob->getProject()->getId();
                            $obs[$i]['projectName'] = $ob->getProject()->getName();
                            $obs[$i]['idInaturalist'] = $ob->getIdInaturalist();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['collector'] = $ob->getCollector();
                           // if($obs['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/'.$ob->getImg1();
                           //$images['img1'] =  'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                           if($ob->getImg1() == "observation1.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.testsirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.testsirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.testsirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.testsirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                            }
                           // $obs[$i]['image'] = $ob->getImg1();
                           $obs[$i]['images'] = $images ;
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservationId'] = $ob->getTypeObservation()->getId();
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "";
                            }
                            if($ob->getSpecie()){
                            $obs[$i]['specieId'] = $ob->getSpecie()->getId();
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                             if($ob->getSpecie()->getSubgroup() == NULL){
                                  $obs[$i]['family'] = "";
                           
                            }else{
                                $obs[$i]['familyId'] = $ob->getSpecie()->getSubgroup()->getId();
                                $obs[$i]['family'] = $ob->getSpecie()->getSubgroup()->getName();
                                
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                            $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            }
                            }else{
                                $obs[$i]['specie'] = "";
                            }
                            
                        
                            if($ob->getUser()){
                            $obs[$i]['user'] = $ob->getUser()->getEmail();
                            }else{
                                $obs[$i]['user'] = "";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                             $obs[$i]['updatedate'] = $ob->getUpdateDate();
                           if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                            $obs[$i]['siteId'] = $ob->getSegment()->getSite()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";            
                              $obs[$i]['site'] =  "";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                           
                            if($ob->getSite()){
                                 $obs[$i]['siteId'] = $ob->getSite()->getId();
                                 $obs[$i]['site'] = $ob->getSite()->getName();
                            
                           }else{
                             $obs[$i]['site'] =  "";
                             }
                            /* if($ob->getSegment()->getSite() ==  'Null'){
                                  $obs[$i]['site'] =  "NULL";
                            
                           }else{
                               $obs[$i]['siteId'] = $ob->getSegment()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getName();
                             }*/
                           
                          /* if($ob->getSegment()->getSite()){
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['site'] =  "NULL";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }*/
                           
                           $i++;
                 }
                  
                   $observationsNormalizer = $normalizer->normalize($observations, null, ['groups' => 'observation:read']);
      
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $obs);
            
             
    }else{
                 return $this->respondValidationError("This id does not exist ");
             }
           
        
    }
    
     
       /**
     * List of observations unvalid of one patrol of a user  .
     *
     * @Route("/api/Patrol/observationUnvalid", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Patrol::class, groups={"patrol"}))
     *     )
     * )
     *  @OA\Parameter(
     *     name="projectId",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="patrolId",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="patrol")
     */
       public function ListObservationUnvalidByPatrolAction(Request $request,NormalizerInterface $normalizer,
                                  ObservationRepository $ObservationRepository,
                                  ProjectRepository $ProjectRepository): JsonResponse
    {
         
        
      
         
       
            $request = $this->transformJsonBody($request);
            $patrolId = $request->get('patrolId');
            $projectId = $request->get('projectId');
            //$page = $request->get('NumPage');
             $patrol = $this->repository->findOneBy(['id' => $patrolId]);
             $project = $ProjectRepository->findOneBy(['id' => $projectId]);
             
             if($patrol){
             
            $query = "api/patrol/observationUnvalid/list";
            $method = "GET";
            $param = ['ID' => $patrolId]; 
              
             
                  
                      $i = 0;
                      $obs = array();
                      $images = array();
                      
           
                 //$observations = $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC',),(20),(20-20));
                 $observations = $ObservationRepository->findBy(['project' => $project, 'patrol' => $patrol, 'status' => 0], array('id' => 'DESC'));
                 foreach($observations as $ob){
                     
                           // $obs[$i]['email'] = $ob->getUser()->getEmail();
                           
                                    $obs[$i]['patrolId'] = $ob->getPatrol()->getId();
                             $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['projectId'] = $ob->getProject()->getId();
                            $obs[$i]['projectName'] = $ob->getProject()->getName();
                            $obs[$i]['idInaturalist'] = $ob->getIdInaturalist();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['collector'] = $ob->getCollector();
                           // if($obs['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/'.$ob->getImg1();
                           //$images['img1'] =  'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                           if($ob->getImg1() == "observation1.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.testsirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.testsirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.testsirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.testsirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                            }
                           // $obs[$i]['image'] = $ob->getImg1();
                           $obs[$i]['images'] = $images ;
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservationId'] = $ob->getTypeObservation()->getId();
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "";
                            }
                            if($ob->getSpecie()){
                            $obs[$i]['specieId'] = $ob->getSpecie()->getId();
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                             if($ob->getSpecie()->getSubgroup() == NULL){
                                  $obs[$i]['family'] = "";
                           
                            }else{
                                $obs[$i]['familyId'] = $ob->getSpecie()->getSubgroup()->getId();
                                $obs[$i]['family'] = $ob->getSpecie()->getSubgroup()->getName();
                                
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                            $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            }
                            }else{
                                $obs[$i]['specie'] = "";
                            }
                            
                        
                            if($ob->getUser()){
                            $obs[$i]['user'] = $ob->getUser()->getEmail();
                            }else{
                                $obs[$i]['user'] = "";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                             $obs[$i]['updatedate'] = $ob->getUpdateDate();
                           if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                            $obs[$i]['siteId'] = $ob->getSegment()->getSite()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";            
                              $obs[$i]['site'] =  "";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                           
                            if($ob->getSite()){
                                 $obs[$i]['siteId'] = $ob->getSite()->getId();
                                 $obs[$i]['site'] = $ob->getSite()->getName();
                            
                           }else{
                             $obs[$i]['site'] =  "";
                             }
                            /* if($ob->getSegment()->getSite() ==  'Null'){
                                  $obs[$i]['site'] =  "NULL";
                            
                           }else{
                               $obs[$i]['siteId'] = $ob->getSegment()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getName();
                             }*/
                           
                          /* if($ob->getSegment()->getSite()){
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['site'] =  "NULL";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }*/
                           
                           $i++;
                 }
                  
                   $observationsNormalizer = $normalizer->normalize($observations, null, ['groups' => 'observation:read']);
      
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $obs);
             }else{
                 return $this->respondValidationError("This id does not exist ");
             }
            
             
     
           
        
    }
    
}
