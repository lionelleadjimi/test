<?php

namespace App\Controller;

use App\Entity\Question;
use App\Repository\QuestionRepository;
use App\Repository\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class QuestionController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                QuestionRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

     
      /**
     * Create.
     *
     * @Route("api/question/create", name="question_create", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the question's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Question::class, groups={"question"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="title",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="typeAnswer",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="typeBegin",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="interval",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="projectId",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="nextQuestionId",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="image",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="question")
     */
    public function createAction(Request $request,
                                 QuestionRepository $QuestionRepository,
                                 ProjectRepository $ProjectRepository): JsonResponse
    {
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);

        $title = $request->get('title');
        $typeAnswer = $request->get('typeAnswer');
        $typeBegin = $request->get('typeBegin');
        $interval = $request->get('interval');
        $projectId = $request->get('projectId');
        $project = $ProjectRepository->findOneBy(['id' => $projectId ]);
        $nextQuestionId = $request->get('nextQuestionId');
        $nextQuestion = $QuestionRepository->findOneBy(['id' => $nextQuestionId ]);
        $image = $request->get('image');
/*
        if (empty($title) || empty($typeAnswer) || empty($typeBegin) || empty($interval)) {
            return $this->respondValidationError("All fields are required");
        }*/
         $quest = $this->repository->findOneBy([], ['id' => 'DESC']);
          $id = $quest->getId() + 1;
          
        $question = new Question();
        $question->setId($id);
        $question->setTitle($title);
        $question->setTypeAnswer($typeAnswer);
        $question->settypeBegin($typeBegin);
        $question->setIntervale($interval);
        $question->setImage($image);
        $question->setProject($project);
        $question->setNextQuestion($nextQuestion);

       if ($image)
        {
           
                            $photo = $image;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/questions/';
                                   // dd($webPath);
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                   
                                    $question->setImage($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $question->setImage('question.png');
        }


        $this->em->persist($question);
        $this->em->flush();

        $query = "api/question/create";
        $method = "POST";
        $param = [
                 'title ' => $title,
                 'type answer ' => $typeAnswer,
                 'type begin ' => $typeBegin,
                 'interval ' => $interval,
                 'image ' => $image,

                  ];
        $data = ['id' => $question->getId(),
                 'title ' => $question->getTitle(),
                 'type answer ' => $question->getTypeAnswer(),
                 'type begin  ' => $question->getTypeBegin(),
                 'interval ' => $question->getIntervale(),
                 'answers ' => $question->getAnswers(),
                 'projet ' => $question->getProject(),
                 'image ' => $question->getImage()
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The question named %s has been successfully created', 
                                         $question->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/question/read  ", name="question_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $question = $this->repository->findOneBy(['id' => $id]);

        $query = "api/question/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $question->getId(),
                'title ' => $question->getTitle(),
                'type answer ' => $question->getTypeAnswer(),
                'type begin  ' => $question->getTypeBegin(),
                'interval ' => $question->getIntervale(),
                'answers ' => $question->getAnswers(),
                'projet ' => $question->getProject(),
                'image ' => $question->getImage()
                        ];  
        return $this->respondWithSuccess(sprintf('infos of question %s', 
                                         $question->gettitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }


     /**
     * @Route("/api/question/list  ", name="question_list", methods={"GET"})
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/question/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $questions= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $questionsNormalizer = $normalizer->normalize($questions, null, ['groups' => 'question:read']);
       return $this->respondWithSuccess(sprintf('List of questions'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $questionsNormalizer,);
        }

        
        $question = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));


        $questionsNormalizer = $normalizer->normalize($questions, null, ['groups' => 'question:read']);
        // return $this->json("list ofgroups", $groups, 201, [], ['groups' => 'group:read']);
       return $this->respondWithSuccess(sprintf('List of questions'), 
       $query, 
       $method,
      $param,
      $questionsNormalizer);
    }

/**
     * @Route("/api/question/delete", name="question_delete", methods={"DELETE"})
     */
    public function groupdeleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $question = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($question);
            $this->em->flush();
            
            $query = "api/question/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Question %s successfully delete', 
                                                      $question->getTitle()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/question/update", name="question_update", methods={"POST"})
     */
    public function UpdateAction(Request $request): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
             $question_next_id = $request->get('nextQuestion');
          
           // var_dump($question_next_id); die();
            
            $title = $request->get('title');
            $typeAnswer = $request->get('type_answer');
            $typeBegin = $request->get('type_begin');
            $interval = $request->get('intervale');
            $projet = $request->get('projet');
            $image = $request->get('image');
    
           $question = $this->repository->findOneBy(['id' => $id]);
    
           if($typeAnswer)
            {
                $question->setTypeAnswer($typeAnswer);
                
            }
            
            
            if($question_next_id == NULL)
            { 
                
                $question->setNextQuestion($question_next_id);
                
            }
            else{
                $nextquestion = $this->repository->findOneBy(['id' => $question_next_id]);
                $question->setNextQuestion($nextquestion);
            }
    
    
            if($title)
            {
                $question->setTitle($title);
                
            }
    
            if($typeAnswer)
            {
                $question->setTypeAnswer($typeAnswer);
                
            }

            if($typeBegin)
            {
                $question->setTypeBegin($typeBegin);
                
            }

            if($interval)
            {
                $question->setIntervale($interval);
                
            }
    
            if ($image)
            {
               
                                $photo = $image;
                               
                                    
    
                                try {
                                    if ($photo != NULL) {
                                        $photo = explode(',', $photo);
                                        $extension = str_replace('data:image/', '', $photo[0]);
                                        $extension = str_replace(';base64', '', $extension);
                                       // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                        $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/questions/';
                                       // dd($webPath);
                                        $sourcename = $webPath . uniqid() . '.jpg';
    
                                        $file = fopen($sourcename, 'w+');
                                        fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                        fclose($file);
    
                                        $photoUrl = str_replace($webPath, '', $sourcename);
                                       
                                        $question->setImage($photoUrl);
                                    }
                                } catch (\Exception $e) {
                                    $json["success"] = false;
                                    $json["error"] = "image";
                                    $json["message"] = $e->getMessage();
                                }
            }
            else
            {
                $question->setImage('question.png');
            }
    
            $this->em->persist($question);
            $this->em->flush();

            $query = "api/answer/update";
            $method = "PUT";
            $param = [
                'title ' => $title,
                'type answer ' => $typeAnswer,
                'type begin ' => $typeBegin,
                'interval ' => $interval,
                'image ' => $image,

                 ];
       $data = ['id' => $question->getId(),
                'title ' => $question->getTitle(),
                'type answer ' => $question->getTypeAnswer(),
                'type begin  ' => $question->getTypeBegin(),
                'interval ' => $question->getIntervale(),
                'answers ' => $question->getAnswers(),
                'projet ' => $question->getProject(),
                'image ' => $question->getImage()
                
                 ];          
        return $this->respondWithSuccess(sprintf('The question named %s has been successfully update', 
                                         $question->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
}
