<?php

namespace App\Controller;

use App\Entity\Feedback;
use App\Repository\FeedbackRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class FeedbackController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                FeedbackRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

     /**
     * @Route("api/feedback/create", name="feedback_create", methods={"POST"})
     */
    public function createAction(Request $request): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $content = $request->get('content');
        $name = $request->get('name');
        $email = $request->get('email');
        if (empty($content) || empty($name) || empty($email)) {
            return $this->respondValidationError("All fields are required");
        }

        $feedback = new Feedback();
        $feedback->setContent($content);
        $feedback->setName($name);
        $feedback->setEmail($email);
        

        $this->em->persist($feedback);
        $this->em->flush();

        $query = "api/feedback/create";
        $method = "POST";
        $param = [
                 'content ' => $content,
                 'name' => $name,
                 'email ' => $email,
                  ];
        $data = ['id' => $feedback->getId(),
                'content ' => $feedback->getContent(),
                'name ' => $feedback->getName(),
                'email ' => $feedback->getEmail(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The feedback named %s has been successfully created', 
                                         $feedback->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/feedback/read  ", name="feedback_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $feedback = $this->repository->findOneBy(['id' => $id]);

        $query = "api/feedback/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $feedback->getId(),
        'content ' => $feedback->getContent(),
                'name ' => $feedback->getName(),
                'sigle ' => $feedback->getEmail(),
                        ];  
        return $this->respondWithSuccess(sprintf('infos of feedback name %s', 
                                         $feedback->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }
           
     /**
     * @Route("/api/feedback/list  ", name="feedback_list", methods={"GET"})
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/feedback/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $feedback= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $feedbackNormalizer = $normalizer->normalize($feedback, null, ['groups' => 'feedback:read']);
       
           return $this->respondWithSuccess(sprintf('List of feedbacks'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $feedbackNormalizer);
        }

        
        $feedback = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $feedbackNormalizer = $normalizer->normalize($feedback, null, ['groups' => 'feedback:read']);
       
        return $this->respondWithSuccess(sprintf('List of feedbacks'), 
       $query, 
       $method,
      $param,
      $feedbackNormalizer);
    }

/**
     * @Route("/api/feedback/delete", name="feedback_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $feedback = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($feedback);
            $this->em->flush();
            
            $query = "api/feedback/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('feedback %s successfully delete', 
                                                      $feedback->getName()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/feedback/update", name="feedback_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $name = $request->get('name');
            $content = $request->get('content');
            $email = $request->get('email');
        
           $feedback = $this->repository->findOneBy(['id' => $id]);
    
    
    
            if($name)
            {
                $feedback->setName($name);
                
            }

            if($content)
            {
                $feedback->setContent($content);
                
            }

            if($email)
            {
                $feedback->setEmail($email);
                
            }

    
            $this->em->persist($feedback);
            $this->em->flush();

            $query = "api/feedback/update";
            $method = "PUT";
            $param = [
                'content ' => $content,
                'name' => $name,
                'email ' => $email,
                 ];
            $data = ['id' => $feedback->getId(),
            'content ' => $feedback->getContent(),
                    'name ' => $feedback->getName(),
                    'email ' => $feedback->getEmail(),
                        
                        ];          
        return $this->respondWithSuccess(sprintf('The feedback white message %s has been successfully update', 
                                         $feedback->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
}
