<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\User;
use App\Entity\UserProject;
use App\Entity\Site;
use App\Entity\Segment;
use App\Repository\ProjectRepository;
use App\Repository\UserProjectRepository; 
use App\Repository\UserRepository;
use App\Repository\SiteRepository;
use App\Repository\SegmentRepository;
use App\Repository\CountryRepository;
use App\Repository\QuestionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class ProjectController extends ApiController 
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                ProjectRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

    
    
      /**
     * Register.
     *
     * @Route("/api/project/create", name="project_create", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the projects's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Project::class, groups={"project"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="name",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     *  @OA\Parameter(
     *     name="location",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="organization",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="country_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="timeZone",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="public",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="siteSegments",
     *     in="query",
     *     @OA\Schema(type="array()")
     * )
     * 
     * @OA\Tag(name="project")
     */ 
    public function createAction(Request $request, QuestionRepository $QuestionRepository,
                                   UserRepository $UserRepository,
                                   UserProjectRepository $UserProjectRepository,
                                   CountryRepository $CountryRepository): Response
    {
     $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
        $request = $this->transformJsonBody($request);
        $username = $decodedJwtToken['username'];
        $email1 = "admin@ammco.org";
        $user = $UserRepository->findOneBy(['email' => $username]);
        $users = $UserRepository->findBy(['email' => [$username, $email1] ]);
          $siteSegments = $request->get('siteSegments');  
        
          
        $name=$request->get('name');
        $location=$request->get('location');
        $organization = $request->get('organization'); 
        $country_id = $request->get('country_id');
        $country = $CountryRepository->findOneBy(['id' => $country_id]);
        $admin_id = $request->get('admin_id');
       // $admin = $UserRepository->findOneBy(['id' => $admin_id]);
        $public = $request->get('public');
        $timeZone = $request->get('timeZone');
        //var_dump($timeZone);
       // $id = $request->get('id');
       
        $note = $request->get('note');
        $maplink = $request->get('maplink');
        $question_id = $request->get('question_id');
        $color = $request->get('color');
        $legalNotices = $request->get('legal_notices');
        $websiteLink = $request->get('website_link');
        $gpsPrecision = $request->get('gps_precision');
        $image = $request->get('image');
        $coordX =  $request->get('coord_x');
        $coordY =  $request->get('coord_y');
        $zoom =  $request->get('zoom'); 
      


        if (empty($name) || empty($location)){ 
       return $this->respondValidationError("All fields are required");
          /* return $this->respondWithSuccess(sprintf('error register'),
                                         "Null", 
                                         "Null",
                                        ['Null'],
                                        ['name' => $name,
                                        'location' => $request->get('location'),
                                         'timeZone' => $request->get('timeZone'),
                                         'country_id' => $request->get('country_id'),
                                         'public' => $request->get('public'),
                                         'organization' =>$request->get('organization'),
                                         ]);*/
        }
        

        $project = new Project();
       // $project->setId($id);
        $project->setName($name);
        
        $project->setNote($note);
        $project->setLocation($location);
        $project->setOrganization($organization);
        $project->setMaplink($maplink);
       $project->setPublic($public);
        $project->setCountry($country);
        $project->setAdmin($user);
        $project->setColor($color);
        $project->setLegalNotices($legalNotices);
        $project->setWebsiteLink($websiteLink);
        $project->setGpsPrecision(123);
         $project->setCoordX(4.383); 
         $project->setCoordY(8.9);
        $project->setZoom(7);
         $project->setImage($image);
         $project->setTimeZone($timeZone);
       
        $question = $QuestionRepository->findOneBy(['id' => $question_id]);
          $project->setQuestion($question);
        
        if ($image)
        {
           
                            $photo = $image;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/projects/';
                                   // dd($webPath);
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                   
                                    $project->setImage($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $project->setImage('project.png');
        }
        
        $userproject = $UserProjectRepository->findOneBy(['project' => $project, 'user' => $user]);
        if($userproject)
        {
            return $this->respondValidationError("Already exist");
        }
        
        

        //$this->em->persist($userProject);
        
                $k = 0;
        
            foreach($users as $us){
                  //  var_dump($us->getId());
                
         /*$userpor = $UserProjectRepository->findOneBy([], ['id' => 'DESC']);
         $id1 = $userpor->getId() + 1;*/
          
         $userProject = new UserProject();
        //$userProject->setId($id1);
        $userProject->setProject($project);
        $userProject->setUser($us);
        $user->setProject($project);
        $userProject->setRole("Admin");
        
        $this->em->persist($userProject);
        
        
       //$this->em->flush();
        $k++;
            }
            
       
          $j = 0; 
            
            foreach($siteSegments as $siteSegment )
            {
                $i = 0;
                $site = new Site();
                $site->setName($siteSegment["site"]);
                $site->setProject($project);
                 $this->em->persist($site);
                foreach($siteSegment["segments"] as $seg){
                    
                    $segment = new Segment();
                    $segment->setName($seg);
                    $segment->setCity("Null");
                    $segment->setRegion("Null");
                    $segment->setSite($site);
                    $this->em->persist($segment);
                    
                     $i++;
                }
            
                $j++;
                
            }
        $this->em->persist($project);
         $this->em->persist($user);
        
        $this->em->flush();

        $query = "api/project/create";
        $method = "POST";
        $param = [
                 'name ' => $name,
                 'note'  => $note,
                 'location' => $location,
                 'organization' => $organization, 
                 'maplink' => $maplink, 
                 //'public' => $public, 
                 'country' => $country_id, 
                 'admin' => $admin_id, 
                 'color' => $color, 
                 'legal notices' => $legalNotices,
                 'website link' => $websiteLink,
                 'gps precision' => $gpsPrecision,
                 'image' => $image,

                  ];
        $data = ['id' => $project->getId(),
                'name' => $project->getName(),
                'note' => $project->getNote(),
                'location' => $project->getLocation(),
                'organization' => $project->getOrganization(),
                'maplink ' => $project->getMaplink(),
                'public' => $project->getPublic(),
                'country' => $project->getCountry()->getName(),
                'role' => "admin",
                'admin' => $project->getAdmin(),
                'color' => $project->getColor(),
                'legalnotices' => $project->getLegalNotices(),
                'websitelink' => $project->getWebsiteLink(),
                'gpsprecision' => $project->getGpsPrecision(),
                'image' => $project->getImage(),
                 'timezone' => $project->getTimeZone(),
                // 'siteSegments' => $tab,
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The project name %s has been successfully created', 
                                         $project->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }
    
    
    /**
     * Assign user in project.
     *
     * @Route("api/userproject/assign", name="userproject_assign", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the projects's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Project::class, groups={"admin"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Parameter(
     *     name="user_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="role",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="admin")
     */ 
    public function AddAction(Request $request, QuestionRepository $QuestionRepository,
    UserProjectRepository $UserProjectRepository,
    UserRepository $UserRepository): Response
{
                 $request = $this->transformJsonBody($request);    
                /*$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
                $username = $decodedJwtToken['username'];
                $user = $UserRepository->findOneBy(['email' => $username]);*/
                
               /* $up = $UserProjectRepository->findOneBy([], ['id' => 'DESC']);
                $id = $up->getId() + 1; */ 
                $role = $request->get('role'); 
                $project_id = $request->get('project_id');
                $user_id = $request->get('user_id');
                $project = $this->repository->findOneBy(['id' => $project_id]);
                $user = $UserRepository->findOneBy(['id' => $user_id]);
                
                
                
                
                if (empty($project) || empty($role)){ 
                    return $this->respondValidationError("All fields are required");
                  
                }
                
                
                $userProject = new UserProject();
                $userProject->setProject($project);
                $userProject->setUser($user);
                $userProject->setRole($role);
                
                $user->setProject($project);
                
                $this->em->persist($userProject);
                $this->em->persist($user);
                
                $this->em->flush();
                
                $query = "api/userproject/assign";
                $method = "POST";
                $param = [
                'user_id ' => $user->getId(),
                'project_id' => $project_id,
                'role'  => $role,
                
                ];
                $data = ['id' => $userProject->getId(),
                'project ' => $userProject->getProject()->getName(),
                'user ' => $userProject->getUser()->getEmail(),
                'role ' => $userProject->getRole(),
                
                ];          
                return $this->respondWithSuccess(sprintf('The user with email %s has been successfully add to this project', 
                          $user->getEmail()), 
                          $query, 
                          $method,
                         $param,
                         $data);
}

    
     /**
     * @Route("/api/project/read  ", name="project_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $project = $this->repository->findOneBy(['id' => $id]);

        $query = "api/project/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $project->getId(),
                 'date ' => $project->getDate(),
                'name ' => $project->getName(),
                'note ' => $project->getnote(),
                'location ' => $project->getLocation(),
                'organization ' => $project->getOrganization(),
                'maplink ' => $project->getMaplink(),
                'public ' => $project->getPublic(),
                'country ' => $project->getCountry(),
                'admin ' => $project->getAdmin(),
                'color ' => $project->getColor(),
                'legal notices ' => $project->getLegalNotices(),
                'website link ' => $project->getWebsiteLink(),
                'gps precision ' => $project->getGpsPrecision(),
                'image ' => $project->getImage(),
                'questions ' => $project->getQuestions(),
                 
                  ];   
        return $this->respondWithSuccess(sprintf('infos of project name %s', 
                                         $project->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }


    /**
     * List .
     *
     * @Route("/api/project/list  ", name="project_list", methods={"GET"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the list of projects",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Project::class, groups={"project"}))
     *     )
     * )
     * 
     * @OA\Tag(name="project")
     */   
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/project/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $projects= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $projectsNormalizer = $normalizer->normalize($projects, null, ['groups' => 'project:read']);
       
           return $this->respondWithSuccess(sprintf('List of projects'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $projectsNormalizer);
        }

        
        $projects = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $projectsNormalizer = $normalizer->normalize($projects, null, ['groups' => 'project:read']);
       
        return $this->respondWithSuccess(sprintf('List of projects'), 
       $query, 
       $method,
      $param,
      $projectsNormalizer);
    }
    
     /**
     * List projects where a user his admin  .
     *
     * @Route("/api/projectuserAdmin/list  ", name="projectuserAdmin_list", methods={"GET"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the list of projects",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Project::class, groups={"project"}))
     *     )
     * )
     * 
     * @OA\Tag(name="project")
     */   
    public function listProjecuserAdmintAction(Request $request, NormalizerInterface $normalizer,
                                UserProjectRepository $UserProjectRepository,
                                UserRepository $UserRepository,
                                SiteRepository $SiteRepository): Response 
    {

    
    
              $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
                $username = $decodedJwtToken['username'];
         $user = $UserRepository->findOneBy(['email' => $username]);
    

        $query = "api/userProject/list";
        $method = "GET";
        $param = ['NULL']; 
                    $pros = array();
            $projects= $this->repository->findBy(['admin' => $user]);
            if($projects == "Null")
            {
                $pros['id'] = $pro->getId();
                $pros['name'] = $pro->getName();
                $pros['country'] = $pro->getCountry()->getName();
                $pros['location'] = $pro->getLocation();
                $pros['organization'] = $pro->getOrganization();
                $pros['timezone'] = "null";
                $pros['public'] = $pro->getPublic();
                
                           return $this->respondWithSuccess(sprintf('No projects'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $pros);

                
            }
           // var_dump($projects); die();

            $i = 0;
            $tab = array();
            foreach($projects as $pro){
                $j = 0;
                $pros[$i]['id'] = $pro->getId();
                $pros[$i]['name'] = $pro->getName();
                $pros[$i]['country'] = $pro->getCountry()->getName();
                $pros[$i]['location'] = $pro->getLocation();
                $pros[$i]['organization'] = $pro->getOrganization();
                $pros[$i]['public'] = $pro->getPublic();
                $pros[$i]['timezone'] = $pro->getTimeZone();
                $sites= $SiteRepository->findBy(['project' => $pro]);
                foreach($sites as $sit){
                
                $tab[$j]['siteId'] = $sit->getId();
                $tab[$j]['site'] = $sit->getName();
                $tab[$j]['segment'] = $normalizer->normalize($sit->getSegments(), null, ['groups' => 'segment:read']);
                $j++;
            }
            
                $pros[$i]['siteSegments'] = $tab;
                $i++;
            }
           
       
           return $this->respondWithSuccess(sprintf('List of projects'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $pros);

    }
    
    
    
    
    /**
     * List user on this project .
     *
     * @Route("/api/userproject/list  ", name="userproject_list", methods={"GET"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the list of users",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Project::class, groups={"project"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * 
     * @OA\Parameter(
     *     name="role",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="project")
     */   
    public function listuserProjectAction(Request $request, NormalizerInterface $normalizer,
                                UserProjectRepository $UserProjectRepository): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
         $id = $request->get('project_id');
          $role = $request->get('role');
          //echo $role; die();
         $project = $this->repository->findOneBy(['id' => $id]);
    

        $query = "api/userProject/list";
        $method = "GET";
        $param = ['id' => $id]; 

            $projects= $UserProjectRepository->findBy(['project' => $project], ['id' => 'DESC']);
           // var_dump($projects); die();
            $users = array();
            $i = 0;
            foreach($projects as $user){
                 $users[$i]['userprojectid'] = $user->getId();
                 $users[$i]['roleCurrentUser'] = $role;
                $users[$i]['id'] = $user->getUser()->getId();
                //$users[$i]['role'] = $user->getRole();
                 $users[$i]['role'] = $user->getRole();
                $users[$i]['firstname'] = $user->getUser()->getFirstName();
                 $users[$i]['lastname'] = $user->getUser()->getLastName();
                 $users[$i]['username'] = $user->getUser()->getUsername();
                $users[$i]['email'] = $user->getUser()->getEmail();
                $users[$i]['phone'] = $user->getUser()->getPhone();
                $users[$i]['organization'] = $user->getUser()->getOrganization();
                 $users[$i]['location'] ="";
               //$user->getUser()->getLocation();
                $users[$i]['city'] = $user->getUser()->getCity();
               $users[$i]['job'] = $user->getUser()->getFonction()->getTitle();
                $users[$i]['country'] = $user->getUser()->getCountry()->getName();
                $i++;
            }
           
       
           return $this->respondWithSuccess(sprintf('List of users'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $users);

    }
    
    
    
    /**
     * List user no project.
     *
     * @Route("/api/userNoproject/list  ", name="userNoproject_list", methods={"GET"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the list of users",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Project::class, groups={"project"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="project")
     */   
    public function listUserAction(Request $request, NormalizerInterface $normalizer,
                                UserProjectRepository $UserProjectRepository,
                                UserRepository $UserRepository): Response 
    {
     
  
        $request = $this->transformJsonBody($request); 
         $id = $request->get('project_id'); 
         $project = $this->repository->findOneBy(['id' => $id]);

        $query = "api/userNoproject/list";
        $method = "GET";
        $param = ['NULL']; 
        $k = 0;
        $j = 0;
            $usersA= $UserProjectRepository->findBy(['project' => $project]);
           $usersAok = array(); 
            foreach($usersA as $userA){
                $usersAok[$k] = $userA->getUser()->getId();
                $k++; 
            }
           
            
            $allUsers  = $UserRepository->findAll();
           $usersNNormalizer = array();
            foreach($allUsers as $userN){
                $usersNNormalizer[$j] = $userN->getId();
                $j++;
            }
            
           
             $users = array();
          $i = 0;
          $nb = 0;
       foreach($usersNNormalizer as $tab2){
               if(in_array($tab2, $usersAok) == false){
                  $user = $UserRepository->findOneBy(['id' => $tab2]);
                   $users[$i]['id'] = $tab2;
                   $users[$i]['username'] = $user->getUsername();
                    $users[$i]['email'] = $user->getEmail();
                    $users[$i]['firstname'] = $user->getFirstName();
                    $users[$i]['lastname'] = $user->getLastName();
               $i++;
               }
               
            }
            
            
           return $this->respondWithSuccess(sprintf('List of users no assigned'), 
                                         $query, 
                                         $method, 
                                        $param,
                                        $users);

    }


     
    /**
     * delete user in one project.
     *
     * @Route("/api/userproject/delete", name="userproject_delete", methods={"GET"})
     * @OA\Response(
     *     response=201,
     *     description="Returns null",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Project::class, groups={"project"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="userproject_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="project")
     */   
    public function admindeleteAction(Request $request,
                                       UserProjectRepository $UserProjectRepository,
                                       UserRepository $UserRepository): Response 
    {
        
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        

            $request = $this->transformJsonBody($request);
            $id = $request->get('userproject_id');
           
            
            $userproject = $UserProjectRepository->findOneBy(['id' => $id]);
             $user = $userproject->getUser();
           
            
            // var_dump($user->getId()); die();
            
            $this->em->remove($userproject);
              $this->em->flush();
             $project = $UserProjectRepository->findBy(['user' => $user], ['id' => 'DESC'],1);
             foreach($project as $pro){
                 $user->setProject($pro->getProject());
                   $this->em->persist($user);
                $this->em->flush();
             }
             
          

             $query = "api/userproject/POST";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['delete ok']; 
            return $this->respondWithSuccess(sprintf('project name successfully delete'),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

    }
    
     /**
     * change role.
     *
     * @Route("/api/user/changerole", name="user_changerole", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns null",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Project::class, groups={"project"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="userproject_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * 
     * @OA\Parameter(
     *     name="role",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="project")
     */   
    public function changeRoleAction(Request $request,
                                       UserProjectRepository $UserProjectRepository): Response 
    {
        

            $request = $this->transformJsonBody($request);
            $id = $request->get('userproject_id');
            $role = $request->get('role');
            
            $user = $UserProjectRepository->findOneBy(['id' => $id]);
            
            // var_dump($user->getId()); die();
            $user->setRole($role);
            
            $this->em->persist($user);
            $this->em->flush();

              $query = "api/changerole/POST";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['delete ok']; 
            return $this->respondWithSuccess(sprintf('role successfully changed'),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

    }
  
    
    /**
     * Delete.
     *
     * @Route("/api/project/delete", name="project_delete", methods={"DELETE"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the projects's information after delete",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Project::class, groups={"project"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="userproject_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="project")
     */  
    public function deleteAction(Request $request,
                                UserRepository $UserRepository,
                                UserProjectRepository $UserProjectRepository): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $email = $decodedJwtToken['username'];
        $user = $UserRepository->findOneBy(['email' => $email]);
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $project = $this->repository->findOneBy(['id' => $id]);
            $userproject = $UserProjectRepository->findOneBy(['user' => $user, 'project' => $project]);
            
            $this->em->remove($project);
            $this->em->remove($userproject);
            $this->em->flush();
            
            $query = "api/project/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('project name %s successfully delete', 
                                                      $project->getName()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


    
     
     /**
     * Update.
     *
     * @Route("api/project/update", name="project_update", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the projects's information after update",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Project::class, groups={"project"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="name",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     *  @OA\Parameter(
     *     name="location",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="organization",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="country_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="timeZone",
     *     in="query",
     *     @OA\Schema(type="string")
     *),
     * @OA\Parameter(
     *     name="public",
     *     in="query",
     *     @OA\Schema(type="integer") 
     * ),
     * @OA\Parameter(
     *     name="siteSegments",
     *     in="query",
     *     @OA\Schema(type="array()")
     * )
     * 
     * @OA\Tag(name="project")
     */  
    public function UpdateAction(Request $request,
                                 UserRepository $UserRepository,
                                 CountryRepository $CountryRepository,
                                 SiteRepository $SiteRepository,
                                 SegmentRepository $SegmentRepository): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $name = $request->get('name');
            $note = $request->get('note');
            $location = $request->get('location');
            $organization = $request->get('organization');
            $maplink = $request->get('maplink');
            $public = $request->get('public');
            $country_id = $request->get('country_id');
            $country = $CountryRepository->findOneBy(['id' => $country_id]);
            $admin_id = $request->get('admin_id');
            $color = $request->get('color');
            $legalNotices = $request->get('legal_notices');
            $websiteLink = $request->get('website_link');
            $gpsPrecision = $request->get('gps_precision');
            $timeZone = $request->get('timeZone');
            $image = $request->get('image');
            $siteSegments = $request->get('siteSegments'); 

           $project = $this->repository->findOneBy(['id' => $id]);
           
        //   var_dump($public); die();
    
    
            if($public == 0 || $public == 1)
           {     //echo "oui";
               $project->setPublic($public);
               
           }else{
               echo "non";
           }
           if($name)
           {
               $project->setName($name);
               
           }
           if($note)
           {
               $project->setNote($note);
               
           }
           if($location)
           {
               $project->setLocation($location);
               
           }
           if($organization)
           {
               $project->setOrganization($organization);
               
           }
           if($maplink)
           {
               $project->setLegalMaplink($maplink);
               
           }
           if($country)
           {
               $project->setCountry($country);
               
           }
           
           
           if($timeZone)
           {
               $project->setTimeZone($timeZone);
               
           }
          /* if($user)
           {
               $project->setAdmin($user);
               
           }*/
           if($color)
           {
               $project->setColor($color);
               
           }
           if($legalNotices)
           {
               $project->setLegalNotices($legalNotices);
               
           }
           if($websiteLink)
           {
               $project->setWebsiteLink($websiteLink);
               
           }
           if($gpsPrecision)
           {
               $project->setGpsPrecision($gpsPrecision);
               
           }
           
            if ($image)
        {
           
                            $photo = $image;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/projects/';
                                   // dd($webPath);
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                   
                                    $project->setImage($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        
        if($siteSegments){
        
         $j = 0; 
            
            foreach($siteSegments as $siteSegment )
            {
                $i = 0;
                if($siteSegment["siteId"] == -1){ 
                    $site = new Site();
                    $site->setName($siteSegment["site"]);
                    $site->setProject($project);
                     $this->em->persist($site);
                    foreach($siteSegment["segment"] as $seg){
                        if($seg["id"] == -1){
                            $segment = new Segment();
                            $segment->setName($seg["name"]);
                            $segment->setCity("Null");
                            $segment->setRegion("Null");
                            $segment->setSite($site);
                            $this->em->persist($segment);
                        }else{
                             $segment = $SegmentRepository->findOneBy(['id' => $seg["id"]]);
                        if($seg["name"]){
                            $segment->setName($seg["name"]);
                        }
                        if($site){
                            $segment->setSite($site);
                        }
                        $this->em->persist($segment);
                        }
                         $i++;
                    }
                }else{
                    $site = $SiteRepository->findOneBy(['id' => $siteSegment["siteId"]]);
                    if($siteSegment["site"]){
                        $site->setName($siteSegment["site"]);
                    }
                    if($project) {
                        $site->setProject($project);
                    }
                     
                     $this->em->persist($site);
                    
                     foreach($siteSegment["segment"] as $seg){
                        if($seg["id"] == -1){
                             $segment = new Segment();
                        $segment->setName($seg["name"]);
                        $segment->setCity("Null");
                        $segment->setRegion("Null");
                        $segment->setSite($site);
                        $this->em->persist($segment);
                        
                        }else{
                             $segment = $SegmentRepository->findOneBy(['id' => $seg["id"]]);
                        if($seg["name"]){
                            $segment->setName($seg["name"]);
                        }
                        if($site){
                            $segment->setSite($site);
                        }
                        $this->em->persist($segment);
                        }
                       
                        
                         $i++;
                    }
                }
                
            
                $j++;
                
            }
}
    
            $this->em->persist($project);
            $this->em->flush();

            $query = "api/project/update";
            $method = "PUT";
            $param = [
                'name ' => $name,
                'note'  => $note,
                'location' => $location,
                'organization' => $organization, 
                'maplink' => $maplink, 
                'public' => $public, 
                'country' => $country,
                'color' => $color, 
                'legal notices' => $legalNotices,
                'website link' => $websiteLink,
                'gps precision' => $gpsPrecision,
                'image' => $image,

                 ];
       $data = ['id' => $project->getId(),
               'name' => $project->getName(),
               'note' => $project->getnote(),
               'location' => $project->getLocation(),
               'organization' => $project->getOrganization(),
               'public' => $project->getPublic(),
               'country' => $project->getCountry()->getName(),
               'color' => $project->getColor(),
               'legalnotices' => $project->getLegalNotices(),
               'websitelink' => $project->getWebsiteLink(),
               'timezone' => $project->getTimeZone(),
               'gpsprecision ' => $project->getGpsPrecision(),
               'image ' => $project->getImage(),
               //'questions ' => $project->getQuestions(),
                
                 ];            
        return $this->respondWithSuccess(sprintf('The project name %s has been successfully update', 
                                         $project->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
}
