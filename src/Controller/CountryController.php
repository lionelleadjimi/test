<?php

namespace App\Controller;

use App\Entity\Country;
use App\Repository\CountryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;

class CountryController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                CountryRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }
    
    
    
    /**
     * @Route("/updateschema", name="update-schema")
     */

    public function updateSchema(KernelInterface $kernel): Response
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $options = array('command' => 'doctrine:schema:update',"--force" => true);
        $application->run(new \Symfony\Component\Console\Input\ArrayInput($options));

        return new Response("Schemas update succefully.");
    }

     
       /**
     * Register.
     *
     * @Route("/api/country/create", name="country_create", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the country's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Country::class, groups={"country"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="name",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="codeTel",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="sigle",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="country")
     */
    public function createAction(Request $request): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $name = $request->get('name');
        $codeTel = $request->get('codeTel');
        $sigle = $request->get('sigle');
         $id = $request->get('id');
        if (empty($name) || empty($codeTel) || empty($sigle)) {
            return $this->respondValidationError("All fields are required");
        }

        $country = new Country();
                $country->setId($id);
        $country->setName($name);
        $country->setCodeTel($codeTel);
        $country->setSigle($sigle);
        

        $this->em->persist($country);
        $this->em->flush();

        $query = "api/country/create";
        $method = "POST";
        $param = [
                 'name' => $name,
                 'codeTel ' => $codeTel,
                 'sigle ' => $sigle,
                  ];
        $data = ['id' => $country->getId(),
                'name ' => $country->getName(),
                'codeTel ' => $country->getCodeTel(),
                'sigle ' => $country->getSigle(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The country named %s has been successfully created', 
                                         $country->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/country/read  ", name="country_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $country = $this->repository->findOneBy(['id' => $id]);

        $query = "api/country/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $country->getId(),
                'name ' => $country->getName(),
                'codeTel ' => $country->getCodeTel(),
                'sigle ' => $country->getSigle(),
                        ];  
        return $this->respondWithSuccess(sprintf('infos of country named %s', 
                                         $country->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }
    
    
    /**
     * Country list .
     *
     *
     * @Route("/api/noconnect/country/list  ", name="country_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns null",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Country::class, groups={"country"}))
     *     )
     * ),
     * @OA\Tag(name="country")
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
      

        $query = "api/country/list";
        $method = "GET";
        $param = ['NULL']; 

          $country= $this->repository->findAll();
           
            $countryNormalizer = $normalizer->normalize($country, null, ['groups' => 'country:read']);
       
           return $this->respondWithSuccess(sprintf('List of country'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $countryNormalizer);
      
       
        return $this->respondWithSuccess(sprintf('List of conutries'), 
       $query, 
       $method,
      $param,
      $conutryNormalizer);
    }
           
     /**
     * @Route("/api/country/listPage  ", name="country_listPage", methods={"GET"})
     */
    public function listActionPage(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/country/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $country= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $countryNormalizer = $normalizer->normalize($country, null, ['groups' => 'country:read']);
       
           return $this->respondWithSuccess(sprintf('List of country'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $countryNormalizer);
        }

        
        $conutry = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $conutryNormalizer = $normalizer->normalize($conutry, null, ['groups' => 'country:read']);
       
        return $this->respondWithSuccess(sprintf('List of conutries'), 
       $query, 
       $method,
      $param,
      $conutryNormalizer);
    }

/**
     * @Route("/api/country/delete", name="country_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $country = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($country);
            $this->em->flush();
            
            $query = "api/country/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('country named %s successfully delete', 
                                                      $country->getName()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/country/update", name="country_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $name = $request->get('name');
            $codeTel = $request->get('codeTel');
            $sigle = $request->get('sigle');
        
           $country = $this->repository->findOneBy(['id' => $id]);
    
    
    
            if($name)
            {
                $country->setName($name);
                
            }

            if($codeTel)
            {
                $country->setCodeTel($codeTel);
                
            }

            if($sigle)
            {
                $country->setSigle($sigle);
                
            }

    
            $this->em->persist($country);
            $this->em->flush();

            $query = "api/country/update";
            $method = "PUT";
            $param = [
                'name' => $name,
                'codeTel ' => $codeTel,
                'sigle ' => $sigle,
                 ];
            $data = ['id' => $country->getId(),
                    'name ' => $country->getName(),
                    'codeTel ' => $country->getCodeTel(),
                    'sigle ' => $country->getSigle(),
                        
                        ];          
        return $this->respondWithSuccess(sprintf('The conutry named %s has been successfully update', 
                                         $country->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
}
