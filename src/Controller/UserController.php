<?php


namespace App\Controller;


use App\Entity\User;
use App\Entity\Group;
use App\Entity\Project;
use App\Entity\UserProject;
use App\Entity\Tokens;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\UserRepository;
use App\Repository\CountryRepository;
use App\Repository\FonctionRepository; 
use App\Repository\ProjectRepository;
use App\Repository\GroupRepository;
use App\Repository\UserProjectRepository;
use App\Repository\ObservationRepository;
use App\Repository\TypeObservationRepository; 
use App\Services\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTEncodedEvent;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;



class UserController extends ApiController
{
    private $em;

    public function __construct(EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer ,
                                UserRepository $repository,
                                RefreshTokenManagerInterface $refreshTokenManager)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
        $this->refreshTokenManager = $refreshTokenManager;
    }


 
    /**
     * Register: create account 
     *
     * @Route("/api/user/create", name="user_create", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the user's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * ) 
     * @OA\Parameter(
     *     name="first_name",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="last_name",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="organization",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="email",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="password",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="city",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="phone",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="codeTel",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="sigle",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="fonction_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="country_id",
     *     in="query",
     *     description="Country id",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="user")
     */
    public function createAction(Request $request, UserPasswordEncoderInterface $encoder,MailerService $MailerService,
                                  CountryRepository $CountryRepository,
                                  FonctionRepository $FonctionRepository,
                                  ProjectRepository $ProjectRepository,
                                  UserProjectRepository $UserProjectRepository): JsonResponse
    {
        $request = $this->transformJsonBody($request);
        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name');
       // $username = $request->get('username');
        $password = $request->get('password');
        $email = $request->get('email');
       //$role = $request->get('role');
        $city = $request->get('city'); 
        $phone = $request->get('phone');
        $sigle = $request->get('sigle');
        $organization = $request->get('organization');
        $codeTel = $request->get('codeTel');
         $user1 = $this->repository->findOneBy(['email' => $email]);
        
        
       $fonction_id = $request->get('fonction_id');
       $fonction = $FonctionRepository->findOneBy(['id' => $fonction_id]);
        $country_id = $request->get('country_id');
        $country = $CountryRepository->findOneBy(['id' => $country_id]);
        $project_name = "Siren international" ;
        $project = $ProjectRepository->findOneBy(['name' => $project_name]);
        //$code = $request->get('code');
       // $code = random_int(100000, 999999);
        //dd($code);
        $expirationToken = new \Datetime('+4 hours');

        if (empty($password) || empty($email) || empty($city) || empty($phone)) {
            return $this->respondValidationError("All fields are required");
        }
        if($user1)
        {
            return $this->respondValidationError("This email address already exists ");
        }
 
        //dd(new \DateTime('+10 minutes'));
        
        $us = $this->repository->findOneBy([], ['id' => 'DESC']);
        $id = $us->getId() + 1; 
        $user = new User();
        $user->setPassword($encoder->encodePassword($user, $password));
        $user->setEmail($email);
        $user->setFirstName($first_name);
        $user->setLastName($last_name);
       // $user->setUsername($username);
        $user->setId($id);
        $user->setOrganization($organization);
        $user->setCity($city);
        $user->setPhone($phone); 
       $user->setCodeTel($codeTel);
       $user->setSigle($sigle);
        $user->setExpirationToken($expirationToken);
        $user->setCountry($country);
        $user->setFonction($fonction);
        $user->setProject($project);
        $user->setMigrate(1);
        $tokengenere = $this->jwtManager->create($user);
       $user->setToken($tokengenere);
       $this->em->persist($user); 
       
       /* $userpor = $UserProjectRepository->findOneBy([], ['id' => 'DESC']);
        $id1 = $userpor->getId() + 1;*/
        $userProject = new UserProject();

        $userProject->setProject($project);
        $userProject->setUser($user);
        $userProject->setRole('Regular user');

        $this->em->persist($userProject);
       
        $this->em->flush();
        
      /*  $mail = (new TemplatedEmail())
                ->from('noreply@sirenammco.org')
                ->to($user->getEmail())
                ->subject('Siren')
                ->htmlTemplate('emails/signup.html.twig')
                ->context([
                    'expiration_date' => new \DateTime('+3 hours'),
                    'token' => $tokengenere,
                ]);
       $mailer->send($mail);*/
       
       $MailerService->send("Siren Account Verification.",
                            'noreply@sirenammco.org',
                            $user->getEmail(),
                            "emails/signup.html.twig",
                            [ 'token' => $tokengenere]);

        $query = "api/user/create";
        $method = "POST";
        $param = [
                 'email ' => $email,
                 'ville ' => $city,
                 'telephone ' => $phone

                  ];
        $data = ['id' => $user->getId(),
                  'firstname' => $user->getFirstName(),
                  'lastname' => $user->getLastName(),
               //  'username' => $user->getUsername(),
                 'email' => $user->getEmail(),
                 'role' => $user->getRoles(),
                 'city' => $user->getCity(),
                 'phone' => $user->getPhone(),
                  'job' => $user->getFonction()->getTitle(),
                  'country ' => $user->getCountry()->getName(),
                  'sigle ' => $user->getSigle()
                 
                  ];          
        return $this->respondWithSuccess(sprintf('User %s successfully created', 
                                         $user->getUsername()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    }
     
    
    /**
     * Migration.
     *
     * @Route("/api/user/migrate", name="user_migrate", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the user's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="email",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="password",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="city",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="phone",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="fonction_id",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="country_id",
     *     in="query",
     *     description="Country id",
     *     required=true,
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="user")
     */
    public function migrateAction(Request $request, UserPasswordEncoderInterface $encoder,MailerInterface $mailer,
                                  CountryRepository $CountryRepository,
                                  FonctionRepository $FonctionRepository,
                                  ProjectRepository $ProjectRepository): JsonResponse
    {
        $request = $this->transformJsonBody($request);
        $id = $request->get('id');
        $password = $request->get('password');
        $email = $request->get('email');
        $date = $request->get('date');
        $roles = $request->get('roles');
      
      
       
        $city = $request->get('city'); 
        $enabled = $request->get('enabled');
        $phone = $request->get('phone');
        $fonction_id = $request->get('fonction_id');
        $fonction = $FonctionRepository->findOneBy(['id' => $fonction_id]);
        $country_id = $request->get('country_id');
        $country = $CountryRepository->findOneBy(['id' => $country_id]);
        $project_id = $request->get('project_id');
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);
        
         $user1 = $this->repository->findOneBy(['email' => $email]);
        
        
        $expirationToken = new \Datetime('+4 hours');

        /*if (empty($password) || empty($email) || empty($city) || empty($phone)) {
            return $this->respondValidationError("All fields are required");
        }*/
        if($user1)
        {
            return $this->respondValidationError("This email address already exists ");
        }
 
        //dd(new \DateTime('+10 minutes'));
        $user = new User();
        $user->setId($id);
        $user->setPassword($password);
        //$user->setPassword($encoder->encodePassword($user, $password));
        $user->setEmail($email);
        if(stristr($roles, 'ROLE_USER') == false){
        $user->setRoles(['ROLE_ADMIN']); 
            
        }
        $user->setCity($city);
        
        $user->setPhone($phone);
       // $user->setCode($code);
       // $user->setExpirationToken($expirationToken);
        $user->setCountry($country);
        $user->setFonction($fonction);
        $user->setProject($project); 
        $user->setEnabled($enabled); 
        $user->setDate(new \DateTime($date));

        $this->em->persist($user);
        $this->em->flush();
     

        $query = "api/user/create";
        $method = "POST";
        $param = [
                 'email ' => $email,
                 'ville ' => $city,
                 'telephone ' => $phone

                  ];
        $data = ['id' => $user->getId(),
                 'email ' => $user->getEmail(),
                 //'role ' => $user->getRoles(),
                 'city ' => $user->getCity(),
                 'phone ' => $user->getPhone(),
                  'fonction ' => $user->getFonction()->getTitle(),
                  'country ' => $user->getCountry()->getName(),
                // 'expirationToken ' => $user->getExpirationToken()
                 
                  ];          
        return $this->respondWithSuccess(sprintf('User %s successfully created', 
                                         $user->getUsername()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    }

     /**
     * Update the user's informations
     *
     * @Route("/api/user/update", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the user's information after modification",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="first_name",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="last_name",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="organization",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="email",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="city",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="phone",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="codeTel",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="sigle",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="fonction_id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="country_id",
     *     in="query",
     *     description="Country id",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="user")
     */
    public function updateAction(Request $request, UserPasswordEncoderInterface $encoder,
                                  CountryRepository $CountryRepository,
                                  FonctionRepository $FonctionRepository): JsonResponse
    {
        $request = $this->transformJsonBody($request);
        
        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name');
       // $username = $request->get('username');
       // $email1 = $request->get('email');
        $organization = $request->get('organization');
        $codeTel = $request->get('codeTel');
        $city = $request->get('city');
        $phone = $request->get('phone');
        $codeTel = $request->get('codeTel');
        $sigle = $request->get('sigle');
        $fonction_id = $request->get('fonction_id');
        $fonction = $FonctionRepository->findOneBy(['id' => $fonction_id]);
        $country_id = $request->get('country_id');
        $country = $CountryRepository->findOneBy(['id' => $country_id]);

        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $email = $decodedJwtToken['username'];
        $user = $this->repository->findOneBy(['email' => $email]); 


        if($first_name)
        {
            $user->setFirstName($first_name);
            
        }
        if($last_name)
        {
            $user->setLastName($last_name);
            
        }
        if($sigle)
        {
            $user->setSigle($sigle);
            
        }

      /*  if($email)
        {
           
            $user->setEmail($email);
            
        } */
                if($organization)
        {
           
            $user->setOrganization($organization);
            
        } 

        if($city)
        {
            $user->setCity($city);
          
        }

        if($phone)
        {
            $user->setPhone($phone);
        }
        
         if($codeTel)
        {
            $user->setCodeTel($codeTel);
        }
        
          if($fonction_id)
        {
            $user->setFonction($fonction);
          
        }

        if($country_id)
        {
            $user->setCountry($country);
        }


        $this->em->persist($user);
        $this->em->flush();

        $query = "api/user/update";
        $method = "POST";
        $param = [
                 'email ' => $email,
                 'ville ' => $city,
                 'telephone ' => $phone,

                  ];
        $data = ['id' => $user->getId(),
                  'firstname' => $user->getFirstName(),
                  'lastname' => $user->getLastName(),
                // 'username' => $user->getUsername(),
                 'email' => $user->getEmail(),
                 //'role ' => $user->getRoles(),
                 'city' => $user->getCity(),
                 'phone' => $user->getPhone(),
                 'codeTel' => $user->getCodeTel(),
                  'sigle' => $user->getSigle(),
                  'job' => $user->getFonction()->getTitle(),
                  'country' => $user->getCountry()->getName(),
                  'countryid' => $user->getCountry()->getId(),
                  'countrycode' => $user->getCountry()->getCodeTel(),
                  'organization' => $user->getOrganization(),
                  'countrysigle' => $user->getCountry()->getSigle()
                  ];  
        return $this->respondWithSuccess(sprintf('User %s successfully update',  
                                         $user->getUsername()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    }
    
     /**
     * Update password of user 
     *
     * @Route("/api/userPassword/update", methods={"POST"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the user's information after modification",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="current_password",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="new_password",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="user")
     */
    public function updatePasswordAction(Request $request, UserPasswordEncoderInterface $encoder): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $username = $decodedJwtToken['username'];
        $user = $this->repository->findOneBy(['email' => $username]); 
        
        $request = $this->transformJsonBody($request);
        $new_password = $request->get('new_password');
        $current_password = $request->get('current_password');
        $test = $encoder->isPasswordValid($user,  $current_password);
         
        
         if($test == true){
             $user->setPassword($encoder->encodePassword($user, $new_password));
         }else{
             //return $this->respondValidationError("Incorrect current password");
                     return $this->respondWithSuccess(sprintf('Incorrect current password'),
                                         "Null", 
                                         "Null",
                                        ['Null'],
                                        ['Null']);
         }


        $this->em->persist($user);
        $this->em->flush();

        $query = "api/userPassword/update";
        $method = "PUT";
        $param = ['NULL'];
        $data = ['id' => $user->getId(),
                  'firstname' => $user->getFirstName(),
                  'lastname' => $user->getLastName(),
                 //'username' => $user->getUsername(),
                 'email ' => $user->getEmail(),
                 //'role ' => $user->getRoles(),
                 'city ' => $user->getCity(),
                 'phone ' => $user->getPhone(),
                  'fonction ' => $user->getFonction()->getTitle(),
                  'country ' => $user->getCountry()->getName()
                  ];  
        return $this->respondWithSuccess(sprintf('User password with email %s successfully update',  
                                         $user->getUsername()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    }
    
    
    
    /**
     * Login .
     *
     * @Route("/api/user/login", methods={"POST"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="email",
     *     in="query",
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="password",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="user")
     */

    public function getTokenUser(Request $request, UserPasswordEncoderInterface $encoder,
                                 ProjectRepository $ProjectRepository,
                                 UserProjectRepository $UserProjectRepository): Response
    {
        $request = $this->transformJsonBody($request);
        $email = $request->get('email');
        $user = $this->repository->findOneBy(['email' => $email]);
        $password = $request->get('password');
        $test = $encoder->isPasswordValid($user,  $password);
         // var_dump($password,$test); die();

    
        $creationDate = new \DateTime('now');
        $expirationDate = $creationDate->add(new \DateInterval('P1M'));
       
      // var_dump($pass);die();
        if (empty($password) || empty($email)) {
            return $this->respondValidationError("All fields are required");
        }
        
        if ($test == false) {
            return $this->respondValidationError("Incorrect password");
        }
        
       $tokengenere = $this->jwtManager->create($user);
      // $user->setToken($tokengenere);
      // $user->setCreationDate(new \DateTime('now'));
       //$user->setExpirationDate($expirationDate);
        $this->em->persist($user);
       $this->em->flush();

     
     if($user->getFirstname() == NULL )
     {
         $firstname1 = "";
         
     }else{
         $firstname1 = $user->getFirstname();
     }
      if($user->getLastname() == NULL )
     {
         $lastname1 = "";
         
     }else{
         $lastname1 = $user->getLastname();
     }
     
       if(($user->getOrganization() == NULL ) ||($user->getOrganization() == "NULL" ))
     {
         $organization1 = "";
         
     }else{
         $organization1 = $user->getOrganization();
     }
     
     if($user->getCodeTel() == NULL )
     {
         $codeTel1 = "";
         
     }else{
         $codeTel1 = $user->getCodeTel();
     }
     
       if($user->getSigle() == NULL )
     {
         $sigle1 = "US";
         
     }else{
         $sigle1 = $user->getSigle();
     }
      
         
        
        $userprojects = $UserProjectRepository->findBy(['user' => $user]);
        
           $tab1 = [];
        $j = 0;
        foreach($userprojects as $userproject){
                $tab1[$j]['id'] = $userproject->getProject()->getId();
                $tab1[$j]['name'] = $userproject->getProject()->getName();
                $tab1[$j]['note'] = $userproject->getProject()->getNote();
                $tab1[$j]['location'] = $userproject->getProject()->getLocation();
                $tab1[$j]['organization'] = $userproject->getProject()->getOrganization();
                $tab1[$j]['maplink'] = $userproject->getProject()->getMaplink();
                $tab1[$j]['public'] = $userproject->getProject()->getPublic();
                $tab1[$j]['country'] = $userproject->getProject()->getCountry()->getName();
                $tab1[$j]['admin'] = $userproject->getProject()->getAdmin()->getId();
                $tab1[$j]['legalnotices'] = $userproject->getProject()->getLegalNotices();
                $tab1[$j]['websitelink'] = $userproject->getProject()->getWebsiteLink();
                $tab1[$j]['gpsprecision'] = $userproject->getProject()->getGpsPrecision();
                $tab1[$j]['timezone'] = $userproject->getProject()->getTimeZone();
                $tab1[$j]['note'] = $userproject->getProject()->getNote();
                $tab1[$j]['jobId'] = $userproject->getUser()->getFonction()->getId();
                $tab1[$j]['jobTitle'] = $userproject->getUser()->getFonction()->getTitle();
                $j++;
        }
      
     //  return $this->json($tokengenere, 201, []);
        $query = "api/user/login";
        $method = "POST";
        $param = [
                 'email ' => $email

                  ];
        $data = ['id' => $user->getId(),
                 'email' => $user->getEmail(),
                 'city' => $user->getCity(),
                 'phone' => $user->getPhone(),
                 'codeTel' => $user->getCodeTel(),
                  'sigle' => $sigle1,
                  //$user->getSigle(),
                  'firstname' => $firstname1,
                  //$user->getFirstname(),
                  'lastname' => $lastname1,
                  //$user->getLastname(),
                // 'username' => $user->getUsername(),
                 'organization' => $organization1,
                  'fonction' => $user->getFonction()->getTitle(),
                  'country' => $user->getCountry()->getName(),
                  'countryid' => $user->getCountry()->getId(),
                  'organization' => $organization1,
                  //$user->getOrganization(),
                  'codeTel' => $codeTel1,
                  //$user->getCodeTel(),
                  
                  'projectId' => $user->getProject()->getId(),
                  'projectName' => $user->getProject()->getName(),
                  'userProjects' => $tab1,
                  'token' => $tokengenere,
                  ];  
        return $this->respondWithSuccess(sprintf('User %s successfully login', 
                                         $user->getUsername()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       
    }
    
    
    /**
     *
     * @Route("/privacys", methods={"Get"})
     * @OA\Response(
     *     response=200,
     *     description="Returns Ok",
     * ),
     */
   
    public function PrivacyAction(Request $request): Response 
    {
        // var_dump('yo'); 
        // die();
        return $this->render('privacy/index.html.twig');
    }
    
     /**
     * Send multiple emails .
     *
     *
     * @Route("/api/user/sendMail", methods={"POST"})
     * @OA\Response(
     *     response=200,
     *     description="Returns Ok",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * ),
     * @OA\Tag(name="user")
     */
   
    public function SendMailAction(MailerService $MailerService): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $username = $decodedJwtToken['username'];
        $users = $this->repository->findBy(array(),array('id' => 'DESC'),1);
        $link = "https://sirenammco.org/forgottenPassword";
        $i = 0;
        foreach($users as $user){
           /* echo $user->getEmail();
            echo "\n";*/
             $MailerService->send('Migration of your account to the new Siren website ',
                                "noreply@sirenammco.org",
                                $user->getEmail(),
                                "emails/info.html.twig",
                                ['link' =>  $link]);
                        $i++;        
        }
        //echo count($users); die();
        
        $query = "api/user/sendMail";
        $method = "POST";
        $param = ['Null'];
        $data = ['id' => $user->getId(),
                 'email ' => $user->getEmail(),
                  ];  
        return $this->respondWithSuccess(sprintf('e-mails successfully sent'), 
                                         $query, 
                                         $method,
                                         $param,
                                        $data);
    }
    

    /**
     * user deletion .
     *
     *
     * @Route("/api/user/delete", methods={"DELETE"})
     * @OA\Response(
     *     response=200,
     *     description="Returns null",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * ),
     *  @OA\Parameter(
     *     name="userId",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="user")
     */
   
    public function deleteAction(Request $request,
                                 UserProjectRepository $UserProjectRepository,
                                 ObservationRepository $ObservationRepository): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $username = $decodedJwtToken['username'];
        
                    $request = $this->transformJsonBody($request);
                    $userId =  $request->get('userId');
                    $user = $this->repository->findOneBy(['id' => $userId]);
                    $userProject = $UserProjectRepository->findOneBy(['user' => $user]);
                    $observations = $ObservationRepository->findBy(['user' => $user]);

        
        if($username == "lab126bonansea@gmail.com"){
            foreach($observations as $obs){
                  $this->em->remove($obs);
            }
            $this->em->remove($user);
            $this->em->remove($userProject);
        $this->em->flush();
        }else{
             return $this->respondValidationError("You are not allowed to delete");
        }
        
        

        //return $this->json('Suppression ok');
        $query = "api/user/delete";
        $method = "DELETE";
        $param = ['Null'];
        $data = ['id' => $user->getId(),
                 'email ' => $user->getEmail(),
                  ];  
        return $this->respondWithSuccess(sprintf('User successfully delete'), 
                                         $query, 
                                         $method,
                                         $param,
                                        $data);
    }

    
    
        /**
     * Show.
     *
     * This call takes into account all confirmed awards, but not pending or refused awards.
     *
     * @Route("/api/user/read", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the user's information",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * ),
     * @OA\Tag(name="user")
     */
    public function readAction(Request $request,
                                CountryRepository $CountryRepository,
                                 FonctionRepository $FonctionRepository): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $username = $decodedJwtToken['username'];
        $user = $this->repository->findOneBy(['email' => $username]);
        //dd($decodedJwtToken);
       

       // return $this->json($user, 201, [], ['groups' => 'user:read']);
        $query = "api/user/read";
        $method = "GET";
        $param = ['id' => $user->getId()];
        $data = ['id' => $user->getId(),
                 'email ' => $user->getEmail(),
                 'role ' => $user->getRoles(),
                 'city ' => $user->getCity(),
                 'phone ' => $user->getPhone(),
                 'fonction ' => $user->getFonction()->getTitle(),
                  'country ' => $user->getCountry()->getName()
                  ];  
        return $this->respondWithSuccess(sprintf('infos of user %s', 
                                         $user->getUsername()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }
    

     
      /**
     *number's user.
     *
     *
     * @Route("/api/user/count  ", name="user_count", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the user's information after suspension",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * ),
     * @OA\Tag(name="user")
     */
    public function CountAction(Request $request): Response 
    {

        $enable= 0;
        $user = $this->repository->findAll();
        $nb = count($user);
       // var_dump($nb); 
        
         $query = "api/user/count";
        $method = "GET";
        $param = ['Null'];
        $data = ['nb_user' => $nb,]; 
         return $this->respondWithSuccess(sprintf('Number of users in database'), 
                                         $query, 
                                         $method,
                                         $param,
                                        $data);
    
      
    }



    
    /**
     *Account suspension.
     *
     *
     * @Route("/api/user/suspend", methods={"PUT"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the user's information after suspension",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * ),
     * @OA\Tag(name="user")
     */
    public function suspendAction(): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $username = $decodedJwtToken['username'];
        $user = $this->repository->findOneBy(['email' => $username]);
        $user->setEnabled('0');
        $this->em->flush();

       $query = "api/user/suspend";
       $method = "PUT";
       $param = ['Null'];
       $data = ['id' => $user->getId(),
                'email ' => $user->getEmail(),
                'role ' => $user->getRoles(),
                'ville ' => $user->getVille(),
                'telephone ' => $user->getTelephone(),
                 'fonction ' => $user->getFonction()->getTitle(),
                  'country ' => $user->getCountry()->getName()
                 ];  
       return $this->respondWithSuccess(sprintf('User %s successfully suspended', 
                                        $user->getUsername()), 
                                        $query, 
                                        $method,
                                       $param,
                                       $data);
    }

   /**
     * Account activation.
     *
     * @Route("/api/user/enabled", methods={"PUT"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the user's information after activation",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * ),
     * @OA\Tag(name="user")
     */
    public function enabledAction(): Response 
    {
         
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $username = $decodedJwtToken['username'];
        $user = $this->repository->findOneBy(['email' => $username]);
        $user->setEnabled('1');
        $this->em->flush();
        
       $query = "api/user/enabled";
       $method = "PUT";
       $param = ['Null'];
       $data = ['id' => $user->getId(),
                'email ' => $user->getEmail(),
                'role ' => $user->getRoles(),
                'ville ' => $user->getVille(),
                'telephone ' => $user->getTelephone(),
                 'fonction ' => $user->getFonction()->getTitle(),
                  'country ' => $user->getCountry()->getName()
                 ];  
       return $this->respondWithSuccess(sprintf('User %s successfully activated', 
                                        $user->getUsername()), 
                                        $query, 
                                        $method,
                                       $param,
                                       $data);

       }

   
    
        /**
     * Acount validate
     *
     *
     * @Route("/api/user/validate", name="validate_account",  methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the rewards of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="token",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="user")
     */
     public function ValidatedAction(Request $request): Response 
    {
        
        /*$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $username = $decodedJwtToken['username'];*/
          $token = $request->get('token');
         // var_dump($code); die();
        
        $user = $this->repository->findOneBy(['token' => $token]);
        

        if($user){
            if($user->getExpirationToken() > new \DateTime("now")){

                $user->setEnabled(true);
               // $user->setCode(0);
                 $user->setToken('null');
                $this->em->persist($user);
                $this->em->flush();
                    
               
        
                $query = "api/user/validate";
               $method = "POST";
               $param = ['token' => $token];
               $data = ['id' => $user->getId(),
                        'email ' => $user->getEmail(),
                         ];  
               return $this->respondWithSuccess(sprintf('The user\'s account has been successfully activated', 
                                                $user->getUsername()), 
                                                $query, 
                                                $method,
                                               $param,
                                               $data);
            }else{
                return $this->respondValidationError("this link is no longer valid");
            }                                   
        }else{
            return $this->respondValidationError("This user does not exist");
        }
    }
   /* public function ValidatedAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $username = $decodedJwtToken['username'];
          $code = $request->get('code');
         // var_dump($code); die();
        
        $user = $this->repository->findOneBy(['code' => $code]);
        

        if($user){

        $user->setEnabled(true);
        $user->setCode(0);
        // $user->setToken('null');
        $this->em->persist($user);
        $this->em->flush();
            
       

        $query = "api/user/validate";
       $method = "POST";
       $param = ['code' => $code];
       $data = ['id' => $user->getId(),
                'email ' => $user->getEmail(),
                 ];  
       return $this->respondWithSuccess(sprintf('The user\'s account has been successfully activated', 
                                        $user->getUsername()), 
                                        $query, 
                                        $method,
                                       $param,
                                       $data);
        }else{
            return $this->respondValidationError("This user does not exist");
        }
    }
*/
      /**
     * ForgotPassword .
     *
     * @Route("/api/user/forgotPassword", methods={"POST"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="email",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="user")
     */
    public function forgotPasswordAction(Request $request, UserPasswordEncoderInterface $encoder,MailerService $MailerService): JsonResponse
    {
        $request = $this->transformJsonBody($request);
        $email = $request->get('email');
         $code = random_int(100000, 999999);

        if (empty($email)) {
            return $this->respondValidationError("All fields are required");
        }

        $user = $this->repository->findOneBy(['email' => $email]);
            if (!$user) {
            return $this->respondValidationError("Incorrect email ");
        }else{
         $user->setCode($code);
          $user->setExpirationCode(new \DateTime('+4 hours'));
        $this->em->persist($user);
        $this->em->flush();
        
        $MailerService->send('Siren reset password',
                                "noreply@sirenammco.org",
                                $user->getEmail(),
                                "emails/resetPassword.html.twig",
                                ['code' => $code,]);
            /* $mail = (new TemplatedEmail())
            ->from('noreply@sirenammco.org')
            ->to($user->getEmail())
            ->subject('Votre code est : '.$user->getCode())
            ->htmlTemplate('emails/resetPassword.html.twig')

    // pass variables (name => value) to the template
    ->context([
        'expiration_date' => new \DateTime('+1 hour'),
        'code' => $code,
    ]);
       $mailer->send($mail);*/
       
                    $query = "api/user/forgotPassword";
                    $method = "POST";
                    $param = ['email' => $email
                             ];
                    $data = ['id' => $user->getId(),
                             'email ' => $user->getEmail(),
                             'role ' => $user->getRoles(),
                             'ville ' => $user->getCity(),
                             'telephone ' => $user->getPhone()
                              ];  
                    return $this->respondWithSuccess(sprintf('The email was successfully sent to user %s', 
                                                     $user->getUsername()), 
                                                     $query, 
                                                     $method,
                                                    $param,
                                                    $data);

        }
    }

    /**
     * @Route("/api/user/resetPassword", name="resetPassword", methods={"POST"})
     */
     
     
      /**
     * ResetPassword .
     *
     * @Route("/api/user/resetPassword", methods={"POST"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="code",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="password",
     *     in="query",
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="user")
     */
    public function resetPassworddAction(Request $request,
                                         UserPasswordEncoderInterface $encoder,
                                         ProjectRepository $ProjectRepository): Response 
    {
        
       
        $request = $this->transformJsonBody($request);
        $code = $request->get('code');
        $password = $request->get('password');
        
                $user = $this->repository->findOneBy(['code' => $code]);

        if($user){
            if($user->getExpirationCode() > new \DateTime('now')){
             if($user->getProject()->getId() == 62155){
                $projectId = 62184;
                $project = $ProjectRepository->findOneBy(['id' => $projectId]);
                $user->setProject($project);
            }
        $user->setPassword($encoder->encodePassword($user, $password));
        $user->setEnabled('1');
         $user->setCode(false);
        $this->em->flush();

        $query = "api/user/forgatpassword";
        $method = "POST";
        $param = ['code' => $code ];
        $data = ['id' => $user->getId(),
                 'email ' => $user->getEmail(),
                 'ville ' => $user->getCity(),
                 'telephone ' => $user->getPhone()
                  ];  
        return $this->respondWithSuccess(sprintf('The password of the user %s has been successfully changed ', 
                                         $user->getUsername()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
            }else{
                            return $this->respondValidationError("this code is no longer valid");
            }

      
        }else{
            return $this->respondValidationError("Invalid code");
        }
    }
    
    
    
        
    /**
     * List user's projects .
     *
     * @Route("/api/user/project", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * )
     * 
     * @OA\Tag(name="list view")
     */
       public function ListProjectAction(Request $request,NormalizerInterface $normalizer,
                                  UserProjectRepository $UserProjectRepository
                                  ): JsonResponse
    {
          $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
         $email= $decodedJwtToken['username'];
          $dateExpiration= $decodedJwtToken['exp'];
          $temps_actuel = date("U");
          //echo $temps_actuel; die();
        
         $user = $this->repository->findOneBy(['email' => $email]);
         
    
         
            $query = "api/user_project/list";
            $method = "GET";
            $param = ['NULL']; 

           $i = 0; 
           
         
                $projects = $UserProjectRepository->findBy(['user' => $user]);
                $pros = array();
                if(!$projects){
                    $pros =  []; 
                }
              //  var_dump($pros); die();
                  foreach($projects as $pro){
                      
                        $pros[$i]['id'] = $pro->getProject()->getId();
                        $pros[$i]['name'] = $pro->getProject()->getName();
                        $pros[$i]['note'] = $pro->getProject()->getNote();
                        $pros[$i]['location'] = $pro->getProject()->getLocation();
                        $pros[$i]['organization'] = $pro->getProject()->getOrganization();
                        $pros[$i]['maplink'] = $pro->getProject()->getMaplink();
                        $pros[$i]['public'] = $pro->getProject()->getPublic();
                        $pros[$i]['country'] = $pro->getProject()->getCountry()->getName();
                        $pros[$i]['user'] = $email;
                        //$pro->getProject()->getAdmin()->getEmail();
                        $pros[$i]['date'] = $pro->getProject()->getDate();
                        $pros[$i]['color'] = $pro->getProject()->getColor();
                        $pros[$i]['coordx'] = $pro->getProject()->getCoordX();
                        $pros[$i]['coordy'] = $pro->getProject()->getCoordY();
                        $pros[$i]['zoom'] = $pro->getProject()->getZoom();
                        $pros[$i]['legalNotices'] = $pro->getProject()->getLegalNotices();
                        $pros[$i]['webSiteLink'] = $pro->getProject()->getWebsiteLink();
                        $pros[$i]['gpsPrecision'] = $pro->getProject()->getGpsPrecision();
                        $pros[$i]['timezone'] = $pro->getProject()->getTimeZone();
                        $pros[$i]['image'] = $pro->getProject()->getImage();
                          $pros[$i]['role'] = $pro->getRole();
                        if($pro->getRole() == "Admin"){
                            $pros[$i]['role'] = "admin";
                        }
                         if($pro->getRole() == "admin"){
                            $pros[$i]['role'] = "admin";
                        }
                         if($pro->getRole() == "Regular user"){
                            $pros[$i]['role'] = "regular user";
                        }
                        //$pros[$i]['name'] = $pro->getProject()->getQuestion()->getTitle();
                      $i++;
                  }
                //dd($projects);
               
                $projectsNormalizer = $normalizer->normalize($projects, null, ['groups' => 'project:read']);
      
                return $this->respondWithSuccess(sprintf("List of user's projects"), 
                $query, 
                $method,
               $param,
               $pros);
       
     
           
        
    }
    /**
     * List of observations of all projects of a user  .
     *
     * @Route("/api/local", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * )
     * 
     * @OA\Tag(name="list view")
     */ 
     /*  public function geocode2(){
        
       $apiKey = "AIzaSyAynDbAhqx8Xj7aTyADvr6br02-R4Clkz0";

        try {
        $address = "None";
         $addressx = 4.383 ;
        $addressy = 8.9;
        $googleApiUrl = 'https://maps.googleapis.com/maps/api/geocode/json?';   
        $options = array("key"=>$apiKey,"latlng"=>$addressx.",".$addressy);
        $googleApiUrl .= http_build_query($options,'','&');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $googleApiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($ch);
        curl_close($ch);


        $obj = json_decode($json);
        var_dump($obj);die();
            if (count($obj->results) > 0) {
                $address = $obj->results[0]->formatted_address;
            }

        } catch (ErrorException $e) {
            $address = "error";
        }
        
                    $query = "api/user/observation/list";
            $method = "GET";
            $param = ['NULL']; 
             $data = ['local' => $address]; 
            
        return $this->respondWithSuccess(sprintf("Local"), 
                $query, 
                $method,
               $param,
               $data);
        
    }*/


        
    /**
     * List of observations of all projects of a user  .
     *
     * @Route("/api/userProject/observation", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * )
     * 
     * @OA\Tag(name="list view")
     */
       public function ListObservationByuserAction(Request $request,NormalizerInterface $normalizer,
                                  UserProjectRepository $UserProjectRepository,
                                  ObservationRepository $ObservationRepository,
                                  ProjectRepository $ProjectRepository): JsonResponse
    {
          $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
         $email= $decodedJwtToken['username'];
          $dateExpiration= $decodedJwtToken['exp'];
          $temps_actuel = date("U");
       
            $request = $this->transformJsonBody($request);

         
            $query = "api/user/observation/list";
            $method = "GET";
            $param = ['NULL']; 
           
             $user1 = $this->repository->findBy(['email' => $email]);
                
                   $projects = $UserProjectRepository->findBy(['user' => $user1]);
                   //var_dump(count($projects));die();
                   
                   $obs = array();
                   $observations = array();
                   $i = 0;
                   $j = 0;
                   $nb = 0;
                   $images = array();
                   
                   foreach($projects as $pro){
                                              
                       
                        $observations[$j] = $ObservationRepository->findBy(['project' => $pro->getProject()]);
                      
                   
                        foreach($observations[$j] as $ob){
                            $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['idInaturalist'] = $ob->getIdInaturalist();
                            $obs[$i]['email'] = $ob->getUser()->getEmail();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['collector'] = $ob->getCollector();
                           /* $obs[$i]['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/'.$ob->getImg1();
                            if($ob->getImg2() == "observation2.png"){
                                $obs[$i]['img2'] = "NULL"; 
                            }else{
                                $images[$i]['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images[$i]['img3'] = "NULL"; 
                            }else{
                               $images[$i]['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images[$i]['img4'] = "NULL"; 
                            }else{
                                $obs[$i]['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/'.$ob->getImg4();
                            }*/
                           // $obs[$i]['image'] = $ob->getImg1();
                           $obs[$i]['img'] = $images ;
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "NULL";
                            }
                            if($ob->getSpecie()->getName()){
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                            }else{
                                $obs[$i]['specie'] = "NULL";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                           if($ob->getSegment()){
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "NULL";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                           // $nb = $nb + 1;
                            $i++;
                        }
                        $j++;
                   }
                 // var_dump($i);die();
                $observationsNormalizer = $normalizer->normalize($observations, null, ['groups' => 'observation:read']);
      
                return $this->respondWithSuccess(sprintf("List of user's observations"), 
                $query, 
                $method,
               $param,
               $obs);
       
     
           
        
    }
    
    /**
     * List of observations of one project of a user  .
     *
     * @Route("/api/user/Project/observation", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * ),
     * @OA\Parameter(
     *     name="id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="list view")
     */
       public function ListObservationByProjectAction(Request $request,NormalizerInterface $normalizer,
                                  ObservationRepository $ObservationRepository,
                                  ProjectRepository $ProjectRepository): JsonResponse
    {
         
        
      
         
       
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            //$page = $request->get('NumPage');
             $project = $ProjectRepository->findOneBy(['id' => $id]);
             
            $query = "api/user/project/observation/list";
            $method = "GET";
            $param = ['ID' => $id]; 
              
             
                  
                      $i = 0;
                      $obs = array();
                      $images = array();
                      
           
                 //$observations = $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC',),(20),(20-20));
                 $observations = $ObservationRepository->findBy(['project' => $project, 'status' => 1], array('id' => 'DESC'));
                 foreach($observations as $ob){
                     
                           // $obs[$i]['email'] = $ob->getUser()->getEmail();
                             $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['projectId'] = $ob->getProject()->getId();
                            $obs[$i]['projectName'] = $ob->getProject()->getName();
                            $obs[$i]['idInaturalist'] = $ob->getIdInaturalist();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['collector'] = $ob->getCollector();
                           // if($obs['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/'.$ob->getImg1();
                           //$images['img1'] =  'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                           if($ob->getImg1() == "observation1.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                            }
                           // $obs[$i]['image'] = $ob->getImg1();
                           $obs[$i]['images'] = $images ;
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservationId'] = $ob->getTypeObservation()->getId();
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "";
                            }
                            if($ob->getSpecie()){
                            $obs[$i]['specieId'] = $ob->getSpecie()->getId();
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                             if($ob->getSpecie()->getSubgroup() == NULL){
                                  $obs[$i]['family'] = "";
                           
                            }else{
                                $obs[$i]['familyId'] = $ob->getSpecie()->getSubgroup()->getId();
                                $obs[$i]['family'] = $ob->getSpecie()->getSubgroup()->getName();
                                
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                            $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            }
                            }else{
                                $obs[$i]['specie'] = "";
                            }
                            
                        
                            if($ob->getUser()){
                            $obs[$i]['user'] = $ob->getUser()->getEmail();
                            }else{
                                $obs[$i]['user'] = "";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                             $obs[$i]['updatedate'] = $ob->getUpdateDate();
                           if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                            $obs[$i]['siteId'] = $ob->getSegment()->getSite()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";            
                              $obs[$i]['site'] =  "";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                           
                            if($ob->getSite()){
                                 $obs[$i]['siteId'] = $ob->getSite()->getId();
                                 $obs[$i]['site'] = $ob->getSite()->getName();
                            
                           }else{
                             $obs[$i]['site'] =  "";
                             }
                            /* if($ob->getSegment()->getSite() ==  'Null'){
                                  $obs[$i]['site'] =  "NULL";
                            
                           }else{
                               $obs[$i]['siteId'] = $ob->getSegment()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getName();
                             }*/
                           
                          /* if($ob->getSegment()->getSite()){
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['site'] =  "NULL";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }*/
                           
                           $i++;
                 }
                  
                   $observationsNormalizer = $normalizer->normalize($observations, null, ['groups' => 'observation:read']);
      
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $obs);
            
             
     
           
        
    }
    
    
     /**
     * List of observations of one project of a user by page .
     *
     * @Route("/api/user/project/observation/byPage/list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * ),
     *  @OA\Parameter(
     *     name="id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="numPage",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="itemsPerPage",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="data",
     *     in="query",
     *     @OA\Schema(type="array",
     *     @OA\Items(type="int"))
     * ),
     *  @OA\Parameter(
     *     name="date",
     *     in="query",
     *     @OA\Schema(type="array",
     *     @OA\Items(type="int"))
     * ),
     *  @OA\Parameter(
     *     name="search",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="list view")
     */
       public function ObservationByPagetAction(Request $request,NormalizerInterface $normalizer,
                                  ObservationRepository $ObservationRepository,
                                  ProjectRepository $ProjectRepository): JsonResponse
    {
         
        
      
         
       
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
               $page = $request->get('numPage');
               $itemsPerPage = $request->get('itemsPerPage');
             $project = $ProjectRepository->findOneBy(['id' => $id]);
             $search = $request->get('search');
             $data = explode(',',$request->get('data'));
            $countData = count($data);
            
            
            $date = explode(',',$request->get('date'));
            $checkDate=$request->query->has('date');
            $dateStart = 0;
            $dateEnd = 0;
            // $countDate = count($date);
            if($checkDate)
                if($date[0] != 0){
                    $dateStart = date('Y-m-d H:i:s', $date[0]);
                    $dateEnd = date('Y-m-d H:i:s', $date[1]);
                    
                }
            
            
            // if($data[0] != 0)
            // {
            //     var_dump('ya des elment');
            // die();
            // }
            // else {var_dump($countData);
            // die();}
              
             
                  
                      $i = 0;
                      $obs = array();
                      $images = array();
                      
        // return $this->respondWithSuccess(sprintf("List of observations"), "", '',['start' => $dateStart,'end' => $dateEnd, 'obs' => count($data)],[]);
        if(!$page)
        {
              
            if($data[0] != 0){

                if($date[0] != 0)
                    $Totalobservations= $ObservationRepository->findByInterval($project, 1 , $data, $dateStart, $dateEnd,0,0);
                else
                    $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'typeObservation' => $data],array('id' => 'DESC'));
            }
            else{
                
                $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC'));
            }
                
              
          $nbPage = count($Totalobservations)/$itemsPerPage;
         //  ceil($nbPage);
          
             $query = "api/user/project/observation/list";
            $method = "GET";
            $param = ['ID' => $id,
                    'nbPage'  => $nbPage,
                    'Page' =>  ceil($nbPage)]; 
                    
        
                    
        if($data[0] != 0){
            if($date[0] != 0)
                $observations= $ObservationRepository->findByInterval($project, 1 , $data, $dateStart, $dateEnd,$itemsPerPage-$itemsPerPage,$itemsPerPage);
            else
                $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'typeObservation' => $data],array('id' => 'DESC',),($itemsPerPage),($itemsPerPage-$itemsPerPage));
            
        }
        else
            $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC',),($itemsPerPage),($itemsPerPage-$itemsPerPage));
    
       
                 foreach($observations as $ob){
                     
                           // $obs[$i]['email'] = $ob->getUser()->getEmail();
                             $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['projectId'] = $ob->getProject()->getId();
                            $obs[$i]['projectName'] = $ob->getProject()->getName();
                            $obs[$i]['iNaturalistId'] = $ob->getIdInaturalist();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['collector'] = $ob->getCollector();
                            if($ob->getPatrol())
                                    $obs[$i]['patrolId'] = $ob->getPatrol()->getId();
                           if($ob->getImg1() == "observation1.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img/'.$ob->getImg1();
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                            }
                           // $obs[$i]['image'] = $ob->getImg1();
                           $obs[$i]['images'] = $images ;
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservationId'] = $ob->getTypeObservation()->getId();
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "";
                            }
                            if($ob->getSpecie()){
                            $obs[$i]['specieId'] = $ob->getSpecie()->getId();
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                             if($ob->getSpecie()->getSubgroup() == NULL){
                                  $obs[$i]['family'] = "";
                           
                            }else{
                                $obs[$i]['familyId'] = $ob->getSpecie()->getSubgroup()->getId();
                                $obs[$i]['family'] = $ob->getSpecie()->getSubgroup()->getName();
                                
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                            $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            }
                            }else{
                                $obs[$i]['specie'] = "";
                            }
                            
                        
                            if($ob->getUser()){
                            $obs[$i]['user'] = $ob->getUser()->getEmail();
                            }else{
                                $obs[$i]['user'] = "";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                             $obs[$i]['updatedate'] = $ob->getUpdateDate();
                           if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                           
                             if($ob->getSite()){
                                 $obs[$i]['siteId'] = $ob->getSite()->getId();
                                 $obs[$i]['site'] = $ob->getSite()->getName();
                            
                           }else{
                             $obs[$i]['site'] =  "";
                             }
                           
                          // $obs[$i]['nbPage'] =  $nbPage;
                            /* if($ob->getSegment()->getSite() ==  'Null'){
                                  $obs[$i]['site'] =  "NULL";
                            
                           }else{
                               $obs[$i]['siteId'] = $ob->getSegment()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getName();
                             }*/
                           
                          /* if($ob->getSegment()->getSite()){
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['site'] =  "NULL";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }*/
                           
                           $i++;
                 }
                  
      
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $obs);
            
        }
        
        // return $this->respondWithSuccess(sprintf("List of observations"), "", '',['start' => $dateStart,'end' => $dateEnd, 'obs' => $data],[]);
        
        if($data[0] != 0){

            // if($date[0] != 0)
                $Totalobservations= $ObservationRepository->findByInterval($project, 1 , $data, $dateStart, $dateEnd,0,0);
            // else
            //     $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'typeObservation' => $data],array('id' => 'DESC'));
        }
        else{
            // $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC'));
            $Totalobservations= $ObservationRepository->findByInterval($project, 1 , [], $dateStart, $dateEnd,0,0);
        }
        
        
        // if($data[0] != 0)
        //     $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'typeObservation' => $data],array('id' => 'DESC'));
        // else
        //     $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC'));
            
            
         
          $nbPage = count($Totalobservations)/$itemsPerPage;
          
             $query = "api/user/project/observation/list";
            $method = "GET";
            $param = ['ID' => $id,
                    'nbPage'  => $nbPage,
                    'Page' =>  ceil($nbPage)]; 
                    
        
        // if($data[0] != 0)
        //     $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'typeObservation' => $data],array('id' => 'DESC',),($itemsPerPage),(($page *$itemsPerPage)-$itemsPerPage));
        // else
        //     $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC',),($itemsPerPage),(($page *$itemsPerPage)-$itemsPerPage));
            
            
        if($data[0] != 0){
            // if($date[0] != 0)
                $observations= $ObservationRepository->findByInterval($project, 1 , $data, $dateStart, $dateEnd,($page*$itemsPerPage)-$itemsPerPage, $itemsPerPage);
            // else
            //     $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'typeObservation' => $data],array('id' => 'DESC',),($itemsPerPage),(($page *$itemsPerPage)-$itemsPerPage));
        }
        else
            // $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC',),($itemsPerPage),(($page *$itemsPerPage)-$itemsPerPage));
            $observations= $ObservationRepository->findByInterval($project, 1 , [], $dateStart, $dateEnd,($page*$itemsPerPage)-$itemsPerPage, $itemsPerPage);
       

                 foreach($observations as $ob){
                     
                           // $obs[$i]['email'] = $ob->getUser()->getEmail();
                             $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['projectId'] = $ob->getProject()->getId();
                            $obs[$i]['projectName'] = $ob->getProject()->getName();
                            $obs[$i]['idInaturalist'] = $ob->getIdInaturalist();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['collector'] = $ob->getCollector();
                            if($ob->getPatrol())
                                    $obs[$i]['patrolId'] = $ob->getPatrol()->getId();
                           // if($obs['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/'.$ob->getImg1();
                           //$images['img1'] =  'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                           if($ob->getImg1() == "observation1.png" || $ob->getImg1() == "observation.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                            }
                           // $obs[$i]['image'] = $ob->getImg1();
                           $obs[$i]['images'] = $images ;
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservationId'] = $ob->getTypeObservation()->getId();
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "";
                            }
                            if($ob->getSpecie()){
                            $obs[$i]['specieId'] = $ob->getSpecie()->getId();
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                             if($ob->getSpecie()->getSubgroup() == NULL){
                                  $obs[$i]['family'] = "";
                           
                            }else{
                                $obs[$i]['familyId'] = $ob->getSpecie()->getSubgroup()->getId();
                                $obs[$i]['family'] = $ob->getSpecie()->getSubgroup()->getName();
                                
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                            $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            }
                            }else{
                                $obs[$i]['specie'] = "";
                            }
                            
                        
                            if($ob->getUser()){
                            $obs[$i]['user'] = $ob->getUser()->getEmail();
                            }else{
                                $obs[$i]['user'] = "";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                             $obs[$i]['updatedate'] = $ob->getUpdateDate();
                           if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";  
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                            if($ob->getSite()){
                                 $obs[$i]['siteId'] = $ob->getSite()->getId();
                                 $obs[$i]['site'] = $ob->getSite()->getName();
                            
                           }else{
                             $obs[$i]['site'] =  "";
                             }
                          
                            /* if($ob->getSegment()->getSite() ==  'Null'){
                                  $obs[$i]['site'] =  "NULL";
                            
                           }else{
                               $obs[$i]['siteId'] = $ob->getSegment()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getName();
                             }*/
                           
                          /* if($ob->getSegment()->getSite()){
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['site'] =  "NULL";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }*/
                           
                           $i++;
                 }
                  
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $obs);
     
           
        
    }
    
    
     /**
     * List of all observations of one project of a user by page .
     *
     * @Route("/api/user/Allobservation/byPage/list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * ),
     *  @OA\Parameter(
     *     name="numPage",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="itemsPerPage",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="data",
     *     in="query",
     *     @OA\Schema(type="array",
     *     @OA\Items(type="int"))
     * ),
     *  @OA\Parameter(
     *     name="date",
     *     in="query",
     *     @OA\Schema(type="array",
     *     @OA\Items(type="int"))
     * ),
     *  @OA\Parameter(
     *     name="search",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="list view")
     */
       public function AllObservationByPagetAction(Request $request,NormalizerInterface $normalizer,
                                  ObservationRepository $ObservationRepository,
                                  ProjectRepository $ProjectRepository): JsonResponse
    {
         
        
      
         
       
            $request = $this->transformJsonBody($request);
            //$id = $request->get('id');
               $page = $request->get('numPage');
               $itemsPerPage = $request->get('itemsPerPage');
            // $project = $ProjectRepository->findOneBy(['id' => $id]);
             $search = $request->get('search');
             $data = explode(',',$request->get('data'));
            $countData = count($data);
            
            
            $date = explode(',',$request->get('date'));
            $checkDate=$request->query->has('date');
            $dateStart = 0;
            $dateEnd = 0;
            // $countDate = count($date);
            if($checkDate)
                if($date[0] != 0){
                    $dateStart = date('Y-m-d H:i:s', $date[0]);
                    $dateEnd = date('Y-m-d H:i:s', $date[1]);
                    
                }
            
            
            // if($data[0] != 0)
            // {
            //     var_dump('ya des elment');
            // die();
            // }
            // else {var_dump($countData);
            // die();}
              
             
                  
                      $i = 0;
                      $obs = array();
                      $images = array();
                      
        // return $this->respondWithSuccess(sprintf("List of observations"), "", '',['start' => $dateStart,'end' => $dateEnd, 'obs' => count($data)],[]);
        if(!$page)
        {
              
            if($data[0] != 0){

                if($date[0] != 0)
                    $Totalobservations= $ObservationRepository->findAllByfilter(1 , $data, $dateStart, $dateEnd,0,0);
                else
                    $Totalobservations= $ObservationRepository->findBy(['status' => 1, 'typeObservation' => $data],array('id' => 'DESC'));
            }
            else{
                
                $Totalobservations= $ObservationRepository->findBy(['status' => 1],array('id' => 'DESC'));
            }
                
              
          $nbPage = count($Totalobservations)/$itemsPerPage;
         //  ceil($nbPage);
          
             $query = "api/user/project/observation/list";
            $method = "GET";
            $param = ['ID' => $id,
                    'nbPage'  => $nbPage,
                    'Page' =>  ceil($nbPage)]; 
                    
        
                    
        if($data[0] != 0){
            if($date[0] != 0)
                $observations= $ObservationRepository->findAllByfilter(1 , $data, $dateStart, $dateEnd,$itemsPerPage-$itemsPerPage,$itemsPerPage);
            else
                $observations= $ObservationRepository->findBy(['status' => 1, 'typeObservation' => $data],array('id' => 'DESC',),($itemsPerPage),($itemsPerPage-$itemsPerPage));
            
        }
        else
            $observations= $ObservationRepository->findBy(['status' => 1],array('id' => 'DESC',),($itemsPerPage),($itemsPerPage-$itemsPerPage));
    
       
                 foreach($observations as $ob){
                      if($ob->getProject()->getId() != 62155){
                           // $obs[$i]['email'] = $ob->getUser()->getEmail();
                             $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['projectId'] = $ob->getProject()->getId();
                            $obs[$i]['projectName'] = $ob->getProject()->getName();
                            $obs[$i]['iNaturalistId'] = $ob->getIdInaturalist();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['collector'] = $ob->getCollector();
                            if($ob->getPatrol())
                                    $obs[$i]['patrolId'] = $ob->getPatrol()->getId();
                           if($ob->getImg1() == "observation1.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                            }
                           // $obs[$i]['image'] = $ob->getImg1();
                           $obs[$i]['images'] = $images ;
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservationId'] = $ob->getTypeObservation()->getId();
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "";
                            }
                            if($ob->getSpecie()){
                            $obs[$i]['specieId'] = $ob->getSpecie()->getId();
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                             if($ob->getSpecie()->getSubgroup() == NULL){
                                  $obs[$i]['family'] = "";
                           
                            }else{
                                $obs[$i]['familyId'] = $ob->getSpecie()->getSubgroup()->getId();
                                $obs[$i]['family'] = $ob->getSpecie()->getSubgroup()->getName();
                                
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                            $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            }
                            }else{
                                $obs[$i]['specie'] = "";
                            }
                            
                        
                            if($ob->getUser()){
                            $obs[$i]['user'] = $ob->getUser()->getEmail();
                            }else{
                                $obs[$i]['user'] = "";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                             $obs[$i]['updatedate'] = $ob->getUpdateDate();
                           if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                           
                             if($ob->getSite()){
                                 $obs[$i]['siteId'] = $ob->getSite()->getId();
                                 $obs[$i]['site'] = $ob->getSite()->getName();
                            
                           }else{
                             $obs[$i]['site'] =  "";
                             }
                                                        $i++;

                 }
                 }
                  
      
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $obs);
            
        }
        
        // return $this->respondWithSuccess(sprintf("List of observations"), "", '',['start' => $dateStart,'end' => $dateEnd, 'obs' => $data],[]);
        
        if($data[0] != 0){

            // if($date[0] != 0)
                $Totalobservations= $ObservationRepository->findAllByfilter(1 , $data, $dateStart, $dateEnd,0,0);
            // else
            //     $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'typeObservation' => $data],array('id' => 'DESC'));
        }
        else{
            // $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC'));
            $Totalobservations= $ObservationRepository->findAllByfilter(1 , [], $dateStart, $dateEnd,0,0);
        }
        
        
        // if($data[0] != 0)
        //     $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'typeObservation' => $data],array('id' => 'DESC'));
        // else
        //     $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC'));
            
            
         
          $nbPage = count($Totalobservations)/$itemsPerPage;
          
             $query = "api/user/project/observation/list";
            $method = "GET";
            $param = [
                    'nbPage'  => $nbPage,
                    'Page' =>  ceil($nbPage)]; 
                    
        
        // if($data[0] != 0)
        //     $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'typeObservation' => $data],array('id' => 'DESC',),($itemsPerPage),(($page *$itemsPerPage)-$itemsPerPage));
        // else
        //     $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC',),($itemsPerPage),(($page *$itemsPerPage)-$itemsPerPage));
            
            
        if($data[0] != 0){
            // if($date[0] != 0)
                $observations= $ObservationRepository->findAllByfilter(1 , $data, $dateStart, $dateEnd,($page*$itemsPerPage)-$itemsPerPage, $itemsPerPage);
            // else
            //     $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'typeObservation' => $data],array('id' => 'DESC',),($itemsPerPage),(($page *$itemsPerPage)-$itemsPerPage));
        }
        else
            // $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC',),($itemsPerPage),(($page *$itemsPerPage)-$itemsPerPage));
            $observations= $ObservationRepository->findAllByfilter(1 , [], $dateStart, $dateEnd,($page*$itemsPerPage)-$itemsPerPage, $itemsPerPage);
       

                 foreach($observations as $ob){
                     
                           // $obs[$i]['email'] = $ob->getUser()->getEmail();
                           if($ob->getProject()->getId() != 62155){
                             $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['projectId'] = $ob->getProject()->getId();
                            $obs[$i]['projectName'] = $ob->getProject()->getName();
                            $obs[$i]['idInaturalist'] = $ob->getIdInaturalist();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['collector'] = $ob->getCollector();
                            if($ob->getPatrol())
                                    $obs[$i]['patrolId'] = $ob->getPatrol()->getId();
                           // if($obs['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/'.$ob->getImg1();
                           //$images['img1'] =  'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                           if($ob->getImg1() == "observation1.png" || $ob->getImg1() == "observation.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                            }
                           // $obs[$i]['image'] = $ob->getImg1();
                           $obs[$i]['images'] = $images ;
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservationId'] = $ob->getTypeObservation()->getId();
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "";
                            }
                            if($ob->getSpecie()){
                            $obs[$i]['specieId'] = $ob->getSpecie()->getId();
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                             if($ob->getSpecie()->getSubgroup() == NULL){
                                  $obs[$i]['family'] = "";
                           
                            }else{
                                $obs[$i]['familyId'] = $ob->getSpecie()->getSubgroup()->getId();
                                $obs[$i]['family'] = $ob->getSpecie()->getSubgroup()->getName();
                                
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                            $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            }
                            }else{
                                $obs[$i]['specie'] = "";
                            }
                            
                        
                            if($ob->getUser()){
                            $obs[$i]['user'] = $ob->getUser()->getEmail();
                            }else{
                                $obs[$i]['user'] = "";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                             $obs[$i]['updatedate'] = $ob->getUpdateDate();
                           if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";  
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                            if($ob->getSite()){
                                 $obs[$i]['siteId'] = $ob->getSite()->getId();
                                 $obs[$i]['site'] = $ob->getSite()->getName();
                            
                           }else{
                             $obs[$i]['site'] =  "";
                             }
                                                     $i++;

                 }
                 }
                  
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $obs);
     
           
        
    }
    
    
    
    
     /**
     * List of observations unvalid of one project of a user by page .
     *
     * @Route("/api/user/project/observationUnvalid/byPage/list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns list of observations",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * ),
     *  @OA\Parameter(
     *     name="id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="numPage",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="itemsPerPage",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     *  @OA\Parameter(
     *     name="data",
     *     in="query",
     *     @OA\Schema(type="array",
     *     @OA\Items(type="int"))
     * ),
     *  @OA\Parameter(
     *     name="date",
     *     in="query",
     *     @OA\Schema(type="array",
     *     @OA\Items(type="int"))
     * ),
     *  @OA\Parameter(
     *     name="search",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="list view")
     */
       public function ObservationUnvalidByPagetAction(Request $request,NormalizerInterface $normalizer,
                                  ObservationRepository $ObservationRepository,
                                  ProjectRepository $ProjectRepository): JsonResponse
    {
         
        
      
         
       
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
               $page = $request->get('numPage');
               $itemsPerPage = $request->get('itemsPerPage');
             $project = $ProjectRepository->findOneBy(['id' => $id]);
             $search = $request->get('search');
             $data = explode(',',$request->get('data'));
            $countData = count($data);
            
            
            $date = explode(',',$request->get('date'));
            $checkDate=$request->query->has('date');
            $dateStart = 0;
            $dateEnd = 0;
            // $countDate = count($date);
            if($checkDate)
                if($date[0] != 0){
                    $dateStart = date('Y-m-d H:i:s', $date[0]);
                    $dateEnd = date('Y-m-d H:i:s', $date[1]);
                    
                }
            
            
            // if($data[0] != 0)
            // {
            //     var_dump('ya des elment');
            // die();
            // }
            // else {var_dump($countData);
            // die();}
              
             
                  
                      $i = 0;
                      $obs = array();
                      $images = array();
                      
        // return $this->respondWithSuccess(sprintf("List of observations"), "", '',['start' => $dateStart,'end' => $dateEnd, 'obs' => count($data)],[]);
        if(!$page)
        {
              
            if($data[0] != 0){

                if($date[0] != 0)
                    $Totalobservations= $ObservationRepository->findByInterval($project, 0 , $data, $dateStart, $dateEnd,0,0);
                else
                    $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 0, 'typeObservation' => $data],array('id' => 'DESC'));
            }
            else{
                
                $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 0],array('id' => 'DESC'));
            }
                
              
          $nbPage = count($Totalobservations)/$itemsPerPage;
         //  ceil($nbPage);
          
             $query = "api/user/project/observation/list";
            $method = "GET";
            $param = ['ID' => $id,
                    'nbPage'  => $nbPage,
                    'Page' =>  ceil($nbPage)]; 
                    
        
                    
        if($data[0] != 0){
            if($date[0] != 0)
                $observations= $ObservationRepository->findByInterval($project, 0 , $data, $dateStart, $dateEnd,$itemsPerPage-$itemsPerPage,$itemsPerPage);
            else
                $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 0, 'typeObservation' => $data],array('id' => 'DESC',),($itemsPerPage),($itemsPerPage-$itemsPerPage));
            
        }
        else
            $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 0],array('id' => 'DESC',),($itemsPerPage),($itemsPerPage-$itemsPerPage));
    
       
                 foreach($observations as $ob){
                     
                           // $obs[$i]['email'] = $ob->getUser()->getEmail();
                             $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['projectId'] = $ob->getProject()->getId();
                            $obs[$i]['projectName'] = $ob->getProject()->getName();
                            $obs[$i]['idInaturalist'] = $ob->getIdInaturalist();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['collector'] = $ob->getCollector();
                            if($ob->getPatrol())
                                    $obs[$i]['patrolId'] = $ob->getPatrol()->getId();
                           if($ob->getImg1() == "observation1.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                            }
                           // $obs[$i]['image'] = $ob->getImg1();
                           $obs[$i]['images'] = $images ;
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservationId'] = $ob->getTypeObservation()->getId();
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "";
                            }
                            if($ob->getSpecie()){
                            $obs[$i]['specieId'] = $ob->getSpecie()->getId();
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                             if($ob->getSpecie()->getSubgroup() == NULL){
                                  $obs[$i]['family'] = "";
                           
                            }else{
                                $obs[$i]['familyId'] = $ob->getSpecie()->getSubgroup()->getId();
                                $obs[$i]['family'] = $ob->getSpecie()->getSubgroup()->getName();
                                
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                            $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            }
                            }else{
                                $obs[$i]['specie'] = "";
                            }
                            
                        
                            if($ob->getUser()){
                            $obs[$i]['user'] = $ob->getUser()->getEmail();
                            }else{
                                $obs[$i]['user'] = "";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                             $obs[$i]['updatedate'] = $ob->getUpdateDate();
                           if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                           
                             if($ob->getSite()){
                                 $obs[$i]['siteId'] = $ob->getSite()->getId();
                                 $obs[$i]['site'] = $ob->getSite()->getName();
                            
                           }else{
                             $obs[$i]['site'] =  "";
                             }
                           
                          // $obs[$i]['nbPage'] =  $nbPage;
                            /* if($ob->getSegment()->getSite() ==  'Null'){
                                  $obs[$i]['site'] =  "NULL";
                            
                           }else{
                               $obs[$i]['siteId'] = $ob->getSegment()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getName();
                             }*/
                           
                          /* if($ob->getSegment()->getSite()){
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['site'] =  "NULL";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }*/
                           
                           $i++;
                 }
                  
      
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $obs);
            
        }
        
        // return $this->respondWithSuccess(sprintf("List of observations"), "", '',['start' => $dateStart,'end' => $dateEnd, 'obs' => $data],[]);
        
        if($data[0] != 0){

            // if($date[0] != 0)
                $Totalobservations= $ObservationRepository->findByInterval($project, 0 , $data, $dateStart, $dateEnd,0,0);
            // else
            //     $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'typeObservation' => $data],array('id' => 'DESC'));
        }
        else{
            // $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC'));
            $Totalobservations= $ObservationRepository->findByInterval($project, 0 , [], $dateStart, $dateEnd,0,0);
        }
        
        
        // if($data[0] != 0)
        //     $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'typeObservation' => $data],array('id' => 'DESC'));
        // else
        //     $Totalobservations= $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC'));
            
            
         
          $nbPage = count($Totalobservations)/$itemsPerPage;
          
             $query = "api/user/project/observation/list";
            $method = "GET";
            $param = ['ID' => $id,
                    'nbPage'  => $nbPage,
                    'Page' =>  ceil($nbPage)]; 
                    
        
        // if($data[0] != 0)
        //     $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'typeObservation' => $data],array('id' => 'DESC',),($itemsPerPage),(($page *$itemsPerPage)-$itemsPerPage));
        // else
        //     $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC',),($itemsPerPage),(($page *$itemsPerPage)-$itemsPerPage));
            
            
        if($data[0] != 0){
            // if($date[0] != 0)
                $observations= $ObservationRepository->findByInterval($project, 0 , $data, $dateStart, $dateEnd,($page*$itemsPerPage)-$itemsPerPage, $itemsPerPage);
            // else
            //     $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 1, 'typeObservation' => $data],array('id' => 'DESC',),($itemsPerPage),(($page *$itemsPerPage)-$itemsPerPage));
        }
        else
            // $observations= $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC',),($itemsPerPage),(($page *$itemsPerPage)-$itemsPerPage));
            $observations= $ObservationRepository->findByInterval($project, 0 , [], $dateStart, $dateEnd,($page*$itemsPerPage)-$itemsPerPage, $itemsPerPage);
       

                 foreach($observations as $ob){
                     
                           // $obs[$i]['email'] = $ob->getUser()->getEmail();
                             $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['projectId'] = $ob->getProject()->getId();
                            $obs[$i]['projectName'] = $ob->getProject()->getName();
                            $obs[$i]['idInaturalist'] = $ob->getIdInaturalist();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['collector'] = $ob->getCollector();
                            if($ob->getPatrol())
                                    $obs[$i]['patrolId'] = $ob->getPatrol()->getId();
                           // if($obs['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/'.$ob->getImg1();
                           //$images['img1'] =  'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                           if($ob->getImg1() == "observation1.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                            }
                           // $obs[$i]['image'] = $ob->getImg1();
                           $obs[$i]['images'] = $images ;
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservationId'] = $ob->getTypeObservation()->getId();
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "";
                            }
                            if($ob->getSpecie()){
                            $obs[$i]['specieId'] = $ob->getSpecie()->getId();
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                             if($ob->getSpecie()->getSubgroup() == NULL){
                                  $obs[$i]['family'] = "";
                           
                            }else{
                                $obs[$i]['familyId'] = $ob->getSpecie()->getSubgroup()->getId();
                                $obs[$i]['family'] = $ob->getSpecie()->getSubgroup()->getName();
                                
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                            $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            }
                            }else{
                                $obs[$i]['specie'] = "";
                            }
                            
                        
                            if($ob->getUser()){
                            $obs[$i]['user'] = $ob->getUser()->getEmail();
                            }else{
                                $obs[$i]['user'] = "";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                             $obs[$i]['updatedate'] = $ob->getUpdateDate();
                           if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";  
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                            if($ob->getSite()){
                                 $obs[$i]['siteId'] = $ob->getSite()->getId();
                                 $obs[$i]['site'] = $ob->getSite()->getName();
                            
                           }else{
                             $obs[$i]['site'] =  "";
                             }
                          
                            /* if($ob->getSegment()->getSite() ==  'Null'){
                                  $obs[$i]['site'] =  "NULL";
                            
                           }else{
                               $obs[$i]['siteId'] = $ob->getSegment()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getName();
                             }*/
                           
                          /* if($ob->getSegment()->getSite()){
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['site'] =  "NULL";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }*/
                           
                           $i++;
                 }
                  
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $obs);
     
    }
    
     /**
     * List of typeObservation of one project of a user  .
     *
     * @Route("/api/user/typeObservation/project", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns list of type observation",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * ),
     * @OA\Parameter(
     *     name="id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="list view")
     */
       public function ListTypeObservationByProjectAction(Request $request,NormalizerInterface $normalizer,
                                  ObservationRepository $ObservationRepository,
                                  ProjectRepository $ProjectRepository,
                                  TypeObservationRepository $TypeObservationRepository): JsonResponse
    {
         
        
      
         
       
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            //$page = $request->get('NumPage');
             $project = $ProjectRepository->findOneBy(['id' => $id]);
             
            $query = "api/user/project/observation/list";
            $method = "GET";
            $param = ['ID' => $id]; 
              
             
                  
                      $i = 0;
                      $j = 0;
                      $obs = array();
                      $typeObs = array();
                      
           
                 //$observations = $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC',),(20),(20-20));
                 $observations = $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC',),1000,0);
                 
                 foreach($observations as $ob){
                     
                          
                         
                            $obs[$i] = $ob->getTypeObservation()->getId();
                          
                           
                           
                           $i++;
                 }
                 
                 foreach(array_unique($obs) as $typeOId){
                     
                     $typeOb = $TypeObservationRepository->findOneBy(['id' => $typeOId]);
                     
                     //var_dump($typeOb);
                     $typeObs[$j]['id'] = $typeOId;
                    $typeObs[$j]['name']  = $typeOb->getName();
                     $j++;
                 }
                 
                 //print_r(array_unique($obs)); die();
                  
                   $observationsNormalizer = $normalizer->normalize($observations, null, ['groups' => 'observation:read']);
      
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $typeObs);
            
             
     
           
        
    }
    
      
    /**
     * List of observations not validated of one project of a user  .
     *
     * @Route("/api/userObservationNoValidate/project", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the list of observartions no validated",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * ),
     * @OA\Parameter(
     *     name="id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="list view")
     */
       public function ListObservationNoValidction(Request $request,NormalizerInterface $normalizer,
                                  ObservationRepository $ObservationRepository,
                                  ProjectRepository $ProjectRepository): JsonResponse
    {
         
        
      
         
       
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            //$page = $request->get('NumPage');
             $project = $ProjectRepository->findOneBy(['id' => $id]);
             
            $query = "api/user/project/observation/list";
            $method = "GET";
            $param = ['ID' => $id]; 
              
             
                  
                      $i = 0;
                      $obs = array();
                      $images = array();
           
                 //$observations = $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC',),(20),(20-20));
                 $observations = $ObservationRepository->findBy(['project' => $project, 'status' => 0],array('id' => 'DESC'), 1000);
                 foreach($observations as $ob){
                            
                             $obs[$i]['id'] = $ob->getId();
                            $obs[$i]['projectId'] = $ob->getProject()->getId();
                            $obs[$i]['projectName'] = $ob->getProject()->getName();
                            $obs[$i]['idInaturalist'] = $ob->getIdInaturalist();
                           // $obs[$i]['email'] = $ob->getUser()->getEmail();
                            $obs[$i]['coordX'] = $ob->getCoordX();
                            $obs[$i]['coordY'] = $ob->getCoordY(); 
                            $obs[$i]['note'] = $ob->getNote();
                            $obs[$i]['alpha'] = $ob->getAlpha();
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['collector'] = $ob->getCollector();
                           // if($obs['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/'.$ob->getImg1();
                           //$images['img1'] =  'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                           if($ob->getImg1() == "observation1.png"){
                                $images['img1'] = "NULL"; 
                            }else{
                                $images['img1'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img1/'.$ob->getImg1();
                            }
                            if($ob->getImg2() == "observation2.png"){
                                $images['img2'] = "NULL"; 
                            }else{
                                $images['img2'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img2/'.$ob->getImg2();
                            }
                            if($ob->getImg3() == "observation3.png"){
                                 $images['img3'] = "NULL"; 
                            }else{
                               $images['img3'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img3/'.$ob->getImg3();   
                            }
                            if($ob->getImg4() == "observation4.png"){
                             $images['img4'] = "NULL"; 
                            }else{
                                $images['img4'] = 'https://api.sirenammco.org'. '/public/uploads/observations/img4/'.$ob->getImg4();
                            }
                           // $obs[$i]['image'] = $ob->getImg1();
                           $obs[$i]['images'] = $images ;
                            $obs[$i]['dead'] = $ob->getDead();
                            $obs[$i]['status'] = $ob->getStatus();
                            if($ob->getTypeObservation()){
                            $obs[$i]['TypeObservationId'] = $ob->getTypeObservation()->getId();
                            $obs[$i]['TypeObservation'] = $ob->getTypeObservation()->getName();
                            }else{
                                $obs[$i]['TypeObservation'] = "";
                            }
                            if($ob->getSpecie()){
                            $obs[$i]['specieId'] = $ob->getSpecie()->getId();
                            $obs[$i]['specie'] = $ob->getSpecie()->getName();
                            
                         $obs[$i]['familyId'] = $ob->getSpecie()->getSubgroup()->getId();
                                $obs[$i]['family'] = $ob->getSpecie()->getSubgroup()->getName();
                                
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                        $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            
                            }else{
                                $obs[$i]['specie'] = "";
                            }
                            
                        /*     if($ob->getSpecie()->getSubgroup()->getGroupe()){
                            $obs[$i]['groupId'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getId();
                            $obs[$i]['group'] = $ob->getSpecie()->getSubgroup()->getGroupe()->getName();
                            }else{
                                $obs[$i]['group'] = "";
                            }*/
                            if($ob->getUser()){
                            $obs[$i]['user'] = $ob->getUser()->getEmail();
                            }else{
                                $obs[$i]['user'] = "";
                            }
                              
                           $obs[$i]['date'] = $ob->getDate();
                                                        $obs[$i]['updatedate'] = $ob->getUpdateDate();
                           if($ob->getSegment()){
                            $obs[$i]['segmentId'] = $ob->getSegment()->getId();
                            $obs[$i]['segment'] = $ob->getSegment()->getName();
                           }else{
                               
                              $obs[$i]['segment'] =  "";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }
                           
                            if($ob->getSite()){
                                 $obs[$i]['siteId'] = $ob->getSite()->getId();
                                 $obs[$i]['site'] = $ob->getSite()->getName();
                            
                           }else{
                             $obs[$i]['site'] =  "";
                             }
                            /* if($ob->getSegment()->getSite() ==  'Null'){
                                  $obs[$i]['site'] =  "NULL";
                            
                           }else{
                               $obs[$i]['siteId'] = $ob->getSegment()->getId();
                            $obs[$i]['site'] = $ob->getSegment()->getName();
                             }*/
                           
                          /* if($ob->getSegment()->getSite()){
                            $obs[$i]['site'] = $ob->getSegment()->getSite()->getName();
                           }else{
                               
                              $obs[$i]['site'] =  "NULL";
                              //geocode2($ob->getCoordX(),$ob->getCoordY());
                           }*/
                           
                           $i++;
                 }
                  
                   $observationsNormalizer = $normalizer->normalize($observations, null, ['groups' => 'observation:read']);
      
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $obs);
            
             
     
           
        
    }
    
     /**
     * List of observations of one project of a user par nombre .
     *
     * @Route("/api/user/Project/observation1", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * ),
     * @OA\Parameter(
     *     name="id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="NumPage",
     *     in="query",
     *     @OA\Schema(type="integer")
     * ),
     * @OA\Parameter(
     *     name="per_page",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="list view")
     */
       public function ListObservationByProject1Action(Request $request,NormalizerInterface $normalizer,
                                  ObservationRepository $ObservationRepository,
                                  ProjectRepository $ProjectRepository): JsonResponse
    {
         
        
      
         
       
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $page = $request->get('NumPage');
             $per_page = $request->get('$per_page');
             $project = $ProjectRepository->findOneBy(['id' => $id]);
             
            $query = "api/user/project/observation/list";
            $method = "GET";
            $param = ['ID' => $id]; 
              
             
             if(!$page){
                  

           
                 $observations = $ObservationRepository->findBy(['project' => $project, 'status' => 1],array('id' => 'DESC',),(20),(20-20));
                  
                   $observationsNormalizer = $normalizer->normalize($observations, null, ['groups' => 'observation:read']);
      
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $observationsNormalizer);
             }else{
                   $observations = $ObservationRepository->findBy(['project' => $pro->getProject()],array('id' => 'DESC',),($page*$per_page),(($page*$per_page)-$per_page));
                  
                   $observationsNormalizer = $normalizer->normalize($observations, null, ['groups' => 'observation:read']);
      
                return $this->respondWithSuccess(sprintf("List of user's observations"), 
                $query, 
                $method,
               $param,
               $obs);
             }
             
     
           
        
    }
    
    
        /**
     * Number of observations of one project of a user  .
     *
     * @Route("/api/user/Project/observation/count", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"user"}))
     *     )
     * ),
     * @OA\Parameter(
     *     name="id",
     *     in="query",
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="list view")
     */
       public function NumberObservationByProjectAction(Request $request,NormalizerInterface $normalizer,
                                  ObservationRepository $ObservationRepository,
                                  ProjectRepository $ProjectRepository): JsonResponse
    {
         
        
      
         
       
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
             $project = $ProjectRepository->findOneBy(['id' => $id]);
             
            $query = "api/user/project/observation/count";
            $method = "GET";
            $param = ['ID' => $id]; 
           
             
             $NbObservations = count($ObservationRepository->findBy(['project' => $project, 'status' => 1]));
                   $data = ['NbObservations' => $NbObservations];
      
                return $this->respondWithSuccess(sprintf("List of observations"), 
                $query, 
                $method,
               $param,
               $data);
           
             
     
           
        
    }
    
     /**
     * list of observations by date range.
     *
     * @Route("/api/user/project/observationIbtervalDate", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns the token of an user",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"list view"}))
     *     )
     * ),
     * @OA\Parameter(
     *     name="startdate",
     *     in="query",
     *     @OA\Schema(type="datetime")
     * ),
     * @OA\Parameter(
     *     name="enddate",
     *     in="query",
     *     @OA\Schema(type="datetime")
     * )
     * 
     * @OA\Tag(name="list view")
     */
       public function ListObservationByDateAction(Request $request,NormalizerInterface $normalizer,
                                  ProjectRepository $ProjectRepository,
                                  ObservationRepository $ObservationRepository): JsonResponse
    {
          $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
         $email= $decodedJwtToken['username'];
          $dateExpiration= $decodedJwtToken['exp'];
          $temps_actuel = date("U");
          //echo $temps_actuel; die();
        
         $user = $this->repository->findOneBy(['email' => $email]);
         
       
            $request = $this->transformJsonBody($request);
              $startdate = $request->get('startdate');
              $enddate = $request->get('enddate');
              echo $startdate; echo "<br>"; echo $enddate; die();
             $id = $request->get('id');
             $project = $ProjectRepository->findById(['id' => $id]);
             
         
            $query = "api/user_project_observation/list";
            $method = "GET";
            $param = ['NULL']; 

           
                $observations = $ObservationRepository->findBy(['project' => $project]);
                
            
                //dd($projects);
               
                $observationsNormalizer = $normalizer->normalize($observations, null, ['groups' => 'observation:read']);
      
                return $this->respondWithSuccess(sprintf("List of user's observations"), 
                $query, 
                $method,
               $param,
               $data);
       
     
           
        
    }

     /**
     * @Route("api/admin/user/update", name="admin_user_update", methods={"PUT"})
     */
    public function adminupdateAction(Request $request, UserPasswordEncoderInterface $encoder): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $role = $decodedJwtToken['roles'][0];

        if($role == "ROLE_ADMIN")
        {
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $password = $request->get('password');
            $email = $request->get('email');
            $role = $request->get('roles');
            $ville = $request->get('ville');
            $telephone = $request->get('telephone');
    
           $user = $this->repository->findOneBy(['id' => $id]);
    
    
            if($email)
            {
                $user->setEmail($email);
                
            }
    
            if($password)
            {
                $user->setPassword($encoder->encodePassword($user, $password));
                
            }
    
            if($role)
            {
                $user->setRoles($role);
                
            }
    
            if($ville)
            {
                $user->setVille($ville);
              
            }
    
            if($telephone)
            {
                $user->setTelephone($telephone);
            }
    
    
            $this->em->persist($user);
            $this->em->flush();
            $query = "api/admin/user/update";
            $method = "PUT";
            $param = [
                      'id'   => $id,
                     'email ' => $email,
                     'role'   => $role,
                     'ville ' => $user->getVille(),
                     'telephone ' => $telephone,
    
                      ];
            $data = ['id' => $user->getId(),
                     'email ' => $user->getEmail(),
                     'role ' => $user->getRoles(),
                     'ville ' => $user->getVille(),
                     'telephone ' => $user->getTelephone(),
                     'enabled ' => $user->getEnabled()
                      ];  
            return $this->respondWithSuccess(sprintf('User %s successfully update', 
                                             $user->getUsername()), 
                                             $query, 
                                             $method,
                                            $param,
                                            $data);
           // return $this->respondWithSuccess(sprintf('User %s successfully update', $user->getUsername()));
        }
        return $this->json('Law admin requires');
    }

    /**
     * @Route("/api/userproject/delete", name="admin_user_delete", methods={"DELETE"})
     */
    public function admindeleteAction(Request $request,
                                       UserProjectRepository $UserProjectRepository): Response 
    {
        
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $user = $UserProjectRepository->findOneBy(['id' => $id]);
            
            $this->em->remove($user);
            $this->em->flush();

            return $this->respondWithSuccess(sprintf('User %s successfully delete', $user->getUsername()));

    }
    
    
     
    /**
     * List users .
     *
     * @Route("/api/admin/user/list  ", name="adminuser_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns list of  users",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class, groups={"admin"}))
     *     )
     * )
     * 
     * @OA\Tag(name="admin")
     */ 
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {
        $request = $this->transformJsonBody($request);
        

            $query = "api/user/list";
            $method = "GET";
            $param = ['NULL']; 

                $users = $this->repository->findAll();
               
                $usersNormalizer = $normalizer->normalize($users, null, ['groups' => 'user:read']);
      
                return $this->respondWithSuccess(sprintf('List of users'), 
                $query, 
                $method,
               $param,
               $usersNormalizer);
          
       
    }

    /**
     * @Route("/api/admin/user/list  ", name="admin_user_list", methods={"GET"})
     */
    public function adminlistAction(Request $request, NormalizerInterface $normalizer): Response 
    {
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $role = $decodedJwtToken['roles'][0];

        if($role == "ROLE_ADMIN")
        { 

            $query = "api/user/list";
            $method = "GET";
            $param = ['NULL']; 

            if(!$page)
            {
                $users = $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
               
                $questionsNormalizer = $normalizer->normalize($users, null, ['groups' => 'user:read']);
      
                return $this->respondWithSuccess(sprintf('infos of user %s', 
                $user->getUsername()), 
                $query, 
                $method,
               $param,
               $usersNormalizer);
            }

            
            $users = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
    
            return $this->json($users, 201, [], ['groups' => 'user:read']);

        }

        return $this->json('Law admin requires');
       
    }

      /**
     * @Route("/api/admin/user/read  ", name="admin_user_read", methods={"GET"})
     */
    public function adminreadAction(Request $request): Response 
    {
        //$token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $role = $decodedJwtToken['roles'][0];
        //dd($role);

        if($role == "ROLE_ADMIN")
        {
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $user = $this->repository->findOneBy(['id' => $id]);

            $query = "api/user/read";
        $method = "GET";
        $param = ['id' =>$id];
        $data = ['id' => $user->getId(),
                 'email ' => $user->getEmail(),
                 'role ' => $user->getRoles(),
                 'ville ' => $user->getVille(),
                 'telephone ' => $user->getTelephone(),
                 'enabled ' => $user->getEnabled()
                  ];  
        return $this->respondWithSuccess(sprintf('infos of user %s', 
                                         $user->getUsername()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
    
         //   return $this->json($user, 201, [], ['groups' => 'user:read']);

        }

          return $this->json('Law admin requires');
       
    }

     /**
     * @Route("/api/admin/user/suspend", name="admin_user_suspend", methods={"PUT"})
     */
    public function adminsuspendAction(Request $request): Response 
    {

        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $role = $decodedJwtToken['roles'][0];

        if($role == "ROLE_ADMIN")
        {
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $user = $this->repository->findOneBy(['id' => $id]);
    
            $user->setEnabled('0');
            $this->em->flush();
            $query = "api/user/suspend";
            $method = "GET";
            $param = ['id' => $id];
            $data = ['id' => $user->getId(),
                     'email ' => $user->getEmail(),
                     'role ' => $user->getRoles(),
                     'ville ' => $user->getVille(),
                     'telephone ' => $user->getTelephone(),
                     'enabled ' => $user->getEnabled()
                      ];  
            return $this->respondWithSuccess(sprintf('User %s successfully suspended', 
                                             $user->getUsername()), 
                                             $query, 
                                             $method,
                                            $param,
                                            $data);
          }

        return $this->json('Law admin requires');
    }


    /**
     * @Route("/api/admin/user/enabled", name="admin_user_enabled", methods={"PUT"})
     */
    public function adminenabledAction(Request $request): Response 
    {

        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $role = $decodedJwtToken['roles'][0];

        if($role == "ROLE_ADMIN")
        {
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $user = $this->repository->findOneBy(['id' => $id]);
    
            $user->setEnabled('1');
            $this->em->flush();

            $this->em->flush();
            $query = "api/user/enabled";
            $method = "GET";
            $param = ['id' => $id];
            $data = ['id' => $user->getId(),
                     'email ' => $user->getEmail(),
                     'role ' => $user->getRoles(),
                     'ville ' => $user->getVille(),
                     'telephone ' => $user->getTelephone(),
                     'enabled ' => $user->getEnabled()
                      ];  
            return $this->respondWithSuccess(sprintf('User %s successfully activated', 
                                             $user->getUsername()), 
                                             $query, 
                                             $method,
                                            $param,
                                            $data);
    
        }

        return $this->json('Law admin requires');
    }

    
}