<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Repository\NotificationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class NotificationController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                NotificationRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

     /**
     * @Route("api/notification/create", name="notification_create", methods={"POST"})
     */
    public function createAction(Request $request): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $title = $request->get('title');
        if (empty($title)){
        return $this->respondValidationError("All fields are required");;
        }

        $notification = new Notification();
        $notification->setTitle($title);

        $this->em->persist($notification);
        $this->em->flush();

        $query = "api/notification/create";
        $method = "POST";
        $param = [
                 'title ' => $title,

                  ];
        $data = ['id' => $notification->getId(),
                'title ' => $notification->getTitle(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The notification whit a message %s has been successfully created', 
                                         $notification->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }
    
     /**
     * @Route("api/notification/transfert", name="notification_transfert", methods={"POST"})
     */
    public function transfertAction(Request $request): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $id = $request->get('id');
        $date = $request->get('date');
        $title = $request->get('title');
        if (empty($title)){
        return $this->respondValidationError("All fields are required");;
        }

        $notification = new Notification();
        $notification->setId($id);
        $notification->setDate(new \DateTime($date));
        $notification->setTitle($title);

        $this->em->persist($notification);
        $this->em->flush();

        $query = "api/notification/create";
        $method = "POST";
        $param = [
                 'title ' => $title,

                  ];
        $data = ['id' => $notification->getId(),
                'title ' => $notification->getTitle(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The notification whit a message %s has been successfully created', 
                                         $notification->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/notification/read  ", name="notification_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $notification = $this->repository->findOneBy(['id' => $id]);

        $query = "api/notification/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $notification->getId(),
                'title ' => $notification->getTitle(),
                        ];  
        return $this->respondWithSuccess(sprintf('infos of notification %s', 
                                         $notification->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }


     /**
     * @Route("/api/notification/list  ", name="notification_list", methods={"GET"})
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/notification/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $notifications= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $notificationNormalizer = $normalizer->normalize($notifications, null, ['groups' => 'notification:read']);
       
           return $this->respondWithSuccess(sprintf('List of notifications'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $notificationNormalizer);
        }

        
        $notifications = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $notificationNormalizer = $normalizer->normalize($notifications, null, ['groups' => 'notification:read']);
       
        return $this->respondWithSuccess(sprintf('List of notifications'), 
       $query, 
       $method,
      $param,
      $notificationNormalizer);
    }

/**
     * @Route("/api/notification/delete", name="notification_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $notification = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($notification);
            $this->em->flush();
            
            $query = "api/notification/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Notification %s successfully delete', 
                                                      $notification->getTitle()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/notification/update", name="notification_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $title = $request->get('title');
    
           $notification = $this->repository->findOneBy(['id' => $id]);
    
    

            if($title)
            {
                $notification->setTitle($title);
                
            }

    
            $this->em->persist($notification);
            $this->em->flush();

            $query = "api/notification/update";
            $method = "PUT";
            $param = [
                'title ' => $title,

                 ];
            $data = ['id' => $notification->getId(),
                    'title ' => $notification->getTitle(),
                        
                        ];           
        return $this->respondWithSuccess(sprintf('The notification with message %s has been successfully update', 
                                         $notification->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }

}
