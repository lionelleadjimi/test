<?php

namespace App\Controller;

use App\Entity\Marketplace;
use App\Repository\MarketplaceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class MarketplaceController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                MarketplaceRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

     /**
     * @Route("api/marketplace/create", name="marketplace_create", methods={"POST"})
     */
    public function createAction(Request $request): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $typeOfFish = $request->get('type_of_fish');
        $description = $request->get('description');
        $unloader = $request->get('unloader');
        $fishSite = $request->get('fish_site');
        $quantity = $request->get('quantity');
        $price = $request->get('price');
        $phone = $request->get('phone');
        $img1 = $request->get('img1');
        $img2 = $request->get('img2');
        $img3 = $request->get('img3');
        $img4= $request->get('img4');
       // dd($typeOfFish);

        if (empty($typeOfFish) || empty($description) || empty($unloader) || empty($fishSite) || empty($quantity) || empty($price) || empty($phone)){
        return $this->respondValidationError("All fields are required");;
        }

        $marketplace = new Marketplace();
        $marketplace->setTypeOfFish($typeOfFish);
        $marketplace->setDescription($description);
        $marketplace->setUnloader($unloader);
        $marketplace->setFishSite($fishSite);
        $marketplace->setQuantity($quantity);
        $marketplace->setPrice($price);
        $marketplace->setPhone($phone);
        if ($img1)
        {
           
                            $photo = $img1;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/marketplaces/';
                                   // dd($webPath);
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                   
                                    $marketplace->setImg1($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $marketplace->setImg1('marketplace1.png');
        }
        if ($img2)
        {
           
                            $photo = $img2;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/marketplaces/';
                                   // dd($webPath);
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                   
                                    $marketplace->setImg2($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $marketplace->setImg2('marketplace2.png');
        }
        if ($img3)
        {
           
                            $photo = $img3;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/marketplaces/';
                                   // dd($webPath);
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                   
                                    $marketplace->setImg3($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $marketplace->setImg3('marketplace3.png');
        }
        if ($img4)
        {
           
                            $photo = $img4;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/marketplaces/';
                                   // dd($webPath);
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                   
                                    $marketplace->setImg4($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $marketplace->setImg4('marketplace4.png');
        }


        $this->em->persist($marketplace);
        $this->em->flush();

        $query = "api/marketplace/create";
        $method = "POST";
        $param = [
                 'type oh fish ' => $typeOfFish,
                 'description ' => $description,
                 'unloader ' => $unloader,
                 'fish site' => $fishSite,
                 'quantity ' => $quantity,
                 'price ' => $price,
                 'phone ' => $phone,
                 'img1 ' => $img1,
                 'img2 ' => $img2,
                 'img3 ' => $img3,
                 'img4 ' => $img4,

                  ];
        $data = ['id' => $marketplace->getId(),
                'date ' => $marketplace->getDate(),
                'type oh fish ' => $marketplace->getTypeOfFish(),
                'description ' => $marketplace->getDescription(),
                'unloader ' => $marketplace->getUnloader(),
                'fish site ' => $marketplace->getFishSite(),
                'quantity ' => $marketplace->getQuantity(),
                'price ' => $marketplace->getPrice(),
                'phone ' => $marketplace->getPhone(),
                'img1 ' => $marketplace->getImg1(),
                'img2 ' => $marketplace->getImg2(),
                'img3 ' => $marketplace->getImg3(),
                'img4 ' => $marketplace->getImg4(),
                'status ' => $marketplace->getstatus(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The marketplace with description %s has been successfully created', 
                                         $marketplace->getDescription()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/marketplace/read  ", name="marketplace_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $marketplace = $this->repository->findOneBy(['id' => $id]);

        $query = "api/marketplace/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $marketplace->getId(),
                'date ' => $marketplace->getDate(),
                'type oh fish ' => $marketplace->getTypeOfFish(),
                'description ' => $marketplace->getDescription(),
                'unloader ' => $marketplace->getUnloader(),
                'fish site ' => $marketplace->getFishSite(),
                'quantity ' => $marketplace->getQuantity(),
                'price ' => $marketplace->getPrice(),
                'phone ' => $marketplace->getPhone(),
                'img1 ' => $marketplace->getImg1(),
                'img2 ' => $marketplace->getImg2(),
                'img3 ' => $marketplace->getImg3(),
                'img4 ' => $marketplace->getImg4(),
                'status ' => $marketplace->getstatus(),
                 
                  ];           
        return $this->respondWithSuccess(sprintf('infos of marketplace with description  %s', 
                                         $marketplace->getDescription()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }


     /**
     * @Route("/api/marketplace/list  ", name="marketplace_list", methods={"GET"})
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/marketplace/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $marketplaces= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $marketplacesNormalizer = $normalizer->normalize($marketplaces, null, ['groups' => 'marketplace:read']);
       
           return $this->respondWithSuccess(sprintf('List of marketplaces'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $marketplacesNormalizer);
        }

        
        $marketplaces = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $marketplacesNormalizer = $normalizer->normalize($marketplaces, null, ['groups' => 'marketplace:read']);
       
        return $this->respondWithSuccess(sprintf('List of marketplaces'), 
       $query, 
       $method,
      $param,
      $marketplacesNormalizer);
    }

/**
     * @Route("/api/marketplace/delete", name="marketplace_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $marketplace = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($marketplace);
            $this->em->flush();
            
            $query = "api/marketplace/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('marketplace with description %s successfully delete', 
                                                      $marketplace->getDescription()),
                                                     $query,
                                                     $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/marketplace/update", name="marketplace_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $typeOfFish = $request->get('type_of_fish');
        $description = $request->get('description');
        $unloader = $request->get('unloader');
        $fishSite = $request->get('fish_site');
        $quantity = $request->get('quantity');
        $price = $request->get('price');
        $phone = $request->get('phone');
        $img1 = $request->get('img1');
        $img2 = $request->get('img2');
        $img3 = $request->get('img3');
        $img4= $request->get('img4');

    
           $marketplace = $this->repository->findOneBy(['id' => $id]);
    
           $marketplace->setPhone($phone);

            if($typeOfFish)
            {
                $marketplace->setTypeOfFish($typeOfFish);
                
            }
            if($description)
            {
                $marketplace->setDescription($description);
                
            }
            if($unloader)
            {
                $marketplace->setUnloader($unloader);
                
            }
            if($fishSite)
            {
                $marketplace->setFishSite($fishSite);
                
            }
            if($quantity)
            {
                $marketplace->setQuantity($quantity);
                
            }
            if($price)
            {
                $marketplace->setPrice($price);
                
            }
            if($phone)
            {
                $marketplace->getPhone($phone);
                
            }

            if ($img1)
            {
               
                                $photo = $img1;
                               
                                    
    
                                try {
                                    if ($photo != NULL) {
                                        $photo = explode(',', $photo);
                                        $extension = str_replace('data:image/', '', $photo[0]);
                                        $extension = str_replace(';base64', '', $extension);
                                       // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                        $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/marketplaces/';
                                       // dd($webPath);
                                        $sourcename = $webPath . uniqid() . '.jpg';
    
                                        $file = fopen($sourcename, 'w+');
                                        fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                        fclose($file);
    
                                        $photoUrl = str_replace($webPath, '', $sourcename);
                                       
                                        $marketplace->setImg1($photoUrl);
                                    }
                                } catch (\Exception $e) {
                                    $json["success"] = false;
                                    $json["error"] = "image";
                                    $json["message"] = $e->getMessage();
                                }
            }
            if ($img2)
            {
               
                                $photo = $img2;
                               
                                    
    
                                try {
                                    if ($photo != NULL) {
                                        $photo = explode(',', $photo);
                                        $extension = str_replace('data:image/', '', $photo[0]);
                                        $extension = str_replace(';base64', '', $extension);
                                       // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                        $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/marketplaces/';
                                       // dd($webPath);
                                        $sourcename = $webPath . uniqid() . '.jpg';
    
                                        $file = fopen($sourcename, 'w+');
                                        fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                        fclose($file);
    
                                        $photoUrl = str_replace($webPath, '', $sourcename);
                                       
                                        $marketplace->setImg2($photoUrl);
                                    }
                                } catch (\Exception $e) {
                                    $json["success"] = false;
                                    $json["error"] = "image";
                                    $json["message"] = $e->getMessage();
                                }
            }

            if ($img3)
            {
               
                                $photo = $img3;
                               
                                    
    
                                try {
                                    if ($photo != NULL) {
                                        $photo = explode(',', $photo);
                                        $extension = str_replace('data:image/', '', $photo[0]);
                                        $extension = str_replace(';base64', '', $extension);
                                       // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                        $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/marketplaces/';
                                       // dd($webPath);
                                        $sourcename = $webPath . uniqid() . '.jpg';
    
                                        $file = fopen($sourcename, 'w+');
                                        fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                        fclose($file);
    
                                        $photoUrl = str_replace($webPath, '', $sourcename);
                                       
                                        $marketplace->setImg3($photoUrl);
                                    }
                                } catch (\Exception $e) {
                                    $json["success"] = false;
                                    $json["error"] = "image";
                                    $json["message"] = $e->getMessage();
                                }
            }
            if ($img4)
        {
           
                            $photo = $img4;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/marketplaces/';
                                   // dd($webPath);
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                   
                                    $marketplace->setImg4($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }

    
            $this->em->persist($marketplace);
            $this->em->flush();

            $query = "api/marketplace/update";
            $method = "PUT";
            $param = [
                'type oh fish ' => $typeOfFish,
                'description ' => $description,
                'unloader ' => $unloader,
                'fish site' => $fishSite,
                'quantity ' => $quantity,
                'price ' => $price,
                'phone ' => $phone,
                'img1 ' => $img1,
                'img2 ' => $img2,
                'img3 ' => $img3,
                'img4 ' => $img4,

                 ];
       $data = ['id' => $marketplace->getId(),
               'date ' => $marketplace->getDate(),
               'type oh fish ' => $marketplace->getTypeOfFish(),
               'description ' => $marketplace->getDescription(),
               'unloader ' => $marketplace->getUnloader(),
               'fish site ' => $marketplace->getFishSite(),
               'quantity ' => $marketplace->getQuantity(),
               'price ' => $marketplace->getPrice(),
               'phone ' => $marketplace->getPhone(),
               'img1 ' => $marketplace->getImg1(),
               'img2 ' => $marketplace->getImg2(),
               'img3 ' => $marketplace->getImg3(),
               'img4 ' => $marketplace->getImg4(),
               'status ' => $marketplace->getstatus(),
                
                 ];           
        return $this->respondWithSuccess(sprintf('The marketplace with message %s has been successfully update', 
                                         $marketplace->getDescription()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
}
