<?php

namespace App\Controller;

use App\Entity\Site;
use App\Repository\SiteRepository;
use App\Repository\SegmentRepository;
use App\Repository\ObservationRepository;
use App\Repository\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class SiteController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                SiteRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

     

     
     /**
     * Register.
     *
     * @Route("/api/site/create", name="site_create", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the site's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Site::class, groups={"site"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="name",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="projectId",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="site")
     */ 
    public function createAction(Request $request, ProjectRepository $ProjectRepository): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $id = $request->get('id');
         $name = $request->get('name');
        $project_id = $request->get('projectId');
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);

        if (empty($name)) {
            return $this->respondValidationError("All fields are required");
        }

        $site = new Site();
        $site->setName($name);
        $site->setProject($project);
        

        $this->em->persist($site);
        $this->em->flush();

        $query = "api/site/create";
        $method = "POST";
        $param = [
                 'name' => $name,
                 'project_id ' => $project_id,
                  ];
        $data = ['id' => $site->getId(),
                'name ' => $site->getName(),
                'project ' => $site->getProject()->getName(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The site named %s has been successfully created', 
                                         $site->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/site/read  ", name="site_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $site = $this->repository->findOneBy(['id' => $id]);

        $query = "api/site/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $site->getId(),
                'name ' => $site->getName(),
                'project ' => $site->getProject(),
                 
                  ]; 
        return $this->respondWithSuccess(sprintf('infos of site name %s', 
                                         $site->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }
    
   
    /**
     * List  .
     *
     * @Route("/api/site/list  ", name="site_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns list of sites ",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Site::class, groups={"site"}))
     *     )
     * )
     * 
     * @OA\Tag(name="site")
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

           

        $query = "api/site/list";
        $method = "GET";
        $param = ['NULL']; 

            $sites= $this->repository->findAll();
           
            $sitesNormalizer = $normalizer->normalize($sites, null, ['groups' => 'site:read']);
       
           return $this->respondWithSuccess(sprintf('List of sites'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $sitesNormalizer);

    }
    
    
    
     
         /**
     * List  .
     *
     * @Route("/api/siteSegment/list  ", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns a list of all sites and segments ",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Site::class, groups={"site"}))
     *     )
     * ),
     * @OA\Parameter(
     *     name="projectId",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="site")
     */
    public function listAllAction(Request $request, NormalizerInterface $normalizer,
                                   SegmentRepository $SegmentRepository,
                                   ProjectRepository $ProjectRepository): Response 
    {


     $request = $this->transformJsonBody($request);
          $project_id = $request->get('projectId');
           $project = $ProjectRepository->findOneBy(['id' => $project_id]);
        $query = "api/site/list";
        $method = "GET";
        $param = ['NULL']; 
        $tab = array();
        $i = 0;

           $sites= $this->repository->findBy(['project' => $project]);
            
            foreach($sites as $sit){
                
                $tab[$i]['siteId'] = $sit->getId();
                $tab[$i]['site'] = $sit->getName();
                $tab[$i]['segment'] = $normalizer->normalize($sit->getSegments(), null, ['groups' => 'segment:read']);
                //$normalizer->normalize($site = $SegmentRepository->findOneBy(['site' => $sit->getId()]), null, ['groups' => 'segment:read']);
                $i++;
            }
           
          //  $sitesNormalizer = $normalizer->normalize($sites, null, ['groups' => 'site:read']);
       
           return $this->respondWithSuccess(sprintf('List of sites'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $tab);

    }
           
     /**
     * @Route("/api/sitePage/list  ", name="sitePage_list", methods={"GET"})
     */
    public function listPageAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/site/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $sites= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $sitesNormalizer = $normalizer->normalize($sites, null, ['groups' => 'site:read']);
       
           return $this->respondWithSuccess(sprintf('List of sites'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $sitesNormalizer);
        }

        
        $sites = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $sitesNormalizer = $normalizer->normalize($sites, null, ['groups' => 'site:read']);
       
        return $this->respondWithSuccess(sprintf('List of sites'), 
       $query, 
       $method,
      $param,
      $sitesNormalizer);
    }

/**
     * @Route("/api/site/delete", name="site_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
             $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $site = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($site);
            $this->em->flush();
     
            
            $query = "api/site/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Site name %s successfully delete', 
                                                      $site->getName()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }



     
          /**
     * Delete.
     *
     * @Route("/api/siteSegment/delete", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the site's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Site::class, groups={"site"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="siteId",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="site")
     */ 
    public function deleteSiteSegmentAction(Request $request,
                                 SegmentRepository $SegmentRepository,
                                 ObservationRepository $ObservationRepository): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
           $request = $this->transformJsonBody($request);
            $id = $request->get('siteId');
            $site = $this->repository->findOneBy(['id' => $id]);
            
            if($site){
            $segment = $SegmentRepository->findBy(['site'  => $site]);
            $observations = $ObservationRepository->findBy(['site'  => $site]);
            $i = 0; $j = 0; $k = 0;
            
            foreach($observations as $obs){
                $obs->setSite(null);
                 $this->em->persist($obs);
                    $k++;
            }
               
            foreach($segment as $seg){
                $observations1 = $ObservationRepository->findBy(['segment'  => $seg]);
                foreach($observations1 as $obs1){
                    $obs1->setSegment(null);
                    $this->em->persist($obs1);
                    $j++;
                }
                 $this->em->remove($seg);
               $i++;  
            }
               
         
           $this->em->remove($site);
            $this->em->flush();
            
            $query = "api/site/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Site name %s successfully delete', 
                                                      $site->getName()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);
            
            }else{
         return $this->respondValidationError("This id does not exist");
    }                                         

        }
    



     /**
     * @Route("api/site/update", name="site_update", methods={"POST"})
     */
    public function UpdateAction(Request $request, ProjectRepository $ProjectRepository): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $name = $request->get('name');
            $project_id = $request->get('project_id');
           
        
           $site = $this->repository->findOneBy(['id' => $id]);
    
    
    
            if($name)
            {
                $site->setName($name);
                
            }

            if($project_id)
            {
                 $project = $ProjectRepository->findOneBy(['id' => $project_id]);
                $site->setProject($project);
                
            }

    
            $this->em->persist($site);
            $this->em->flush();

            $query = "api/site/update";
            $method = "PUT";
            $param = [
                'name' => $name,
                'project_id ' => $project_id,
                 ];
       $data = ['id' => $site->getId(),
               'name ' => $site->getName(),
               'project ' => $site->getProject(),
                
                 ];         
        return $this->respondWithSuccess(sprintf('The site name %s has been successfully update', 
                                         $site->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
    
     
      /**
     * List sites by project .
     *
     * @Route("api/site/byproject", name="site_by_project", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns a list of all sites and segments ",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Site::class, groups={"site"}))
     *     )
     * )
     *     @OA\Parameter(
     *     name="project_id",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="site")
     */
    public function SiteByProjectAction(Request $request, ProjectRepository $ProjectRepository,NormalizerInterface $normalizer): JsonResponse
    {
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
        $request = $this->transformJsonBody($request);
            
        $project_id = $request->get('project_id');
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);
           
        
       $sites = $this->repository->findBy(['project' => $project],array('id' => 'DESC'));
       $data = $normalizer->normalize($sites, null, ['groups' => 'site:read']);
       
       
       $query = "api/site/siteByProject";
            $method = "POST";
            $param = [
                'project_id ' => $project_id,
                 ];
               
        return $this->respondWithSuccess(sprintf('The site list result.'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
        
    }
}
