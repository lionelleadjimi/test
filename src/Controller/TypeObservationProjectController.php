<?php

namespace App\Controller;

use App\Entity\TypeObservationProject;
use App\Repository\TypeObservationProjectRepository;
use App\Repository\ProjectRepository;
use App\Repository\TypeObservationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class TypeObservationProjectController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                TypeObservationProjectRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

     /**
     * @Route("api/typeObservationProject/create", name="typeObservationProject_create", methods={"POST"})
     */
    public function createAction(Request $request,
                                  ProjectRepository $ProjectRepository,
                                  TypeObservationRepository $TypeObservationRepository): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $id = $request->get('id');
        $project_id = $request->get('project_id');
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);
        $typeobservation_id = $request->get('typeobservation_id');
        $typeobservation = $TypeObservationRepository->findOneBy(['id' => $typeobservation_id]);
        if (empty($typeobservation) || empty($project_id)){
        return $this->respondValidationError("All fields are required");;
        }

        $typeObservationProject = new TypeObservationProject();
        $typeObservationProject->setId($id);
        $typeObservationProject->setProject($project);
        $typeObservationProject->setTypeObservation($typeobservation);

        $this->em->persist($typeObservationProject);
        $this->em->flush();

        $query = "api/typeObservationProject/create";
        $method = "POST";
        $param = [
                 'project_id ' => $project_id,
                 'menu_id ' => $typeobservation_id,

                  ];
        $data = ['id' => $typeObservationProject->getId(),
                'project ' => $typeObservationProject->getProject(),
                'menu ' => $typeObservationProject->getTypeObservation(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The typeObservationProject  has been successfully created'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/menuProject/read  ", name="menuProject_read", methods={"GET"})
     */
    public function readAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $menuProject = $this->repository->findOneBy(['id' => $id]);
            $menuProjectNormalizer = $normalizer->normalize($menuProject, null, ['groups' => 'menuProject:read']);

        $query = "api/menuProject/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $menuProject->getId(),
                'menu ' => $menuProject->getMenu(),
                'project ' => $notificationsProject->getProject(),
                        ];  
        return $this->respondWithSuccess(sprintf('infos of menuProject number %s', 
                                         $menuProject->getId()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }


     /**
     * @Route("/api/menuProject/list  ", name="menuProject_list", methods={"GET"})
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/menuProject/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $menuProjects= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           //dd($userProjects);
            $menuProjectsNormalizer = $normalizer->normalize($menuProjects, null, ['groups' => 'menuProject:read']);
       
           return $this->respondWithSuccess(sprintf('List of menuProject'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $menuProjectsNormalizer);
        }

        
        $menuProjects = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $menuProjecstNormalizer = $normalizer->normalize($menuProjects, null, ['groups' => 'menuProject:read']);
       
        return $this->respondWithSuccess(sprintf('List of menuProject'), 
       $query, 
       $method,
      $param,
      $menuProjectsNormalizer);
    }

    /**
     * @Route("/api/menu/project/list  ", name="menuProject_list", methods={"GET"})
     */
    public function listMenuProjectAction(Request $request, NormalizerInterface $normalizer,
                                ProjectRepository $ProjectRepository): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');
        $project_id =  $request->get('project_id');
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);

        $query = "api/menu/project/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $menuProjects= $this->repository->findBy(['project' => $project_id],array('id' => 'DESC',),(20),(20-20));
    
        $i=0;
        $tab = array();
        foreach($menuProjects as $mj)
        {
            $tab[$i]["nome"]= $mj->getProject()->getName();
            $i++;
        }
      
           //dd($userProjects);
            $menuProjectsNormalizer = $normalizer->normalize($tab, null, ['groups' => 'menuProject:read']);
       
           return $this->respondWithSuccess(sprintf('List of menu of project wiht id  %s', $project->getId()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $menuProjectsNormalizer);
        }

        
        $menuProjects = $this->repository->findBy(array('project' => $project_id),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $i=0;
        $tab = array();
        foreach($menuProjects as $mj)
        {
            $tab[$i]["project"]=$mj->getProject();
            $i++;
        }
        $menuProjectsNormalizer = $normalizer->normalize($tab, null, ['groups' => 'menuProject:read']);
        
        return $this->respondWithSuccess(sprintf('List of menu of project wiht id  %s', $project->getId()), 
       $query, 
       $method,
      $param,
      $menuProjectsNormalizer);
    }

    

/**
     * @Route("/api/menuProject/delete", name="menuProject_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $menuProject = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($menuProject);
            $this->em->flush();
            
            $query = "api/menuProject/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('menuProject successfully delete'),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/menuProject/update", name="menuProject_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request, 
    ProjectRepository $ProjectRepository,
    MenuRepository $MenuRepository): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $project_id = $request->get('project_id');
            $project = $ProjectRepository->findOneBy(['id' => $project_id]);
            $menu_id = $request->get('menu_id');
            $project = $MenuRepository->findOneBy(['id' => $menu_id]);
    
           $menuProject = $this->repository->findOneBy(['id' => $id]);
    
    
           if($project)
            {
                $menuProject->setProject($project);
                
            }

            if($menu)
            {
                $menuProject->setMenu($menu);
                
            }

    
            $this->em->persist($menuProject);
            $this->em->flush();

            $query = "api/menuProject/update";
            $method = "PUT";
            $param = [
                'project_id  ' => $project_id,
                'menu_id  ' => $menu_id,
                ];
            $data = ['id' => $menuProject->getId(),
                    'project ' => $menuProject->getProject(),
                    'mennu ' => $menuProject->getMenu(),
                        
                        ];           
        return $this->respondWithSuccess(sprintf('The menuProject has been successfully update'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
}
