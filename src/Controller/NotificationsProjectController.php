<?php

namespace App\Controller;

use App\Entity\NotificationsProject;
use App\Repository\NotificationsProjectRepository;
use App\Repository\ProjectRepository;
use App\Repository\NotificationsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class NotificationsProjectController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                NotificationsProjectRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

     /**
     * @Route("api/notificationsProject/create", name="notificationsProject_create", methods={"POST"})
     */
    public function createAction(Request $request,
                                  ProjectRepository $ProjectRepository): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $project_id = $request->get('project_id');
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);
        $name = $request->get('name');
        if (empty($name) || empty($project_id)){
        return $this->respondValidationError("All fields are required");;
        }

        $notificationsProject = new NotificationsProject();
        $notificationsProject->setProject($project);
        $notificationsProject->setName($name);

        $this->em->persist($notificationsProject);
        $this->em->flush();

        $query = "api/notificationsProject/create";
        $method = "POST";
        $param = [
                 'project_id ' => $project_id,
                 'name ' => $name,

                  ];
        $data = ['id' => $notificationsProject->getId(),
                'project ' => $notificationsProject->getProject(),
                'name ' => $notificationsProject->getName(),
                 
                  ];            
        return $this->respondWithSuccess(sprintf('The notificationsProject  has been successfully created'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }


     /**
     * @Route("api/notificationsProject/transfert", name="notificationsProject_transfert", methods={"POST"})
     */
    public function transction(Request $request,
                                  ProjectRepository $ProjectRepository): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $project_id = $request->get('project_id');
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);
        $name = $request->get('name');
        $id = $request->get('id');
        $date = $request->get('date');
        
       

        $notificationsProject = new NotificationsProject();
        $notificationsProject->setId($id);
        $notificationsProject->setDate(new \DateTime($date));
        $notificationsProject->setProject($project);
        $notificationsProject->setName($name);

        $this->em->persist($notificationsProject);
        $this->em->flush();

        $query = "api/notificationsProject/create";
        $method = "POST";
        $param = [
                 'project_id ' => $project_id,
                 'name ' => $name,

                  ];
        $data = ['id' => $notificationsProject->getId(),
                'project ' => $notificationsProject->getProject(),
                'name ' => $notificationsProject->getName(),
                 
                  ];            
        return $this->respondWithSuccess(sprintf('The notificationsProject  has been successfully created'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }
     /**
     * @Route("/api/notificationsProject/read  ", name="notificationsProject_read", methods={"GET"})
     */
    public function readAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $notificationsProject = $this->repository->findOneBy(['id' => $id]);
            $notificationsProjectsNormalizer = $normalizer->normalize($notificationsProject, null, ['groups' => 'notificationsProject:read']);

        $query = "api/notificationsProject/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $notificationsProject->getId(),
                'name ' => $notificationsProject->getName(),
                'project ' => $notificationsProject->getProject(),
                        ];  
        return $this->respondWithSuccess(sprintf('infos of notificationsProject number %s', 
                                         $notificationsProject->getId()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }


     /**
     * @Route("/api/notificationsProject/list  ", name="notificationsProject_list", methods={"GET"})
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/notificationsProject/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $notificationsProjects= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           //dd($userProjects);
            $notificationsProjectsNormalizer = $normalizer->normalize($notificationsProjects, null, ['groups' => 'notificationsProject:read']);
       
           return $this->respondWithSuccess(sprintf('List of notificationsProject'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $notificationsProjectsNormalizer);
        }

        
        $notificationsProjects = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $notificationsProjectsNormalizer = $normalizer->normalize($notificationsProjects, null, ['groups' => 'notificationsProject:read']);
       
        return $this->respondWithSuccess(sprintf('List of notificationsProject'), 
       $query, 
       $method,
      $param,
      $userProjectsNormalizer);
    }

    /**
     * @Route("/api/notification/Project/list  ", name="notifications_list", methods={"GET"})
     */
    public function listNotificationProjectAction(Request $request, NormalizerInterface $normalizer,
                                ProjectRepository $ProjectRepository): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');
        $project_id =  $request->get('project_id');
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);

        $query = "api/notification/Project/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $notificationsProjects= $this->repository->findBy(['project' => $project_id],array('id' => 'DESC',),(20),(20-20));
    
        $i=0;
        $tab = array();
        foreach($notificationsProjects as $pj)
        {
            $tab[$i]["project"]= $pj->getProject()->getName();
            $i++;
        }
      
           //dd($userProjects);
            $notificationsProjectsNormalizer = $normalizer->normalize($tab, null, ['groups' => 'notificationsProject:read']);
       
           return $this->respondWithSuccess(sprintf('List of notifications of project wiht id  %s', $project->getId()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $notificationsProjectsNormalizer);
        }

        
        $notificationsProjects = $this->repository->findBy(array('project' => $project_id),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $i=0;
        $tab = array();
        foreach($notificationsProjects as $pj)
        {
            $tab[$i]["project"]=$pj->getProject();
            $i++;
        }
        $notificationsProjectsNormalizer = $normalizer->normalize($tab, null, ['groups' => 'notificationsProject:read']);
        
        return $this->respondWithSuccess(sprintf('List of notifications of project wiht id  %s', $project->getId()), 
       $query, 
       $method,
      $param,
      $notificationsProjectsNormalizer);
    }

    

/**
     * @Route("/api/notificationsProject/delete", name="notificationsProject_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $notificationsProject = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($notificationsProject);
            $this->em->flush();
            
            $query = "api/notificationsProject/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('notificationsProject successfully delete'),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/notificationsProject/update", name="notificationsProject_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request, 
    ProjectRepository $ProjectRepository): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $project_id = $request->get('project_id');
            $project = $ProjectRepository->findOneBy(['id' => $project_id]);
            $name = $request->get('name');
    
           $notificationsProject = $this->repository->findOneBy(['id' => $id]);
    
    
           if($project_id)
            {
                $notificationsProject->setProject($project);
                
            }

            if($name)
            {
                $notificationsProject->setName($name);
                
            }

    
            $this->em->persist($notificationsProject);
            $this->em->flush();

            $query = "api/notificationsProject/update";
            $method = "PUT";
            $param = [
                'project_id  ' => $project_id,
                'name  ' => $name,
                ];
            $data = ['id' => $notificationsProject->getId(),
                    'project ' => $notificationsProject->getProject(),
                    'name ' => $notificationsProject->getName(),
                        
                        ];           
        return $this->respondWithSuccess(sprintf('The notificationsProject has been successfully update'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
}
