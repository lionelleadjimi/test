<?php

namespace App\Controller;

use App\Entity\UserProject;
use App\Entity\User;
use App\Repository\UserProjectRepository;
use App\Repository\ProjectRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class UserProjectController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                UserProjectRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

     /**
     * @Route("api/userProject/create", name="userProject_create", methods={"POST"})
     */
    public function createAction(Request $request,
                                  ProjectRepository $ProjectRepository,
                                  UserRepository $UserRepository): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $project_id = $request->get('project_id');
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);
        $user_id = $request->get('user_id');
        $user = $UserRepository->findOneBy(['id' => $user_id]);
        if (empty($user_id) || empty($project_id)){
        return $this->respondValidationError("All fields are required");;
        }

        $userProject = new UserProject();
        $userProject->setProject($project);
        $userProject->setUser($user);

        $this->em->persist($userProject);
        $this->em->flush();

        $query = "api/userProject/create";
        $method = "POST";
        $param = [
                 'project_id ' => $project_id,
                 'user_id ' => $user_id,

                  ];
        $data = ['id' => $userProject->getId(),
                'project ' => $userProject->getProject(),
                'user ' => $userProject->getUser(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The userProect  has been successfully created'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }
    
    
    /**
     * @Route("api/userProject/transfert", name="userProject_transfert", methods={"POST"})
     */
    public function transfertAction(Request $request,
                                  ProjectRepository $ProjectRepository,
                                  UserRepository $UserRepository): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $id = $request->get('id');
        $project_id = $request->get('project_id');
        $project = $ProjectRepository->findOneBy(['id' => $project_id]);
        $user_id = $request->get('user_id');
        $user = $UserRepository->findOneBy(['id' => $user_id]);
       

        $userProject = new UserProject();
        $userProject->setId($id);
        $userProject->setProject($project);
        $userProject->setUser($user);

        $this->em->persist($userProject);
        $this->em->flush();

        $query = "api/userProject/create";
        $method = "POST";
        $param = [
                 'project_id ' => $project_id,
                 'user_id ' => $user_id,

                  ];
        $data = ['id' => $userProject->getId(),
                'project ' => $userProject->getProject(),
                'user ' => $userProject->getUser(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The userProect  has been successfully created'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/userProject/read  ", name="userProject_read", methods={"GET"})
     */
    public function readAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $userProject = $this->repository->findOneBy(['id' => $id]);
            $userProjectsNormalizer = $normalizer->normalize($userProject, null, ['groups' => 'userProject:read']);

        $query = "api/userProject/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $userProject->getId(),
                'user ' => $userProject->getUser(),
                'project ' => $userProject->getProject(),
                        ];  
        return $this->respondWithSuccess(sprintf('infos of userProject number %s', 
                                         $userProject->getId()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }



     /**
     * user project .
     *
     *
     * @Route("/api/userProject/list  ", name="userProject_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns null",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=UserProject::class, groups={"userProject"}))
     *     )
     * ),
     * @OA\Tag(name="userProject")
     */
    public function listeAction(Request $request, NormalizerInterface $normalizer,
                                ProjectRepository $ProjectRepository,
                                UserRepository $UserRepository): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $email = $decodedJwtToken['username'];
        $user = $UserRepository->findBy(['email' => $email]);
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/userProject/list";
        $method = "GET";
        $param = ['NULL']; 

       
            $userProjects= $this->repository->findBy(['user' => $user],array('id' => 'DESC',));
           //dd($userProjects);
            $userProjectsNormalizer = $normalizer->normalize($userProjects, null, ['groups' => 'userProject:read']);
            
            $count = 0;
            foreach($userProjects as $pro1)
            { if($pro1->isDefaul() == true){
                   $count = 1;
            }
            }
             
            $i = 0;
            $projects = array();
            
            foreach($userProjects as $pro)
            { if($count == 1){
                    

               
                $projects[$i]['id'] = $pro->getProject()->getId();
                $projects[$i]['name'] = $pro->getProject()->getName();
                $projects[$i]['note'] = $pro->getProject()->getNote();
                $projects[$i]['location'] = $pro->getProject()->getLocation();
                 $projects[$i]['organization'] = $pro->getProject()->getOrganization();
                $projects[$i]['maplink'] = $pro->getProject()->getMaplink();
                $projects[$i]['color'] = $pro->getProject()->getColor();
                $projects[$i]['name'] = $pro->getProject()->getLegalNotices();
                $projects[$i]['website link'] = $pro->getProject()->getWebsiteLink();
                $projects[$i]['gps precision'] = $pro->getProject()->getGpsPrecision();
                $projects[$i]['image'] = $pro->getProject()->getImage();
                $projects[$i]['coordX'] = $pro->getProject()->getCoordX();
                $projects[$i]['coordY'] = $pro->getProject()->getCoordY();
                $projects[$i]['zoom'] = $pro->getProject()->getZoom();
                $projects[$i]['country'] = $pro->getProject()->getCountry();
                $projects[$i]['admin'] = $pro->getProject()->getAdmin();
                $projects[$i]['question'] = $pro->getProject()->getQuestion();
                $projects[$i]['default'] = $pro->isDefaul();
            }else{
                
                 $project= $userProjects[0];
                 $project->setDefaul(1);
                
                 $this->em->persist($project);
                 $this->em->flush();
                 $projects[$i]['id'] = $pro->getProject()->getId();
                $projects[$i]['name'] = $pro->getProject()->getName();
                $projects[$i]['note'] = $pro->getProject()->getNote();
                $projects[$i]['location'] = $pro->getProject()->getLocation();
                 $projects[$i]['organization'] = $pro->getProject()->getOrganization();
                $projects[$i]['maplink'] = $pro->getProject()->getMaplink();
                $projects[$i]['color'] = $pro->getProject()->getColor();
                $projects[$i]['name'] = $pro->getProject()->getLegalNotices();
                $projects[$i]['website link'] = $pro->getProject()->getWebsiteLink();
                $projects[$i]['gps precision'] = $pro->getProject()->getGpsPrecision();
                $projects[$i]['image'] = $pro->getProject()->getImage();
                $projects[$i]['coordX'] = $pro->getProject()->getCoordX();
                $projects[$i]['coordY'] = $pro->getProject()->getCoordY();
                $projects[$i]['zoom'] = $pro->getProject()->getZoom();
                $projects[$i]['country'] = $pro->getProject()->getCountry();
                $projects[$i]['admin'] = $pro->getProject()->getAdmin();
                $projects[$i]['question'] = $pro->getProject()->getQuestion();
                $projects[$i]['default'] = $pro->isDefaul();
                 
            }
                $i++;
                
            }
       
           return $this->respondWithSuccess(sprintf('List of Userprojects'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $projects);
    
    }
     /**
     * @Route("/api/userProjectPage/list  ", name="userProject_listpage", methods={"GET"})
     */
    public function listPageAction(Request $request, NormalizerInterface $normalizer,
                                ProjectRepository $ProjectRepository,
                                UserRepository $UserRepository): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/userProject/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $userProjects= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           //dd($userProjects);
            $userProjectsNormalizer = $normalizer->normalize($userProjects, null, ['groups' => 'userProject:read']);
       
           return $this->respondWithSuccess(sprintf('List of Userprojects'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $userProjects);
        }

        
        $userProjects = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $userProjectsNormalizer = $normalizer->normalize($userProjects, null, ['groups' => 'userProject:read']);
       
        return $this->respondWithSuccess(sprintf('List projects of user wiht email adress %s', $user->getEmail()), 
       $query, 
       $method,
      $param,
      $userProjectsNormalizer);
    }

    /**
     * @Route("/api/user/Project/list  ", name="user_list", methods={"GET"})
     */
    public function listProjectUserAction(Request $request, NormalizerInterface $normalizer,
                                ProjectRepository $ProjectRepository,
                                UserRepository $UserRepository): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');
        $user_id =  $request->get('user_id');
        $user = $UserRepository->findOneBy(['id' => $user_id]);

        $query = "api/userProject/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $userProjects= $this->repository->findBy(['user' => $user_id],array('id' => 'DESC',),(20),(20-20));
           //dd($userProjects);
            $userProjectsNormalizer = $normalizer->normalize($userProjects, null, ['groups' => 'userProject:read']);
       
           return $this->respondWithSuccess(sprintf('List projects of user wiht email adress %s', $user->getEmail()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $userProjectsNormalizer);
        }

        
        $userProjects = $this->repository->findBy(array('id' => $user_id),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $userProjectsNormalizer = $normalizer->normalize($userProjects, null, ['groups' => 'userProject:read']);
       
        return $this->respondWithSuccess(sprintf('List projects of user wiht email adress %s', $user->getEmail()), 
       $query, 
       $method,
      $param,
      $userProjectsNormalizer);
    }

    

/**
     * @Route("/api/userProject/delete", name="userProject_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $userProject = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($userProject);
            $this->em->flush();
            
            $query = "api/userProject/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('userProject successfully delete'),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/userProject/update", name="userProject_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request, 
    ProjectRepository $ProjectRepository,
    UserRepository $UserRepository): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $project_id = $request->get('project_id');
            $project = $ProjectRepository->findOneBy(['id' => $project_id]);
            $user_id = $request->get('user_id');
            $user = $UserRepository->findOneBy(['id' => $user_id]);
    
           $userProject = $this->repository->findOneBy(['id' => $id]);
    
    
           if($project_id)
            {
                $userProject->setProject($project);
                
            }

            if($user_id)
            {
                $userProject->setUser($user);
                
            }

    
            $this->em->persist($userProject);
            $this->em->flush();

            $query = "api/userProject/update";
            $method = "PUT";
            $param = [
                'project_id  ' => $project_id,
                'user_id  ' => $user_id,
                ];
            $data = ['id' => $userProject->getId(),
                    'project ' => $userProject->getProject(),
                    'user ' => $userProject->getUser(),
                        
                        ];           
        return $this->respondWithSuccess(sprintf('The userProject has been successfully update'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
}
