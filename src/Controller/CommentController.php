<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class CommentController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                CommentRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }
    
    
     
     /**
     * Register.
     *
     * @Route("/api/comment/create", name="comment_create", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the subgroup's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Comment::class, groups={"comment"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="message",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="comment")
     */ 
    public function createAction(Request $request, UserRepository $UserRepository): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
         $id = $request->get('id');
        $date = $request->get('date');
        $marketplace = $request->get('marche');
        $user = $request->get('user');
        $message = $request->get('message');
       // $user_id = $request->get('user_id');
       // $user = $UserRepository->findOneBy(['id' => $user_id]);

        if (empty($message)) {
            return $this->respondValidationError("All fields are required");
        }

        $comment = new Comment();
        $comment->setId($id);
        $comment->setMarketplace($marketplace);
        $comment->setMessage($message);
        $comment->setUser($user);
        $comment->setDate(new \DateTime($date));
        

        $this->em->persist($comment);
        $this->em->flush();

        $query = "api/comment/create";
        $method = "POST";
        $param = [
                 'message ' => $message,
                 'user'   => $user,

                  ];
        $data = ['id' => $comment->getId(),
                'message ' => $comment->getMessage(),
                'user'   => $comment->getUser(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The comment named %s has been successfully created', 
                                         $comment->getMessage()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/comment/read  ", name="comment_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $comment = $this->repository->findOneBy(['id' => $id]);

        $query = "api/comment/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $comment->getId(),
                 'message ' => $comment->getMessage(),
                 'user'   => $comment->getUser(),
                        ];  
        return $this->respondWithSuccess(sprintf('infos of comment %s', 
                                         $comment->getMessage()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }


     /**
     * @Route("/api/comment/list  ", name="comment_list", methods={"GET"})
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/comment/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $comments= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $commentsNormalizer = $normalizer->normalize($comments, null, ['groups' => 'comment:read']);
       
           return $this->respondWithSuccess(sprintf('List of comments'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $commentsNormalizer);
        }

        
        $comments = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $commentsNormalizer = $normalizer->normalize($comments, null, ['groups' => 'comment:read']);
       
        return $this->respondWithSuccess(sprintf('List of comments'), 
       $query, 
       $method,
      $param,
      $commentsNormalizer);
    }

/**
     * @Route("/api/comment/delete", name="comment_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $comment = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($comment);
            $this->em->flush();
            
            $query = "api/comment/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Comment %s successfully delete', 
                                                      $comment->getMessage()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/comment/update", name="comment_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $message = $request->get('message');
            $user_id = $request->get('user_id');
            $user = $this->repository->findOneBy(['id' => $user_id]);
    
           $comment = $this->repository->findOneBy(['id' => $id]);
    
    
    
            if($message)
            {
                $comment->setMessage($message);
                
            }

            if($user)
            {
                $comment->setUser($user);
                
            }
    
            $this->em->persist($comment);
            $this->em->flush();

            $query = "api/comment/update";
            $method = "PUT";
            $param = [
                 'message ' => $message,
                 'id user ' => $user_id,

                  ];
        $data = ['id' => $comment->getId(),
                 'message ' => $comment->getMessage(),
                 'marketplace ' => $comment->getMarketplace(),
                 'user ' => $comment->getUser()
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The comment white message %s has been successfully update', 
                                         $comment->getMessage()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
}
