<?php

namespace App\Controller;

use App\Entity\Segment;
use App\Repository\SiteRepository;
use App\Repository\SegmentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class SegmentController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                SegmentRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

     
     
     /**
     * Register.
     *
     * @Route("/api/segment/create", name="segment_create", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the subgroup's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Segment::class, groups={"segment"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="name",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="city",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="region",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="site_id",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="segment")
     */  
    public function createAction(Request $request, SiteRepository $SiteRepository): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $id = $request->get('id');
        $name = $request->get('name');
        $city = $request->get('city');
        $region = $request->get('region');
        $site_id = $request->get('site_id');
        $site = $SiteRepository->findOneBy(['id' => $site_id]);
        //dd($site);

        if (empty($name) || empty($city) || empty($region)) {
            return $this->respondValidationError("All fields are required");
        }

        $segment = new Segment();
        $segment->setName($name);
        $segment->setCity($city);
        $segment->setRegion($region);
        $segment->setSite($site);
        

        $this->em->persist($segment);
        $this->em->flush();

        $query = "api/segment/create";
        $method = "POST";
        $param = [
                 'name' => $name,
                 'city' => $city,
                 'region' => $region,
                 'site_id ' => $site_id,
                  ];
        $data = ['id' => $segment->getId(),
                'name ' => $segment->getName(),
                'city ' => $segment->getCity(),
                'region ' => $segment->getRegion(),
                'date ' => $segment->getDate(),
                'site ' => $segment->getSite(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The segment named %s has been successfully created', 
                                         $segment->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/segment/read  ", name="segment_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $segment = $this->repository->findOneBy(['id' => $id]);

        $query = "api/segment/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $segment->getId(),
                'name ' => $segment->getName(),
                'city ' => $segment->getCity(),
                'region ' => $segment->getRegion(),
                'date ' => $segment->getDate(),
                'site ' => $segment->getSite(),
                 
                  ];  
        return $this->respondWithSuccess(sprintf('infos of segment name %s', 
                                         $segment->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
      
    }
     /**
     * List
     *
     * @Route("/api/segment/list  ", name="segment_list", methods={"GET"})
     * @OA\Response(
     *     response=201,
     *     description="Returns list of segments",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Segment::class, groups={"segment"}))
     *     )
     * )
     * 
     * @OA\Tag(name="segment")
     */  
           

    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       $request = $this->transformJsonBody($request);

        $query = "api/segment/list";
        $method = "GET";
        $param = ['NULL']; 

       
            $segments= $this->repository->findAll();
           
            $segmentsNormalizer = $normalizer->normalize($segments, null, ['groups' => 'segment:read']);
       
           return $this->respondWithSuccess(sprintf('List of segments'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $segmentsNormalizer);
        
    }
    
         /**
     * List
     *
     * @Route("/api/segment/site/list  ", name="segment_list", methods={"GET"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the subgroup's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Segment::class, groups={"segment"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="site_id",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="segment")
     */  
           

    public function listSiteSegementAction(Request $request, NormalizerInterface $normalizer,
                                            SiteRepository $SiteRepository): Response 
    {

        $request = $this->transformJsonBody($request);
            $id = $request->get('site_id');
            $site = $SiteRepository->findOneBy(['id' => $id]);

        $query = "api/segment/site/list";
        $method = "GET";
        $param = ['NULL']; 

       
            $segments= $this->repository->findBy(['site' => $site]);
           
            $segmentsNormalizer = $normalizer->normalize($segments, null, ['groups' => 'segment:read']);
       
           return $this->respondWithSuccess(sprintf('List of segments'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $segmentsNormalizer);
        
    }
    
      /**
     * List par page
     *
     * @Route("/api/segmentPage/list  ", methods={"GET"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the subgroup's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Segment::class, groups={"segment"}))
     *     )
     * )
     * 
     * @OA\Tag(name="segment")
     */  
           

    public function listPageAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/segment/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $segments= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $segmentsNormalizer = $normalizer->normalize($segments, null, ['groups' => 'segment:read']);
       
           return $this->respondWithSuccess(sprintf('List of segments'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $segmentsNormalizer);
        }

        
        $segments = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $segmentsNormalizer = $normalizer->normalize($segments, null, ['groups' => 'segment:read']);
       
        return $this->respondWithSuccess(sprintf('List of segments'), 
       $query, 
       $method,
      $param,
      $segmentsNormalizer);
    }

     
         /**
     * Delete
     *
     * @Route("/api/segment/delete", name="segment_delete", methods={"DELETE"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the subgroup's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Segment::class, groups={"segment"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="segmentId",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="segment")
     */  
           
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('segmentId');
            $segment = $this->repository->findOneBy(['id' => $id]);
            
            if($segment){
            
            $this->em->remove($segment);
            $this->em->flush();
            
            $query = "api/segment/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Segment name %s successfully delete', 
                                                      $segment->getName()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);
                                                     
            }else{
                 return $this->respondValidationError("This id does not exist");
            }                                             

        }


     /**
     * @Route("api/segment/update", name="segment_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request, SiteRepository $SiteRepository): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $name = $request->get('name');
        $city = $request->get('cyti');
        $region = $request->get('region');
        $site_id = $request->get('site_id');
        $site = $SiteRepository->findOneBy(['id' => $site_id]);
        
           $segment = $this->repository->findOneBy(['id' => $id]);
    
    
    
            if($name)
            {
                $segment->setName($name);
                
            }

            if($city)
            {
                $segment->setCity($city);
                
            }
            if($region)
            {
                $segment->setRegion($region);
                
            }
            if($city)
            {
                $segment->setSite($site);
                
            }

    
            $this->em->persist($segment);
            $this->em->flush();

            $query = "api/segment/update";
            $method = "PUT";
            $param = [
                'name' => $name,
                'city' => $city,
                'region' => $region,
                'site_id ' => $site_id,
                 ];
       $data = ['id' => $segment->getId(),
               'name ' => $segment->getName(),
               'city ' => $segment->getCity(),
               'region ' => $segment->getRegion(),
               'date ' => $segment->getDate(),
               'site ' => $segment->getSite(),
                
                 ];         
        return $this->respondWithSuccess(sprintf('The segment name %s has been successfully update', 
                                         $segment->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
}
