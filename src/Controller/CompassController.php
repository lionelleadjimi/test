<?php

namespace App\Controller;

use App\Entity\Compass;
use App\Repository\CompassRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class CompassController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                CompassRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

     
    /**
     * Register.
     *
     * @Route("/api/compass/create", name="compass_create", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Returns the compass's information after register",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Compass::class, groups={"compass"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="coordx",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     *  @OA\Parameter(
     *     name="coordy",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * ),
     * @OA\Parameter(
     *     name="title",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="string")
     * )
     * 
     * @OA\Tag(name="compass")
     */
    public function createAction(Request $request, UserRepository $UserRepository): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $id = $request->get('id');
        $coordx = $request->get('coordx');
        $coordy = $request->get('coordy');
        $title = $request->get('title');
        $date = $request->get('date');
         $user = $request->get('user');
        $user_id = $request->get('user_id');
        $user = $UserRepository->findOneBy(['id' => $user_id]);

        if (empty($coordx) || empty($coordx) || empty($coordy)) {
            return $this->respondValidationError("All fields are required");
        }

        $compass = new Compass();
        $compass->setId($id);
        $compass->setCoordX($coordx);
        $compass->setCoordY($coordy);
        $compass->setTitle($title);
        $compass->setUser($user);
        $compass->setDate(new \DateTime($date));
        

        $this->em->persist($compass);
        $this->em->flush();

        $query = "api/compass/create";
        $method = "POST";
        $param = [
                 'coordx' => $coordx,
                 'coordy ' => $coordy,
                 'title ' => $title,
                 'user'   => $user,

                  ];
        $data = ['id' => $compass->getId(),
                'coordx ' => $compass->getCoordX(),
                'coordx ' => $compass->getCoordY(),
                'title ' => $compass->getTitle(),
                'status'   => $compass->getStatus(),
                'user'   => $compass->getUser(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The compass named %s has been successfully created', 
                                         $compass->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/compass/read  ", name="compass_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $compass = $this->repository->findOneBy(['id' => $id]);

        $query = "api/compass/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $compass->getId(),
                'coordx ' => $compass->getCoordX(),
                'coordx ' => $compass->getCoordY(),
                'title ' => $compass->getTitle(),
                'status'   => $compass->getStatus(),
                'user'   => $compass->getUser(),
                        ];  
        return $this->respondWithSuccess(sprintf('infos of compass %s', 
                                         $compass->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }


     /**
     * @Route("/api/compass/list  ", name="compass_list", methods={"GET"})
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/compass/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $compass= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $compassNormalizer = $normalizer->normalize($compass, null, ['groups' => 'compass:read']);
       
           return $this->respondWithSuccess(sprintf('List of compass'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $compassNormalizer);
        }

        
        $compass = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $compassNormalizer = $normalizer->normalize($compass, null, ['groups' => 'compass:read']);
       
        return $this->respondWithSuccess(sprintf('List of compass'), 
       $query, 
       $method,
      $param,
      $compassNormalizer);
    }

/**
     * @Route("/api/compass/delete", name="compass_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $compass = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($compass);
            $this->em->flush();
            
            $query = "api/compass/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Compass %s successfully delete', 
                                                      $compass->getTitle()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/compass/update", name="compass_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request, UserRepository $UserRepository): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $coordx = $request->get('coordx');
            $coordy = $request->get('coordy');
            $title = $request->get('title');
            $user_id = $request->get('user_id');
            $user = $UserRepository->findOneBy(['id' => $user_id]);
    
           $compass = $this->repository->findOneBy(['id' => $id]);
    
    
    
            if($coordx)
            {
                $compass->setCoordX($coordx);
                
            }

            if($coordy)
            {
                $compass->setCoordY($coordy);
                
            }

            if($title)
            {
                $compass->setTitle($title);
                
            }

            if($user)
            {
                $compass->setUser($user);
                
            }
    
            $this->em->persist($compass);
            $this->em->flush();

            $query = "api/compass/update";
            $method = "PUT";
            $param = [
                'coordx' => $coordx,
                'coordy ' => $coordy,
                'title ' => $title,
                'user'   => $user,

                 ];
            $data = ['id' => $compass->getId(),
                    'coordx ' => $compass->getCoordX(),
                    'coordy ' => $compass->getCoordY(),
                    'title ' => $compass->getTitle(),
                    'status'   => $compass->getStatus(),
                    'user'   => $compass->getUser(),
                        
                        ];           
        return $this->respondWithSuccess(sprintf('The compass white message %s has been successfully update', 
                                         $compass->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
}
