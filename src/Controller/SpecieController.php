<?php

namespace App\Controller;

use App\Entity\Specie;
use App\Repository\SpecieRepository;
use App\Repository\QuestionRepository;
use App\Repository\SubgroupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class SpecieController extends ApiController
{
   
    
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                SpecieRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

     /**
     * @Route("api/specie/create", name="specie_create", methods={"POST"})
     */
    public function createAction(Request $request, QuestionRepository $QuestionRepository,
    SubgroupRepository $SubgroupRepository): Response
    {
       // $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $name = $request->get('name');
        $id = $request->get('id');
        $date = $request->get('date');
        $name_inaturalist = $request->get('name_inaturalist');
        $description = $request->get('description');
        $defaut = $request->get('defaut');
        $image = $request->get('image');
        $subgroup_id = $request->get('subgroup_id');
        $question_id = $request->get('question_id');
        
       
        
    
        //dd($site);

        if (empty($name) || empty($description) || empty($image) || empty($defaut) || empty($subgroup_id) || empty($question_id)) {
            return $this->respondValidationError("All fields are required");
        }

        $specie = new Specie();
         $specie->setId($id);
        $specie->setName($name);
        $specie->setNameInaturalist($name_inaturalist);
        $specie->setDescription($description);
        $specie->setDefaut($defaut);
        if($subgroup_id == NULL){
            $specie->setSubgroup($subgroup_id);
        }else{
             $subgroup = $SubgroupRepository->findOneBy(['id' => $subgroup_id]);
             $specie->setSubgroup($subgroup);
        }
        
         if($question_id == NULL){
            $specie->setSubgroup($question_id);
        }else{
             $question = $QuestionRepository->findOneBy(['id' => $question_id]);
              $specie->setQuestion($question);
        }
        $specie->setDate(new \DateTime($date));
        $specie->setImage($image);
       /* if ($image)
        {
           
                            $photo = $image;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/answers/';
                                   // dd($webPath);
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                   
                                    $specie->setImage($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $specie->setImage('specie.png');
        }*/
        

        $this->em->persist($specie);
        $this->em->flush();

        $query = "api/specie/create";
        $method = "POST";
        $param = [
                 'name' => $name,
                 'name_inaturalist' => $name_inaturalist,
                 'description' => $description,
                 'defaut' => $defaut,
                 'image' => $image,
                 'subgroup_id' => $subgroup_id,
                 'questio_idn ' => $question_id,
                  ];
        $data = ['id' => $specie->getId(),
                'name' => $specie->getName(),
                'name_inaturalist' => $specie->getNameInaturalist(),
                'description' => $specie->getDescription(),
                'defaut' => $specie->getDefaut(),
                'date' => $specie->getDate(),
                'image' => $specie->getImage(),
                'subgroup' => $specie->getSubgroup(),
                'question ' => $specie->getQuestion(),
                        
                  ];          
        return $this->respondWithSuccess(sprintf('The specie has been successfully created'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/specie/read  ", name="specie_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $specie = $this->repository->findOneBy(['id' => $id]);

        $query = "api/specie/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $specie->getId(),
                'name' => $specie->getName(),
                'name_inaturalist' => $specie->getNameInaturalist(),
                'description' => $specie->getDescription(),
                'defaut' => $specie->getDefaut(),
                'date' => $specie->getDate(),
                'image' => $specie->getImage(),
                'subgroup' => $specie->getSubgroup(),
                'question ' => $specie->getQuestion(),
                        
                  ];   
        return $this->respondWithSuccess(sprintf('infos of specie '), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }
    
    
     
    /**
     * List  .
     *
     * @Route("/api/specie/list  ", name="specie_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns list species ",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Specie::class, groups={"specie"}))
     *     )
     * )
     * 
     * @OA\Tag(name="specie")
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

     

        $query = "api/specie/list";
        $method = "GET";
        $param = ['NULL']; 

      
            $species= $this->repository->findAll();
           
            $speciesNormalizer = $normalizer->normalize($species, null, ['groups' => 'specie:read']);
       
           return $this->respondWithSuccess(sprintf('List of species'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $speciesNormalizer);

    }
    
     /**
     * List  .
     *
     * @Route("/api/specie/subgroup/list  ", name="speciesubgroup_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns list species ",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Specie::class, groups={"specie"}))
     *     )
     * ),
     * @OA\Parameter(
     *     name="subgroup_id",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     * )
     * 
     * @OA\Tag(name="specie")
     */
    public function listSubgroupAction(Request $request, NormalizerInterface $normalizer,
                                        SubgroupRepository $SubgroupRepository): Response 
    {



        $query = "api/specie/subgroup/list";
        $method = "GET";
        $param = ['NULL']; 

           $request = $this->transformJsonBody($request);
            $id = $request->get('subgroup_id');
             $subgroup = $SubgroupRepository->findOneBy(['id' => $id]);
            $species= $this->repository->findBy(['subgroup' => $subgroup ]);
           
            $speciesNormalizer = $normalizer->normalize($species, null, ['groups' => 'specie:read']);
       
           return $this->respondWithSuccess(sprintf('List of species'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $speciesNormalizer);

    }
           
    
       
    /**
     * List par page .
     *
     * @Route("/api/speciePage/list  ", name="speciePage_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns list species ",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Specie::class, groups={"specie"}))
     *     )
     * )
     * 
     * @OA\Tag(name="specie")
     */
    public function listPAgeAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/specie/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $species= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $speciesNormalizer = $normalizer->normalize($species, null, ['groups' => 'specie:read']);
       
           return $this->respondWithSuccess(sprintf('List of species'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $speciesNormalizer);
        }

        
        $species = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $speciesNormalizer = $normalizer->normalize($species, null, ['groups' => 'specie:read']);
       
        return $this->respondWithSuccess(sprintf('List of species'), 
       $query, 
       $method,
      $param,
      $speciesNormalizer);
    }

/**
     * @Route("/api/specie/delete", name="specie_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $specie = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($specie);
            $this->em->flush();
            
            $query = "api/specie/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Specie successfully delete'),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/specie/update", name="specie_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request, QuestionRepository $QuestionRepository,
    SubgroupRepository $SubgroupRepository): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $name = $request->get('name');
        $name_inaturalist = $request->get('name_inaturalist');
        $description = $request->get('description');
        $defaut = $request->get('defaut');
        $image = $request->get('image');
        $subgroup_id = $request->get('subgroup_id');
        $question_id = $request->get('question_id');
        $subgroup = $SubgroupRepository->findOneBy(['id' => $subgroup_id]);
        $question = $QuestionRepository->findOneBy(['id' => $question_id]);
        
           $specie = $this->repository->findOneBy(['id' => $id]);
    
    
    
            if($name)
            {
                $specie->setName($name);
                
            }

            if($name_inaturalist)
            {
                $specie->setNameInaturalist($name_inaturalist);
                
            }
            if($description)
            {
                $specie->setDescription($description);
                
            }
            if($defaut)
            {
                $specie->setDefaut($defaut);
                
            }
            if($subgroup)
            {
                $specie->setSubgroup($subgroup);
                
            }
            if($question)
            {
                $specie->setQuestion($question);
                
            }
            if ($image)
        {
           
                            $photo = $image;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/answers/';
                                   // dd($webPath);
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                   
                                    $specie->setImage($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }

    
            $this->em->persist($specie);
            $this->em->flush();

            $query = "api/patrol/update";
            $method = "PUT";
            $param = [
                'name' => $name,
                'name_inaturalist' => $name_inaturalist,
                'description' => $description,
                'defaut' => $defaut,
                'image' => $image,
                'subgroup_id' => $subgroup_id,
                'question_id ' => $question_id,
                 ];
       $data = ['id' => $specie->getId(),
               'name' => $specie->getName(),
               'name_inaturalist' => $specie->getNameInaturalist(),
               'description' => $specie->getDescription(),
               'defaut' => $specie->getDefaut(),
               'date' => $specie->getDate(),
               'image' => $specie->getImage(),
               'subgroup' => $specie->getSubgroup(),
               'question ' => $specie->getQuestion(),
                       
                 ];         
        return $this->respondWithSuccess(sprintf('The specie has been successfully update'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }
}
