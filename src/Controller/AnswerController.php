<?php

namespace App\Controller;

use App\Entity\Answer;
use App\Repository\AnswerRepository;
use App\Repository\QuestionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class AnswerController extends ApiController
{

    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                AnswerRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

   /**
     * @Route("api/answer/create", name="answer_create", methods={"POST"})
     */
    public function create(Request $request, QuestionRepository $QuestionRepository): JsonResponse
    {
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
        $id = $request->get('id');
        $date = $request->get('date');
        $title = $request->get('title');
        $questionid = $request->get('question_id');
        $question  = $QuestionRepository->findOneBy(['id' => $questionid]);
        $question_nextid = $request->get('question_next_id');
        $question_next = $QuestionRepository->findOneBy(['id' => $question_nextid]);
        $image = $request->get('image');

        if (empty($title)) {
            return $this->respondValidationError("All fields are required");
        }

        $answer = new Answer();
        $answer->setId($id);
        $answer->setTitle($title);
        $answer->setQuestion($question);
        $answer->setQuestionNext($question_next);
        $answer->setUpdatedAt(new \DateTime($date));
        $answer->setImage($image);

       /* if ($image)
        {
           
                            $photo = $image;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/answers/';
                                   // dd($webPath);
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                   
                                    $answer->setImage($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $answer->setImage('answer.png');
        }*/


        $this->em->persist($answer);
        $this->em->flush();

        $query = "api/answer/create";
        $method = "POST";
        $param = [
                 'title ' => $title,
                 'question ' => $question,
                 'next question ' => $question_next,
                 'image ' => $image,

                  ];
        $data = ['id' => $answer->getId(),
                 'title ' => $answer->getTitle(),
                 'question ' => $answer->getQuestion(),
                 'next question ' => $answer->getQuestionNext(),
                 'image ' => $answer->getImage(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The answer named %s has been successfully created', 
                                         $answer->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/answer/read  ", name="answer_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $answer = $this->repository->findOneBy(['id' => $id]);

        $query = "api/answer/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $answer->getId(),
                'title ' => $answer->getTitle(),
                'question ' => $answer->getQuestion(),
                ' next question ' => $answer->getQuestionNext(),
                'image ' => $answer->getImage()
                        ];  
        return $this->respondWithSuccess(sprintf('infos of answer %s', 
                                         $answer->gettitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }


     /**
     * @Route("/api/answer/list  ", name="answer_list", methods={"GET"})
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/answer/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $answers= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $answersNormalizer = $normalizer->normalize($answers, null, ['groups' => 'answer:read']);
      
      return $this->respondWithSuccess(sprintf('List of Answers'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $answersNormalizer, );
        }

        
        $answers = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $answersNormalizer = $normalizer->normalize($answers, null, ['groups' => 'answer:read']);
       
        return $this->respondWithSuccess(sprintf('List of Answers'), 
                                            $query, 
                                            $method,
                                            $param,
                                            $answersNormalizer);
     }

/**
     * @Route("/api/answer/delete", name="answer_delete", methods={"DELETE"})
     */
    public function groupdeleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $answer = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($answer);
            $this->em->flush();
            
            $query = "api/answer/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('Answer %s successfully delete', 
                                                      $answer->getTitle()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/answer/update", name="answer_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $title = $request->get('title');
            $question = $request->get('question');
            $question_next = $request->get('question_next');
            $image = $request->get('image');
    
           $answer = $this->repository->findOneBy(['id' => $id]);
    
    
            if($title)
            {
                $answer->setTitle($title);
                
            }
    
            if($question)
            {
                $answer->setQuestion($question);
                
            }

            if($question_next)
            {
                $answer->setQuestionNext($question_next);
                
            }
            if ($image)
            {
               
                                $photo = $image;
                               
                                    
    
                                try {
                                    if ($photo != NULL) {
                                        $photo = explode(',', $photo);
                                        $extension = str_replace('data:image/', '', $photo[0]);
                                        $extension = str_replace(';base64', '', $extension);
                                       // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                        $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/answers/';
                                       // dd($webPath);
                                        $sourcename = $webPath . uniqid() . '.jpg';
    
                                        $file = fopen($sourcename, 'w+');
                                        fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                        fclose($file);
    
                                        $photoUrl = str_replace($webPath, '', $sourcename);
                                       
                                        $answer->setImage($photoUrl);
                                    }
                                } catch (\Exception $e) {
                                    $json["success"] = false;
                                    $json["error"] = "image";
                                    $json["message"] = $e->getMessage();
                                }
            }
    
            $this->em->persist($answer);
            $this->em->flush();

            $query = "api/answer/update";
            $method = "PUT";
            $param = [
                 'id' => $id,
                 'title ' => $title,
                 'question ' => $question,
                 'next question' => $question_next,
                 'image ' => $image,

                  ];
        $data = ['id' => $answer->getId(),
                 'title ' => $answer->getTitle(),
                 'question ' => $answer->getQuestion(),
                 'next question ' => $answer->getQuestionNext(),
                 'image ' => $answer->getImage()
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The answer named %s has been successfully update', 
                                         $answer->getTitle()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }

}
