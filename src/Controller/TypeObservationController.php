<?php

namespace App\Controller;

use App\Entity\TypeObservation;
use App\Repository\TypeObservationRepository;
use App\Repository\QuestionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class TypeObservationController extends ApiController
{
    private $em;

    public function __construct(
                                EntityManagerInterface $em,
                                TokenStorageInterface $tokenStorageInterface, 
                                JWTTokenManagerInterface $jwtManager,
                                NormalizerInterface $serializer,
                                TypeObservationRepository $repository)
    {
        $this->em = $em;
        $this->jwtManager = $jwtManager;
        $this->serializer = $serializer;
        $this->repository = $repository;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

   /**
     * @Route("api/typeObservation/create", name="typeObservation_create", methods={"POST"})
     */
    public function create(Request $request, QuestionRepository $QuestionRepository): JsonResponse
    {
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       // $projectRoot = $this->getParameter('kernel.project_dir') . '/public';
       // dd($projectRoot);

        $request = $this->transformJsonBody($request);
         $id = $request->get('id');
        $name = $request->get('name');
        $type_question = $request->get('type_question');
        $level = $request->get('level');
        $questionid = $request->get('question_id');
      //  var_dump($questionid); die();
        $image = $request->get('image');
         $date = $request->get('date');

        if (empty($name) || empty($level) || empty($type_question)) {
            return $this->respondValidationError("All fields are required");
        }

        $typeObservation = new TypeObservation();
        $typeObservation->setId($id);
        $typeObservation->setName($name);
        $typeObservation->setTypeQuestion($type_question);
        $typeObservation->setLevel($level);
        $typeObservation->setImage($image);
        $typeObservation->setUpdatedAt(new \DateTime($date));
        
        if($questionid == NULL)
        {
             $typeObservation->setQuestion($questionid);
        }
        else{
            
        $question  = $QuestionRepository->findOneBy(['id' => $questionid]);
        
        $typeObservation->setQuestion($question);
        }

      /*  if ($image)
        {
           
                            $photo = $image;
                           
                                

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                   // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                    $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/typeObservations/';
                                   // dd($webPath);
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                   
                                    $typeObservation->setImage($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $typeObservation->setImage('typeObservation.png');
        }*/


        $this->em->persist($typeObservation);
        $this->em->flush();

        $query = "api/typeObservation/create";
        $method = "POST";
        $param = [
                 'name ' => $name,
                 'type_question ' => $type_question,
                 'level ' => $level,
                 'question_id ' => $questionid,
                 'image ' => $image,

                  ];
        $data = ['id' => $typeObservation->getId(),
                 'name ' => $typeObservation->getName(),
                 'question ' => $typeObservation->getTypeQuestion(),
                 'level ' => $typeObservation->getLevel(),
                 'question ' => $typeObservation->getQuestion(),
                 'image ' => $typeObservation->getImage(),
                 
                  ];          
        return $this->respondWithSuccess(sprintf('The typeObservation named %s has been successfully created', 
                                         $typeObservation->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       // dd("ok");
    }

     /**
     * @Route("/api/typeObservation/read  ", name="typeObservation_read", methods={"GET"})
     */
    public function readAction(Request $request): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
       

            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $typeObservation = $this->repository->findOneBy(['id' => $id]);

        $query = "api/typeObservation/read";
        $method = "GET";
        $param = ['id' => $id];
        $data = ['id' => $typeObservation->getId(),
                 'name ' => $typeObservation->getName(),
                 'question ' => $typeObservation->getTypeQuestion(),
                 'level ' => $typeObservation->getLevel(),
                 'question ' => $typeObservation->getQuestion(),
                 'image ' => $typeObservation->getImage(),
                 
                  ];   
        return $this->respondWithSuccess(sprintf('infos of typeObservation %s', 
                                         $typeObservation->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
    
      
    }

     
         /**
     * List  .
     *
     * @Route("/api/typeObservation/list  ", name="typeObservation_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns list typeObservation ",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=TypeObservation::class, groups={"typeobservation"}))
     *     )
     * )
     * 
     * @OA\Tag(name="typeobservation")
     */
    public function listAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        //$decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
      

        $query = "api/typeObservation/list";
        $method = "GET";
        $param = ['NULL']; 

        
            $typeObservations= $this->repository->findAll();
           
            $typeObservationsNormalizer = $normalizer->normalize($typeObservations, null, ['groups' => 'typeObservation:read']);
      
      return $this->respondWithSuccess(sprintf('List of typeObservations'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $typeObservationsNormalizer, );

     }

     
     
    /**
     * List par page  .
     *
     * @Route("/api/typeObservationPage/list  ", name="typeObservationPage_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns list typeObservation ",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=TypeObservation::class, groups={"typeobservation"}))
     *     )
     * )
     * 
     * @OA\Tag(name="typeobservation")
     */
    public function listPageAction(Request $request, NormalizerInterface $normalizer): Response 
    {

       /* $token = str_replace('bearer ', '', $request->headers->get('Authorization'));
        dd($token);*/
    
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        $request = $this->transformJsonBody($request);
        $page = $request->get('numPage');

        $query = "api/typeObservation/list";
        $method = "GET";
        $param = ['NULL']; 

        if(!$page)
        {
            $typeObservations= $this->repository->findBy(array(),array('id' => 'DESC',),(20),(20-20));
           
            $typeObservationsNormalizer = $normalizer->normalize($typeObservations, null, ['groups' => 'typeObservation:read']);
      
      return $this->respondWithSuccess(sprintf('List of typeObservations'), 
                                         $query, 
                                         $method,
                                        $param,
                                        $typeObservationsNormalizer, );
        }

        
        $typeObservations = $this->repository->findBy(array(),array('id' => 'DESC',),($page *20),(($page *20)-20));
        $typeObservationsNormalizer = $normalizer->normalize($typeObservations, null, ['groups' => 'typeObservation:read']);
       
        return $this->respondWithSuccess(sprintf('List of typeObservations'), 
                                            $query, 
                                            $method,
                                            $param,
                                            $typeObservationsNormalizer);
     }

/**
     * @Route("/api/typeObservation/delete", name="typeObservation_delete", methods={"DELETE"})
     */
    public function groupdeleteAction(Request $request): Response 
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
     
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            $typeObservation = $this->repository->findOneBy(['id' => $id]);
            
            $this->em->remove($typeObservation);
            $this->em->flush();
            
            $query = "api/typeObservation/delete";
            $method = "DELETE";
            $param = ['id' => $id]; 
            $data = ['NULL']; 
            return $this->respondWithSuccess(sprintf('TypeObservation %s successfully delete', 
                                                      $typeObservation->getName()),
                                                     $query,
                                                    $method,
                                                     $param,
                                                     $data);

        }


     /**
     * @Route("api/typeObservation/update", name="typeObservation_update", methods={"PUT"})
     */
    public function UpdateAction(Request $request, QuestionRepository $QuestionRepository): JsonResponse
    {
        
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
            $request = $this->transformJsonBody($request);
            $id = $request->get('id');
            
            $name = $request->get('name');
            $type_question = $request->get('type_question');
            $level = $request->get('level');
            $questionid = $request->get('question');
            $question  = $QuestionRepository->findOneBy(['id' => $questionid]);
            $image = $request->get('image');
    
           $typeObservation = $this->repository->findOneBy(['id' => $id]);
    
    
            if($name)
            {
                $typeObservation->setName($name);
                
            }
    
            if($type_question)
            {
                $typeObservation->setTypeQuestion($type_question);
                
            }

            if($level)
            {
                $typeObservation->setLevel($level);
                
            }
    
            if($question)
            {
                $typeObservation->setQuestion($question);
                
            }
            if ($image)
            {
               
                                $photo = $image;
                               
                                    
    
                                try {
                                    if ($photo != NULL) {
                                        $photo = explode(',', $photo);
                                        $extension = str_replace('data:image/', '', $photo[0]);
                                        $extension = str_replace(';base64', '', $extension);
                                       // $webPath =  $this->get('kernel')->getRootDir().'/../public/uploads/groups/';
                                        $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/typeObservations/';
                                       // dd($webPath);
                                        $sourcename = $webPath . uniqid() . '.jpg';
    
                                        $file = fopen($sourcename, 'w+');
                                        fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                        fclose($file);
    
                                        $photoUrl = str_replace($webPath, '', $sourcename);
                                       
                                        $typeObservation->setImage($photoUrl);
                                    }
                                } catch (\Exception $e) {
                                    $json["success"] = false;
                                    $json["error"] = "image";
                                    $json["message"] = $e->getMessage();
                                }
            }
    
            $this->em->persist($typeObservation);
            $this->em->flush();

            $query = "api/typeObservation/update";
            $method = "PUT";
            $param = [
                'name ' => $name,
                'type_question ' => $type_question,
                'level ' => $level,
                'question_id ' => $questionid,
                'image ' => $image,

                 ];
       $data = ['id' => $typeObservation->getId(),
                'name ' => $typeObservation->getName(),
                'question ' => $typeObservation->getTypeQuestion(),
                'level ' => $typeObservation->getLevel(),
                'question ' => $typeObservation->getQuestion(),
                'image ' => $typeObservation->getImage(),
                
                 ];          
        return $this->respondWithSuccess(sprintf('The typeObservation named %s has been successfully update', 
                                         $typeObservation->getName()), 
                                         $query, 
                                         $method,
                                        $param,
                                        $data);
       }

}
