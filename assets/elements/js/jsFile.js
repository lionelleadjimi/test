$(function () {
  $(".login").click(function () {
    console.log($(".password").val());

    if (!isEmail($(".emailtxt").val())) $(".mailerror").removeClass("d-none");
    else $(".mailerror").addClass("d-none");

    if (!$(".password").val()) $(".passworderror").removeClass("d-none");
    else $(".passworderror").addClass("d-none");
  });

  $(".eye-reveal").click(function () {
    $(this)
      .closest(".position-relative")
      .children("input")
      .attr("type", "password");
    $(this).addClass("d-none");
    $(this)
      .closest(".position-relative")
      .children(".eye-close")
      .removeClass("d-none");
  });

  $(".eye-close").click(function () {
    $(this)
      .closest(".position-relative")
      .children("input")
      .attr("type", "text");
    $(this).addClass("d-none");
    $(this)
      .closest(".position-relative")
      .children(".eye-reveal")
      .removeClass("d-none");
  });

  function isEmail(email) {
    var EmailRegex =
      /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return EmailRegex.test(email);
  }

  $(".setsend").click(function () {
    if (!isEmail($(".emailtxt").val())) {
      $(".mailerror").removeClass("d-none");
    } else {
      $(".mailerror").addClass("d-none");
      $("#block-reset").addClass("d-none");
      $("#block-mail").removeClass("d-none");
      $("#block-try").removeClass("d-none");
    }
  });
});
