/* global bootstrap: false */
(function () {
  var tooltipTriggerList = [].slice.call(
    document.querySelectorAll('[data-bs-toggle="tooltip"]')
  );
  tooltipTriggerList.forEach(function (tooltipTriggerEl) {
    new bootstrap.Tooltip(tooltipTriggerEl);
  });

  $(".btn-toggle").click(function () {
    let allcheck = 0;
    var val = $(this).attr("data-name");

    let nbrChild = $(this)
      .parent()
      .children("#" + val)
      .children("ul")
      .children().length;
    // let check = $(this).parent().children('#'+val).children('ul').children('li').children('.checker');

    for (let i = 1; i <= nbrChild; i++) {
      let checked = $(this)
        .parent()
        .children("#" + val)
        .children("ul")
        .children("li:nth-child(" + i + ")")
        .children(".checker")
        .hasClass("check-checked");

      if (checked) allcheck = allcheck + 1;
    }

    let exist = $(this).parent().children(".checker").hasClass("empty");

    if (allcheck == nbrChild) {
      $(this).parent().children(".checker").removeClass("empty");
      $(this).parent().children(".checker").removeClass("check-toggle");
      $(this).parent().children(".checker").addClass("check-checked");
    }

    if (allcheck == 0) {
      $(this).parent().children(".checker").removeClass("empty");
      $(this).parent().children(".checker").removeClass("check-toggle");
      $(this).parent().children(".checker").removeClass("check-checked");
    }

    if (allcheck != nbrChild && allcheck != 0) {
      $(this).parent().children(".checker").removeClass("empty");
      $(this).parent().children(".checker").removeClass("check-checked");
      $(this).parent().children(".checker").addClass("check-toggle");
    }
  });

  $(".btn-toggle-item").click(function () {
    let allcheck = 0;
    let exist = $(this).children(".checker").hasClass("check-empty");

    let nbrChild = $(this).parent().children().length;

    if (exist) {
      $(this).children(".nav-link").addClass("clbl");
      $(this).children(".nav-link").children(".men-text").addClass("fw-bold ");
      $(this).children(".checker").removeClass("check-empty");
      $(this).children(".checker").addClass("check-checked");
    } else {
      $(this).children(".nav-link").removeClass("clbl");
      $(this)
        .children(".nav-link")
        .children(".men-text")
        .removeClass("fw-bold ");
      $(this).children(".checker").removeClass("check-checked");
      $(this).children(".checker").addClass("check-empty");
    }

    for (let i = 1; i <= nbrChild; i++) {
      let checked = $(this)
        .parent()
        .children("li:nth-child(" + i + ")")
        .children(".checker")
        .hasClass("check-checked");

      if (checked) allcheck = allcheck + 1;
    }

    if (allcheck == nbrChild) {
      $(this)
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .removeClass("empty");
      $(this)
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .removeClass("check-toggle");
      $(this)
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .addClass("check-checked");
    }

    if (allcheck == 0) {
      $(this)
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .removeClass("empty");
      $(this)
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .removeClass("check-toggle");
      $(this)
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .removeClass("check-checked");
    }

    if (allcheck != nbrChild && allcheck != 0) {
      $(this)
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .removeClass("empty");
      $(this)
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .removeClass("check-checked");
      $(this)
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .addClass("check-toggle");
    }

    allcheck = 0;
    let nbrChildTop = $(this)
      .parent()
      .parent()
      .parent()
      .parent()
      .children().length;
    for (let i = 1; i <= nbrChildTop; i++) {
      let checked = $(this)
        .parent()
        .parent()
        .parent()
        .parent()
        .children("li:nth-child(" + i + ")")
        .children(".checker")
        .hasClass("check-checked");

      if (checked) allcheck = allcheck + 1;
    }

    if (allcheck == nbrChild) {
      $(this)
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .removeClass("empty");
      $(this)
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .removeClass("check-toggle");
      $(this)
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .addClass("check-checked");
    }

    if (allcheck == 0) {
      $(this)
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .removeClass("empty");
      $(this)
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .removeClass("check-toggle");
      $(this)
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .removeClass("check-checked");
    }

    if (allcheck != nbrChild && allcheck != 0) {
      $(this)
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .removeClass("empty");
      $(this)
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .removeClass("check-checked");
      $(this)
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .parent()
        .children(".checker")
        .addClass("check-toggle");
    }
  });

  $(".filter-close").click(function () {
    $(".filter-list").addClass("d-none");
    $(".b-example-divider").addClass("d-none");
  });
})();
