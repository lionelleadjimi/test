$(function () {
  // initMap();
  $("body").click(function () {
    $(".show-block").addClass("d-none");
  });
  let retHeight = 0;
  let wwidth = $(window).width();
  var list_checkall = false;

  retHeight =
    $(window).height() - ($(".head02").height() + $(".head02").height());
  $(".filter-list").height(retHeight);
  $(".cont-det-obsv").height(retHeight);
  $(".b-example-divider").height(retHeight);
  $(".map").height(retHeight);
  $(".content-obs").height(retHeight);

  var tableHeight = $(window).height() - ($(".pagination").height() + 90);
  $(".table-wrapper").height(tableHeight);

  if (wwidth <= 1285) {
    $(".content-obs").width(300);
    $(".content-sort").width(300);
    // $(".content-search").width(250);
  }
  $(".content-sort").click(function () {
    $(this).addClass("d-none");
  });
  $(".list-v").width(300);

  $(".press").click(function () {
    $("#block-register").addClass("d-none");
    $("#block-tanks").removeClass("d-none");
  });

  function scrolling(event) {
    const el = event.currentTarget,
      tY = el.scrollTop;
    el.querySelectorAll("thead").forEach(
      (itemth) => (itemth.style.transform = `translateY(${tY}px)`)
    );
  }

  document
    .querySelectorAll(".table-wrapper")
    .forEach((item) => item.addEventListener("scroll", scrolling));

  function scrollX(event) {
    const el = event.currentTarget,
      tX = el.scrollLeft;
    el.querySelectorAll("tr").forEach(function (itemth) {
      itemth.children[0].style.transform = `translateX(${tX}px)`;
      itemth.children[1].style.transform = `translateX(${tX}px)`;
    });
  }
  document
    .querySelectorAll(".main")
    .forEach((item) => item.addEventListener("scroll", scrollX));

  $(".lst").click(function (e) {
    if ($(this).hasClass("check-empty")) {
      $(this).removeClass("check-empty");
      $(this).addClass("check-checked");
    } else {
      $(this).removeClass("check-checked");
      $(this).addClass("check-empty");
    }

    var total_check = document.querySelectorAll(".lst").length;
    var partial_check = document.querySelectorAll(".lst.check-checked").length;

    if (total_check != partial_check) {
      $(".checkall").removeClass("check-empty");
      $(".checkall").removeClass("check-checked");
      $(".checkall").addClass("check-toggle");
    } else {
      $(".checkall").removeClass("check-empty");
      $(".checkall").removeClass("check-toggle");
      $(".checkall").addClass("check-checked");
    }

    if (partial_check == 0) {
      $(".checkall").removeClass("check-checked");
      $(".checkall").removeClass("check-toggle");
      $(".checkall").addClass("check-empty");

      $(".btn-action").removeClass("btn-action-on");
      $(".btn-action").addClass("btn-action-off");

      $(".cont-action").addClass("d-none");
      list_checkall = false;
    } else {
      $(".btn-action").removeClass("btn-action-off");
      $(".btn-action").addClass("btn-action-on");
    }
  });

  $(".checkall").click(function (e) {
    if (list_checkall) {
      list_checkall = false;
      $(".lst").removeClass("check-checked");
      $(".lst").addClass("check-empty");
      $(this).removeClass("check-checked");
      $(this).addClass("check-empty");
      $(".btn-action").removeClass("btn-action-on");
      $(".btn-action").addClass("btn-action-off");
      $(".cont-action").addClass("d-none");
    } else {
      list_checkall = true;
      $(".lst").removeClass("check-empty");
      $(".lst").addClass("check-checked");
      $(this).removeClass("check-empty");
      $(this).addClass("check-checked");
      $(".btn-action").removeClass("btn-action-off");
      $(".btn-action").addClass("btn-action-on");
    }
  });

  $(".btn-action").click(function () {
    closeContentObs();
    closeContent();

    if ($(this).hasClass("btn-action-on")) {
      $(".cont-action").css({ top: $(".act-cnt").height() + 1 });
      $(".cont-action").css({ right: 0 });
      $(".cont-action").removeClass("d-none");
    }
  });

  $(".btn-delete").click(function () {
    $(".cont-action").addClass("d-none");
    $(".validation-modal").removeClass("d-none");
  });

  $(".btn-validate").click(function () {
    $(".cont-action").addClass("d-none");
    $(".validation-modal").removeClass("d-none");
  });

  $(".modal-close, .cancel-operation").click(function () {
    $(".validation-modal").addClass("d-none");
  });

  $(".setnew").click(function (e) {
    console.log($(".password").val());

    if (!($(".password").val() == $(".repeatpass").val())) {
      $(".mustmatch").addClass("d-none");
      $(".passworderror").removeClass("d-none");
      //e.stopPropagation();
    } else {
      $(".mustmatch").removeClass("d-none");
      $(".passworderror").addClass("d-none");
      $("#block-create").addClass("d-none");
      $("#block-succed").removeClass("d-none");
    }
  });

  $(".lui").click(function () {
    console.log($(".shorsearch").offset().left);
    $(this).width($(".shorsearch").width());
    $(this).height("100vh");
    // $(this).height($(".shorsearch").height());
    $(this).css({
      top: $(".shorsearch").height() + $(".shorsearch").offset().top,
      left: $(".shorsearch").offset().left,
      position: "absolute",
    });
  });

  $(".bsort").click(function () {
    closeContent();
    $(".content-sort").css({ top: $(".shorsearch").height() + 2 });
    $(".content-sort").removeClass("d-none");
    if (wwidth <= 1285) $(".content-sort").css({ right: 0 });
  });

  // $(".bsearch").click(function () {
  //   closeContentObs();
  //   closeContent();

  //   $(".content-search").css({ top: $(".shorsearch").height() + 2 });
  //   // $(".content-search").css({ left: $(".shorsearch").offset().left });
  //   // $(".content-search").css({ top: 100 });
  //   $(".content-search").removeClass("d-none");
  //   if (wwidth <= 1285) $(".content-search").css({ right: 0 });
  // });

  $(".projet-close").click(function (e) {
    e.stopPropagation();
    $(".content-projet").addClass("d-none");
  });

  // $(".content-projet").click(function (e) {
  //   $(this).addClass("d-none");
  // });

  // $(".bobserv").click(function (e) {
  //   e.stopPropagation();
  //   $(".cont-obs").css({ top: 50, left: $(".bobserv").offset().left });
  //   // $('.cont-obs').css({top: $(".bpro").height(), left: $(".bobserv").offset().left});
  //   $(".cont-obs").removeClass("d-none");
  // });
  // $(".menuView").click(function (e) {
  //   e.stopPropagation();
  //   $(".menu-content").css({ top: 50, left: $(".menuView").offset().left });
  //   // $('.cont-obs').css({top: $(".bpro").height(), left: $(".bobserv").offset().left});
  //   $(".menu-content").removeClass("d-none");
  // });

  $(".observ").click(function (e) {
    e.stopPropagation();
    closeContent();

    $(".content-obs").css({ top: $(".bpro").height() + 1 });
    $(".content-obs").removeClass("d-none");

    $(".obs-close").css({ right: $(".content-obs").width() + 20 });
    $(".obs-close").removeClass("d-none");

    if (wwidth <= 1285) $(".content-obs").css({ right: 0 });
  });

  $(".obs-close").click(function (e) {
    e.stopPropagation();
    $(".content-obs").addClass("d-none");
    $(".obs-close").addClass("d-none");
    $(".obs-close").addClass("d-none");
  });

  $(".menu-mobile").click(function (e) {
    e.stopPropagation();
    $(".menu-list").removeClass("d-none");
  });
  $(".close-menu").click(function (e) {
    e.stopPropagation();
    $(".menu-list").addClass("d-none");
  });

  $(".bpro").click(function () {
    closeContentObs();
    closeContent();

    $(".content-projet").css({ top: $(".bpro").height() + 2 });
    if (wwidth <= 985) $(".content-projet").width(400);
    else $(".content-projet").width($(".bpro").width());

    $(".content-projet").removeClass("d-none");
  });

  $(".sort-close").click(function () {
    $(this).parent().parent().addClass("d-none");
  });

  $(".search-close").click(function () {
    $(this).parent().parent().addClass("d-none");
  });

  $(".date-close").click(function () {
    $(".cont-date").addClass("d-none");
  });

  $(".legend-close").click(function () {
    $(".legend").addClass("d-none");
  });

  $(".filter-close ").click(function () {
    $(".filter-list").addClass("d-none");
    $(".filter-close").addClass("d-none");
  });

  $(".btn-legend").click(function () {
    $(".legend").removeClass("d-none");
  });

  $(".btn-filter").click(function () {
    closeContentObs();
    closeContent();

    if (wwidth <= 1166) $(".filter-list").width(300);
    else $(".filter-list").width($(".staffiltre").width());

    $(".filter-close").css({
      bottom: $(".filter-list").height() - 21,
      left: $(".filter-list").width() - 14,
    });

    $(".filter-close").removeClass("d-none");
    $(".filter-list").removeClass("d-none");
    $(".b-example-divider").removeClass("d-none");
  });

  $(".btn-obs-close").click(function () {
    $(this).parent().remove();
  });

  $(".dropdown-toggle").dropdown();

  // $(".inpstart, .inpend").focus(dateWindow);

  // $(".calander-mobile").click(dateWindow);

  // $("#datestart").datepicker({
  //   // container:"datepick"
  //   format: "M,mm,yyy",
  //   todayHighlight: true,
  //   autoclose: true,
  // });
  // $("#datestart")
  //   .datepicker()
  //   .on("changeDate", function (e) {
  //     // alert('yo')
  //     var output = $("#datestart").datepicker("getDate");
  //     var d = new Date(output);
  //     let ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
  //     let mo = new Intl.DateTimeFormat("en", { month: "short" }).format(d);
  //     let da = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);

  //     $("#startdate").val(`${mo}.${da},${ye}`);
  //   });

  // $("#dateend").datepicker({
  //   // container:"datepick"
  //   format: "yyyy/mm/dd",
  //   autoclose: true,
  //   formet: "M,mm,yyyy",
  // });

  // $("#dateend")
  //   .datepicker()
  //   .on("changeDate", function (e) {
  //     // alert('yo')
  //     var output = $("#dateend").datepicker("getDate");
  //     var d = new Date(output);
  //     let ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
  //     let mo = new Intl.DateTimeFormat("en", { month: "short" }).format(d);
  //     let da = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);

  //     $("#enddate").val(`${mo}.${da},${ye}`);
  //   });

  // $("#datetimepicker6").on("dp.change", function (e) {
  //     $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
  // });
  // $("#datetimepicker7").on("dp.change", function (e) {
  //     $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
  // });

  // function dateWindow() {
  //   closeContent();

  //   $(".datepicker-switch").parent().addClass("py-2");
  //   $(".cont-date").css({ top: $(".stafdate").height() + 2 });

  //   if (wwidth <= 570) {
  //     $(".cont-date").width(wwidth);
  //   } else {
  //     $(".cont-date").width($(".stafdate").width());
  //   }

  //   if ($(".cont-date").hasClass("d-none")) {
  //     $(".cont-date").removeClass("d-none");
  //   }

  //   setdate();
  // }

  // function setdate() {
  //   var output = $("#datestart").datepicker("getDate");
  //   var d = new Date(output);
  //   let ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
  //   let mo = new Intl.DateTimeFormat("en", { month: "short" }).format(d);
  //   let da = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);

  //   if (`${mo}.${da},${ye}` == "Jan.01,1970") {
  //     d = new Date();
  //     ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
  //     mo = new Intl.DateTimeFormat("en", { month: "short" }).format(d);
  //     da = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);
  //   }

  //   $("#startdate").val(`${mo}.${da},${ye}`);
  // }

  // $(".clr-date").click(function () {
  //   $("#datestart").datepicker("clearDates");
  //   $("#dateend").datepicker("clearDates");
  //   $("#enddate").val("");
  //   $("#startdate").val("");
  // });

  // $(".clr-filter").click(function () {
  //   $(".checker").removeClass("check-toggle");
  //   $(".checker").removeClass("check-checked");
  //   $(".checker").addClass("check-empty");
  //   $(".btn-toggle").parent().children(".checker").removeClass("check-empty");
  // });

  // function initMap() {
  //   var options = {
  //     zoom: 15,
  //     center: { lat: 3.866667, lng: 11.516667 },
  //     // mapTypeId: 'terrain',
  //   };
  //   var map = new google.maps.Map(document.getElementById("map"), options);
  //   var marker = new google.maps.Marker({
  //     position: { lat: 33.933241, lng: -84.340288 },
  //     map: map,
  //   });

  //   $(".btn-plus").click(function () {
  //     map.setZoom(10);
  //   });
  //   $(".btn-moin").click(function () {
  //     map.setZoom(-1);
  //   });
  // }

  function closeContent() {
    if (!$(".filter-list").hasClass("d-none")) {
      $(".filter-close").addClass("d-none");
      $(".filter-list").addClass("d-none");
    }

    if (!$(".content-projet").hasClass("d-none"))
      $(".content-projet").addClass("d-none");

    if (!$(".content-search").hasClass("d-none"))
      $(".content-search").addClass("d-none");

    if (!$(".cont-action").hasClass("d-none"))
      $(".cont-action").addClass("d-none");
  }

  function closeContentObs() {
    if (!$(".content-obs").hasClass("d-none")) {
      $(".obs-close").addClass("d-none");
      $(".content-obs").addClass("d-none");
    }

    if (!$(".content-sort").hasClass("d-none"))
      $(".content-sort").addClass("d-none");

    if (!$(".cont-date").hasClass("d-none")) $(".cont-date").addClass("d-none");
  }
});
